var fs                        = require('fs');

var CertificateConfigs = {

	CONFIGS_OVERWRITE_FILE                 : "configs_overwrite.js",
	MAIN_COURSE_COMPLETION_TEMPLATE        : "/home/ubuntu/eckovation-testplugin/certificates/html/main_course_completion.html",
	// SIDE_COURSE_COMPLETION_TEMPLATE        : "/home/ubuntu/eckovation-testplugin/certificates/html/ht.html",
	SIDE_COURSE_COMPLETION_TEMPLATE        : "/home/ubuntu/eckovation-testplugin/certificates/html/side_course_completion.html",
	EXCELLENCE_CERTIFICATE_TEMPLATE        : "/home/ubuntu/eckovation-testplugin/certificates/html/excellence.html",
	PARTICIPATION_CERTIFICATE_TEMPLATE     : "/home/ubuntu/eckovation-testplugin/certificates/html/participation.html",
	CERTIFCATE_PDF_DIR                     : "/var/log/testplugin/certificates/pdf/",
	CERTIFICATE_DISPATCH_FROM              : "course@eckovation.com",
	CERTIFICATE_DISPATCH_CC                : "course@eckovation.com",
	CERTIFICATE_HTML_FILE_DIR              : "/var/log/testplugin/certificates/html/"

};

var overwriteConfigFulFileName  = __dirname+'/'+CertificateConfigs.CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		CertificateConfigs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any Plugin configs key');
}

module.exports = CertificateConfigs;