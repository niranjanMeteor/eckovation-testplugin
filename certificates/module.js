const mongoose 							= require('mongoose');

const CertificateSend       = require('./send.js');
const RootServerApi         = require('../server/apicalls.js');

require('../models/tests.js');
require('../models/usertests.js');
require('../models/users.js');
const Test 							    = mongoose.model('Test');
const UserTest 							= mongoose.model('UserTest');
const User 							    = mongoose.model('User');

exports.checkAndSend = function(user_id, user_already_gave_test, test, usertest_id, marks_obtained, cb){
	if(user_already_gave_test){
		return cb("user_already_gave_test",null);
	}
	if(!test.i_cert){
		return cb("test_not_eligible_certificates",null);
	}
	if(!test.dt_cert){
		return cb("test_certificate_Details_not_found",null);
	}
	if(test.t_cert != Test.CERTIFICATE_TYPES.PARTICIPATION && !test.dt_cert.min_req){
		return cb("test_certificate_not_of_type_particiaption_but_Details_not_proper",null);
	}
	var percentage_marks = (marks_obtained / test.total_marks ) * 100;
	if(test.t_cert != Test.CERTIFICATE_TYPES.PARTICIPATION && percentage_marks < test.dt_cert.min_req){
		return cb("test_certificate_not_of_type_particiaption_but_min_req_not_met",null);
	}
	User.findOne({
		_id : user_id
	},{
		identifier : 1,
		name       : 1,
		server_id  : 1
	},function(err,user){
		if(err){
			return cb(err,null);
		}
		if(!user){
			return cb("user_with_id_"+user_id+"_not_found_for_certificates_sending",null);
		}
		RootServerApi.getServerIdMail(user.identifier, function(err,resp){
      if(err){
        return cb(err,null);
      }
      if(resp.success == false){
        return cb('Oops, found server error in fetching mail details for certificate sending',null);
      }
      user_emails = resp.data.emls;
      if(!user_emails || user_emails.length == 0){
        return cb("no_user_emails_of_user_found_forCertificate_sending",null);
      }
      var target_user_email = getFirstValidEmailFromList(user_emails);
      if(!target_user_email){
        return cb("no_valid_email_of_user_found_forCertificate_sending",null);
      }
      sendCertificate(test, user, usertest_id, target_user_email, function(err,resp){
        if(err){
          return cb(err,null);
        }
        return cb(null,true);
      });
    });
	});
};

function sendCertificate(test, user, usertest_id, toEmail, cb){
  var test_id      = test._id;
  var pid          = user.server_id;
  var t_cert       = test.t_cert;
  var dt_cert      = test.dt_cert;
  var subject      = "Congratulations on completing the course of "+dt_cert.title;

  var data_obj = {
    COURSE_TITLE : dt_cert.title,
    COURSE_NAME  : dt_cert.name,
    USER_NAME    : user.name,
    DATETIME     : getValidDateTimeForCertificate(),
    SIGN         : dt_cert.sign_name,
    DURATION     : dt_cert.duration
  };

  switch(t_cert){

    case Test.CERTIFICATE_TYPES.SIDE_COURSE:
      return CertificateSend.sideCourseCompletion(usertest_id, toEmail, subject, data_obj, cb);

    case Test.CERTIFICATE_TYPES.MAIN_COURSE:
      return CertificateSend.mainCourseCompletion(usertest_id, toEmail, subject, data_obj, cb);

    case Test.CERTIFICATE_TYPES.EXCELLENCE:
      return CertificateSend.excellence(usertest_id, toEmail, subject, data_obj, cb);

    case Test.CERTIFICATE_TYPES.PARTICIPATION:
      return CertificateSend.participation(usertest_id, toEmail, subject, data_obj, cb);

  }
  return cb(null,null);

}

function getValidDateTimeForCertificate(){
  var final_text;
  var tim = new Date().getTime();
  var toAdd = (3600*1000*5)+(1800*1000);
  var date  = new Date(tim + toAdd);
  var day   = date.getDate();
  var month = date.getMonth();
  var year  = date.getFullYear();
  final_text  = day+" "+getMonthName(month)+" "+year;
  return final_text;
}

function getFirstValidEmailFromList(emails){
  var length = emails.length;
  for(var i=0; i<length; i++){
    if(emails[i] && emails[i] != '' && emails[i] != 'noemail@gmail.com'){
      return emails[i];
    }
  }
  return null;
}

function getMonthName(index){
  var Month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  return Month[index];
}