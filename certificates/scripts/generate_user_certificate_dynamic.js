const mongoose 							= require('mongoose');
const readline              = require('readline');

const DbConfigs             = require('../../database/configs.js');
const CertificateSend       = require('../send.js');
const RootServerApi         = require('../../server/apicalls.js');
const InstructorSigns       = require('../signs.js');

require('../../models/tests.js');
require('../../models/usertests.js');
require('../../models/users.js');
const Test 							    = mongoose.model('Test');
const UserTest 							= mongoose.model('UserTest');
const User 							    = mongoose.model('User');

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('----------------   test Certificates Sending Script  ------------------')
  createCertificateAndSend();
});

function createCertificateAndSend(){
  var arguments = process.argv;

  var course_name = arguments[2];
  var cert_type = arguments[3];
  var name = arguments[4];
  var sign_name = arguments[5];
  var duration = arguments[6];
  var toEmail = arguments[7];

  if(!course_name) {
    console.log("No course name defined! Exiting...");
    process.exit();
  }
  if(!cert_type) {
    console.log("No certificate type defined! Exiting...");
    process.exit();
  }
  if(!name) {
    console.log("No name defined! Exiting...");
    process.exit();
  }
  if(!sign_name) {
    console.log("No sign name defined! Exiting...");
    process.exit();
  }
  if(!duration) {
    console.log("No duration defined! Exiting...");
    process.exit();
  }
  if(!toEmail) {
    console.log("No email defined! Exiting...");
    process.exit();
  }

  cert_type = parseInt(cert_type);

  //course_title,course_name,t_cert,name,sign_name,duration,cb
  sendCertificate(course_name,cert_type,name,sign_name,duration,toEmail,function(){
    console.log("Probably generated the certificate! Thanks!");
    process.exit();
  });	
}

function updateName(name) {
	if(!name){
		return "";
	}

	var now = name.split(' ');

	if(name.toString().length >= 16) {
		if(now.length >= 3) {
			return updateName(now[0] + " " + now[now.length-1]);
		} else if(now.length == 2){
			return updateName(now[0]);
		} else if(now.length == 1) {
			var x = name.substr(now[0],13)+"...";
      return x.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
		} else {
			//implies now.length == 0
			return "";
		}
	} else {
		return name.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	}
}

function sendCertificate(course_name,t_cert,name,sign_name,duration,toEmail,cb){
  var subject      = "Congratulations on completing the " + course_name + " course";

  var data_obj = {
    COURSE_TITLE : course_name,
    COURSE_NAME  : course_name,
    USER_NAME    : updateName(name),
    DATETIME     : getValidDateTimeForCertificate(),
    SIGN         : sign_name,
    DURATION     : duration
  };

  switch(t_cert){
    case Test.CERTIFICATE_TYPES.SIDE_COURSE:
      return CertificateSend.sideCourseCompletion("dummy_id", toEmail, subject, data_obj, cb);

    case Test.CERTIFICATE_TYPES.MAIN_COURSE:
      return CertificateSend.mainCourseCompletion("dummy_id", toEmail, subject, data_obj, cb);

    case Test.CERTIFICATE_TYPES.EXCELLENCE:
      return CertificateSend.excellence("dummy_id", toEmail, subject, data_obj, cb);

    case Test.CERTIFICATE_TYPES.PARTICIPATION:
      return CertificateSend.participation("dummy_id", toEmail, subject, data_obj, cb);

  }

  console.log("nothing yet");
  console.log([course_name,t_cert,name,sign_name,duration,toEmail]);
  return cb(null,null);
}

function getValidDateTimeForCertificate(){
  var final_text;
  var tim = new Date().getTime();
  var toAdd = (3600*1000*5)+(1800*1000);
  var date  = new Date(tim + toAdd);
  var day   = date.getDate();
  var month = date.getMonth();
  var year  = date.getFullYear();
  final_text  = day+" "+getMonthName(month)+" "+year;
  return final_text;
}

function getMonthName(index){
  var Month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  return Month[index];
}

function getFirstValidEmailFromList(emails){
  var length = emails.length;
  for(var i=0; i<length; i++){
    if(emails[i] && emails[i] != '' && emails[i] != 'noemail@gmail.com'){
      return emails[i];
    }
  }
  return null;
}


function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}
