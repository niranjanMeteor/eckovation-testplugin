const mongoose 							= require('mongoose');
const readline              = require('readline');

const DbConfigs             = require('../../database/configs.js');
const CertificateSend       = require('../send.js');
const RootServerApi         = require('../../server/apicalls.js');
const InstructorSigns       = require('../signs.js');

require('../../models/tests.js');
require('../../models/usertests.js');
require('../../models/users.js');
const Test 							    = mongoose.model('Test');
const UserTest 							= mongoose.model('UserTest');
const User 							    = mongoose.model('User');

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('----------------   test Certificates Sending Script  ------------------')
	createCertificateAndSend();
});

function createCertificateAndSend(){
	rL.question("Enter the test id for which certificates are to be send : ", function(test_id){
		test_id = test_id.trim();
    Test.findOne({
    	_id : test_id
    },function(err,test){
    	if(err){
    		console.trace(err);
    		return process.exit(0);
    	}
    	if(!test.act){
    		console.log("Test is not active anymore");
    		return process.exit(0);
    	}
    	if(!test.i_cert){
    		console.log('Seems like the test is not enabled for sending certificates, try another test id');
    		console.log('\n');
    		return createCertificateAndSend();
    	}
    	console.log('\n');
    	console.log(test);
    	console.log('\n');
    	console.log('Success in finding User, proceeding forward');
    	console.log('\n');
    	if(!test.t_cert){
        console.log('Oops, test has not been specified with the type of certificate to send');
    		return process.exit(0);
    	}
    	if(!test.dt_cert){
        console.log('Oops, test has not been specified with certificate specific details like course title, duration and min_req');
    		return process.exit(0);
    	}
    	if(test.t_cert != Test.CERTIFICATE_TYPES.PARTICIPATION && !test.dt_cert.min_req){
    		console.log('Oops, test certificate is not of type participation and hence min_req is required it test cetificate details');
    		return process.exit(0);
    	}
    	rL.question("Enter the user pid for which certificates are to be send : ", function(pid){
				pid = pid.trim();
				User.findOne({
					server_id : pid,
					act       : true
				},function(err,user){
					if(err){
		    		console.trace(err);
		    		return process.exit(0);
		    	}
		    	if(!user){
		    		console.log("User Pid not found");
		    		return process.exit(0);
		    	}
		    	console.log('Success in finding User, proceeding forward');
		    	console.log('\n');
		    	UserTest.findOne({
		    		pid     : pid,
		    		test_id : test_id,
		    		is_submitted : true,
		    		is_first : true
		    	},function(err,usertest){
		    		if(err){
			    		console.trace(err);
			    		return process.exit(0);
			    	}
			    	if(!usertest){
			    		console.log("User Pid not found");
			    		return process.exit(0);
			    	}
			    	console.log('Successfully, found first submit of user for this test id');
			    	console.log('\n');
			    	console.log(usertest);
			    	console.log('\n');
			    	var percentage_marks = (usertest.marks_obtained / test.total_marks ) * 100;
			    	if(test.t_cert != Test.CERTIFICATE_TYPES.PARTICIPATION && percentage_marks < test.dt_cert.min_req){
			    		console.log('Oops, the candidate does not meet the minimum requirement criteria');
			    		console.log('Minimum Criteria to qualify in test : '+ptest.dt_cert.min_req);
			    		console.log('User percentage in test : '+percentage_marks);
			    		return process.exit(0);
			    	}
			    	console.log('\n');
			    	console.log('Successfully, verified User Qualification for certificate');
			    	console.log('Yes, User pid qualifies for test_id certificate... proceeding further');
			    	console.log('\n');
			    	RootServerApi.getServerIdMail(user.identifier, function(err,resp){
              if(err){
                console.trace(err);
                return process.exit(0);
              }
              if(resp.success == false){
                console.log('Oops, found server error in fetching mail details for certificate sending');
                return process.exit(0);
              }
              user_emails = resp.data.emls;
              console.log(user_emails);
              if(!user_emails || user_emails.length == 0){
                console.log("Oops , user emails are not found and hence, no certificate will be sent");
                return process.exit(0);
              }
              var target_user_email = getFirstValidEmailFromList(user_emails);
              if(!target_user_email){
                console.log("Oops , out of the user emails list, no valid email found, hence certificate will not be sent");
                return process.exit(0);
              }
              sendCertificate(test, user, usertest._id, target_user_email, function(err,resp){
                if(err){
                  console.log('error from sendCertificate');
                  console.trace(err);
                  return process.exit(0);
                }
                console.log('Certificate successfully delived to user email : '+target_user_email);
                return;
              });
            });
		    	});
				});
			});
    });
  });
}

function updateName(name) {
	if(!name){
		return "";
	}

	var now = name.split(' ');

	if(name.toString().length >= 25) {
		if(now.length >= 3) {
			return updateName(now[0] + " " + now[now.length-1]);
		} else if(now.length == 2){
			return updateName(now[0]);
		} else if(now.length == 1) {
			var x = name.substr(now[0],22)+"...";
      return x.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
		} else {
			//implies now.length == 0
			return "";
		}
	} else {
		return name.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	}
}

function sendCertificate(test, user, usertest_id, toEmail, cb){
  var test_id      = test._id;
  var pid          = user.server_id;
  var t_cert       = test.t_cert;
  var dt_cert      = test.dt_cert;
  var subject      = "Congratulations on completing the course of "+dt_cert.title;

  var data_obj = {
    COURSE_TITLE : dt_cert.title,
    COURSE_NAME  : dt_cert.name,
    USER_NAME    : updateName(user.name),
    DATETIME     : getValidDateTimeForCertificate(),
    SIGN         : dt_cert.sign_name,
    DURATION     : dt_cert.duration
  };


  switch(t_cert){

    case Test.CERTIFICATE_TYPES.SIDE_COURSE:
      return CertificateSend.sideCourseCompletion(usertest_id, toEmail, subject, data_obj, cb);

    case Test.CERTIFICATE_TYPES.MAIN_COURSE:
      return CertificateSend.mainCourseCompletion(usertest_id, toEmail, subject, data_obj, cb);

    case Test.CERTIFICATE_TYPES.EXCELLENCE:
      return CertificateSend.excellence(usertest_id, toEmail, subject, data_obj, cb);

    case Test.CERTIFICATE_TYPES.PARTICIPATION:
      return CertificateSend.participation(usertest_id, toEmail, subject, data_obj, cb);

  }
  return cb(null,null);

}

function getValidDateTimeForCertificate(){
  var final_text;
  var tim = new Date().getTime();
  var toAdd = (3600*1000*5)+(1800*1000);
  var date  = new Date(tim + toAdd);
  var day   = date.getDate();
  var month = date.getMonth();
  var year  = date.getFullYear();
  final_text  = day+" "+getMonthName(month)+" "+year;
  return final_text;
}

function getMonthName(index){
  var Month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  return Month[index];
}

function getFirstValidEmailFromList(emails){
  var length = emails.length;
  for(var i=0; i<length; i++){
    if(emails[i] && emails[i] != '' && emails[i] != 'noemail@gmail.com'){
      return emails[i];
    }
  }
  return null;
}


function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}
