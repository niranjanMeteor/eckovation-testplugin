const _                  = require('underscore');
const fs                 = require('fs');
const wkhtmltopdfC       = require('wkhtmltopdf');
const UserSignatures     = require('./signs.js');
require('shelljs/global');

const configs            = require('./configs.js');
const MAIL_CLIENT        = require('../mail/mail_client.js');

exports.sideCourseCompletion = function(certificate_id, toEmail ,subject, data_obj, cb){ 

  fs.readFile(configs.SIDE_COURSE_COMPLETION_TEMPLATE, function(err, html){
    if(err){ 
      console.log('Certificate will not be sent for CERTIFICATE_ID : '+certificate_id);
      console.trace(err);
      return;
    }
    if(html){
      var html_string     = html.toString('utf8');
      var compiled        = _.template(html_string);
      html_string = compiled({
        course_title     : data_obj.COURSE_TITLE,
        course_name      : data_obj.COURSE_NAME,
        name             : data_obj.USER_NAME,
        datetime         : data_obj.DATETIME,
        sign             : UserSignatures[data_obj.SIGN],
        duration         : data_obj.DURATION
      });
      
      sendCertificate(
        certificate_id, 
        data_obj.USER_NAME, 
        toEmail,
        subject, 
        html_string,
        data_obj.COURSE_TITLE,
        data_obj.SIGN,
      function(err,mail_resp){
        if(err){
          console.log('Certificate will not be sent for CERTIFICATE_ID : '+certificate_id);
          console.trace(err);
          return;
        }
        console.log('Certificate successfully sent for CERTIFICATE_ID : '+certificate_id);
        return;
      });
    } else {
      console.log('html for invoice not found and hence');
      console.log('Certificate will not be sent for CERTIFICATE_ID : '+certificate_id);
      return;
    }
  });
};

exports.mainCourseCompletion = function(certificate_id, toEmail ,subject, data_obj, cb){ 

  fs.readFile(configs.MAIN_COURSE_COMPLETION_TEMPLATE, function(err, html){
    if(err){ 
      console.log('Certificate will not be sent for CERTIFICATE_ID : '+certificate_id);
      console.trace(err);
      return;
    }
    if(html){
      var html_string     = html.toString('utf8');
      var compiled        = _.template(html_string);
      html_string = compiled({
        course_title     : data_obj.COURSE_TITLE,
        course_name      : data_obj.COURSE_NAME,
        name             : data_obj.USER_NAME,
        datetime         : data_obj.DATETIME,
        sign             : UserSignatures[data_obj.SIGN],
        duration         : data_obj.DURATION
      });
      
      sendCertificate(
        certificate_id, 
        data_obj.USER_NAME, 
        toEmail,
        subject, 
        html_string,
        data_obj.COURSE_TITLE,
        data_obj.SIGN,
      function(err,mail_resp){
        if(err){
          console.log('Certificate will not be sent for CERTIFICATE_ID : '+certificate_id);
          console.trace(err);
          return;
        }
        console.log('Certificate successfully sent for CERTIFICATE_ID : '+certificate_id);
        return;
      });
    } else {
      console.log('html for invoice not found and hence');
      console.log('Certificate will not be sent for CERTIFICATE_ID : '+certificate_id);
      return;
    }
  });
  
};

exports.excellence = function(certificate_id, toEmail ,subject, data_obj, cb){ 

  fs.readFile(configs.EXCELLENCE_CERTIFICATE_TEMPLATE, function(err, html){
    if(err){ 
      console.log('Certificate will not be sent for CERTIFICATE_ID : '+certificate_id);
      console.trace(err);
      return;
    }
    if(html){
      var html_string     = html.toString('utf8');
      var compiled        = _.template(html_string);
      html_string = compiled({
        course_title     : data_obj.COURSE_TITLE,
        course_name      : data_obj.COURSE_NAME,
        name             : data_obj.USER_NAME,
        datetime         : data_obj.DATETIME,
        sign             : UserSignatures[data_obj.SIGN],
        duration         : data_obj.DURATION
      });
      
      sendCertificate(
        certificate_id, 
        data_obj.USER_NAME, 
        toEmail,
        subject, 
        html_string,
        data_obj.COURSE_TITLE,
        data_obj.SIGN,
      function(err,mail_resp){
        if(err){
          console.log('Certificate will not be sent for CERTIFICATE_ID : '+certificate_id);
          console.trace(err);
          return;
        }
        console.log('Certificate successfully sent for CERTIFICATE_ID : '+certificate_id);
        return;
      });
    } else {
      console.log('html for invoice not found and hence');
      console.log('Certificate will not be sent for CERTIFICATE_ID : '+certificate_id);
      return;
    }
  });
  
};

exports.participation = function(certificate_id, toEmail ,subject, data_obj, cb){ 

  fs.readFile(configs.PARTICIPATION_CERTIFICATE_TEMPLATE, function(err, html){
    if(err){ 
      console.log('Certificate will not be sent for CERTIFICATE_ID : '+certificate_id);
      console.trace(err);
      return;
    }
    if(html){
      var html_string     = html.toString('utf8');
      var compiled        = _.template(html_string);
      html_string = compiled({
        course_title     : data_obj.COURSE_TITLE,
        course_name      : data_obj.COURSE_NAME,
        name             : data_obj.USER_NAME,
        datetime         : data_obj.DATETIME,
        sign             : UserSignatures[data_obj.SIGN],
        duration         : data_obj.DURATION
      });
      sendCertificate(
        certificate_id, 
        data_obj.USER_NAME, 
        toEmail,
        subject, 
        html_string,
        data_obj.COURSE_TITLE,
        data_obj.SIGN,
      function(err,mail_resp){
        if(err){
          console.log('Certificate will not be sent for CERTIFICATE_ID : '+certificate_id);
          console.trace(err);
          return;
        }
        console.log('Certificate successfully sent for CERTIFICATE_ID : '+certificate_id);
        return;
      });
    } else {
      console.log('html for invoice not found and hence');
      console.log('Certificate will not be sent for CERTIFICATE_ID : '+certificate_id);
      return;
    }
  });
  
};

function sendCertificate(id, username, toEmail, subject, html_string, course_title, instr_name, cb){

  var filename            = "Certificate.pdf";
  var pdf_file_with_path  = configs.CERTIFCATE_PDF_DIR+filename;
  var html_file_with_path = configs.CERTIFICATE_HTML_FILE_DIR+"_"+id+".html";

  var body                = "Dear "+username+",";
  body += "<br><br>";
  body += "You have successfully completed the course on "+course_title+".<br><br> Thank You<br>";
  body += instr_name;

  console.log('before conversion');

  fs.writeFile(html_file_with_path, html_string, function(err,resp){
    if(err){
      return cb(err,null);
    }

    console.log("Certificate html saved to file on disk");

    var command = "wkhtmltopdf -O Landscape "+html_file_with_path+" "+pdf_file_with_path;

    exec(command, function(code,stdout,stderr){
      // if(stderr){
      //   console.log('wkhtmltopdf command failed');
      //   console.log(stderr);
      //   return cb(true,code);
      // }
      // if(code != 0){
      //   console.log('=>wkhtmltopdf command was unsuccesfull');
      //   console.log('=> Program Output Error Code : '+code);
      //   return cb(true,code);
      // }
      console.log("Certificate pdf file saved to disk");

      fs.readFile(pdf_file_with_path, function(err, pdf_data){
        if(err){ 
          return cb(err,null);
        }
        var mailOptions = {
          from       : configs.CERTIFICATE_DISPATCH_FROM, 
          to         : toEmail, 
          subject    : subject, 
          cc         : configs.CERTIFICATE_DISPATCH_CC,
          html       : body,
          attachments: [{'filename':filename, 'content':pdf_data}]
        };

        MAIL_CLIENT.sendCertificate(mailOptions, function(err, response){
          if (err) {
            return cb(err,null);
          }
          return cb(null,true);
        });
      });

    });

  });
}
