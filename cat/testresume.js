const mongoose 							= require('mongoose');

const configs					      = require('../configs/configs.js');
const catConfigs					  = require('../configs/catconfigs.js');

require('../models/tests.js');
require('../models/testquestions.js');
require('../models/usertestquestions.js');
require('../models/usertests.js');
require('../models/questions.js');
const Test 									= mongoose.model('Test');
const TestQuestion					= mongoose.model('TestQuestion');
const UserTestQuestion		  = mongoose.model('UserTestQuestion');
const UserTest		          = mongoose.model('UserTest');
const Question					    = mongoose.model('Question');


exports.isPossible = function(user_id, test_id, cb){

	UserTest.findOne({
		user_id 	   : user_id,
		test_id 	   : test_id,
		act          : true,
		is_submitted : false,
		is_started   : true
	},{},{
		sort : {
			start_time : -1
		}
	},function(err,usertest){
		if(err){
			return cb(err,null);
		}
		var response = {
			is_resm : false
		};
		if(!usertest){
			console.log("getTestResumableValue checking for cat type tests");
			console.log("no previous pending user test found");
			return cb(null,response);
		}
		response.is_resm      = true;
		response.user_test_id = usertest._id;
		return cb(null,response);
	});
};

exports.isPossibleInfo = function(user_id, test_id, tim, cb){

	UserTest.findOne({
		user_id 	   : user_id,
		test_id 	   : test_id,
		act          : true,
		is_submitted : false,
		is_started   : true
	},{},{
		sort : {
			start_time : -1
		}
	},function(err,usertest){
		if(err){
			return cb(err,null);
		}
		var response = {
			is_resm : false
		};
		if(!usertest){
			console.log("getTestResumableValue checking for cat type tests");
			console.log("no previous pending user test found");
			return cb(null,response);
		}

		response.is_resm      = true;
		response.user_test_id = usertest._id;
		response.section      = usertest.lcatsec;
		response.time_consumed= (usertest.lcatsecut - usertest.lresmst)+usertest.tconsm;
		response.allexpiry    = getAllExpiryTime(usertest.lcatsec, response.time_consumed, tim);
		response.rtim         = catConfigs.CAT_SECTION_MAX_TIME - response.time_consumed;
		response.asec         = usertest.lcatsec;
		response.next_sec     = getNextSection(usertest.lcatsec);

		return cb(null,response);
	});
};

function getAllExpiryTime(active_section, time_consumed, tim){

	var total_time_consumed = time_consumed;
	var e1 = tim;
	var e2 = tim;
	var e3 = tim;
	var et = 0;

	switch (active_section){

		case "A":
			e1 += catConfigs.CAT_SECTION_MAX_TIME;
			e1 = e1 - time_consumed;
			e2 = e1+catConfigs.CAT_SECTION_MAX_TIME;
			e3 = e2+catConfigs.CAT_SECTION_MAX_TIME;
			break;

		case "B":
			total_time_consumed += catConfigs.CAT_SECTION_MAX_TIME;
			e2 += catConfigs.CAT_SECTION_MAX_TIME;
			e2 = e2 - time_consumed;
			e3 = e2+catConfigs.CAT_SECTION_MAX_TIME;
			break;

		case "C":
			total_time_consumed += (2*catConfigs.CAT_SECTION_MAX_TIME);
			e3 += catConfigs.CAT_SECTION_MAX_TIME;
			e3 = e3 - time_consumed;
			break;
	}
	total_time_consumed = parseInt(total_time_consumed / 1000);
	et =(catConfigs.JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME_CAT - total_time_consumed) + catConfigs.JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME_BUFFER;
	return {
		e1 : e1,
		e2 : e2,
		e3 : e3,
		et : et
	};
}

function getNextSection(section){
	if(section == "A"){
		return "B";
	}
	if(section == "B"){
		return "C";
	}
	return -1;
}
