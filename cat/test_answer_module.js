const mongoose 							= require('mongoose');

const configs					      = require('../configs/configs.js');
const catConfigs					  = require('../configs/catconfigs.js');

require('../models/tests.js');
require('../models/testquestions.js');
require('../models/usertestquestions.js');
require('../models/usertests.js');
require('../models/questions.js');
require('../models/users.js');
const User                  = mongoose.model('User');
const Test 									= mongoose.model('Test');
const TestQuestion					= mongoose.model('TestQuestion');
const UserTestQuestion		  = mongoose.model('UserTestQuestion');
const UserTest		          = mongoose.model('UserTest');
const Question					    = mongoose.model('Question');

exports.fetch = function(
	test_id, 
	user_test_id, 
	cb) 
{
	TestQuestion.find({
		test_id : test_id,
		act     : true
	},{
		ques_id : 1,
		inegm   : 1,
		negm    : 1,
		marks   : 1,
		code    : 1,
		key     : 1
	},{
		sort : {
			key : 1
		}
	},function(err,testquestions){
		if(err){
			return cb(err,null);
		}

		var total = testquestions.length;
		var ques_ids = [];
		var testquestionsById = {};
		var tempq;
		var sec_wise_ques_count = {
			A : 0,
			B : 0,
			C : 0
		};
		var sec_wise_marks = {
			A : 0,
			B : 0,
			C : 0
		};
		var sec_wise_ques_range = {
			A : {
				s : 1,
				e : 1
			},
			B : {
				s : 1,
				e : 1
			},
			C : {
				s : 1,
				e : 1
			}
		};
		var tsec;
		var tkey;
		for(var i=0; i<total; i++){

			tsec  = testquestions[i].code;
			tkey  = testquestions[i].key;
			tempq = testquestions[i].ques_id+'';

			if(ques_ids.indexOf(tempq) < 0){
				ques_ids.push(tempq);
			}
			
			testquestionsById[tempq] = testquestions[i];
			sec_wise_ques_count[tsec]++;
			sec_wise_marks[tsec] = sec_wise_marks[tsec] + testquestions[i].marks;

			// if(tkey < sec_wise_ques_range[tsec].s){
			// 	sec_wise_ques_range[tsec].s = tkey;
			// }
			// if(tkey > sec_wise_ques_range[tsec].e){
			// 	sec_wise_ques_range[tsec].e = tkey; 
			// }
		}

		Question.find({
			_id : {$in : ques_ids}
		},{
			text        : 1,
			correct_ans : 1,
			ans_options : 1
		},function(err,questions){
			if(err){
				return cb(err,null);
			}
			var totalq = questions.length;
			var quesById = {};
			for(var j=0; j<totalq; j++){
				quesById[questions[j]._id+''] = questions[j];
			}

			return getUpdateData(
				total, 
				user_test_id, 
				quesById, 
				testquestionsById, 
				sec_wise_ques_count, 
				sec_wise_marks, 
				sec_wise_ques_range,
				cb
			);

		});
	});
};

exports.submit = function(
	gid, 
	plugin_id, 
	user_id, 
	test_id, 
	user_test_id, 
	answers,
	section,
	cb) 
{
	TestQuestion.find({
		test_id : test_id,
		act     : true
	},{
		ques_id : 1,
		inegm   : 1,
		negm    : 1,
		marks   : 1,
		code    : 1,
		key     : 1
	},{
		sort : {
			key : 1
		}
	},function(err,testquestions){
		if(err){
			return cb(err,null);
		}

		var total = testquestions.length;
		var ques_ids = [];
		var testquestionsById = {};
		var tempq;
		var sec_wise_ques_count = {
			A : 0,
			B : 0,
			C : 0
		};
		var sec_wise_marks = {
			A : 0,
			B : 0,
			C : 0
		};
		var sec_wise_ques_range = {
			A : {
				s : 1,
				e : 1
			},
			B : {
				s : 1,
				e : 1
			},
			C : {
				s : 1,
				e : 1
			}
		};
		var tsec;
		var tkey;
		for(var i=0; i<total; i++){

			tsec  = testquestions[i].code;
			tkey  = testquestions[i].key;
			tempq = testquestions[i].ques_id+'';

			if(ques_ids.indexOf(tempq) < 0){
				ques_ids.push(tempq);
			}
			
			testquestionsById[tempq] = testquestions[i];
			sec_wise_ques_count[tsec]++;
			sec_wise_marks[tsec] = sec_wise_marks[tsec] + testquestions[i].marks;

			// if(tkey < sec_wise_ques_range[tsec].s){
			// 	sec_wise_ques_range[tsec].s = tkey;
			// }
			// if(tkey > sec_wise_ques_range[tsec].e){
			// 	sec_wise_ques_range[tsec].e = tkey; 
			// }
		}

		Question.find({
			_id : {$in : ques_ids}
		},{
			text        : 1,
			correct_ans : 1,
			ans_options : 1
		},function(err,questions){
			if(err){
				return cb(err,null);
			}
			var totalq = questions.length;
			var quesById = {};
			for(var j=0; j<totalq; j++){
				quesById[questions[j]._id+''] = questions[j];
			}
			var done = 0;
			var toDo = answers.length;

			var cb1 = function(err, resp){
				if(err){
					return cb(err,null);
				}
				done++;
				if(done < toDo){
					updateSingleAnswer(
						user_test_id, 
						user_id, 
						test_id, 
						plugin_id, 
						gid, 
						testquestionsById[answers[done].q.trim()], 
						quesById[answers[done].q.trim()], 
						parseInt(answers[done].a), 
						cb1
					);
				} else {
					return getUpdateData(
						total, 
						user_test_id, 
						quesById, 
						testquestionsById, 
						sec_wise_ques_count, 
						sec_wise_marks, 
						sec_wise_ques_range,
						cb
					);
				}
			};
			updateSingleAnswer(
				user_test_id, 
				user_id, 
				test_id, 
				plugin_id, 
				gid, 
				testquestionsById[answers[done].q.trim()], 
				quesById[answers[done].q.trim()], 
				parseInt(answers[done].a),
				cb1
			);
		});
	});
};

function getUpdateData(
	total_num_ques, 
	user_test_id, 
	quesById, 
	testquestionsById, 
	sec_wise_ques_count, 
	sec_wise_marks,
	sec_wise_ques_range,
	cb)
{
	UserTestQuestion.find({
		user_test_id : user_test_id,
		is_attempted : true,
		act          : true
	},{
		ques_id     : 1,
		ans_no      : 1,
		correct_ans : 1,
		marks_obtained : 1,
		is_correct  : 1,
		ineg        : 1,
		negm        : 1,
		code        : 1
	},function(err,usertestquestions){
		if(err){
			return cb(err,null);
		}
		var total = usertestquestions.length;

		var dataById = {};
		var tmarks = 0;
		var Amarks = 0;
		var Bmarks = 0;
		var Cmarks = 0;
		var Aatmpt = 0;
		var Batmpt = 0;
		var Catmpt = 0;
		var sec_crct = {
			A : 0,
			B : 0,
			C : 0
		};
		var tcode;
		var tempmarks;
		var correct_count = 0;


		for(var i=0 ; i<total; i++){
			dataById[usertestquestions[i].ques_id+''] = usertestquestions[i];
			tempmarks = usertestquestions[i].marks_obtained;
			tcode = usertestquestions[i].code;
			if(tcode == "A"){
				Amarks += tempmarks;
				Aatmpt += 1;
			} else if(tcode == "B"){
				Bmarks += tempmarks;
				Batmpt += 1;
			} else if(tcode == "C"){
				Cmarks += tempmarks;
				Catmpt += 1;
			}
			tmarks += tempmarks;
			if(usertestquestions[i].is_correct){
				correct_count++;
				sec_crct[tcode]++;
			}
		}
		var if_all_correct  = false;
		if(total_num_ques == correct_count){
			if_all_correct = true;
		}
		return cb(null,{
			ques     : quesById,
			tques    : testquestionsById,
			userques : dataById, 
			umarks   : parseFloat(tmarks.toFixed(2)),
			sec_data : {
				A : {
					total : sec_wise_ques_count["A"],
					atmpt : Aatmpt,
					crct  : sec_crct["A"],
					marks : parseFloat(Amarks.toFixed(2)),
					tmarks: sec_wise_marks["A"],
					qrange: sec_wise_ques_range["A"]
				},
				B : {
					total : sec_wise_ques_count["B"],
					atmpt : Batmpt,
					crct  : sec_crct["B"],
					marks : parseFloat(Bmarks.toFixed(2)),
					tmarks: sec_wise_marks["B"],
					qrange: sec_wise_ques_range["B"]
				},
				C : {
					total : sec_wise_ques_count["C"],
					atmpt : Catmpt,
					crct  : sec_crct["C"],
					marks : parseFloat(Cmarks.toFixed(2)),
					tmarks: sec_wise_marks["C"],
					qrange: sec_wise_ques_range["C"]
				}
			},
			attempted      : total,
			correct_count  : correct_count,
			if_all_correct : if_all_correct
		});
	});
}

function updateSingleAnswer(
	user_test_id, 
	user_id, 
	test_id, 
	plugin_id, 
	gid, 
	testquestion, 
	question, 
	ans_id, 
	cb)
{
	if(!question || !testquestion){
		// <------------------- this is a bug and needs to be resolved ----------------->
		// unwanted question id came which did not exist in database, how the hell it came
		// issue with front end
		return cb(null,true);
	}
	var statusAndMarks = getAnsCorrectAndMarks(testquestion, question, ans_id);
	var section        = testquestion.code;
	var ques_id        = question._id;
	var tim            = new Date().getTime();

	UserTestQuestion.update({
		user_test_id : user_test_id,
		ques_id      : ques_id,
		act          : true
	},{
		$setOnInsert : {
			user_test_id : user_test_id,
			ques_id      : ques_id,
			test_id      : test_id,
			plugin_id    : plugin_id,
			gid          : gid,
			act          : true
		},
		$set : {
			is_attempted   : true,
			ans_no         : ans_id,
			is_correct     : statusAndMarks.is_correct,
			correct_answer : question.correct_ans,
			marks_obtained : statusAndMarks.marks,
			max_marks_ques : testquestion.marks,
			ques_key       : testquestion.key,
			ineg           : (!testquestion.ineg) ? false : true,
			negm           : (!testquestion.negm) ? 0 : testquestion.negm,
			code           : section,
			tim            : tim,
		}
	},{
		upsert : true
	},function(err,answer_updated){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
}

function getAnsCorrectAndMarks(testquestion, question, ans_id){
	var is_correct = false;
	var marks_obtained = 0;
	if(question.correct_ans == ans_id){
		is_correct     = true;
		marks_obtained = testquestion.marks;
	} else if(testquestion.ineg && testquestion.negm && testquestion.ineg == true && testquestion.negm > 0){
		marks_obtained = (-1*testquestion.negm);
	}
	marks_obtained = parseFloat(marks_obtained.toFixed(2));
	return {
		is_correct : is_correct,
		marks      : marks_obtained
	};
}