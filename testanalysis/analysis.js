const mongoose 							= require('mongoose');

const DbConfigs             = require('../database/configs.js');

const TEST_ID               = process.argv[2];

require('../models/tests.js');
require('../models/testquestions.js');
require('../models/questions.js');
require('../models/users.js');
require('../models/usertests.js');
require('../models/usertestquestions.js');
const User                  = mongoose.model('User');
const Test 									= mongoose.model('Test');
const UserTest 						  = mongoose.model('UserTest');
const TestQuestion					= mongoose.model('TestQuestion');
const Question					    = mongoose.model('Question');
const UserTestQuestion		  = mongoose.model('UserTestQuestion');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('------------------------------------QUESTIONS IMPORT STARTED------------------------------------')
	analyse_test();
});

function analyse_test(){
	if(!TEST_ID || TEST_ID == ""){
		console.log('TEST_ID has not been set correctly');
		return process.exit(0);
	}
	Test.findOne({
		_id : TEST_ID
	},{
		catg_id             : 1,
		name                : 1,
		desc                : 1,
		level               : 1,
		type                : 1,
		q_order             : 1,
		ssol                : 1,
		total_attempts      : 1,
		total_submits       : 1,
		submits_all_correct : 1,
		total_marks         : 1,
		num_ques            : 1
	},function(err,test){
		if(err){
			console.trace(err);
			return process.exit(0);
		}
		if(!test){
			console.log('test with TEST_ID_:_'+TEST_ID+'_not found');
			return process.exit(0);
		}
		console.log('Analysing the following test results');
		console.log('********************************');
		console.log(test);
		console.log('********************************');
		console.log('\n');
		TestQuestion.distinct('key',{
			test_id : TEST_ID,
			act     : true
		},function(err, testquestions){
			if(err){
				console.trace(err);
				return process.exit(0);
			}
			if(!testquestions || testquestions.length == 0){
				console.log('No TestQuestinos Found For Test with TEST_ID_:_'+TEST_ID);
				return process.exit(0);
			}
			// console.log('1');
			// console.log('testquestions keys are');
			// console.log(testquestions);
			getAllUserTestIds(TEST_ID, function(err,usertestobjects){
				if(err){
					console.trace(err);
					return process.exit(0);
				}
				if(!usertestobjects || usertestobjects.length == 0){
					console.log('No usertestobjects Found For Test with TEST_ID_:_'+TEST_ID);
					return process.exit(0);
				}
				// console.log('2');
				var userIdWiseResult = {};
				var done = 0;
				var toDo = Object.keys(usertestobjects).length;
				// console.log('todo : '+toDo);
				for(var key in usertestobjects){
					getUserTestResults(key, usertestobjects[key]._id, testquestions, function(err,userresult){
						if(err){
							console.trace(err);
							return process.exit(0);
						}
						userIdWiseResult[userresult.user_id] = {
							profile : userresult.profile,
							result  : userresult.result,
							marks   : usertestobjects[userresult.user_id].marks_obtained,
							correct : usertestobjects[userresult.user_id].total_correct
						}
						done++;
						// console.log('done is : '+done);
						if(done == toDo){
							console.log('congratulations, test analysis has been successfully done');
							console.log('FOllowing are the analysis result');
							console.log('***************************************');
							console.log('\n');
							for(var key in userIdWiseResult){
								var temp = userIdWiseResult[key];
								var temp_string = temp.profile.name+',';
								temp_string += temp.marks+',';
								temp_string += temp.correct+',';
								var result_arr = temp.result;
								for(var i=0; i<result_arr.length; i++){
									temp_string += result_arr[i];
									if(i < result_arr.length-1){
										temp_string += ',';
									}
								}
								console.log(temp_string);
							}
							// console.log(userIdWiseResult);
							console.log('\n');
							console.log('***************************************');
							return process.exit(0);
						}
					});
				}
			});
		});
	});
}

function getUserTestResults(user_id, user_test_id, testquestions, cb){
	var user_ques_array = [];
	for(var i=0; i<testquestions.length; i++){
		user_ques_array[testquestions[i]-1] = 0;
	}
	UserTestQuestion.find({
		user_test_id : user_test_id,
		is_attempted : true
	},{
		ques_key       : 1,
		is_correct     : 1,
		ans_no         : 1,
		marks_obtained : 1
	},function(err,usertestquestions){
		if(err){
			return cb(err,null);
		}
		User.findOne({
			_id : user_id
		},{
			name      : 1,
			server_id : 1,
			role      : 1
		},function(err,user){
			if(err){
				return cb(err,null);
			}
			if(!user){
				return cb("User_Not_Found_with_User_Id_:_"+user_id,null);
			}
			for(var j=0; j<usertestquestions.length; j++){
				user_ques_array[usertestquestions[j].ques_key-1] = usertestquestions[j].marks_obtained;
			}
			var profile = {
				name : user.name,
				role : user.role,
				pid  : user.server_id
			};
			return cb(null,{profile : profile, result : user_ques_array, user_id : user_id});
		});
	});
}

function getAllUserTestIds(test_id, cb){
	UserTest.find({
		test_id : test_id,
		act     : false,
		is_submitted : true
	},{
		user_id        : 1,
		marks_obtained : 1,
		total_correct  : 1,
		createdAt      : 1
	},{
		sort : {
			createdAt : 1
		}
	},function(err,usertests){
		if(err){
			return cb(err,null);
		}
		if(!usertests || usertests.length == 0){
			return cb("getAllUserTestIds_NO_usertests_for_testID_:_"+test_id,null);
		}
		var finalusertestids = {};
		for(var i=0; i<usertests.length; i++){
			if(usertests[i].user_id in finalusertestids){
				continue;
			}
			finalusertestids[usertests[i].user_id] = usertests[i];
		}
		return cb(null,finalusertestids);
	});
}


function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}