var fs                        = require('fs');

var ReviewConfigs = {

	CONFIGS_OVERWRITE_FILE              : "configs_overwrite.js",
	USER_TEST_SUBMIT_REVIEW_TEMPLATE    : "/home/ubuntu/eckovation-testplugin/review/html/usertest_review.html",
	TEST_REVIEW_FROM                    : "course@eckovation.com",
	TEST_REVIEW_BCC                     : "notification@eckovation.com",
	TEST_REVIEW_CC                      : "notification@eckovation.com"

};

var overwriteConfigFulFileName  = __dirname+'/'+ReviewConfigs.CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		ReviewConfigs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any Plugin configs key');
}

module.exports = ReviewConfigs;