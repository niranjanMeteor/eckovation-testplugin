const mongoose 							= require('mongoose');
const _                     = require('underscore');
const fs                    = require('fs');

const configs					      = require('../configs/configs.js');
const RootServerApi      		= require('../server/apicalls.js');
const MailClient            = require('../mail/mail_client.js');
const ReviewConfigs         = require('./configs.js');

require('../models/users.js');
require('../models/categories.js');
require('../models/tests.js');
require('../models/usertests.js');
require('../models/groupstats.js');
const User                  = mongoose.model('User');
const Category 							= mongoose.model('Category');
const Test 									= mongoose.model('Test');
const UserTest 							= mongoose.model('UserTest');
const GroupStats		        = mongoose.model('GroupStats');

exports.createTestReport = function(
														user_already_gave_test, 
														test,
														user_id, 
														user_test_id, 
														user_test_marks, 
														user_test_time_taken,
														user_test_num_ques_attempted,
														user_test_total_ques_correct, 
														plugin_id, 
														group_id,
														cb)
{
	if(user_already_gave_test){
		return cb("user_already_gave_test_is_TRUE",null);
	}


	User.findOne({
		_id : user_id
	},{
		identifier : 1,
		name       : 1
	},function(err,user){
		if(err){
			return cb(err,null);
		}
		if(!user){
			return cb("user_with_id_"+user_id+"_not_found_for_Test_review",null);
		}

		Category.findOne({
			_id : test.catg_id
		},{
			name : 1
		},function(err,category){
			if(err){
				return cb(err,null);
			}
			if(!category){
				return cb("Test_Category_Not_Found",null);
			}
			var user_test_rank,
					highest_test_score, 
					average_test_score, 
					average_correct_count, 
					average_time_taken, 
					cummulative_rank,
					user_emails,
					plugin_name;

			var test_id         = test._id;
			var test_name       = test.name;
			var test_total_ques = test.num_ques;
			var test_total_marks= test.total_marks;

			UserTest.find({
				test_id      : test_id,
				is_submitted : true,
				is_first     : true
			},{
				marks_obtained : 1,
				total_correct  : 1,
				time_taken     : 1
			},{
				sort : {
					marks_obtained : -1
				}
			},function(err,usertests){
				if(err){
					return cb(err,null);
				}
				if(usertests.length == 0){
					return cb("no_usertests_found_to_send_test_mail",null);
				}
				var total_usertests          = usertests.length;

				var total_marks_all_users    = 0;
				var total_correct_all_users  = 0;
				var total_time_taken_by_users= 0;
				var temp;

				highest_test_score = usertests[0].marks_obtained;

				for(var i=0; i<total_usertests; i++){
					temp = usertests[i];

					total_marks_all_users     += temp.marks_obtained;
					total_correct_all_users   += temp.total_correct;
					total_time_taken_by_users += temp.time_taken;

					if(temp._id == user_test_id){
						user_test_rank = i+1;
					}
				}

				average_test_score    = parseFloat(total_marks_all_users / total_usertests).toFixed(2);
				average_correct_count = parseFloat(total_correct_all_users / total_usertests).toFixed(2);
				average_time_taken    = parseFloat(total_time_taken_by_users / total_usertests).toFixed(2);

				GroupStats.find({
					plugin_id : plugin_id,
					gid       : group_id
				},{
					user_id : 1
				},{
					sort : {
						score : -1
					}
				},function(err,stats){
					if(err){
						return cb(err,null);
					}
					if(stats.length == 0){
						return cb("cummulative_rank_not_found",null);
					}

					var stats_length = stats.length;

					for(var j=0; j<stats_length; j++){
						if(stats[j].user_id == user_id){
							cummulative_rank = j+1;
							break;
						}
					}

					RootServerApi.getServerIdMail(user.identifier, function(err,resp){
			      if(err){
			        return cb(err,null);
			      }

			      if(resp.success == false){
			        return cb('Oops, found server error in fetching mail details for certificate sending',null);
			      }

			      user_emails = resp.data.emls;
			      if(!user_emails || user_emails.length == 0){
			        return cb("no_user_emails_of_user_found_forCertificate_sending",null);
			      }

			      var target_user_email = getFirstValidEmailFromList(user_emails);
			      if(!target_user_email){
			        return cb("no_valid_email_of_user_found_forCertificate_sending",null);
			      }


			      var data = {
			      	// MODULE_NAME                     : resp.data.plnm,
			      	// CHAPTER_NAME                    : category.name,
			      	TEST_NAME                       : test_name+" in "+category.name,
			      	// TEST_TOTAL_QUES                 : test_total_ques,
			      	// TEST_TOTAL_MARKS                : test_total_marks,
			      	USER_TEST_MARKS                 : user_test_marks+" / "+test_total_marks,
			      	USER_TEST_RANK                  : user_test_rank,
			      	USER_TEST_NUM_OF_CORRECT_ANS    : user_test_total_ques_correct+" / "+test_total_ques,
			      	// USER_TEST_NUM_OF_QUES_ATTEMPTED : user_test_num_ques_attempted,
			      	USER_TEST_TIME_TAKEN            : getUserTestTimeString(user_test_time_taken),
			      	HIGHEST_TEST_SCORE              : highest_test_score,
			      	AVERAGE_TEST_SCORE              : average_test_score,
			      	// AVERAGE_TEST_CORRECT_ANS_COUNT  : average_correct_count,
			      	AVERAGE_TEST_TIME_TAKEN         : average_time_taken,
			      	USER_CUMMULATIVE_RANK           : cummulative_rank,
			      	USER_NAME                       : user.name
			      };

			      var group_name = resp.data.gnam;
			      var group_code = resp.data.gcode;
			      var plugin_name= resp.data.plnm;


			      prepareHtmlStringForTestReview(data, function(err, html_string){
			      	if(err){
			      		return cb(err,null);
			      	}

			      	var subject = "Eckovation Test Submission Review - "+test_name+" in "+category.name+" ("+plugin_name+","+group_code+","+group_name+")";

			      	var mailOptions = {
			          from       : ReviewConfigs.TEST_REVIEW_FROM, 
			          to         : target_user_email, 
			          subject    : subject, 
			          bcc        : ReviewConfigs.TEST_REVIEW_BCC,
			          html       : html_string
			        };

			        MailClient.sendTestReview(mailOptions, function(err, response){
			          if (err) {
			            return cb(err,null);
			          }
			          return cb(null,true);
			        });
			      });

			    });
				});
			});
		});
	});
};

function prepareHtmlStringForTestReview(data, cb){
  fs.readFile(ReviewConfigs.USER_TEST_SUBMIT_REVIEW_TEMPLATE, function(err, html){
    if(err){ 
      return cb(err,null);
    }
    if(html){
      var html_string     = html.toString('utf8');
      var compiled        = _.template(html_string);
      var html_string = compiled({
      	username : data.USER_NAME,
        // k1 : "Module",
        // v1 : data.MODULE_NAME,
        // k2 : "Chapter Name",
        // v2 : data.CHAPTER_NAME,
        k3 : "Test Name",
        v3 : data.TEST_NAME,
        // k4 : "Total Number of Questions in Test",
        // v4 : data.TEST_TOTAL_QUES,
        // k5 : "Total Marks in Test",
        // v5 : data.TEST_TOTAL_MARKS,
        k6 : "Your Score",
        v6 : data.USER_TEST_MARKS,
        k7 : "Your Test Rank",
        v7 : data.USER_TEST_RANK,
        // k8 : "Number of Questions Attempted by you",
        // v8 : data.USER_TEST_NUM_OF_QUES_ATTEMPTED,
        k9 : "Number of correct answers",
        v9 : data.USER_TEST_NUM_OF_CORRECT_ANS,
        k10 : "Time Taken",
        v10 : data.USER_TEST_TIME_TAKEN,
        k11 : "Highest Score in Test",
        v11 : data.HIGHEST_TEST_SCORE,
        k12 : "Average Score in Test",
        v12 : data.AVERAGE_TEST_SCORE,
        // k13 : "Average number of correct answers in Test",
        // v13 : data.AVERAGE_TEST_CORRECT_ANS_COUNT,
        k14 : "Your Cummulative Rank",
        v14 : data.USER_CUMMULATIVE_RANK
        // k8 : "Average Time Taken in Test",
        // v8 : data.AVERAGE_TEST_TIME_TAKEN,
      });
      return cb(null,html_string);
    } else {
      return cb("ReviewUserTestTemplatePreparationFailed",null);
    }
  });
}

function getUserTestTimeString(time_milliseconds){

	var one_second = 1000;
	var one_minute = 60 * one_second;
	var one_hour   = 60 * one_minute;

	if(time_milliseconds < one_second){
		return "1 sec";
	}
	if(time_milliseconds <= one_minute){
		return parseInt(time_milliseconds / one_second) +" secs";
	}
	if(time_milliseconds < one_hour){
		return parseInt(time_milliseconds / one_minute) +" mins";
	}
	var hours   = parseInt(time_milliseconds / one_hour);
	var minutes = parseInt((time_milliseconds % one_hour) / one_minute);

	var final = hours+"";

	if(hours > 1){
		final += " hrs";
	} else {
		final += " hr";
	}
	final += minutes;
	if(minutes > 1){
		final += " mins";
	} else {
		final += " min";
	}
	return hours+" hrs "+minutes+" mins";
}

function getFirstValidEmailFromList(emails){
  var length = emails.length;
  for(var i=0; i<length; i++){
    if(emails[i] && emails[i] != '' && emails[i] != 'noemail@gmail.com'){
      return emails[i];
    }
  }
  return null;
}
