const mongoose 							= require('mongoose');

const configs					      = require('../configs/configs.js');

require('../models/users.js');
require('../models/categories.js');
require('../models/tests.js');
require('../models/questions.js');
require('../models/usertests.js');
require('../models/testquestions.js');
require('../models/usertestquestions.js');
require('../models/groupstats.js');

const User                  = mongoose.model('User');
const Category 							= mongoose.model('Category');
const Test 									= mongoose.model('Test');
const Question					    = mongoose.model('Question');
const UserTest 							= mongoose.model('UserTest');
const TestQuestion					= mongoose.model('TestQuestion');
const UserTestQuestion		  = mongoose.model('UserTestQuestion');
const GroupStats		        = mongoose.model('GroupStats');

exports.updateAttempt = function(
	plugin_id, 
	user_id, 
	server_id, 
	test_id, 
	catg_id, 
	cb)
{
	var toDo = 3;
	var done = 0;
	Category.update({
		_id : catg_id
	},{
		$inc : { total_attempts : 1 }
	},function(err,resp){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
	Test.update({
		_id : test_id
	},{
		$inc : { total_attempts : 1 }
	},function(err,resp){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
	updateGroupStatsForStartTest(plugin_id, user_id, server_id, catg_id, function(err,resp){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
}

exports.updateCatSubmit = function(
	plugin_id, 
	user_test_id, 
	test_id, 
	total_ques, 
	time_taken, 
	user_id, 
	user_already_gave_test, 
	test_model,
	marks, 
	correct_count,
	if_all_correct,
	attempted,
	cb)
{
	var done = 0;
	var toDo = 4;
	var result = {
		marks          : marks,
		correct_count  : correct_count,
		if_all_correct : if_all_correct,
		attempted      : attempted
	};
	updateTestOnSubmit(test_id, if_all_correct, user_already_gave_test,  marks, function(err,resp){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == toDo){
			return cb(null,result);
		}
	});
	updateCategoryOnSubmit(test_id, function(err,resp){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == toDo){
			return cb(null,result);
		}
	});
	updateUserOnSubmit(user_id, if_all_correct, marks, user_already_gave_test, function(err,resp){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == toDo){
			return cb(null,result);
		}
	});
	updateGroupStatsOnSubmit(plugin_id, user_id, test_id, if_all_correct, marks, user_already_gave_test, test_model, function(err,resp){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == toDo){
			return cb(null,result);
		}
	});
};

function updateGroupStatsForStartTest(plugin_id, user_id, server_id, catg_id, cb){
	Category.findOne({
		_id : catg_id
	},function(err,category){
		if(err){
			return cb(err,null);
		}
		if(!category){
			return cb("category_id : "+catg_id+' , not found');
		}
		var gid = category.gid
		GroupStats.findOne({
			plugin_id : plugin_id,
			gid       : gid,
			user_id   : user_id
		},function(err,if_groupstats_exists){
			if(err){
				return cb(err,null);
			}
			if(if_groupstats_exists){
				return cb(null,true);
			}
			var new_group_stats = new GroupStats({
				plugin_id : plugin_id,
				user_id   : user_id,
				pid       : server_id,
				gid       : gid,
				submits 	: 0, 
				attempts  : 0,
				score 		: 0,
				marks 		: 0,
				all_correct_count : 0
			});
			new_group_stats.save(function(err,resp){
				if(err){
					return cb(err,null);
				}
				return cb(null,true);
			});
		});
	});
}

function updateUserTestOnSubmit(user_test_id, correct_count, if_all_correct, time_taken, cb){
	UserTest.update({
		_id : user_test_id
	},{
		total_correct  : correct_count,
		if_all_correct : if_all_correct,
		time_taken     : time_taken
	},function(err,resp){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
}

function updateTestOnSubmit(test_id, if_all_correct, if_already_given, marks, cb){
	var toIncrement = {
		total_submits 			: 1, 
		submits_all_correct : 0
	};
	if(if_all_correct){
		toIncrement.submits_all_correct = 1;
	}
	if(!if_already_given){
		toIncrement.marks_scored = marks;
	}
	Test.update({
		_id : test_id
	},{
		$inc : toIncrement
	},function(err,resp){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
}

function updateCategoryOnSubmit(test_id, cb){
	Test.findOne({
		_id : test_id
	},{
		catg_id : 1
	},function(err,test){
		if(err){
			return cb(err,null);
		}
		if(!test){
			return cb('test_id : '+test_id+', not found',null);
		}
		Category.update({
			_id : test.catg_id
		},{
			$inc : { total_submits : 1 }
		},function(err,resp){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}

function updateUserOnSubmit(user_id, if_all_correct, marks, user_already_gave_test, cb){
	if(user_already_gave_test){
		return cb(null,true);
	}
	var total_correct = 0;
	if(if_all_correct){
		total_correct = 1;
	}
	if(!marks){
		marks = 0;
	}
	User.update({
		_id : user_id
	},{
		$inc : { 
			submits 	: 1, 
			attempts  : 1,
			score 		: marks,
			marks 		: marks,
			all_correct_count : total_correct
		}
	},function(err,resp){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
}

function updateGroupStatsOnSubmit(
	plugin_id, 
	user_id, 
	test_id, 
	if_all_correct, 
	marks, 
	user_already_gave_test, 
	test_model, 
	cb)
{
	if(test_model && test_model != Test.MODEL.REGULAR){
		console.log("updateGroupStatsOnSubmit : test model is not of type regular");
		return cb(null,true);
	}
	if(user_already_gave_test){
		return cb(null,true);
	}
	var total_correct = 0;
	if(if_all_correct){
		total_correct = 1;
	}
	if(!marks){
		marks = 0;
	}
	Test.findOne({
		_id : test_id
	},function(err,test){
		if(err){
			return cb(err,null);
		}
		if(!test){
			return cb("test_id : "+test_id+' not found',null);
		}
		Category.findOne({
			_id : test.catg_id
		},function(err,category){
			if(err){
				return cb(err,null);
			}
			if(!category){
				return cb("category_id : "+test.catg_id+' not found',null);
			}
			GroupStats.update({
				plugin_id : plugin_id,
				gid       : category.gid,
				user_id   : user_id
			},{
				$inc : { 
					submits 	: 1, 
					attempts  : 1,
					score 		: marks,
					marks 		: marks,
					all_correct_count : total_correct
				}
			},function(err,resp){
				if(err){
					return cb(err,null);
				}
				return cb(null,true);
			});
		});
	});
}