var fs                                = require('fs');

var DbConfigs = {

	DB_CONFIGS_OVERWRITE_FILE           : "dbconfigs_overwrite.js",
	DB_NAME 														: "testplugin",
	MONGO_HOST                          : "127.0.0.1",
	MONGO_PORT                          : "27017",
	MONGO_UNAME													: "",
	MONGO_PASS													: "",
	MONGO_REPLICA_SET_NAME              : "rs2",
	MONGO_POOLSIZE                      : 10,
	MONGO_LOWER_POOLSIZE                : 3,
	MONGO_SERVER_KEEPALIVE              : 120,
	MONGO_REPLICA_KEEPALIVE             : 120
};

var overwriteConfigFulFileName  = __dirname+'/'+DbConfigs.DB_CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		DbConfigs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any Database configs key');
}

module.exports = DbConfigs;