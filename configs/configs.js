var fs                                = require('fs');

var configs = {

	CONFIGS_OVERWRITE_FILE                    : "configs_overwrite.js",
	PLUGIN_ID                          				: "5771304d381e0d36e17102e4",//"576a7a007d328e973afd94f9",
	SERVER_PORT                               : 3008,
	DEFAULT_NUM_QUESTIONS_FOR_TEST     				: 10,
	DEFAULT_NUM_LEADERS_IN_LEADERBOARD 				: 50,
	ATTEMPTED_TEST_VALIDITY_TIME       				: 60*60*1000,
	JWT_ACCESS_TOKEN_PRIVATE_KEY       				: "eckovationECKOVATION654321123456ECKOVATIONeckovation",
	JWT_ACCESS_TOKEN_EXPIRY_TIME       				: 60*30,     // 30 MINUTES
	JWT_ACCESS_TOKEN_HCODE_LENGTH      				: 20,
	JWT_USERTEST_ACCESS_TOKEN_PRIVATE_KEY     : "eckovationECKOVATION654321123456ECKOVATIONeckovation20162018",
	JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME     : 60*30,     // 30 MINUTES
	JWT_USERTEST_ACCESS_TOKEN_HCODE_LENGTH    : 15,
	MORGAN_LOG_DIRECTORY               				: "/var/log/testplugin/daily",
	DEFAULT_IPP_FOR_CATEGORIES                : 100,
	DEFAULT_IPP_FOR_TEST                      : 100,
	TESTPLUGIN_SERVER_SERVICE_NAME 		        : 'testplugin-server',
	TESTPLUGIN_SERVER_LOG_DIR 								: '/var/log/testplugin',
	TESTPLUGIN_SERVER_SERVICE_USER    				: 'ubuntu',
	TESTPLUGIN_SERVER_HEAP_SIZE_MAX   				: 990,
	OPS_PANEL_ACCESS_TOKEN_PRIVATE_KEY        : "9586hjgkjdfghugdgiodgndjkndgkjdgd885jkgdngkjng8",
	ECKOVATION_API_KEY                        : "UGHSDO7RENVERVN89dfjr890ng4guw85uw40nw58w485980",
	PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY       	: "eckovationECKOVATION654321123456ECKOVATIONeckovation",
	MIN_ALLOWED_QUESTIONS_IN_A_TEST           : 10,
  MAX_ALLOWED_QUESTIONS_IN_A_TEST           : 100,
  MIN_MARKS_PER_QUESTION_IN_A_TEST          : 1,
  MAX_MARKS_PER_QUESTION_IN_A_TEST          : 25,
  MIN_START_FOR_QUESTION_BANK               : 1,
  MIN_IPP_FOR_QUESTION_BANK                 : 1,
  MAX_IPP_FOR_QUESTION_BANK                 : 30,
  MIN_TIME_FOR_TIMED_TEST                   : 5*60*1000,        // 5 minutes
  EXTRA_BUFFER_FOR_TEST_SUBMIT              : 60*1,             // 1 minute expressed in seconds
  CODE_KERNEL_GROUPS                        : ["382195"],
  SIZE_OF_TEST_LEADERBOARD									: 100,
  TEST_AVERGAE_GRAPH_SIZE_RATIO             : 2,
  TEST_STUDENT_RANKING_FIRST_POINT          : 25,
  TEST_STUDENT_RANKING_SECOND_POINT         : 50,
  TIMED_TEST_EXPIRY_AUTO_SUBMIT_MESSAGE     : "Your last attempt for this test has expired and hence it has been auto submitted",
  MINIMUM_TAGS_PER_TEST_FOR_ADAPTIVITY      : 3,
  MINIMUM_QUESTIONS_PER_LEVEL_FOR_ADAPTIVITY: 3,
  IS_STRONG_AREA_THRESHOLD_LIMIT            : 40,
  DEFAULT_MAXIMUM_QUESTION_LEVEL            : 3,
  FIXED_MAXIMUM_QUESTION_LEVEL              : 4,
  MINIMUM_QUES_LEVEL_FOR_ADAPTIVITY_STAGE   : 3,
  TEST_ANALYTICS_QUES_ATMP_BARRIER          : 40,
  TEST_ANALYTICS_QUES_CRCT_BARRIER          : 25

};

var overwriteConfigFulFileName  = __dirname+'/'+configs.CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		configs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any Application configs key');
}

module.exports = configs;