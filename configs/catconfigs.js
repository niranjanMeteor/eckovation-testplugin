var fs                                = require('fs');

var configs = {

	CONFIGS_OVERWRITE_FILE                       : "cat_configs_overwrite.js",
	JWT_USERTEST_ACCESS_TOKEN_PRIVATE_KEY_CAT    : "JNFG087ERGHERPIGUNR8P32IJGNEGUIRGU8ldiufjk",
	JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME_CAT    : 6*60,   // in seconds
	JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME_BUFFER : 2*60,    // in seconds
	CAT_TEST_MAXIMUM_TIME                        : 6*60,   // in seconds
	CAT_INITIAL_REMAINING_TIME                   : 2*60*1000,    // in milliseconds
	CAT_SECTION_MAX_TIME                         : 2*60*1000,    // cat section max time in milliseconds
	CAT_TEST_SECTIONS                            : {
		"A" : "Quant ",
		"B" : "Verbal Ability",
		"C" : "DI & LR"
	},
	TARGET_TEST_IDS                              : [
		"5a02a424f7288b9957c14b0e",
		"5a02f20c7607431b54e10473",
		"5a02a424f7288b9957c14b10",
		"5a02f20b7607431b54e1028a",
		"5a02a424f7288b9957c14b12",
		"5a02f20c7607431b54e102f3",
		"5a02a424f7288b9957c14b15",
		"5a02f20c7607431b54e102d0",
		"5a02a424f7288b9957c14b17",
		"5a02f20c7607431b54e10315",
		"5a02a424f7288b9957c14b18",
		"5a02f20c7607431b54e10495",
		"5a02a424f7288b9957c14b1a",
		"5a02f20d7607431b54e10500",
		"578a3aee884f2d62532613e8"
	]

};

var overwriteConfigFulFileName  = __dirname+'/'+configs.CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		configs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any Application configs key');
}

module.exports = configs;