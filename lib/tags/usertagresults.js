const express 							= require('express');
const mongoose 							= require('mongoose');

const configs					      = require('../../configs/configs.js');

require('../../models/tags.js');
require('../../models/testtags.js');
require('../../models/usertests.js');
const Tag 									= mongoose.model('Tag');
const TestTag 							= mongoose.model('TestTag');
const UserTest 							= mongoose.model('UserTest');

exports.get_data = function(
	user_id, 
	user_test_id, 
	test_id,
  total_num_ques,
	maximum_marks, 
	bulkUserAnsSubmitData,
  total_time,
  total_correct,
  marks_obtained,
	cb
){
	// return cb(null,{adaptive_data : dummydata()});
	// var marks_obtained    = bulkUserAnsSubmitData.final_marks;
	var total_correct     = bulkUserAnsSubmitData.total_correct;
	var testquestionsById = bulkUserAnsSubmitData.tqs;
	var userAnsByQueId    = bulkUserAnsSubmitData.ansdata;

	var tagMarks  = {};
	var tagTime   = {};
	var tagNumQues= {};
	var tagCrtCnt = {};

	var total_ques = Object.keys(testquestionsById).length;
  var all_tags   = [];
  var temp_tags, tlen, temp;

  var num_correct     = 0;
  var num_unattempted = 0;
  var num_wrong       = 0;

  for(var key in testquestionsById){

    temp_tags   = testquestionsById[key].tags;
    tlen        = temp_tags.length;   

    for(var j=0; j<tlen; j++){
      temp = temp_tags[j]+'';
      if(!tagNumQues[temp]){
        tagNumQues[temp] = 0;
      } 
      if(!tagMarks[temp]){
        tagMarks[temp] = 0;
      }
      if(!tagTime[temp]){
        tagTime[temp] = 0;
      }
      if(!tagCrtCnt[temp]){
        tagCrtCnt[temp] = 0;
      }
      tagNumQues[temp] = tagNumQues[temp] + 1;
      if(userAnsByQueId[key]){
        tagTime[temp] += userAnsByQueId[key].qt;
        tagMarks[temp] += userAnsByQueId[key].mrk;
        if(userAnsByQueId[key].ict){
          tagCrtCnt[temp] += 1;
        }
      }
      if(all_tags.indexOf(temp) < 0){
        all_tags.push(temp);
      }
    }
    if(key in userAnsByQueId){
      if(userAnsByQueId[key].ict){
        num_correct++;
      }
    } else {
      num_unattempted++;
    }
  }
  num_wrong = total_ques - (num_correct + num_unattempted);

  var num_correct_prctg     = Math.round((num_correct / total_ques)*100);
  var num_wrong_prctg       = Math.round((num_wrong / total_ques)*100);
  var num_unattempted_prctg = Math.round((num_unattempted / total_ques)*100);

  var time_graph = {
    title : "Attempt analysis",
    data  : []
    // data : [
    //   { name: 'right answers', y: num_correct },
    //   { name: 'wrong answers', y: num_wrong },
    //   { name: 'unattempted', y: num_unattempted }
    // ]
  };
  if(num_correct > 0){
    time_graph.data.push({ name: 'Right answers', y: num_correct_prctg });
  }
  if(num_wrong > 0){
    time_graph.data.push({ name: 'Wrong answers', y: num_wrong_prctg });
  }
  if(num_unattempted > 0){
    time_graph.data.push({ name: 'Unattempted', y: num_unattempted_prctg });
  }

  getTagDetails(test_id, all_tags, function(err, tag_data){
    if(err){
      return cb(err,null);
    }
    scoreAndCorrectBarGraphData(
      test_id, 
      marks_obtained, 
      maximum_marks, 
      total_correct, 
      total_num_ques,
    function(err,scoreAndCorrectData){
      if(err){
        return cb(err,null);
      }
      var strongAndWeakAreasData = getStrongAndWeakAreas(tagNumQues, tagCrtCnt, tagTime, total_time, tag_data.tags);
      // var tagWiseTimeData        = tagWiseTimeSpentRatios(tagNumQues, tagTime, total_time, tag_data.tags);
      // var tagWiseMarksData       = tagWiseMarksRatios(tagMarks, total_marks, tag_data.tags);
      var data = {
        analytics_header       : "Quiz Analytics",
        strong_and_weak_header : "Identify your Strong and Weak Areas",
        recommendation_header  : "Based on above analytics, following are some recommendations for you",
        strong_areas           : strongAndWeakAreasData.strong_areas,
        weak_areas             : strongAndWeakAreasData.weak_areas,
        // time_graph             : strongAndWeakAreasData.time_graph,
        time_graph             : time_graph,
        tag_time_graph         : tagWiseTimeSpentRatios(tagNumQues, tagTime, total_time, tag_data.tags),
        tag_marks_graph        : tagWiseMarksRatios(tagMarks, maximum_marks, tag_data.tags),
        marks_bar_graph        : scoreAndCorrectData.score,
        correct_bar_graph      : scoreAndCorrectData.correct,
        topic_accuracy_graph   : strongAndWeakAreasData.topic_accuracy,
        tagsById               : tag_data.tags,
        resources              : tag_data.resources
      };
      console.log("adaptive test result");
      console.log(data);

      return cb(null,{adaptive_data : data, is_adaptive : true});
    });
  });

};

function getStrongAndWeakAreas(
  tagNumQues, 
  tagCrtCnt, 
  tagTime, 
  total_time,
  taginfo
){

  var strong_areas     = [];
  var weak_areas       = [];
  var strong_area_time = 0;
  var weak_area_time   = 0;

  var tlen = tagNumQues.length;
  var temp;

  var topic_accuracy = {
    title      : "Topic Wise Accuracy",
    header     : "Topic Wise Accuracy",
    x_axis     : "Topic",
    y_axis     : "Accuracy",
    categories : [],
    data       : []
  };

  for(var key in tagNumQues){
    temp = {
      _id : key,
      nm  : taginfo[key]
    };
    if(isStrongArea(tagNumQues[key], tagCrtCnt[key])){
      strong_areas.push(temp);
      strong_area_time += tagTime[key];
    } else {
      weak_areas.push(temp);
      weak_area_time += tagTime[key];
    }
    topic_accuracy.categories.push(temp.nm);
    topic_accuracy.data.push({
      name : temp.nm,
      drilldown : temp.nm,
      y : getTopicAccuracy(tagCrtCnt[key], tagNumQues[key])
    });
  }

  var strong_pecentage = Math.round((strong_area_time / total_time)*100);
  var weak_percentage  = Math.round(100 - strong_pecentage);

  var time_graph = {
    title : "Ratio of Time Spent in Strong and Weak areas",
    data : [
      { name: 'Strong Areas', y: strong_pecentage },
      { name: 'Weak Areas', y: weak_percentage }
    ]
  };

  return {
    strong_areas   : strong_areas,
    weak_areas     : weak_areas,
    time_graph     : time_graph,
    topic_accuracy : topic_accuracy
  };
}

function getTopicAccuracy(tag_correct_count, tag_num_ques){
  if(!tag_num_ques || tag_num_ques == 0){
    return 0;
  }
  return Math.round((tag_correct_count / tag_num_ques) * 100);
}

function tagWiseTimeSpentRatios(tagNumQues, tagTime, total_time, taginfo){

  var tag_wise_time_percentage = {};
  var tlen                     = tagTime.length;

  var tag_time_graph = {
    title : "Topic Wise Time Spent Ratio",
    header: "Topic Wise Time Spent Ratio"
  };

  for(var key in tagTime){
    if(tagTime[key]){
      tag_wise_time_percentage[key] = Math.round((tagTime[key] / total_time) * 100);
    }
  }

  var data = [];
  for(var key in tag_wise_time_percentage){
    data.push({name : taginfo[key], y : tag_wise_time_percentage[key]});
  }
  tag_time_graph.data = data;
  return tag_time_graph;
}

function tagWiseMarksRatios(tagMarks, total_marks, taginfo){
  var tag_wise_marks = {};
  var tag_marks_graph = {
    title : "Topic Wise Marks Obtained Ratio",
    header: "Topic Wise Marks Obtained Ratio"
  };

  for(var key in tagMarks){
    if(tagMarks[key]){
      tag_wise_marks[key] = Math.round((tagMarks[key] / total_marks) * 100);
    }
  }
  var data = [];
  for(var key in tag_wise_marks){
    data.push({name : taginfo[key], y : tag_wise_marks[key]});
  }
  tag_marks_graph.data = data;
  return tag_marks_graph;
}

function scoreAndCorrectBarGraphData(
  test_id, 
  user_marks, 
  total_marks, 
  user_correct, 
  total_ques, 
  cb
){
  // console.log('scoreAndCorrectBarGraphData');
  // console.log(user_marks);
  // console.log(total_marks);
  // console.log(user_correct);
  // console.log(total_ques);
  var score_data = {
    title : "Scoring Comparison",
    header: "Scoring Comparison",
    x_axis: "Candidate",
    y_axis: "Score",
    categories : ["Topper","Average","You"],
    data : [
      {
        name: 'Topper',
        y: 56.33,
        drilldown: 'Topper'
      }, 
      {
        name: 'Average',
        y: 24.03,
        drilldown: 'Average'
      }, 
      {
          name: 'You',
          y: Math.round((user_marks / total_marks) * 100),
          drilldown: 'You'
      }
    ]
  };
  var correct_data = {
    title      : "Accuracy Comparison",
    header     : "Accuracy Comparison",
    x_axis     : "Candidate",
    y_axis     : "Accuracy",
    categories : ["Topper","Average","You"],
    data : [
      {
        name: 'Topper',
        y: 0.00,
        drilldown: 'Topper'
      }, 
      {
        name: 'Average',
        y: 0.00,
        drilldown: 'Average'
      }, 
      {
        name: 'You',
        y: Math.round((user_correct / total_ques) * 100),
        drilldown: 'You'
      }
    ]
  };
  
  UserTest.find({
    test_id      : test_id,
    is_submitted : true,
    is_first     : true
  },{
    marks_obtained : 1,
    total_correct  : 1
  },{
    sort : {
      marks_obtained : -1
    }
  },function(err,usertests){
    if(err){
      return cb(err,null);
    }
    if(usertests.length == 0){
      return cb(null,{score : score_data, correct : correct_data});
    }

    var max_correct = 0;
    var sum_marks   = 0;
    var sum_correct = 0;
    var tlen        = usertests.length;

    score_data.data[0].y   =  Math.round((usertests[0].marks_obtained / total_marks) * 100);

    for(var i=0; i<tlen; i++){
      sum_marks = sum_marks + usertests[i].marks_obtained;
      if(usertests[i].total_correct > max_correct){
        max_correct = usertests[i].total_correct;
      }
      sum_correct = sum_correct + usertests[i].total_correct;
    }

    var avg_tot_ques = tlen * total_ques;
    var avg_tot_marks= tlen * total_marks;
    score_data.data[1].y   = Math.round((sum_marks / avg_tot_marks) * 100); 
    correct_data.data[0].y = Math.round((max_correct / total_ques) * 100);
    correct_data.data[1].y = Math.round((sum_correct / avg_tot_ques) * 100); 

    // console.log(usertests);
    // console.log('scoreAndCorrectBarGraphData');
    // console.log(sum_marks);
    // console.log(max_correct);
    // console.log(sum_correct);
    // console.log(tlen);

    return cb(null,{score : score_data, correct : correct_data});
  });
}

function getTagDetails(test_id, tag_ids, cb){
  Tag.find({
    _id : {$in : tag_ids},
    act : true
  },{
    nm : 1
  },function(err,tags){
    if(err){
      return cb(err,null);
    }
    if(tags.length == 0){
      return cb("no_tags_found",null);
    }
    TestTag.find({
      tid : test_id,
      tgid: {$in : tag_ids},
      act : true
    },{
      tgid: 1,
      rst : 1,
      rsd : 1
    },function(err,testtags){
      if(err){
        return cb(err,null);
      }
      if(testtags.length == 0){
        return cb("tag_resources_not_found",null);
      }

      var tagNameById = {};
      var tglen       = tags.length;
      
      for(var j=0; j<tglen; j++){
        tagNameById[tags[j]._id+''] = tags[j].nm;
      }

      var temp;
      var tlen      = testtags.length;
      var resources = {};

      for(var i=0; i<tlen; i++){
        temp = testtags[i].tgid;
        if(!resources[temp]){
          resources[temp] = [];
        }
        resources[temp].push({rst:testtags[i].rst,rsd:testtags[i].rsd});
      }
      return cb(null,{tags:tagNameById, resources:resources});
    });
  });
}

function isStrongArea(total_ques, total_correct){
  if(!total_correct){
    return false;
  }
  if(((total_correct / total_ques) * 100 ) > configs.IS_STRONG_AREA_THRESHOLD_LIMIT){
    return true;
  }
  return false;
}

function dummydata(){
  var data = {
    analytics_header       : "Quiz Analytics",
    strong_and_weak_header : "Identify your Strong and Weak Areas",
    recommendation_header  : "Based on above analytics, following are some recommendations for you",
    tagsById : {
      "5a73fc76ac5ff55cfb2c06fe" : "Permutation",
      "5a73fc76ac5ff55cfb2c06fa" : "Combination",
      "5a73fc76ac5ff55cfb2c06fb" : "Factoriral",
      "5a73fc76ac5ff55cfb2c06fc" : "Arragement",
      "5a73fc76ac5ff55cfb2c06fd" : "Dearragement"
    },
    strong_areas : [
      {_id : "5a73fc76ac5ff55cfb2c06fe", nm : "Permutation"},
      {_id : "5a73fc76ac5ff55cfb2c06fa", nm : "Combination"},
      {_id : "5a73fc76ac5ff55cfb2c06fb", nm : "Factoriral"}
    ],
    weak_areas : [
      {_id : "5a73fc76ac5ff55cfb2c06fc", nm : "Arragement"},
      {_id : "5a73fc76ac5ff55cfb2c06fd", nm : "Dearragement"}
    ],
    resources : {
      "5a73fc76ac5ff55cfb2c06fe" : [{rst:1,rsd:[{"n":"Lec1"},{"n":"Lec2"}]},{rst:2,rsd:[{"n":"Quiz3"},{"n":"Quiz4"}]}],
      "5a73fc76ac5ff55cfb2c06fa" : [{rst:1,rsd:[{"n":"Lec1"},{"n":"Lec2"}]},{rst:2,rsd:[{"n":"Quiz3"},{"n":"Quiz4"}]}],
      "5a73fc76ac5ff55cfb2c06fb" : [{rst:1,rsd:[{"n":"Lec1"},{"n":"Lec2"}]},{rst:2,rsd:[{"n":"Quiz3"},{"n":"Quiz4"}]}],
      "5a73fc76ac5ff55cfb2c06fc" : [{rst:1,rsd:[{"n":"Lec1"},{"n":"Lec2"}]},{rst:2,rsd:[{"n":"Quiz3"},{"n":"Quiz4"}]}],
      "5a73fc76ac5ff55cfb2c06fd" : [{rst:3,rsd:[{"n":"pdf1"},{"n":"pdf2"}]},{rst:2,rsd:[{"n":"Quiz31"},{"n":"Quiz41"}]}]
    },
    // resources : {
    //   "5a73fc76ac5ff55cfb2c06fe" : [{rst:1,rsd:[{"n":"Lec1"},{"n":"Lec2"}]},{rst:2,rsd:[{"n":"Quiz3"},{"n":"Quiz4"}]}],
    //   "5a73fc76ac5ff55cfb2c06fa" : [{rst:1,rsd:[{"n":"Lec1"},{"n":"Lec2"}]},{rst:2,rsd:[{"n":"Quiz3"},{"n":"Quiz4"}]}],
    //   "5a73fc76ac5ff55cfb2c06fb" : [{rst:1,rsd:[{"n":"Lec1"},{"n":"Lec2"}]},{rst:2,rsd:[{"n":"Quiz3"},{"n":"Quiz4"}]}],
    //   "5a73fc76ac5ff55cfb2c06fc" : [{rst:1,rsd:[{"n":"Lec1"},{"n":"Lec2"}]},{rst:2,rsd:[{"n":"Quiz3"},{"n":"Quiz4"}]}],
    //   "5a73fc76ac5ff55cfb2c06fd" : [{rst:3,rsd:[{"n":"pdf1"},{"n":"pdf2"}]},{rst:2,rsd:[{"n":"Quiz31"},{"n":"Quiz41"}]}]
    // },
    time_graph : {
      title : "Ratio of Time Spent in Strong and Weak areas",
      data : [
          { name: 'Strong Areas', y: 56.33 },
          { name: 'Weak Areas', y: 24.03 }
        ]
    },
    tag_time_graph : {
      title : "Topic Wise Time Spent Ratio",
      header: "Topic Wise Time Spent Ratio",
      data : [{
          name: 'Permutation',
          y: 56.33
      }, {
          name: 'Combination',
          y: 24.03,
          sliced: true,
          selected: true
      }, {
          name: 'Factorial',
          y: 10.38
      }, {
          name: 'Arrangement',
          y: 4.77
      }, {
          name: 'Multiplication',
          y: 0.91
      }]
    },
    tag_marks_graph : {
      title : "Topic Wise Marks Obtained Ratio",
      header: "Topic Wise Marks Obtained Ratio",
      data : [{
          name: 'Permutation',
          y: 56.33
      }, {
          name: 'Combination',
          y: 24.03,
          sliced: true,
          selected: true
      }, {
          name: 'Factorial',
          y: 10.38
      }, {
          name: 'Arrangement',
          y: 4.77
      }, {
          name: 'Multiplication',
          y: 0.91
      }]
    },
    marks_bar_graph : {
      title : "Scoring Comparison",
      header: "Scoring Comparison",
      x_axis: "Candidate",
      y_axis: "Score",
      categories : ["Topper","Average","You"],
      data : [{
          name: 'Topper',
          y: 56.33,
          drilldown: 'Topper'
      }, {
          name: 'Average',
          y: 24.03,
          drilldown: 'Average'
      }, {
          name: 'You',
          y: 10.38,
          drilldown: 'You'
      }]
    },
    correct_bar_graph : {
      title : "Accuracy Comparison",
      header: "Accuracy Comparison",
      x_axis: "Candidate",
      y_axis: "#of Correct Ans",
      categories : ["Topper","Average","You"],
      data : [{
          name: 'Topper',
          y: 56.33,
          drilldown: 'Topper'
      }, {
          name: 'Average',
          y: 24.03,
          drilldown: 'Average'
      }, {
          name: 'You',
          y: 10.38,
          drilldown: 'You'
      }]
    }
  };
  return data;
}