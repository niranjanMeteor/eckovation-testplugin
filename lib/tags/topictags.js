const express 							= require('express');
const mongoose 							= require('mongoose');

const configs					      = require('../../configs/configs.js');

require('../../models/tags.js');
require('../../models/testtags.js');
const Tag 									= mongoose.model('Tag');
const TestTag 							= mongoose.model('TestTag');

exports.update_tag = function(test_id, tag_id, toEdit, cb){
	editTag(tag_id, toEdit, function(err,tageditresp){
		if(err){
			return cb(err,null);
		}
		if(!toEdit.resc){
			return cb(null,true);
		}
		var tim = new Date().getTime();
		TestTag.update({
			tid : test_id,
			tgid: tag_id,
			act : true
		},{
			act : false
		},{
			multi : true
		},function(err,deleted_tags){
			if(err){
				return cb(err,null);
			}
			addOrEditTagResources(test_id, tag_id, toEdit.resc, tim, function(err,editresp){
				if(err){
					return cb(err,null);
				}
				return cb(null,true);
			});
		});
	});
};

exports.save_tag = function(test_id, tagname, resources, desc, tim, cb){
	return saveTagAndResource(test_id, tagname, resources, desc, tim, cb);
};

function saveTagAndResource(test_id, tagname, resources, desc, tim, cb){
	saveTagS(tagname, desc, tim, function(err,tag_resp){
		if(err){
			return cb(err,null);
		}console.log(51);
		console.log(tag_resp);
		checkIfTestTagExists(test_id, tag_resp, function(err,ifexists){
			if(err){
				return cb(err,null);
			}console.log(52);
			if(ifexists){
				return cb(null,{inew:false});
			}console.log(53);console.log(ifexists);
			addOrEditTagResources(test_id, tag_resp.id, resources, tim, function(err,resp){
				if(err){
					return cb(err,null);
				}console.log(54);console.log(resp);
				return cb(null,{inew:true,data:{_id:tag_resp.id,n:tagname,d:desc,r:resources}});
			});
		});
	});
}

function saveTagS(tagname, desc, tim, cb){
	Tag.findOne({
		nm : tagname,
		act: true
	},{
		_id : 1
	},function(err,tag){
		if(err){
			return cb(err,null);
		}
		if(tag){
			return cb(null,{inew:false,id:tag._id});
		}
		var new_tag = new Tag({
			nm  : tagname,
			dsc : desc,
			tim : tim,
			act : true
		});
		new_tag.save(function(err,resp){
			if(err){
				return cb(err,null);
			}
			return cb(null,{inew:true,id:new_tag._id});
		});
	});
}

function editTag(tag_id, toEdit, cb){
	var edit = {};
	if(toEdit.nm){
		edit.nm = toEdit.nm;
	}
	if(toEdit.dsc){
		edit.dsc = toEdit.dsc;
	}
	if(Object.keys(edit).length == 0){
		return cb(null,false);
	}
	Tag.update({
		_id : tag_id,
		act : true
	},edit,function(err,tag_update){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
}

function addOrEditTagResources(test_id, tag_id, resources, tim, cb){
	var total = resources.length;
	var done  = 0;

	var cb1 = function(err,resp){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done < total){
			addOrEditRescource(test_id, tag_id, resources[done], tim, cb1);
		} else {	
			return cb(null, true);
		}
	};
	addOrEditRescource(test_id, tag_id, resources[done], tim, cb1);
}

function addOrEditRescource(test_id, tag_id, resource, tim, cb){
	TestTag.findOne({
		tid  : test_id,
		tgid : tag_id,
		rst  : resource.t,
		act  : true
	},function(err,testtag){
		if(err){
			return cb(err,null);
		}
		if(testtag){
			console.log('700');
			console.log(resource);
			TestTag.update({
				_id : testtag._id
			},{
				$push : {
					rsd : resource.d
				}
			},function(err,resp){
				if(err){
					return cb(err,null);
				}
				return cb(null,true);
			});
		} else {
			var rsd = [];
			rsd.push(resource.d);
			console.log('701');
			console.log(rsd);
			TestTag.update({
				tid  : test_id,
				tgid : tag_id,
				rst  : resource.t,
				act  : true
			},{
				$setOnInsert : {
					tid  : test_id,
					tgid : tag_id,
					rst  : resource.t,
					rsd  : rsd,
					act  : true
				},
				$set : {
					tim : tim
				}
			},{
				upsert : true
			},function(err,tagadded){
				if(err){
					return cb(err,null);
				}
				return cb(null,true);
			});
		}
	});
}

function checkIfTestTagExists(test_id, tag_resp, cb){
	if(!tag_resp.inew){
		return cb(null,false);
	}
	TestTag.findOne({
		tid : test_id,
		tgid: tag_resp.id,
		act : true
	},function(err,testtag){
		if(err){
			return cb(err,null);
		}
		if(!testtag){
			return cb(null,false);
		}
		return cb(null,true);
	});
}




