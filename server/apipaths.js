var ApiPaths  = {
	USER_IDENTIFIER_VERIFY           								: "/plugin/verify_usr_idnt",
	USER_IDENTIFIER_VERIFY_IF_GROUPADMIN           	: "/plugin/check_adm",
	USER_IDENTIFIER_EMAIL                          	: "/plugin/g_srv_ml"
};

module.exports = ApiPaths;