/*  ************************* APP BASIC PACKAGES *******************************    */
var express                     = require('express');
var mongoose                    = require('mongoose');
var expressValidator            = require('express-validator');
var path                        = require('path');
var favicon                     = require('serve-favicon');
var cookieParser                = require('cookie-parser');
var bodyParser                  = require('body-parser');
var fs                          = require('fs');
var morgan                      = require('morgan');
var cors                        = require('cors');
var compression                 = require('compression');
var os                          = require('os');
/*  ****************************************************************************    */





/*  ***************************** APP ROUTES  **********************************    */
var category                    = require('./routes/category');
var categoryA                   = require('./routes/categoryA');
var test                        = require('./routes/test');
var testA                       = require('./routes/testA');
var testquestion                = require('./routes/testquestion');
var testquestionA               = require('./routes/testquestionA');
var questionA                   = require('./routes/questionA');
var user                        = require('./routes/user');
var usertest                    = require('./routes/usertest');
var usertestquestion            = require('./routes/usertestquestion');
var eckovation                  = require('./routes/eckovation');
var media                       = require('./routes/media');
var mediaA                      = require('./routes/mediaA');
var reporterror                 = require('./routes/reporterror');
var analytics                   = require('./routes/analytics');
var cattestquestion             = require('./routes/cattestquestion');
var catusertest                 = require('./routes/catusertest');
var catanswer                   = require('./routes/catanswer');
var tag                         = require('./routes/tag');
var ops                         = require('./routes/ops');
var api                         = require('./routes/api');
/*  ****************************************************************************    */




/*  ***************************** APP MODELS  **********************************    */

/*  ****************************************************************************    */




/*  ************************* APP BASIC UTILITIES *******************************    */
var configs                     = require('./configs/configs.js');
var DbConfigs                   = require('./database/configs.js');
/*  *****************************************************************************    */


var app                         = express();



/*  *************************** APP TRUST PROXIES *******************************    */
//enable only particular ips(of nginx servers) under trust proxy for logging client ip address

/*  *****************************************************************************    */





/*  **************************** APP MORGAN LOGER *******************************    */
var morganLogFullFileName     = getMorganLoggerFileName();
var accessLogStream           = fs.createWriteStream(morganLogFullFileName, {flags: 'a'});
app.use(morgan('{"remoteAddr":":remote-addr","remoteUser":":remote-user","date":":date","method":":method","url":":url","httpVersion":":http-version","status":":status","resp_cont_len":":res[content-length]","referrer":":referrer","user_agent":":user-agent","response_time":":response-time"}', {stream: accessLogStream}));
/*  ****************************************************************************    */





/*  **************************** APP DATABASE CONNECTION *******************************    */
connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
});
/*  ************************************************************************************    */



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// create application/json parser 
app.use(bodyParser.json());
// create application/x-www-form-urlencoded parser 
app.use(bodyParser.urlencoded({ extended: false }));



/*  ******************************** APP VALIDATORS ***********************************    */
app.use(expressValidator({
  customValidators: {
    isValidMongoId : function(value) {
      var regex = /^[0-9a-f]{24}$/;
      return regex.test(value);
    }
  }
}));

/*  **********************************************************************************    */


app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,content-encoding');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    res.setHeader('Access-Control-Max-Age', 86400);
    // Pass to next layer of middleware
    next();
});

// we are not using cookies for now
app.use(cookieParser());

// to serve the static files, from the server
app.use(express.static(path.join(__dirname, 'web')));

app.use(cors());

// disable etag for request caching
app.set('etag',false);

// app.use(compression());


/*  ***************************** APP ROUTE BINDING *********************************    */
app.use('/category',category);
app.use('/categoryA',categoryA);
app.use('/test',test);
app.use('/testA',testA);
app.use('/questionA',questionA);
app.use('/testquestion',testquestion);
app.use('/testquestionA',testquestionA);
app.use('/user',user);
app.use('/usertest',usertest);
app.use('/usertestquestion',usertestquestion);
app.use('/eckovation',eckovation);
app.use('/media',media);
app.use('/mediaA',mediaA);
app.use('/reporterror',reporterror);
app.use('/analytics',analytics);
app.use('/cattestquestion',cattestquestion);
app.use('/catusertest',catusertest);
app.use('/catanswer',catanswer);
app.use('/tag',tag);
app.use('/ops',ops);
app.use('/api',api);
/*  **********************************************************************************    */

app.get('/proxy.html', function(req,res,next){
  res.set("KeepAlive", false);
  res.status(200).send('<!DOCTYPE HTML><script src="//cdn.eckovation.com/xdomain/xdomain.min.js" data-master="http://*.pathshala.eckovation.com"></script>');
  return;
});


/*  ******************************** ERROR HANDLER ***********************************    */
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500).json({});
});
/*  **********************************************************************************    */


function getMorganLoggerFileName(){
  var morganLogDirectory= configs.MORGAN_LOG_DIRECTORY;
  var current_date      = new Date();
  var file_date_ext     = current_date.getFullYear()+'_'+(current_date.getMonth()+1)+'_'+current_date.getDate();
  var fullFileName      = morganLogDirectory + '/access-'+file_date_ext+'.log';
  return fullFileName;
}

// function connectMongoDb(){
//   var options = {
//     server : {
//       socketOptions : {
//         keepAlive : 120
//       }
//     }
//   };
//   var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
//   connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
//   connect_string     += "/"+DbConfigs.DB_NAME;
//   return mongoose.connect(connect_string,options);
// }

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : DbConfigs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!DbConfigs.MONGO_UNAME || DbConfigs.MONGO_UNAME == ""){
    options.server.poolSize = DbConfigs.MONGO_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN DbConfigs, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+DbConfigs.MONGO_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT+"/"+DbConfigs.DB_NAME,options);
  }
  options.replset = {
    rs_name : DbConfigs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : DbConfigs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : DbConfigs.MONGO_POOLSIZE
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  var mongo_hosts    = DbConfigs.MONGO_HOST;
  var mongo_port     = DbConfigs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+DbConfigs.DB_NAME;
  console.log(connect_string);
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+DbConfigs.MONGO_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}

module.exports = app;
