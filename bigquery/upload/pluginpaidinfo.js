var executeCommands  = require('./execute_commands.js');

var command = '--collection pluginpaidinfos ';
command += '--fields _id,plugin_id,gid,act,createdAt,updatedAt ';
command += '--out /home/ubuntu/bigquery/data/quiz/pluginpaidinfo.json';

executeCommands.runAll(command, "pluginpaidinfo", function(err,resp){
	if(err){
		console.log(err);
		return process.exit(0);
	}
	return;
});