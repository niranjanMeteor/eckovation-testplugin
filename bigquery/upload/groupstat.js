var executeCommands  = require('./execute_commands.js');

var command = '--collection groupstats ';
command += '--fields _id,plugin_id,pid,gid,act,submits,score,marks,rank,all_correct_count,createdAt,updatedAt ';
command += '--out /home/ubuntu/bigquery/data/quiz/groupstat.json';

executeCommands.runAll(command, "groupstat", function(err,resp){
	if(err){
		console.log(err);
		return process.exit(0);
	}
	return;
});