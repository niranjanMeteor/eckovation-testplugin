var configs          = require('../../database/configs.js');
var bigQueryConfigs  = require('../configs.js');

require('shelljs/global');

var command = 'mongoexport --quiet --username '+configs.MONGO_UNAME+' ';
command += "--password '"+configs.MONGO_PASS+"' ";
command += '--host '+bigQueryConfigs.MONGO_HOST+' ';
command += '--port '+configs.MONGO_PORT+' ';
command += '--db '+configs.DB_NAME+' ';

exports.runAll = function(command1, collection, cb){
	command1 = command+''+command1;
	console.log(command1);
	exec(command1,function(code1,stdout1,stderr1){
		if(stderr1){
			console.trace(stderr1);
			return cb("command1_error",null);
		}
		if(code1 != 0){
			console.trace('=> Program Output Error Code : '+code1);
			return cb("command1_code_error",null);
		}

		var command2 = "sed -i ";
		command2 += "'s/";
		command2 += '"\$oid"/"oid"/g';
		command2 += "' /home/ubuntu/bigquery/data/quiz/"+collection+".json";
		exec(command2,function(code2,stdout2,stderr2){
			if(stderr2){
				console.trace(stderr2);
				return cb("command2_error",null);
			}
			if(code2 != 0){
				console.trace('=> Program Output Error Code : '+code2);
				return cb("command2_code_error",null);
			}

			var command3 = "sed -i ";
			command3 += "'s/";
			command3 += '"\$date"/"date"/g';
			command3 += "' /home/ubuntu/bigquery/data/quiz/"+collection+".json";
			exec(command3,function(code3,stdout3,stderr3){
				if(stderr3){
					console.trace(stderr3);
					return cb("command3_error",null);
				}
				if(code3 != 0){
					console.trace('=> Program Output Error Code : '+code3);
					return cb("command3_code_error",null);
				}
				console.log('Data Extraction From Mongo SuccessFull for collection : '+collection);

				var command4 = "bq rm -f -t ";
				command4 += bigQueryConfigs.DATASET+"."+collection+" ";
				exec(command4,function(code4,stdout4){
					if(code4 != 0){
						console.trace('=> Program Output Error Code : '+code4);
						return cb("command4_code_error",null);
					}
					console.log('Table Deletion from BigQuery SuccessFull for collection : '+collection);
					
					var command5 = "bq load --source_format=NEWLINE_DELIMITED_JSON ";
					command5 += bigQueryConfigs.DATASET+"."+collection+" ";
					command5 += "/home/ubuntu/bigquery/data/quiz/"+collection+".json ";
					command5 += "/home/ubuntu/eckovation-testplugin/bigquery/schema/"+collection+".json ";
					console.log(command5);
					exec(command5,function(code5,stdout5){
						if(code5 != 0){
							console.trace('=> Program Output Error Code : '+code5);
							return cb("command5_code_error",null);
						}
						console.log('Data Upload to BigQuery SuccessFull for collection : '+collection);
						return cb(null,true);
					});
				});
			});
		});
	});
};

exports.runTestData = function(collection, cb){
	console.log('Data Extraction From Mongo SuccessFull for collection : '+collection);

	var command4 = "bq rm -f -t ";
	command4 += bigQueryConfigs.DATASET+"."+collection+" ";
	exec(command4,function(code4,stdout4){
		if(code4 != 0){
			console.trace('=> Program Output Error Code : '+code4);
			return cb("command4_code_error",null);
		}
		console.log('Table Deletion from BigQuery SuccessFull for collection : '+collection);
		
		var command5 = "bq load --source_format=NEWLINE_DELIMITED_JSON ";
		command5 += bigQueryConfigs.DATASET+"."+collection+" ";
		command5 += "/home/ubuntu/bigquery/data/quiz/"+collection+".json ";
		command5 += "/home/ubuntu/eckovation-testplugin/bigquery/schema/"+collection+".json ";
		console.log(command5);
		exec(command5,function(code5,stdout5){
			if(code5 != 0){
				console.trace('=> Program Output Error Code : '+code5);
				return cb("command5_code_error",null);
			}
			console.log('Data Upload to BigQuery SuccessFull for collection : '+collection);
			return cb(null,true);
		});
	});
};