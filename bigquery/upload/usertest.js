var executeCommands  = require('./execute_commands.js');

var command = '--collection usertests ';
command += '--fields _id,test_id,catg_id,pid,gid,user_id,plugin_id,is_started,is_submitted,is_first,test_type,q_order,start_time,end_time,total_ques,total_marks,time_alloted,time_taken,total_correct,marks_obtained,score,createdAt,updatedAt ';
command += '--out /home/ubuntu/bigquery/data/quiz/usertest.json';

executeCommands.runAll(command, "usertest", function(err,resp){
	if(err){
		console.log(err);
		return process.exit(0);
	}
	return;
});