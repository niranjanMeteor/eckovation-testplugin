var executeCommands  = require('./execute_commands.js');

var command = '--collection testquestions ';
command += '--fields _id,test_id,ques_id,act,key,marks,ineg,negm,createdAt,updatedAt ';
command += '--out /home/ubuntu/bigquery/data/quiz/testquestion.json';

executeCommands.runAll(command, "testquestion", function(err,resp){
	if(err){
		console.log(err);
		return process.exit(0);
	}
	return;
});