var executeCommands  = require('./execute_commands.js');

var command = '--collection questions ';
command += '--fields _id,type,ans_options,text,level,plugin_id,attempts,tim,createdAt,updatedAt ';
command += '--out /home/ubuntu/bigquery/data/quiz/question.json';

executeCommands.runAll(command, "question", function(err,resp){
	if(err){
		console.log(err);
		return process.exit(0);
	}
	return;
});