var executeCommands  = require('./execute_commands.js');

var command = '--collection users ';
command += '--fields _id,plugin_id,server_id,gid,role,attempts,submits,score,marks,all_correct_count,act,createdAt,updatedAt ';
command += '--out /home/ubuntu/bigquery/data/quiz/user.json';

executeCommands.runAll(command, "user", function(err,resp){
	if(err){
		console.log(err);
		return process.exit(0);
	}
	return;
});