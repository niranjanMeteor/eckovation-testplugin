var fs               = require('fs')
var executeCommands  = require('./execute_commands.js');
var bigQueryConfigs  = require('../configs.js');

var command = '--collection tests ';
command += '--fields _id,act,catg_id,name,level,type,is_default,num_ques,mpq,total_marks,marks_scored,q_order,ssol,timed,ineg,negm,publ,stim,total_attempts,total_submits,submits_all_correct,tim,createdAt,updatedAt ';
command += '--out /home/ubuntu/bigquery/data/quiz/test.json';

var mongoose 							= require('mongoose');

var DbConfigs             = require('../../database/configs.js');

require('../../models/tests.js');
var Test		              = mongoose.model('Test');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  return process.exit(0);
});
mongoose.connection.on('connected', function(){
	Test.find({},{
		catg_id : 1,
		name    : 1,
		act     : 1,
		level   : 1,
		type    : 1,
		num_ques: 1,
		mpq     : 1,
		total_marks : 1,
		marks_scored : 1,
		q_order : 1,
		ssol    : 1,
		timed   : 1,
		ineg    : 1,
		negm    : 1,
		publ    : 1,
		stim    : 1,
		total_attempts : 1,
		total_submits  : 1,
		submits_all_correct : 1,
		tim     : 1,
		createdAt : 1,
		updatedAt : 1
	},function(err,testdata){
		if(err){
			console.log(err);
			return process.exit(0);
		}
		if(testdata.length == 0){
			console.log('empty test data');
			return process.exit(0);
		}
		var total = testdata.length;
		var filepath = '/home/ubuntu/bigquery/data/quiz/test.json';
		deleteFile(filepath, function(err,resp){
			if(err){
	      console.log('could not remove file : '+filepath+' , for test data uplaod');
	      return process.exit(0);
	    }
      console.log('successfully deleted any existing file : '+filepath+' , for test data uplaod');
			var datawriter = fs.createWriteStream(filepath, {
			  flags: 'a'
			});
			var temp;
			for(var i=0; i<total; i++){
				datawriter.write(processedData(testdata[i]));
				if(i < (total-1))
					datawriter.write('\n');
			}
			datawriter.end();
			executeCommands.runTestData("test", function(err,resp){
				if(err){
					console.log(err);
					return process.exit(0);
				}
				return;
			});
		});
	}).read('s');
});

function processedData(data){
	var result = {};
	result._id = data._id;
	if("catg_id" in data){
		result.catg_id = data.catg_id;
	}
	if("name" in data){
		result.name = data.name;
	}
	if("act" in data){
		if(data.act == true || data.act == 'true')
			result.act = true;
		else result.act = false;
	}
	if("level" in data){
		result.level = parseInt(data.level);
	}
	if("type" in data){
		result.type = parseInt(data.type);
	}
	if("num_ques" in data){
		result.num_ques = parseInt(data.num_ques);
	}
	if("mpq" in data){
		result.mpq = parseInt(data.mpq);
	}
	if("total_marks" in data){
		result.total_marks = parseInt(data.total_marks);
	}
	if("marks_scored" in data){
		result.marks_scored = parseInt(data.marks_scored);
	}
	if("q_order" in data){
		result.q_order = parseInt(data.q_order);
	}
	if("ssol" in data){
		if(data.ssol == true || data.ssol == 'true')
			result.ssol = true;
		else result.ssol = false;
	}
	if("timed" in data){
		if(data.timed == true || data.timed == 'true')
			result.timed = true;
		else result.timed = false;
	}
	if("ineg" in data){
		if(data.ineg == true || data.ineg == 'true')
			result.ineg = true;
		else result.ineg = false;
	}
	if("negm" in data){
		result.negm = parseInt(data.negm);
	}
	if("stim" in data){
		result.stim = parseInt(data.stim);
	}
	if("total_attempts" in data){
		result.total_attempts = parseInt(data.total_attempts);
	}
	if("total_submits" in data){
		result.total_submits = parseInt(data.total_submits);
	}
	if("submits_all_correct" in data){
		result.submits_all_correct = parseInt(data.submits_all_correct);
	}
	if("tim" in data){
		result.tim = parseInt(data.tim);
	}
	if("createdAt" in data){
		result.createdAt = data.createdAt;
	}
	if("updatedAt" in data){
		result.updatedAt = data.updatedAt;
	}
	return JSON.stringify(result);
}

function deleteFile(filepath, cb){
  fs.unlink(filepath,function(err,resp){
    if(err) {
    	return cb(err,null);
    } 
    return cb(null,true);
  });
}

// executeCommands.runAll(command, "test", function(err,resp){
// 	if(err){
// 		console.log(err);
// 		return process.exit(0);
// 	}
// 	return;
// });


function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += bigQueryConfigs.MONGO_HOST+":"+bigQueryConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}