var executeCommands  = require('./execute_commands.js');

var command = '--collection usertestquestions ';
command += '--fields _id,test_id,ques_id,user_test_id,plugin_id,gid,ques_key,ans_no,is_attempted,is_correct,marks_obtained,ineg,negm,createdAt,updatedAt ';
command += '--out /home/ubuntu/bigquery/data/quiz/usertestquestion.json';

executeCommands.runAll(command, "usertestquestion", function(err,resp){
	if(err){
		console.log(err);
		return process.exit(0);
	}
	return;
});