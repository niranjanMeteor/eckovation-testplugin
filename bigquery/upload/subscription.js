var executeCommands  = require('./execute_commands.js');

var command = '--collection subscriptions ';
command += '--fields _id,plugin_id,group_id,package_id,order_id,start_time,end_time,act,frm_cmplt,tim,createdAt,updatedAt ';
command += '--out /home/ubuntu/bigquery/data/quiz/subscription.json';

executeCommands.runAll(command, "subscription", function(err,resp){
	if(err){
		console.log(err);
		return process.exit(0);
	}
	return;
});