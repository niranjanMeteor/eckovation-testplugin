var executeCommands  = require('./execute_commands.js');

var command = '--collection categories ';
command += '--fields _id,plugin_id,name,catg_no,act,type,gid,total_num_tests,total_attempts,total_submits,tim,createdAt,updatedAt ';
command += '--out /home/ubuntu/bigquery/data/quiz/category.json';

executeCommands.runAll(command, "category", function(err,resp){
	if(err){
		console.log(err);
		return process.exit(0);
	}
	return;
});