var fs                        = require('fs');

var bigQueryConfigs = {

	CONFIGS_OVERWRITE_FILE          : "configs_overwrite.js",
	DATASET                         : "ecko_mongo_sql_quiz_t",
	MONGO_HOST                      : ""
	
};

var overwriteConfigFulFileName  = __dirname+'/'+bigQueryConfigs.CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		bigQueryConfigs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any Plugin configs key');
}

module.exports = bigQueryConfigs;