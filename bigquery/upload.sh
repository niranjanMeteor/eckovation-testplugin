echo "Starting the Eckovation Quiz Data upload to BigQuery..............."
echo "`date`"
echo ""

if ! /usr/bin/node /home/ubuntu/eckovation-testplugin/bigquery/upload/category.js
then 
	echo "category upload was unsuccessful, terminating the script";
	exit 0
fi
echo "category uploaded successfull"

if ! /usr/bin/node /home/ubuntu/eckovation-testplugin/bigquery/upload/groupstat.js
then 
	echo "groupstat upload was unsuccessful, terminating the script";
	exit 0
fi
echo "groupstat uploaded successfull"

if ! /usr/bin/node /home/ubuntu/eckovation-testplugin/bigquery/upload/pluginpaidinfo.js
then 
	echo "pluginpaidinfo upload was unsuccessful, terminating the script";
	exit 0
fi
echo "pluginpaidinfo uploaded successfull"

if ! /usr/bin/node /home/ubuntu/eckovation-testplugin/bigquery/upload/question.js
then 
	echo "question upload was unsuccessful, terminating the script";
	exit 0
fi
echo "question uploaded successfull"

if ! /usr/bin/node /home/ubuntu/eckovation-testplugin/bigquery/upload/subscription.js
then 
	echo "subscription upload was unsuccessful, terminating the script";
	exit 0
fi
echo "subscription uploaded successfull"

if ! /usr/bin/node /home/ubuntu/eckovation-testplugin/bigquery/upload/user.js
then 
	echo "user upload was unsuccessful, terminating the script";
	exit 0
fi
echo "user uploaded successfull"

if ! /usr/bin/node /home/ubuntu/eckovation-testplugin/bigquery/upload/usertest.js
then 
	echo "usertest upload was unsuccessful, terminating the script";
	exit 0
fi
echo "usertest uploaded successfull"

if ! /usr/bin/node /home/ubuntu/eckovation-testplugin/bigquery/upload/usertestquestion.js
then 
	echo "usertestquestion upload was unsuccessful, terminating the script";
	exit 0
fi
echo "usertestquestion uploaded successfull"

if ! /usr/bin/node /home/ubuntu/eckovation-testplugin/bigquery/upload/tests.js
then 
	echo "tests upload was unsuccessful, terminating the script";
	exit 0
fi
echo "tests uploaded successfull"

echo ""
echo "Eckovation Quiz Data upload successful.........."
