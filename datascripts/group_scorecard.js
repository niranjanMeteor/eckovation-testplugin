var mongoose 							= require('mongoose');
var readline 			        = require('readline');

var DbConfigs             = require('../database/configs.js');

require('../models/users.js');
require('../models/groupstats.js');
var User 									= mongoose.model('User');
var GroupStats		        = mongoose.model('GroupStats');

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('----------------   Group Scorecard Script Started ------------------')
	preapreGroupPerformanceReport();
});


function preapreGroupPerformanceReport(){
	rL.question("Enter the group_code you wanna analyze : ", function(code) {
		code = code.trim();
    if(isNaN(code) || code.length < 6) {
      console.log('Oops , you entered a wrong group_code option, Please enter a right group_code');
      return addNewPaidPlugin();
    }
    GroupStats.find({
			gid       : code,
		},{
			user_id   : 1,
			score     : 1,
		},function(err, user_stats){
			if(err){
				console.trace(err);
				return endScript();
			}
			if(user_stats.length == 0){
				console.log("No Group Stats record found");
				return endScript();
			}
			
			var userIds = [];
			var total   = user_stats.length;
			var user_id;
			for(var i=0; i<total; i++){
				user_id = user_stats[i].user_id+'';
				if(userIds.indexOf(user_id)){
					userIds.push(user_id);
				}
			}
			User.find({
				_id : {$in : userIds}
			},{
				server_id : 1,
				name      : 1
			},function(err,users){
				if(err){
					console.trace(err);
					return endScript();
				}
				var t_user_id;
				var pidByUserId = {};
				var pidByName   = {};
				var total_users = users.length; 
				for(var k=0; k<total_users; k++){
					t_user_id = users[k]._id;
					if(!(t_user_id in pidByUserId)){
						pidByUserId[t_user_id] = users[k].server_id+'';
					}
					pidByName[users[k].server_id+''] = users[k].name;
				}
				var pidWiseScore = {};
				var pid;
				i = 0;
				for(var i=0; i<total; i++){
					pid = pidByUserId[user_stats[i].user_id+''];
					if(!(pid in pidWiseScore)){
						pidWiseScore[pid] = 0;
					}
					pidWiseScore[pid] = pidWiseScore[pid] + user_stats[i].score;
				}
				var new_user_stats  = [];
				for(var key in pidWiseScore){
					new_user_stats.push({
						pid     : key,
						score   : pidWiseScore[key]
					});
				}
				new_user_stats.sort(function(a, b){
				 return b.score-a.score;
				});
				console.log('\n');
				var total_i = new_user_stats.length;
				var pid, name;
				for(var m=0; m<total_i; m++){
					pid = new_user_stats[m].pid;
					name= pidByName[pid];
					console.log(pid+' : '+name+' : '+new_user_stats[m].score);
				}
				console.log('\n');
				return endScript();
			});
		});
	});
}

function endScript(){
  console.log('Thanks For using our service, Hope to serve you soon');
  return process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}