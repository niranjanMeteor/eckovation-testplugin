var mongoose 							= require('mongoose');
const fs                  = require('fs');

var DbConfigs             = require('../database/configs.js');

require('../models/testquestions.js');
require('../models/questions.js');
var TestQuestion 					= mongoose.model('TestQuestion');
var Question 					    = mongoose.model('Question');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('----------------   answer correction script started ------------------')
	updateTestQuestionMarks();
	// updateCorrectAnswer();
});

var TEST_QUESTION_MARKS = {};
var QUESTION_CORRECT_ANS = {};
var filename1 = "/home/ubuntu/testplugindata/board_tenth/testquestion_marks.txt";
var filename2 = "/home/ubuntu/testplugindata/board_tenth/question_answer.txt";

function updateTestQuestionMarks(){
	var key, value;
	fs.readFileSync(filename1)
	.toString()
	.split('\n')
	.forEach(function(line){ 
		temp = line.split(",");
		key  = temp[0];
		value= temp[1];
		TEST_QUESTION_MARKS[key] = value;
	});
	updateTestQuestionDatabase();
}

function updateCorrectAnswer(){
	var key, value;
	fs.readFileSync(filename2)
	.toString()
	.split('\n')
	.forEach(function(line){ 
		temp = line.split(",");
		key  = temp[0];
		value= temp[1];
		QUESTION_CORRECT_ANS[key] = value;
	});
	updateQuestionDatabase();
}

function updateTestQuestionDatabase(){
	var index = [];
	for(var key in TEST_QUESTION_MARKS){
		if(index.indexOf(key) < 0){
			index.push(key);
		}
	}
	var total = index.length;
	if(total == 0){
		console.trace('no testquestions found to edit marks');
		return process.exit(0);
	}
	var done = 0;
	var succs= 0;
	var cb1 = function(err,resp){
		if(err){
			console.trace(err);
			return process.exit(0);
		}
		if(resp){
			succs++;
		}
		done++;
		if(done < total){
			updateTestQuestion(index[done],TEST_QUESTION_MARKS[index[done]], cb1);
		} else {
			console.log('total test questions update todo : '+total);
			console.log('total test questions update done : '+done);
			console.log('total Succs : '+succs);
			return process.exit(0);
		}
	};
	updateTestQuestion(index[done],TEST_QUESTION_MARKS[index[done]], cb1);	
}

function updateQuestionDatabase(){
	var index = [];
	for(var key in QUESTION_CORRECT_ANS){
		if(index.indexOf(key) < 0){
			index.push(key);
		}
	}
	var total = index.length;
	if(total == 0){
		console.trace('no questions found to edit the correct answer');
		return process.exit(0);
	}
	var done = 0;
	var succs= 0;
	var cb1 = function(err,resp){
		if(err){
			console.trace(err);
			return process.exit(0);
		}
		if(resp){
			succs++;
		}
		done++;
		if(done < total){
			updateQuestion(index[done],QUESTION_CORRECT_ANS[index[done]], cb1);
		} else {
			console.log('total questions update todo : '+total);
			console.log('total questions update done : '+done);
			console.log('total Succs : '+succs);
			return process.exit(0);
		}
	};
	updateQuestion(index[done],QUESTION_CORRECT_ANS[index[done]], cb1);	
}

function updateTestQuestion(tq_id, marks, cb){
	TestQuestion.findOne({
		_id : tq_id
	},{
		marks : 1
	},function(err,testquestion){
		if(err){
			return cb(err,null);
		}
		if(!testquestion){
			return cb("testquestion not found with tq_id : "+tq_id,null);
		}
		if(testquestion.marks == marks){
			return cb(null,false);
		}
		// return cb(null,true);
		TestQuestion.update({
			_id : tq_id
		},{
			marks : marks
		},function(err,updated_testquestion){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}

function updateQuestion(ques_id, correct_ans, cb){
	Question.findOne({
		_id : ques_id
	},{
		correct_ans : 1
	},function(err,question){
		if(err){
			return cb(err,null);
		}
		if(!question){
			return cb("question not found with id : "+ques_id,null);
		}
		if(question.correct_ans == correct_ans){
			return cb(null,false);
		}
		// return cb(null,true);
		Question.update({
			_id : ques_id
		},{
			correct_ans : correct_ans
		},function(err,updated_question){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : DbConfigs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!DbConfigs.MONGO_UNAME || DbConfigs.MONGO_UNAME == ""){
    options.server.poolSize = DbConfigs.MONGO_LOWER_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN DbConfigs, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+DbConfigs.MONGO_LOWER_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT+"/"+DbConfigs.DB_NAME,options);
  }
  options.replset = {
    rs_name : DbConfigs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : DbConfigs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : DbConfigs.MONGO_LOWER_POOLSIZE
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  var mongo_hosts    = DbConfigs.MONGO_HOST;
  var mongo_port     = DbConfigs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+DbConfigs.DB_NAME;
  console.log(connect_string);
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+DbConfigs.MONGO_LOWER_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}