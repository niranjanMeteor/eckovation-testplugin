var mongoose 							= require('mongoose');
var fs                    = require('fs');

var DbConfigs             = require('../database/configs.js');

require('../models/categories.js');
require('../models/tests.js');
require('../models/questions.js');
require('../models/testquestions.js');

var Category 							= mongoose.model('Category');
var Test 									= mongoose.model('Test');
var Question 						  = mongoose.model('Question');
var TestQuestion		      = mongoose.model('TestQuestion');

var TEST_ID               = '5780b6ccf97fe3ac4eaa57a8';
var MERGE_TEST_IDS        = ['5780ae35d765597745342ba6'];
var PER_QUESTION_MARKS    = 4;

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('------------------------------------QUESTIONS IMPORT STARTED------------------------------------')
	startMixTestQuestionsImport();
});

function startMixTestQuestionsImport(){
	TestQuestion.find({
		test_id : { $in : MERGE_TEST_IDS }
	},{
		ques_id   : 1
	},function(err,questions){
		if(err){
			console.trace(err);
			return process.exit(0);
		}
		var total = questions.length;
		var done  = 0;
		var error = 0;
		var sucs  = 0;
		var already_existing = 0;
		for(var i=0; i<total; i++){
			insertTestQuestion(questions[i].ques_id, i, function(err,resp){
				if(err){
					error++;
					console.trace(err);
				}
				if(resp){
					sucs++;
				} else {
					already_existing++;
				}
				done++;
				if(done == total){
					console.log('Total Question to Do : '+total);
					console.log('Total Question Import Sucs : '+sucs);
					console.log('Total Question import Error : '+error);
					console.log('Total Question Already Existing in Test : '+already_existing);
					return process.exit(0);
				}
			});
		}
	});
} 

function insertTestQuestion(ques_id, key, cb){
	TestQuestion.findOne({
		test_id : TEST_ID,
		ques_id : ques_id
	},function(err,existing_testQ){
		if(err){
			return cb(err,null);
		}
		if(existing_testQ){
			return cb(null,false);
		}
		var newTestQ = new TestQuestion({
			test_id : TEST_ID,
			ques_id : ques_id,
			key     : key,
			act     : true,
			marks   : PER_QUESTION_MARKS
		});
		newTestQ.save(function(err,new_ques_inserted){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME,options;
  return mongoose.connect(connect_string);
}
