var mongoose 							= require('mongoose');

var DbConfigs             = require('../database/configs.js');

require('../models/usertests.js');
require('../models/tests.js');
var UserTest 							= mongoose.model('UserTest');
var Test		              = mongoose.model('Test');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('----------------   Fix Usertest Script  ------------------')
	fixUsertestWithNoCatg();
});

function fixUsertestWithNoCatg(){
	UserTest.find({
		catg_id : {$exists:false}
	},{
		_id     : 1,
		test_id : 1
	},function(err, usertests){
		if(err){
			console.trace(err);
			return process.exit(0);
		}
		if(usertests.length == 0){
			console.log("NO usertests Found to Process");
			return process.exit(0);
		}
		var total = usertests.length;
		console.log('Going to process total jobs : '+total);
		var done  = 0;
		var errs  = 0;
		var succs = 0;
		var cb1 = function(err, resp){
			if(err){
				errs++;
			} else {
				succs++;
			}
			done++;
			if(done < total){
				updateUsertestWithCatg(usertests[done]._id, usertests[done].test_id, cb1);
			} else {
				console.log('Total Tasks to be done : '+total);
				console.log('Total Tasks done '+done);
				console.log('Total Errors '+errs);
				console.log('Total Success '+succs);
				return process.exit(0);
			}
		};
		updateUsertestWithCatg(usertests[done]._id, usertests[done].test_id, cb1);
	});
}

function updateUsertestWithCatg(usertest_id, test_id, cb){
	Test.findOne({
		_id : test_id
	},{
		_id : 0,
		catg_id : 1
	},function(err,test){
		if(err){
			return cb(err,null);
		}
		if(!test || !test.catg_id){
			console.log('catg_id_not_found_for_:_'+test_id);
			return cb(null,true);
		}
		UserTest.update({
			_id : usertest_id
		},{
			catg_id : test.catg_id
		},function(err, updated_usertest){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}