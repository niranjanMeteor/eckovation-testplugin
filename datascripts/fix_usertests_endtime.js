var mongoose 							= require('mongoose');

var DbConfigs             = require('../database/configs.js');

require('../models/usertests.js');
var UserTest 							= mongoose.model('UserTest');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('----------------   Fix Usertest Script  ------------------')
	fixUsertestWithNoFirstValue();
});

function fixUsertestWithNoFirstValue(){
	UserTest.find({
		is_submitted : true,
		end_time : {$exists:false}
	},{
		_id        : 1,
		start_time : 1,
		time_taken : 1
	},function(err, usertests){
		if(err){
			console.trace(err);
			return process.exit(0);
		}
		if(usertests.length == 0){
			console.log("NO usertests Found to Process for updating endtime");
			return process.exit(0);
		}
		var total = usertests.length;
		console.log('A total of '+total+' , records will be updated');
		var done  = 0;
		var errs  = 0;
		var succs = 0;
		var cb1 = function(err, resp){
			if(err){
				errs++;
			} else {
				succs++;
			}
			done++;
			if(done < total){
				updateUsertestIfFirst(usertests[done], cb1);
			} else {
				console.log('Total Tasks to be done : '+total);
				console.log('Total Tasks done '+done);
				console.log('Total Errors '+errs);
				console.log('Total Success '+succs);
				return process.exit(0);
			}
		};
		updateUsertestIfFirst(usertests[done], cb1);
	});
}

function updateUsertestIfFirst(usertest, cb){
	
	UserTest.update({
		_id : usertest._id
	},{
		end_time : (usertest.start_time+usertest.time_taken)
	},function(err, updated_usertest){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}