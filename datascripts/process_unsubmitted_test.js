var mongoose 							  = require('mongoose');

var DbConfigs               = require('../database/configs.js');
var processUnsubmittedtest  = require('../routes/usertest.js').updateAllStatisticsOnSubmit;

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('----------------   process unsubmitted test  ------------------')
	updateAllStatisticsOnSubmit();
});

var plugin_id = "5821996ce19a32d659e156ff";
var user_test_id = "597593ecc5ace198573cb3cc";
var test_id = "589ab32c5fa2141f4442a4f5";
var total_ques = 100;
var time_taken = 10800000;
var user_id = "591e9101238c50f52ef028a8";
var user_already_gave_test = false;
var test_model = 1;

function updateAllStatisticsOnSubmit(){
	processUnsubmittedtest(plugin_id, 
													user_test_id, 
													test_id, 
													total_ques, 
													time_taken, 
													user_id, 
													user_already_gave_test, 
													test_model, function(err,resp){
														if(err){
															console.log(err);
															return process.exit(0);
														}
														console.log('successfull proceesesed');
														return process.exit(0);
													});
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}
