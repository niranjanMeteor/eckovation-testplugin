var mongoose 							= require('mongoose');
var fs                    = require('fs');
var csv                   = require("fast-csv");
var readline 			        = require('readline');

var DbConfigs             = require('../database/configs.js');

require('../models/categories.js');
require('../models/tests.js');
require('../models/questions.js');
require('../models/testquestions.js');

var Category 							= mongoose.model('Category');
var Test 									= mongoose.model('Test');
var Question 						  = mongoose.model('Question');
var TestQuestion		      = mongoose.model('TestQuestion');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('------------------------------------QUESTIONS IMPORT STARTED------------------------------------')
	startTestQuestionsImport();
});

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');

var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_IMPORT_QUESTION);

var TOTAL_QUES_IMPORT     = 0;
var TOTAL_MARKS_FOR_TEST  = 0;

function startTestQuestionsImport(){
	var category_group_file = process.argv[2];
	var question_file       = process.argv[3];
	if(!isValidCsvFileFormat(category_group_file)){
		console.log('Oops , you entered a wrong Category file name , Please enter a right category file name');
		return process.exit(0);
	}
	if(!isValidCsvFileFormat(question_file)){
		console.log('Oops , you entered a wrong option , Please enter a right option');
		return process.exit(0);
	}
	category_group_file = category_group_file.trim();
	question_file       = question_file.trim();
	parseCategoryGroupFile(category_group_file,function(err,test){
		if(err){
			console.trace(err);
			return process.exit(0);
		}
		console.log('\n');
		console.log('Following Test has been created');
		console.log(test);
		console.log('\n');
		var test_id = test.id+'';
		start_questions_csv_parsing(question_file, test_id, function(err,resp){
			if(err){
				console.trace(err);
				return process.exit(0);
			}
			console.log('Total Questions tried for Import  : '+resp.done);
			console.log('Total Questions imported successfully : '+resp.success);
			incrementTestTotalQues(test_id, function(err,resp){
				if(err){
					console.trace(err);
					console.log('Test Total Questions could not be successfully updated');
					return process.exit(0);
				}
				console.log('\n');
				console.log('Test Total Questions successfully updated');
				return process.exit(0);
			});
		});
	});
}

function parseCategoryGroupFile(category_group_file, cb){
	var group_code, category_num, level, is_default, catg_name, catg_type, test_name, total_ques, marks_per_ques, plugin_id, test_type;
	var stream = fs.createReadStream(category_group_file);
  var csvStream = csv()
    .on("data", function(data){
    	group_code   = data[0];
    	category_num = data[1];
    	catg_name    = data[2];
    	catg_type    = data[3];
    	level        = data[4];
    	is_default   = data[5];
    	test_name    = data[6];
    	desc         = data[7];
    	total_ques   = data[8];
    	marks_per_ques = data[9];
    	plugin_id      = data[10];
    	test_type      = data[11];
    })
    .on("end", function(){
      console.log("Group Code : "+group_code);
      console.log("Category : "+category_num);
      console.log("level : "+level);
      console.log("type : "+catg_type);
      console.log("is_default : "+is_default);
      console.log("catg name : "+catg_name);
      console.log("test name : "+test_name);
      console.log("total ques : "+total_ques);
      console.log("marks_per_ques : "+marks_per_ques);
      console.log("plugin_id : "+plugin_id);
      console.log("test_type : "+test_type);
      createTest(
      	group_code, 
      	category_num, 
      	catg_name, 
      	test_name, 
      	level, 
      	is_default, 
      	desc, 
      	catg_type, 
      	total_ques, 
      	marks_per_ques, 
      	plugin_id, 
      	test_type, 
      	function(err,resp){
      	if(err){
      		return cb(err,null);
      	}
      	return cb(null,resp);
      });
    });
  stream.pipe(csvStream);
}

function start_questions_csv_parsing(question_file, test_id, cb){
	var num_ques = 0;
	var toDo     = 0;
	var done     = 0;
	var success  = 0;
  var stream = fs.createReadStream(question_file);
  var csvStream = csv()
    .on("data", function(data){
    	toDo++;
      insertQuestionData(data, test_id, toDo, function(err,resp){
      	if(err){
      		console.trace(err);
      	} else {
      		success++;
      	}
      	done++;
      	if(done == toDo){
      		console.log('questions import done');
      		return cb(null,{done:done,success:success});
      	}
      });
    })
    .on("end", function(){
      console.log("All data evaluation done");
    });
  stream.pipe(csvStream);
}

function insertQuestionData(data, test_id, key, cb){
	var ans_array = [];

	var type 			= data[0].trim();
	var level			= data[1].trim();
	var text 			= data[2].trim();
	var ans1      = data[3].trim();
	var ans2      = data[4].trim();
	var ans3      = data[5].trim();
	var ans4      = data[6].trim();
	var cor_ans   = parseInt(data[7].trim());
	var marks     = 4; //parseInt(data[8].trim());

	ans_array.push({
		index : 1,
		text  : ans1
	});
	ans_array.push({
		index : 2,
		text  : ans2
	});
	ans_array.push({
		index : 3,
		text  : ans3
	});
	ans_array.push({
		index : 4,
		text  : ans4
	});

	var question_data = {
		type    : type,
		text    : text,
		level   : level,
		ans_options : ans_array,
		correct_ans : cor_ans
	};
	var new_question = new Question(question_data);
	new_question.save(function(err,new_question_created){
		if(err){
			console.trace(err);
			console.log('question could not be created');
			console.log(question_data);
			return cb(true, false);
		}
		var new_test_question = new TestQuestion({
			test_id : test_id,
			ques_id : new_question._id,
			act     : true,
			key     : key,
			marks   : marks
		});
		new_test_question.save(function(err,new_test_question_created){
			if(err){
				console.trace(err);
				console.log('question could not be created');
				console.log(question_data);
				return cb(true, false);
			}
			TOTAL_QUES_IMPORT++;
	  	TOTAL_MARKS_FOR_TEST += marks;
	  	return cb(null,true);
		});
	});
}

function incrementTestTotalQues(test_id, cb){
	Test.update({
		_id : test_id
	},{
		ques_quota : TOTAL_QUES_IMPORT
	},function(err,resp){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
}

function createTest(group_code, category_num, category_name, test_name, level, default_test, desc, catg_type, total_ques, marks_per_ques, plugin_id, test_type, cb){
	var new_category_num = 1;
	var is_default       = false;

	group_code     = group_code.trim();
	category_num   = category_num.trim();
	category_name  = category_name.trim();
	test_name      = test_name.trim();
	level          = level.trim();
	default_test   = default_test.trim();
	desc 				   = desc.trim();
	catg_type      = catg_type.trim();
	
	catg_type      = parseInt(catg_type);
	level          = parseInt(level);
	total_ques     = parseInt(total_ques);
	marks_per_ques = parseInt(marks_per_ques);
	test_type      = parseInt(test_type);
	total_marks    = (total_ques * marks_per_ques);

	if(default_test == "true"){
		is_default = true;
	} else {
		is_default = false;
	}
	Category.findOne({
		plugin_id : plugin_id,
		gid       : group_code,
		catg_no   : category_num,
	},function(err,category){
		if(err){
			return cb(err,null);
		}
		if(!category){
			Category.findOne({},{},{
				sort : {
					catg_no : -1
				}
			},function(err,max_category_num){
				if(err){
					return cb(err,null);
				}
				if(max_category_num){
					new_category_num = max_category_num.catg_no+1;
				}
				var new_category = new Category({
					plugin_id  : plugin_id,
					name       : category_name,
					gid        : group_code,
					catg_no    : new_category_num,
					type       : catg_type,
					act        : true
				});
				new_category.save(function(err, new_category_created){
					if(err){
						return cb(err,null);
					}
					var category_id = new_category._id;
					new_category    = null;
					var new_test_data = {
						name        : test_name,
						act         : true,
						catg_id     : category_id,
						level       : level,
						type        : test_type,
						is_default  : is_default,
						desc        : desc,
						num_ques    : total_ques,
						total_marks : total_marks
					};
					var new_test = new Test(new_test_data);
					new_test.save(function(err, new_test_created){
						if(err){
							return cb(err,null);
						}
						Category.update({
							_id : category_id
						},{
							$inc : { total_num_tests : 1 }
						},function(err,category_updated){
							if(err){
								return cb(err,null);
							}
							return cb(null,{test:new_test_data,id:new_test._id});
						});
					});
				});
			});
		} else {
			console.log('New Cateogry will not be created, already exist');
			var new_test_data = {
				name         : test_name,
				act          : true,
				catg_id      : category._id,
				level        : level,
				type         : test_type,
				is_default   : is_default,
				desc         : desc,
				num_ques     : total_ques,
				total_marks  : total_marks
			};
			var new_test = new Test(new_test_data);
			new_test.save(function(err, new_test_created){
				if(err){
					return cb(err,null);
				}
				Category.update({
					_id : category._id
				},{
					$inc : { total_num_tests : 1 }
				},function(err,category_updated){
					if(err){
						return cb(err,null);
					}
					return cb(null,{test:new_test_data,id:new_test._id});
				});
			});
		}
	});
}

function isValidCsvFileFormat(file_path){
	var split_dir = file_path.split('/');
	file_name     = split_dir[split_dir.length-1];
	console.log(split_dir);
	console.log(file_name);
	if(!file_name){
		return false;
	}
	file_name = file_name.trim();
	var split = file_name.split('.');
	if(split.length > 2){
		return false;
	}
	if(split[0] == null || split[0] == ''){
		return false;
	}
	if(split[1] == 'csv'){
		return true;
	}
	return false;
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME,options;
  return mongoose.connect(connect_string);
}
