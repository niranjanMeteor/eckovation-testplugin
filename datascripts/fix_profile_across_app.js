var mongoose 							= require('mongoose');

var DbConfigs             = require('../database/configs.js');

require('../models/users.js');
require('../models/usertests.js');
require('../models/groupstats.js');
var User 							    = mongoose.model('User');
var UserTest 							= mongoose.model('UserTest');
var GroupStats		          = mongoose.model('GroupStats');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('----------------  Profile Id Fix Script  ------------------')
	fixProfileAcrossApp();
});

function fixProfileAcrossApp(){
	User.find({},{
		server_id : 1
	},function(err, users){
		if(err){
			console.trace(err);
			return process.exit(0);
		}
		if(users.length == 0){
			console.log("NO users Found to Process");
			return process.exit(0);
		}
		var total = users.length;
		var done  = 0;
		var errs  = 0;
		var succs = 0;
		var cb1 = function(err, resp){
			if(err){
				errs++;
			} else {
				succs++;
			}
			done++;
			if(done < total){
				updateProfileAccrossApp(users[done]._id, users[done].server_id, cb1);
			} else {
				console.log('Total Tasks to be done : '+total);
				console.log('Total Tasks done '+done);
				console.log('Total Errors '+errs);
				console.log('Total Success '+succs);
				return process.exit(0);
			}
		};
		updateProfileAccrossApp(users[done]._id, users[done].server_id, cb1);
	});
}

function updateProfileAccrossApp(user_id, server_id, cb){
	GroupStats.update({
		user_id : user_id
	},{
		pid : server_id
	},{
		multi : true
	},function(err,groupstat_updated){
		if(err){
			return cb(err,null);
		}
		UserTest.update({
			user_id : user_id
		},{
			pid : server_id
		},{
			multi : true
		},function(err,usertest_updated){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}