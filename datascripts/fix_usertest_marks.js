var mongoose 							= require('mongoose');

var DbConfigs             = require('../database/configs.js');

require('../models/usertests.js');
require('../models/usertestquestions.js');
var UserTest 							= mongoose.model('UserTest');
var UserTestQuestion		  = mongoose.model('UserTestQuestion');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('----------------   Fix Usertest Marks Script  ------------------')
	fixUsertestWithNoFirstValue();
});

function fixUsertestWithNoFirstValue(){
	UserTest.find({
		is_submitted : true
	},{
		_id            : 1,
		marks_obtained : 1,
		total_ques     : 1,
		total_correct  : 1,
		if_all_correct : 1
	},function(err, usertests){
		if(err){
			console.trace(err);
			return process.exit(0);
		}
		if(usertests.length == 0){
			console.log("NO usertests Found to Process for Updating Marks");
			return process.exit(0);
		}
		var total = usertests.length;
		console.log('Going to process total jobs : '+total);
		var done  = 0;
		var errs  = 0;
		var succs = 0;
		var changed = 0;
		var cb1 = function(err, resp){
			if(err){
				errs++;
			} else {
				succs++;
			}
			if(resp){
				changed++;
			}
			done++;
			if(done < total){
				updateUsertestMarksIfInconsistent(usertests[done], cb1);
			} else {
				console.log('Total Tasks to be done : '+total);
				console.log('Total Tasks done '+done);
				console.log('Total Errors '+errs);
				console.log('Total Success '+succs);
				console.log('Total Usertests Edited '+changed);
				return process.exit(0);
			}
		};
		updateUsertestMarksIfInconsistent(usertests[done], cb1);
	});
}

function updateUsertestMarksIfInconsistent(usertest, cb){
	var marks         = 0;
	var correct_count = 0;
	var if_all_correct= false;
	UserTestQuestion.find({
		user_test_id : usertest._id,
		is_attempted : true
	},{
		_id 					 : 0,
		is_correct     : 1,
		marks_obtained : 1
	},function(err,usertestquestions){
		if(err){
			return cb(err,null);
		}
		var total = usertestquestions.length;
		for(var i=0; i<total; i++){
			if(!isNaN(usertestquestions[i].marks_obtained))
				marks += usertestquestions[i].marks_obtained;
			if(usertestquestions[i].is_correct)
				correct_count++;
		}
		if(correct_count == usertest.total_ques){
			if_all_correct = true;
		}
		var toUpdate = {};
		if(marks != usertest.marks_obtained){
			toUpdate["marks_obtained"] = parseFloat(marks.toFixed(2));
		}
		if(correct_count != usertest.total_correct){
			toUpdate["total_correct"] = correct_count;
		}
		if(if_all_correct != usertest.if_all_correct){
			toUpdate["if_all_correct"] = if_all_correct;
		}
		if(Object.keys(toUpdate).length == 0){
			return cb(null,null);
		}
		UserTest.update({
			_id : usertest._id
		},toUpdate,function(err, updated_usertest){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}