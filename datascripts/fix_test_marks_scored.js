var mongoose 							= require('mongoose');

var DbConfigs             = require('../database/configs.js');

require('../models/usertests.js');
require('../models/tests.js');
var UserTest 							= mongoose.model('UserTest');
var Test		              = mongoose.model('Test');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('----------------   Category Test Nums Fix Script  ------------------')
	fixTestWithMarksScored();
});

function fixTestWithMarksScored(){
	Test.find({
		catg_id : {$exists:true},
		act     : true,
		total_submits : {$gt:0}
	},{
		_id : 1
	},function(err, tests){
		if(err){
			console.trace(err);
			return process.exit(0);
		}
		if(tests.length == 0){
			console.log("NO Tests Found to Process");
			return process.exit(0);
		}
		var total = tests.length;
		var done  = 0;
		var errs  = 0;
		var succs = 0;
		var cb1 = function(err, resp){
			if(err){
				errs++;
			} else {
				succs++;
			}
			done++;
			if(done < total){
				updateTestWithMarksScored(tests[done]._id, cb1);
			} else {
				console.log('Total Tasks to be done : '+total);
				console.log('Total Tasks done '+done);
				console.log('Total Errors '+errs);
				console.log('Total Success '+succs);
				return process.exit(0);
			}
		};
		updateTestWithMarksScored(tests[done]._id, cb1);
	});
}

function updateTestWithMarksScored(test_id, cb){
	UserTest.find({
		test_id       : test_id,
		is_submitted  : true,
		is_first      : true
	},function(err,usertests){
		if(err){
			return cb(err,null);
		}
		if(usertests.length == 0){
			return cb(null,true);
		}
		var marks = 0;
		var total = usertests.length;
		var user_ids = [];
		for(var i=0; i<total; i++){
			if(user_ids.indexOf(usertests[i].user_id+'') < 0){
				user_ids.push(usertests[i].user_id+'');
				marks = marks + usertests[i].marks_obtained;
			}
		}
		Test.update({
			_id : test_id
		},{
			marks_scored : marks
		},function(err,test_updated){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}