var mongoose 							= require('mongoose');
var readline 			        = require('readline');

var DbConfigs             = require('../database/configs.js');

require('../models/pluginpaidinfos.js');
require('../models/categories.js');
require('../models/tests.js');
require('../models/testquestions.js');
require('../models/datacopyprogress.js');
require('../models/questions.js');
var PluginPaidInfo		    = mongoose.model('PluginPaidInfo');
var Test                  = mongoose.model('Test');
var Category              = mongoose.model('Category');
var TestQuestion          = mongoose.model('TestQuestion');
var DataCopyProgress      = mongoose.model('DataCopyProgress');
var Question = mongoose.model('Question');


connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('------------------------------------QUESTIONS IMPORT STARTED------------------------------------')
	replicateAcrossGroups();
});

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var testId = "58d372bc5e5859127784f310";

function replicateAcrossGroups(){
  var totalQues = 0;
  var ques_ids = [];


	TestQuestion.find({
      test_id : testId
  },function(err,tsq){
    //totalQues++;
    for(var indx in tsq) {
      totalQues++;
      ques_ids.push(tsq[indx]["ques_id"]);
    }

	console.log("total qs = " + totalQues);
	console.log(ques_ids);

    Question.find({
      _id : {$in : ques_ids}
    },function(err,ques_es){
	for(var j in ques_es) {

	var ques = ques_es[j];

        console.log('Text :' + ques["text"]);console.log("<br/>")
        //console.log("\n");
	console.log('Options: ');
	console.log(ques['ans_options']);console.log("<br/>")
	console.log('Ans: ');
        console.log(ques["correct_ans"]);console.log("<br/>")
	console.log("Expl : " + ques["expl"]);
        console.log("<br/>");console.log("<br/>");
	}
    });
  });
}


function endScript(){
  console.log('Thanks For using our service, Hope to serve you soon');
  return process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}
