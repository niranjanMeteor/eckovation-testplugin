var mongoose 							= require('mongoose');

var DbConfigs             = require('../database/configs.js');

require('../models/usertests.js');
var UserTest 							= mongoose.model('UserTest');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('----------------   Fix Usertest Script  ------------------')
	fixUsertestWithNoFirstValue();
});

function fixUsertestWithNoFirstValue(){
	UserTest.find({
		is_submitted : true,
		is_first     : {$exists:false}
	},{
		_id     : 1,
		test_id : 1,
		user_id : 1,
		end_time: 1
	},{
		sort : {
			end_time : 1
		}
	},function(err, usertests){
		if(err){
			console.trace(err);
			return process.exit(0);
		}
		if(usertests.length == 0){
			console.log("NO usertests Found to Process for Marking as First test for User");
			return process.exit(0);
		}
		var total = usertests.length;
		var first_usertest_obj = {};
		var temp_key;
		var temp;
		var toDoUsertests = [];
		for(var i=0; i<total; i++){
			temp     = usertests[i]._id+'';
			temp_key = usertests[i].test_id+''+usertests[i].user_id+'';
			if(!(temp_key in first_usertest_obj)){
				first_usertest_obj[temp_key] = temp;
				toDoUsertests.push(temp);
			}
		}
		total     = toDoUsertests.length;
		console.log('Going to process total jobs : '+total);
		var done  = 0;
		var errs  = 0;
		var succs = 0;
		var cb1 = function(err, resp){
			if(err){
				errs++;
			} else {
				succs++;
			}
			done++;
			if(done < total){
				updateUsertestIfFirst(toDoUsertests[done], cb1);
			} else {
				console.log('Total Tasks to be done : '+total);
				console.log('Total Tasks done '+done);
				console.log('Total Errors '+errs);
				console.log('Total Success '+succs);
				return process.exit(0);
			}
		};
		updateUsertestIfFirst(toDoUsertests[done], cb1);
	});
}

function updateUsertestIfFirst(usertest_id, cb){
	
	UserTest.update({
		_id : usertest_id
	},{
		is_first : true
	},function(err, updated_usertest){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}