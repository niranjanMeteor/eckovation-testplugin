var mongoose 							= require('mongoose');

var DbConfigs             = require('../database/configs.js');

require('../models/testquestions.js');
require('../models/tests.js');
var TestQuestion 					= mongoose.model('TestQuestion');
var Test		              = mongoose.model('Test');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('----------------   test Fix Num Question Script  ------------------')
	fixTestWithCorrectNumQuesCount();
});

function fixTestWithCorrectNumQuesCount(){
	Test.find({
		catg_id : {$exists:true},
		act     : true
	},{
		_id : 1
	},function(err, tests){
		if(err){
			console.trace(err);
			return process.exit(0);
		}
		if(tests.length == 0){
			console.log("NO Tests Found to Process");
			return process.exit(0);
		}
		var total = tests.length;
		var done  = 0;
		var errs  = 0;
		var succs = 0;
		var cb1 = function(err, resp){
			if(err){
				errs++;
			} else {
				succs++;
			}
			done++;
			if(done < total){
				updateTestWithCorrectNumQuesCount(tests[done]._id, cb1);
			} else {
				console.log('Total Tasks to be done : '+total);
				console.log('Total Tasks done '+done);
				console.log('Total Errors '+errs);
				console.log('Total Success '+succs);
				return process.exit(0);
			}
		};
		updateTestWithCorrectNumQuesCount(tests[done]._id, cb1);
	});
}

function updateTestWithCorrectNumQuesCount(test_id, cb){
	TestQuestion.count({
		test_id       : test_id,
		act           : true
	},function(err,count){
		if(err){
			return cb(err,null);
		}
		Test.update({
			_id : test_id
		},{
			num_ques : count
		},function(err,test_updated){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}