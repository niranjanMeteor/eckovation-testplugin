var mongoose 							= require('mongoose');
var readline 			        = require('readline');

var DbConfigs             = require('../database/configs.js');

require('../models/pluginpaidinfos.js');
var PluginPaidInfo		    = mongoose.model('PluginPaidInfo');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('------------------------------------QUESTIONS IMPORT STARTED------------------------------------')
	managePaidPlugins();
});

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function managePaidPlugins(){
	console.log('\n');
	console.log('****************************** MAIN MENU OPTIONS *******************************');
	console.log('\n');

  var answer = process.argv[2];
  var code_plugin = process.argv[3];

		if(isNaN(answer)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			process.exit();
		}
		switch(parseInt(answer)){
			case 1:
				return addNewPaidPlugin(code_plugin);
			break;

			case 2:
				return removePaidPlugin();
			break;

			case 3:
				return viewPaidPlugin();
			break;

			case 0:
				endScript();
			break;

			default:
				console.log('Oops , you entered a wrong option , Please enter a right option');
				process.exit();
		}
	
}

function addNewPaidPlugin(code_plugin) {
  if(code_plugin == 0 || code_plugin == '0')
    endScript();
  if(!code_plugin) {
    console.log('Oops , you entered a wrong option, Please enter a right option');
    process.exit();
  }
  code_plugin = code_plugin.trim();
  var values  = code_plugin.split(",");
  var code    = values[0];
  code = code.trim();
  if(isNaN(code) || code.length < 6) {
    console.log('Oops , you entered a wrong group_code option, Please enter a right group_code');
    process.exit();
  }
  var plugin_id = values[1];
  plugin_id     = plugin_id.trim();
  if(!plugin_id || plugin_id.length < 24) {
    console.log('Oops , you entered a wrong plugin_id, Please enter a right plugin_id');
    process.exit();
  }
  var tim = new Date().getTime();
  PluginPaidInfo.findOne({
  	plugin_id : plugin_id,
  	gid       : code,
  },function(err,paid_plugin){
  	if(err){
  		console.trace(err);
  		process.exit(0);
  	}
  	if(paid_plugin && paid_plugin.act == true){
  		console.log('\n');
  		console.log('Oops..., PLUGIN_ID : '+plugin_id+', in GROUP_CODE : '+code+', is ALREADY_PAID');
  		process.exit();
  	}
  	PluginPaidInfo.update({
  		plugin_id : plugin_id,
    	gid       : code
  	},{
  		$setOnInsert : {
  			plugin_id : plugin_id,
    		gid       : code,
    		act       : true
  		}
  	},{
  		upsert : true
  	},function(err,insertedPaid_plugin){
  		if(err){
  			console.trace(err);
  			process.exit(0);
  		}
  		console.log('\n');
  		console.log('Congratualtions..., PLUGIN_ID : '+plugin_id+', in GROUP_CODE : '+code+', has been MADE PAID');
  		process.exit();
  	});
  });
}

function removePaidPlugin(code_plugin){
  if(code_plugin == 0 || code_plugin == '0')
    endScript();
  if(!code_plugin) {
    console.log('Oops , you entered a wrong option, Please enter a right option');
    process.exit();
  }
  code_plugin = code_plugin.trim();
  var values  = code_plugin.split(",");
  var code    = values[0];
  code = code.trim();
  if(isNaN(code) || code.length < 6) {
    console.log('Oops , you entered a wrong group_code option, Please enter a right group_code');
    process.exit();
  }
  var plugin_id = values[1];
  plugin_id     = plugin_id.trim();
  if(!plugin_id || plugin_id.length < 24) {
    console.log('Oops , you entered a wrong plugin_id, Please enter a right plugin_id');
    process.exit();
  }
  var tim = new Date().getTime();
  PluginPaidInfo.findOne({
  	plugin_id : plugin_id,
  	gid       : code
  },function(err,paid_plugin){
  	if(err){
  		console.trace(err);
  		process.exit(0);
  	}
  	if(!paid_plugin){
  		console.log('\n');
  		console.log('Oops no need for as, PLUGIN_ID : '+plugin_id+', in GROUP_CODE : '+code+', is ALREADY_NOT_IN_PAID_LIST');
  		process.exit();
  	}
  	if(paid_plugin.act == false){
  		console.log('\n');
  		console.log('Oops no need for as, PLUGIN_ID : '+plugin_id+', in GROUP_CODE : '+code+', is ALREADY_NOT_IN_PAID_LIST');
  		process.exit();
  	}
  	PluginPaidInfo.update({
  		plugin_id : plugin_id,
    	gid       : code
  	},{
  		act : false
  	},function(err,insertedUnPaid_plugin){
  		if(err){
  			console.trace(err);
  			process.exit(0);
  		}
  		console.log('\n');
  		console.log('Congratualtions..., PLUGIN_ID : '+plugin_id+', in GROUP_CODE : '+code+', has been removed from PAID PLUGINS');
  		process.exit();
  	});
  });
}

function viewPaidPlugin(){
	PluginPaidInfo.find({act:true},function(err,paid_plugins){
		if(err){
			console.trace(err);
			process.exit(0);
		}
		if(paid_plugins.length == 0){
			console.log('\n');
			console.log('NO Paid Plugins Found In the Server');
			process.exit();
		}
		console.log('\n');
		for(var i=0; i<paid_plugins.length; i++){
			console.log('PLUGIN_ID : '+paid_plugins[i].plugin_id+', GID : '+paid_plugins[i].gid);
		}
		console.log('\n');
		process.exit();
	});
}

function endScript(){
  console.log('Thanks For using our service, Hope to serve you soon');
  return process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}
