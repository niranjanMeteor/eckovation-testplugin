var mongoose 							= require('mongoose');
const fs                  = require('fs');

var DbConfigs             = require('../database/configs.js');

require('../models/testquestions.js');
require('../models/questions.js');
require('../models/tests.js');
var TestQuestion 					= mongoose.model('TestQuestion');
var Test 					        = mongoose.model('Test');
var Question 					    = mongoose.model('Question');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('----------------   testquestion correct ans copy ------------------')
	startSavingCorrectAnswer();
});

var CATEGORY_ID = "595c82efa475d9bab543fedb";  // class 10th borad real numbers
var TEST_QUESTION_MARKS = {};
var QUESTION_CORRECT_ANS = {};
var filename1 = "/home/ubuntu/testplugindata/board_tenth/testquestion_marks.txt";
var filename2 = "/home/ubuntu/testplugindata/board_tenth/question_answer.txt";

function startSavingCorrectAnswer(){
	Test.find({
		catg_id : CATEGORY_ID,
		act     : true
	},{
		_id : 1
	},function(err,tests){
		if(err){
			console.trace(err);
			return process.exit(0);
		}
		var total = tests.length;
		if(total == 0){
			console.log("no tests found for this category");
			return process.exit(0);
		}
		var done = 0;
		var succs= 0;
		var cb1 = function(err,resp){
			if(err){
				console.trace(err);
				return process.exit(0);
			}
			if(resp){
				succs++;
			}
			done++;
			if(done < total){
				saveTestAnswers(tests[done]._id, cb1);
			} else {
				copyDataToFile();
				console.log('total to do : '+total);
				console.log('done : '+done);
				console.log('Succs : '+succs);
				return process.exit(0);
			}
		}
		saveTestAnswers(tests[done]._id, cb1);
	});
}

function saveTestAnswers(test_id, cb){
	TestQuestion.find({
		test_id : test_id,
		act     : true
	},{
		marks   : 1,
		ques_id : 1
	},function(err,testquestions){
		if(err){
			return cb(err,null);
		}
		var total = testquestions.length;
		if(total == 0){
			return cb(null,false);
		}
		var ques_ids = [];
		var temp;
		for(var i=0; i<total; i++){
			temp = testquestions[i].ques_id+'';
			if(ques_ids.indexOf(temp) < 0){
				ques_ids.push(temp);
			}
			TEST_QUESTION_MARKS[testquestions[i]._id+''] = testquestions[i].marks;
		}
		Question.find({
			_id : {$in : ques_ids}
		},{
			correct_ans : 1
		},function(err,questions){
			if(err){
				return cb(err,null);
			}
			var totalq = questions.length;
			for(var j=0; j<totalq; j++){
				QUESTION_CORRECT_ANS[questions[j]._id+''] = questions[j].correct_ans;
			}
			return cb(null,true);
		});
	});
}

function copyDataToFile(){
	var temp = '';
	var count = 0;
	for(var key1 in TEST_QUESTION_MARKS){
		if(count == 0){
			temp = key1+','+TEST_QUESTION_MARKS[key1];
		} else {
    	temp = '\n'+key1+','+TEST_QUESTION_MARKS[key1];
		}
    fs.appendFileSync(filename1,temp);
    count++;
  }
  count = 0;
  for(var key2 in QUESTION_CORRECT_ANS){
  	if(count == 0){
  		temp = key2+','+QUESTION_CORRECT_ANS[key2];
  	} else {
    	temp = '\n'+key2+','+QUESTION_CORRECT_ANS[key2];
  	}
    fs.appendFileSync(filename2,temp);
    count++;
  }
  console.log(Object.keys(TEST_QUESTION_MARKS).length);
  console.log(Object.keys(QUESTION_CORRECT_ANS).length);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}

// function connectMongoDb(){
//   var options = {
//     server : {
//       socketOptions : {
//         keepAlive : DbConfigs.MONGO_SERVER_KEEPALIVE
//       }
//     }
//   };
//   if(!DbConfigs.MONGO_UNAME || DbConfigs.MONGO_UNAME == ""){
//     options.server.poolSize = DbConfigs.MONGO_LOWER_POOLSIZE;
//     console.log('[[[[[[[    NO MONGO_UNAME FOUND IN DbConfigs, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
//     console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+DbConfigs.MONGO_LOWER_POOLSIZE+'     ]]]]]]]]]');
//     return mongoose.connect("mongodb://"+DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT+"/"+DbConfigs.DB_NAME,options);
//   }
//   options.replset = {
//     rs_name : DbConfigs.MONGO_REPLICA_SET_NAME,
//     socketOptions : {
//       keepAlive : DbConfigs.MONGO_REPLICA_KEEPALIVE
//     },
//     poolSize : DbConfigs.MONGO_LOWER_POOLSIZE
//   };
//   var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
//   var mongo_hosts    = DbConfigs.MONGO_HOST;
//   var mongo_port     = DbConfigs.MONGO_PORT;
//   for(var i=0; i<mongo_hosts.length; i++){
//     if(i > 0)
//       connect_string += ",";
//     connect_string += mongo_hosts[i]+":"+mongo_port;
//   }
//   connect_string += "/"+DbConfigs.DB_NAME;
//   console.log(connect_string);
//   console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
//   console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+DbConfigs.MONGO_LOWER_POOLSIZE+'      ]]]]]]]]]');
//   return mongoose.connect(connect_string,options);
// }