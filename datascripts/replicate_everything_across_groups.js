var mongoose 							= require('mongoose');
var readline 			        = require('readline');

var DbConfigs             = require('../database/configs.js');

require('../models/pluginpaidinfos.js');
require('../models/categories.js');
require('../models/tests.js');
require('../models/testquestions.js');
require('../models/datacopyprogress.js')
var PluginPaidInfo		    = mongoose.model('PluginPaidInfo');
var Test                  = mongoose.model('Test');
var Category              = mongoose.model('Category');
var TestQuestion          = mongoose.model('TestQuestion');
var DataCopyProgress      = mongoose.model('DataCopyProgress');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('------------------------------------QUESTIONS IMPORT STARTED------------------------------------')
	replicateAcrossGroups();
});

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function replicateAcrossGroups(){
	console.log('\n');
	console.log('****************************** MAIN MENU OPTIONS *******************************');
	console.log('\n');
	rL.question("Please tell me source group code, and plugin id : ", function(answer1) {
		if(!(answer1)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return replicateAcrossGroups();
		}

    var old_gid       = answer1.split(",")[0];
    var old_plugin_id = answer1.split(",")[1];

    rL.question("Please tell me source group code, and plugin id : ", function(answer2) {
      if(!(answer2)) {
        console.log('Oops , you entered a wrong option , Please enter a right option');
        return replicateAcrossGroups();
      }

      var new_gid       = answer2.split(",")[0];
      var new_plugin_id = answer2.split(",")[1];

      processCategory(old_gid,old_plugin_id,new_gid,new_plugin_id,function(err,data){
          console.log(err);
          console.log("All copying has been done!");
          endScript();
      });
    });
	});
}

function processCategory(gid, plugin_id, new_gid, new_plugin_id, cb) {
  Category.find({
    gid       : gid,
    plugin_id : plugin_id,
  },function(err,categories){
      if(err) { return cb(err,null); }
      console.log("candidates for Category: " + categories.length);

      var category = categories.shift();

      var cb11 = function(err,data){
          if(err) { console.log(err); }
          if(err) { return cb(err,null); }
          console.log("Copied a category");
          processTestByCategory(category,data["new_category_id"],new_gid, new_plugin_id);

          if(categories.length > 0) {
            category = categories.shift();
            attemptToInsertOrCreateCategory(category["id"],new_gid,new_plugin_id,cb11);
          }
      };

      attemptToInsertOrCreateCategory(category["id"],new_gid,new_plugin_id,cb11);

      /*categories.forEach(function(category){
        attemptToInsertOrCreateCategory(category["id"],new_gid,new_plugin_id,function(err,data){
            if(err) { console.log(err); }
            if(err) { return cb(err,null); }
            console.log("Copied a category");
            processTestByCategory(category,data["new_category_id"],new_gid, new_plugin_id);
        });
        //old_catg_id,old_test_id,question_id,new_group_code,new_plugin_id
      });*/
  });
}

function processTestByCategory(old_category,new_category_id,new_gid, new_plugin_id) {
  Test.find({
    catg_id : old_category["id"]
  },function(err,tests){
      if(err) { return cb(err,null); }
      console.log("candidates for tests: Test: " + tests.length);

      tests.forEach(function(test) {
          attemptToInsertOrCreateTest(old_category["id"],test["id"],new_category_id,new_gid, new_plugin_id,function(err,data){
              if(err) { console.log(err); }
              if(err) { return cb(err,null); }
              console.log("Copied a test");
              processTestQuestionByTest(old_category,test,data["new_test_id"],new_gid, new_plugin_id);
          });
          //old_catg_id,old_test_id,question_id,new_category_id,new_group_code,new_plugin_id,cb
      });
  });
}

function processTestQuestionByTest(old_category,old_test,new_test_id,new_group_code,new_plugin_id) {
    TestQuestion.find({
      test_id : old_test["id"]
    },function(err,testquestions){
        if(err) { return cb(err,null); }

        console.log("candidates for questions: Old Category: "+ old_category["id"] +", Test: "+ old_test["id"] +", TestQuestion: " + testquestions.length);

        testquestions.forEach(function(testquestion){
          attemptToInsertOrCreateTestQuestion(old_test["id"],testquestion["ques_id"],new_test_id,new_group_code,new_plugin_id,function(err,data) {
              if(err) { console.log(err); }
              if(err) { return cb(err,null); }
          });
        });

    });
}

var getMaxCategoryNumber = function(cb) {
  Category.findOne({},{},{
    sort : {
      catg_no : -1
    }
  },function(err,max_category){
    if(err){ return cb(err,null); }

    if(max_category){
      max_category_num = max_category.catg_no+1;
    } else {
      max_category_num = 1;
    }

    cb(null,max_category_num);
  });
};

function attemptToInsertOrCreateCategory(old_catg_id,new_group_code,new_plugin_id,cb) {
  getMaxCategoryNumber(function(err,max_category_num){
    if(err) { return cb(err,null); }

    Category.findOne({
        _id : old_catg_id
      },function(err,catg){
          if(err) { return cb(err,null); }
          if(!catg) { console.log("Category not found!"); return cb("Old category not found",null); }

          DataCopyProgress.findOne({
            source_id   : old_catg_id,
            target_aux  : new_group_code+":"+new_plugin_id,
            type        : "Category"
          },function(err,dcp){
            if(err) { return cb(err,null); }

            if(dcp) {
              //new category not found
              console.log("New Category alearedy exists!");
              return cb(null,{new_category_id:dcp["target_id"]});
            } else {
              //category found
              catg["gid"]             = new_group_code;
              catg["plugin_id"]       = new_plugin_id;
              catg["total_attempts"]  = 0;
              catg["total_submits"]   = 0;
              catg["catg_no"]         = max_category_num;

              var new_category_to_be_inserted = new Category({
                  plugin_id           : catg["plugin_id"],
                  name                : catg["name"],
                  catg_no             : catg["catg_no"],
                  act                 : catg["act"],
                  type                : catg["type"],
                  gid                 : catg["gid"],
                  total_num_tests     : catg["total_num_tests"],
                  total_attempts      : catg["total_attempts"],
                  total_submits       : catg["total_submits"],
                  desc                : catg["desc"],
                  pic_url             : catg["pic_url"],
                  most_popular_test   : catg["most_popular_test"],
                  visibility          : catg["visibility"],
                  tim                 : catg["tim"],
              });

              new_category_to_be_inserted.save(function(err,newly_catg_inserted){
                if(err) { return cb(err,null); }

                var datacopyprogress = new DataCopyProgress({
                  source_id : old_catg_id,
                  target_id : newly_catg_inserted["id"],
                  target_aux: new_group_code+":"+new_plugin_id,
                  type      : "Category",
                });

                datacopyprogress.save(function(err,new_dcp){
                  if(err) { return cb(err,null); }
                  return cb(null,{new_category_id:newly_catg_inserted["id"]});
                });
              });
            }
          });
      });
  });
}

function attemptToInsertOrCreateTest(old_catg_id,old_test_id,new_category_id,new_group_code,new_plugin_id,cb) {
  Test.findOne({
    _id : old_test_id,
  },function(err,test){
    if(err) { return cb(err,null); }

    DataCopyProgress.findOne({
      source_id   : test["id"],
      target_aux  : new_group_code+":"+new_plugin_id,
      type        : "Test",
    },function(err,dcp){
      if(err) { return cb(err,null); }

      if(dcp) {
        console.log("New test already exists!");
        return cb(null,{new_test_id:dcp["target_id"]});
      } else {
        console.log("New test going to add!");

        test["catg_id"]           = new_category_id;
        test["total_submits"]     = 0;
        test["total_attempts"]    = 0;

        var new_test_to_be_added = new Test({
          name                          : test["name"],
          act                           : test["act"],
          catg_id                       : test["catg_id"],
          level                         : test["level"],
          type                          : test["type"],
          model                         : test["model"],
          sub_model                     : test["sub_model"],
          is_default                    : test["is_default"],
          desc                          : test["desc"],
          num_ques                      : test["num_ques"],
          ques_quota                    : test["ques_quota"],
          mpq                           : test["mpq"],
          total_marks                   : test["total_marks"],
          marks_scored                  : test["marks_scored"],
          ssol                          : test["ssol"],
          timed                         : test["timed"],
          ml_at                         : test["ml_at"],
          min_tim                       : test["min_tim"],
          max_tim                       : test["max_tim"],
          i_fl_st                       : test["i_fl_st"],
          total_attempts                : test["total_attempts"],
          total_submits                 : test["total_submits"],
          submits_all_correct           : test["submits_all_correct"],
          min_time_submit_all_correct   : test["min_time_submit_all_correct"],
          max_score_attained            : test["max_score_attained"],
          tim                           : test["tim"],
          q_order                       : test["q_order"],
          ineg                          : test["ineg"],
          negm                          : test["negm"],
          publ                          : test["publ"],
          stim                          : test["stim"],
        });

        new_test_to_be_added.save(function(err,newly_created_test){
            if(err) { return cb(err,null); }
            var datacopyprogress = new DataCopyProgress({
              source_id : test["id"],
              target_id : newly_created_test["id"],
              target_aux  : new_group_code+":"+new_plugin_id,
              type      : "Test",
            });

            datacopyprogress.save(function(err,new_dcp){
              if(err) { return cb(err,null); }
              return cb(null,{new_test_id:newly_created_test["id"]});
            });
        });
      }
    });
  });
}

function attemptToInsertOrCreateTestQuestion(old_test_id,old_question_id,new_test_id,new_group_code,new_plugin_id,cb) {
  TestQuestion.findOne({
    test_id : old_test_id,
    ques_id : old_question_id
  },function(err,testquestion){
    if(err) { return cb(err,null); }

    DataCopyProgress.findOne({
      source_id : testquestion["id"],
      target_aux: new_group_code+":"+new_plugin_id,
      type      : "TestQuestion",
    },function(err,dcp){
      if(err) { return cb(err,null); }

      if(dcp) { 
        console.log("Test Question already exists!");
        return cb(null,{});
      } else {
        testquestion["test_id"] = new_test_id;

        var new_test_question = new TestQuestion({
          test_id : testquestion["test_id"],
          ques_id : testquestion["ques_id"],
          act : testquestion["act"],
          key : testquestion["key"],
          marks : testquestion["marks"],
          ineg : testquestion["ineg"],
          negm : testquestion["negm"],
          code : testquestion["code"],
        });

        new_test_question.save(function(err,newly_added_test_question){
            if(err) { return cb(err, null); }

            console.log("Test Question successfully copied!");

            var datacopyprogress = new DataCopyProgress({
              source_id : testquestion["id"],
              target_id : newly_added_test_question["id"],
              target_aux  : new_group_code+":"+new_plugin_id,
              type      : "TestQuestion",
            });

            datacopyprogress.save(function(err,new_dcp){
              if(err) { return cb(err,null); }
              return cb(null,{});
            });
        });
      }
    });
  });
}

function endScript(){
  console.log('Thanks For using our service, Hope to serve you soon');
  return process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}
