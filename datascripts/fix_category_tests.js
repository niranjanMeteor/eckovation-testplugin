var mongoose 							= require('mongoose');

var DbConfigs             = require('../database/configs.js');

require('../models/categories.js');
require('../models/tests.js');
var Category 							= mongoose.model('Category');
var Test		              = mongoose.model('Test');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('----------------   Category Test Nums Fix Script  ------------------')
	fixCategoryTestNums();
});

function fixCategoryTestNums(){
	Category.find({},{
		_id : 1
	},function(err, categories){
		if(err){
			console.trace(err);
			return process.exit(0);
		}
		if(categories.length == 0){
			console.log("NO Categories Found to Process");
			return process.exit(0);
		}
		var total = categories.length;
		var done  = 0;
		var errs  = 0;
		var succs = 0;
		var cb1 = function(err, resp){
			if(err){
				errs++;
			} else {
				succs++;
			}
			done++;
			if(done < total){
				updateCategoryTestNum(categories[done]._id, cb1);
			} else {
				console.log('Total Tasks to be done : '+total);
				console.log('Total Tasks done '+done);
				console.log('Total Errors '+errs);
				console.log('Total Success '+succs);
				return process.exit(0);
			}
		};
		updateCategoryTestNum(categories[done]._id, cb1);
	});
}

function updateCategoryTestNum(catg_id, cb){
	Test.count({
		catg_id : catg_id,
		act     : true,
		publ    : true
	},function(err,test_num){
		if(err){
			return cb(err,null);
		}
		if(!test_num){
			test_num = 0;
		}
		Category.update({
			_id : catg_id
		},{
			total_num_tests : test_num
		},function(err, updated_category){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  connect_string     += DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT;
  connect_string     += "/"+DbConfigs.DB_NAME;
  return mongoose.connect(connect_string,options);
}