var mongoose 							= require('mongoose');

var DbConfigs             = require('../database/configs.js');

require('../models/testquestions.js');
require('../models/usertestquestions.js');
var TestQuestion 					= mongoose.model('TestQuestion');
var UserTestQuestion		  = mongoose.model('UserTestQuestion');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('----------------   testquestion Fix Num Attempts and Correct Count Script  ------------------')
	fixTestWithCorrectNumQuesCount();
});

function fixTestWithCorrectNumQuesCount(){
	TestQuestion.find({
		act     : true
	},{
		test_id : 1,
		ques_id : 1
	},function(err, testquestions){
		if(err){
			console.trace(err);
			return process.exit(0);
		}
		if(testquestions.length == 0){
			console.log("NO testquestions Found to Process");
			return process.exit(0);
		}
		var total = testquestions.length;
		console.log('total tasks to be done : '+total);
		var done  = 0;
		var errs  = 0;
		var succs = 0;
		var cb1 = function(err, resp){
			if(err){
				errs++;
			} else {
				succs++;
			}
			done++;
			if(done < total){
				updateTestQuestionWithAttemptsAndCorrect(done, testquestions[done], cb1);
			} else {
				console.log('Total Tasks to be done : '+total);
				console.log('Total Tasks done '+done);
				console.log('Total Errors '+errs);
				console.log('Total Success '+succs);
				return process.exit(0);
			}
		};
		updateTestQuestionWithAttemptsAndCorrect(done, testquestions[done], cb1);
	});
}

function updateTestQuestionWithAttemptsAndCorrect(done, testquestion, cb){
	var test_id = testquestion.test_id;
	var ques_id = testquestion.ques_id;
	UserTestQuestion.count({
		test_id       : test_id,
		ques_id       : ques_id,
		is_attempted  : true,
		act           : true
	},function(err,countattempt){
		if(err){
			return cb(err,null);
		}
		UserTestQuestion.count({
			test_id       : test_id,
			ques_id       : ques_id,
			is_correct    : true,
			act           : true
		},function(err,countcorrect){
			if(err){
				return cb(err,null);
			}
			TestQuestion.update({
				_id : testquestion._id
			},{
				atmp : countattempt,
				crct : countcorrect
			},function(err,test_updated){
				if(err){
					return cb(err,null);
				}
				console.log('sno : '+done+', tid : '+test_id+', qid : '+ques_id+', atmp : '+countattempt+', crct : '+countcorrect);
				return cb(null,true);
			});
		}).read('s');
	}).read('s');
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : DbConfigs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!DbConfigs.MONGO_UNAME || DbConfigs.MONGO_UNAME == ""){
    options.server.poolSize = DbConfigs.MONGO_LOWER_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN DbConfigs, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+DbConfigs.MONGO_LOWER_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+DbConfigs.MONGO_HOST+":"+DbConfigs.MONGO_PORT+"/"+DbConfigs.DB_NAME,options);
  }
  options.replset = {
    rs_name : DbConfigs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : DbConfigs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : DbConfigs.MONGO_LOWER_POOLSIZE
  };
  var connect_string = "mongodb://"+DbConfigs.MONGO_UNAME+":"+DbConfigs.MONGO_PASS+"@";
  var mongo_hosts    = DbConfigs.MONGO_HOST;
  var mongo_port     = DbConfigs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+DbConfigs.DB_NAME;
  console.log(connect_string);
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+DbConfigs.MONGO_LOWER_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}