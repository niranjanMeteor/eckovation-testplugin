const express 							  = require('express');
const mongoose 							  = require('mongoose');
const jwt    							    = require('jsonwebtoken');

const configs					        = require('../configs/configs.js');
const HttpStatuses					  = require('../global/http_api_return_codes.js');
const ApiReturnHandlers			  = require('../global/api_return_handlers.js');
const errorCodes 						  = require('../global/error_codes.js');
const log4jsLogger  				  = require('../loggers/log4js_module.js');
const log4jsConfigs  			    = require('../loggers/log4js_configs.js');
const AuthSecurity      		  = require('../security/auth_tokens.js');
const RootServerApi      		  = require('../server/apicalls.js');
const TagModule               = require('../lib/tags/topictags.js');

require('../models/categories.js');
require('../models/tests.js');
require('../models/users.js');
require('../models/testquestions.js');
require('../models/pluginpaidinfos.js');
require('../models/tags.js');
require('../models/testtags.js');
const PluginPaidInfo 				  = mongoose.model('PluginPaidInfo');
const User                    = mongoose.model('User');
const Test 									  = mongoose.model('Test');
const TestQuestion					  = mongoose.model('TestQuestion');
const Category 							  = mongoose.model('Category');
const Tag 									  = mongoose.model('Tag');
const TestTag 							  = mongoose.model('TestTag');

const sendError 						  = ApiReturnHandlers.sendError;
const sendSuccess 					  = ApiReturnHandlers.sendSuccess;
const router 								  = express.Router();
const logger        				  = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_TEST_ADMIN);

const validTestTypes          = [Test.TYPE.GENERAL,Test.TYPE.COMPREHENSION,Test.TYPE.MULTIPLE_CORRECT,Test.TYPE.MATCHING,Test.TYPE.ASSERTION_REASON,Test.TYPE.SUBJECTIVE,Test.TYPE.FILL_BLANKS,Test.TYPE.TRUE_FALSE];
const validLevels             = [Test.TEST_LEVELS.BEGINERS,Test.TEST_LEVELS.EXPERIENCED,Test.TEST_LEVELS.ADVANCED,Test.TEST_LEVELS.EXPERT];
const validQuestionOrderTypes = [Test.QUESTION_ORDER.ORDERED,Test.QUESTION_ORDER.JUMBLED];

//route middleware to verify the jwt token for all user whether they are group admin
router.use(function(req, res, next){
	var user_id   = req.body.u_id;
	var gid       = req.body.g_id;
	var plugin_id = req.body.pl_id;
	var private_key;
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body,"r":"2nd_middleware","tkn":req.body.tokn});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
	  jwt.verify(req.body.tokn, private_key, function(err, decoded) {      
	    if(err){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err,"tkn":req.body.tokn});
	      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
	    } 
	    if(decoded.id1 != user_id){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_userid","p":req.body,"tkn":req.body.tokn});
		  	return sendError(res,"Access without valid user id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
	    }
	    if(decoded.id4 != gid){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body,"tkn":req.body.tokn});
		  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
	    }
	    User.findOne({
	    	_id : user_id,
				act : true,
			},function(err,user){
				if(err){
					logger.error({"url":req.originalUrl,"msg":"user_find_failed","er":err,"p":req.body,"tkn":req.body.tokn});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!user){
					logger.info({"url":req.originalUrl,"msg":"user_already_exists","p":req.body,"tkn":req.body.tokn});
					return sendSuccess(res,{ id : user._id, if_new : 0, srv_id : user.server_id, gid : user.gid });
				}
				RootServerApi.checkIfAdmin(user.identifier, function(err,resp){
					if(err){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_err","er":err,"p":req.body,"tkn":req.body.tokn});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!resp.success){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_failed","p":req.body,"tkn":req.body.tokn});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!resp.is_admin){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_UserIsNotGroupAdmin","p":req.body,"tkn":req.body.tokn});
						return sendError(res,"unauthorised","unauthorised",HttpStatuses.UNAUTHORIZED);
					}
			    next();
				});
			});
	  });
	});
});

/**************** Test Adaptivity Settings Starts *********************/

router.post('/a_adpv', function(req, res, next){

	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"a_adpv","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var test_id         = req.body.test_id.trim();

	Test.findOne({
		_id     : test_id,
		act     : true
	},{
		adpv : 1,
		tags : 1
	},function(err,test){
		if(err){
			logger.error({"r":"a_adpv","er":err,"p":req.body,"msg":"Test_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"a_adpv","p":req.body,"msg":"test_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(test.adpv){
			logger.error({"r":"a_adpv","p":req.body,"msg":"test_already_adaptive"});
			return sendError(res,"test_already_adaptive","test_already_adaptive",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		if(!test.tags || test.tags.length < configs.MINIMUM_TAGS_PER_TEST_FOR_ADAPTIVITY){
			logger.error({"r":"a_adpv","p":req.body,"msg":"test_should_have_minimum_tags"});
			return sendError(res,"test_should_have_minimum_tags","test_should_have_minimum_tags",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		TestQuestion.find({
			test_id : test_id,
			act     : true
		},{
			key : 1,
			tags: 1
		},function(err,testquestions){
			if(err){
				logger.error({"r":"a_adpv","er":err,"p":req.body,"msg":"Test_Find_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(testquestions.length == 0){
				logger.error({"r":"a_adpv","p":req.body,"msg":"no_questions_exist_in_test"});
				return sendError(res,"no_questions_exist_in_test","no_questions_exist_in_test",HttpStatuses.SERVER_SPECIFIC_ERROR);
			}
			var total = testquestions.length;
			for(var i=0; i<total; i++){
				if(!testquestions[i].tags || testquestions[i].tags.length == 0){
					logger.error({"r":"a_adpv","p":req.body,"msg":"all_testquestions_do_not_have_tags"});
				  return sendError(res,"all_testquestions_do_not_have_tags","all_testquestions_do_not_have_tags",HttpStatuses.SERVER_SPECIFIC_ERROR);
				}
			}
			Test.update({
				_id : test_id
			},{
				adpv : true
			},function(err,updated_test){
				if(err){
					logger.error({"r":"a_adpv","er":err,"p":req.body,"msg":"TestUpdate_DbError"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				return sendSuccess(res,{});
			});
		});
	});
});

/**************** Test Adaptivity Settings Starts *********************/

router.post('/adpv_stg', function(req, res, next){

	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('stg_no',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()) { 
		logger.error({"r":"adpv_stg","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var test_id         = req.body.test_id.trim();
	var stage_num       = 2;

	Test.findOne({
		_id     : test_id,
		act     : true
	},{
		adpv     : 1,
		adpstg   : 1,
		tags     : 1,
		mxqlv    : 1,
		num_ques : 1
	},function(err,test){
		if(err){
			logger.error({"r":"adpv_stg","er":err,"p":req.body,"msg":"Test_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"adpv_stg","p":req.body,"msg":"test_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(!test.adpv){
			logger.error({"r":"adpv_stg","p":req.body,"msg":"test_not_adaptive"});
			return sendError(res,"test_not_adaptive","test_not_adaptive",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		if(!test.tags || test.tags.length < configs.MINIMUM_TAGS_PER_TEST_FOR_ADAPTIVITY){
			logger.error({"r":"adpv_stg","p":req.body,"msg":"test_should_have_minimum_tags"});
			return sendError(res,"test_should_have_minimum_tags","test_should_have_minimum_tags",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		if(test.adpstg == 2){
			logger.error({"r":"adpv_stg","p":req.body,"msg":"test_already_adaptive_stage_2"});
			return sendError(res,"test_already_adaptive_stage_2","test_already_adaptive_stage_2",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		if(!test.mxqlv){
			logger.error({"r":"adpv_stg","p":req.body,"msg":"test_not_having_max_ques_level"});
			return sendError(res,"test_not_having_max_ques_level","test_not_having_max_ques_level",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		var max_ques_level = test.mxqlv;
		if(test.mxqlv < configs.MINIMUM_QUES_LEVEL_FOR_ADAPTIVITY_STAGE){
			logger.error({"r":"adpv_stg","p":req.body,"msg":"test_max_ques_level_not_within_limits"});
			return sendError(res,"test_max_ques_level_not_within_limits","test_max_ques_level_not_within_limits",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		TestQuestion.find({
			test_id : test_id,
			act     : true
		},{
			lv :1
		},function(err,testquestions){
			if(err){
				logger.error({"r":"adpv_stg","er":err,"p":req.body,"msg":"TestQuestion_Find_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(testquestions.length == 0){
				logger.error({"r":"adpv_stg","p":req.body,"msg":"no_question_found_in_test"});
				return sendError(res,"no_question_found_in_test","no_question_found_in_test",HttpStatuses.SERVER_SPECIFIC_ERROR);
			}
			var levelWiseCount = [];
			for(var m=0; m<max_ques_level; m++){
				levelWiseCount[m] = 0;
			}
			var total = testquestions.length;
			var temp;
			for(var i=0; i<total; i++){
				temp = testquestions[i];
				if(!temp.lv){
					logger.error({"r":"adpv_stg","p":req.body,"msg":"all_testquestions_do_not_have_level"});
					return sendError(res,"all_testquestions_do_not_have_level","all_testquestions_do_not_have_level",HttpStatuses.SERVER_SPECIFIC_ERROR);
				}
				if(temp.lv > max_ques_level || temp.lv < 1){
					logger.error({"r":"adpv_stg","p":req.body,"msg":"testquestion_level_not_within_limits"});
					return sendError(res,"testquestion_level_not_within_limits","testquestion_level_not_within_limits",HttpStatuses.SERVER_SPECIFIC_ERROR);
				}
				levelWiseCount[temp.lv-1] = levelWiseCount[temp.lv-1]+1;
			}
			// console.log('levelWiseCount');
			// console.log(levelWiseCount);
		  var min_num_ques = levelWiseCount[0];
			for(var j=0; j<max_ques_level; j++){
				if(levelWiseCount[j] < configs.MINIMUM_QUESTIONS_PER_LEVEL_FOR_ADAPTIVITY){
					logger.error({"r":"adpv_stg","p":req.body,"msg":"each_test_level_should_have_atleast_5_questions"});
					return sendError(res,"each_test_level_should_have_atleast_5_questions","each_test_level_should_have_atleast_5_questions",HttpStatuses.SERVER_SPECIFIC_ERROR);
			  }
			  if(min_num_ques > levelWiseCount[j]){
			  	min_num_ques = levelWiseCount[j];
			  }
		  }
			Test.update({
				_id : test_id
			},{
				adpstg   : 2,
				num_ques : min_num_ques
 			},function(err,updated_test){
				if(err){
					logger.error({"r":"adpv_stg","er":err,"p":req.body,"msg":"TestUpdate_DbError"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				return sendSuccess(res,{});
			});
		});
	});
});

router.post('/r_adpv_stg', function(req, res, next){

	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('stg_no',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()) { 
		logger.error({"r":"r_adpv_stg","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var test_id         = req.body.test_id.trim();
	var stage_num       = 2;

	Test.findOne({
		_id     : test_id,
		act     : true
	},{
		adpv : 1,
		adpstg : 1,
		tags : 1
	},function(err,test){
		if(err){
			logger.error({"r":"r_adpv_stg","er":err,"p":req.body,"msg":"Test_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"r_adpv_stg","p":req.body,"msg":"test_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(!test.adpstg){
			logger.error({"r":"adpv_stg","p":req.body,"msg":"test_not_having_adaptive_stage"});
			return sendError(res,"test_not_having_adaptive_stage","test_not_having_adaptive_stage",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		if(test.adpstg != 2){
			logger.error({"r":"adpv_stg","p":req.body,"msg":"test_not_having_adaptive_stage_2"});
			return sendError(res,"test_not_having_adaptive_stage_2","test_not_having_adaptive_stage_2",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		TestQuestion.count({
			test_id : test_id,
			act     : true
		},function(err,num_ques){
			if(err){
				logger.error({"r":"r_adpv_stg","er":err,"p":req.body,"msg":"TestQuestioFind_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			Test.update({
				_id : test_id
			},{
				adpstg: 1,
				num_ques : num_ques
			},function(err,updated_test){
				if(err){
					logger.error({"r":"r_adpv_stg","er":err,"p":req.body,"msg":"TestUpdate_DbError"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				return sendSuccess(res,{});
			});
		});
	});
});

router.post('/r_adpv', function(req, res, next){

	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"r_adpv","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var test_id         = req.body.test_id.trim();

	Test.findOne({
		_id     : test_id,
		act     : true
	},{
		adpv : 1,
		tags : 1
	},function(err,test){
		if(err){
			logger.error({"r":"r_adpv","er":err,"p":req.body,"msg":"Test_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"r_adpv","p":req.body,"msg":"test_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(!test.adpv || test.adpv != true){
			logger.error({"r":"r_adpv","p":req.body,"msg":"test_already_not_adaptive"});
			return sendError(res,"test_already_not_adaptive","test_already_not_adaptive",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		Test.update({
			_id : test_id
		},{
			adpv : false
		},function(err,updated_test){
			if(err){
				logger.error({"r":"r_adpv","er":err,"p":req.body,"msg":"TestUpdate_DbError"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			return sendSuccess(res,{});
		});
	});
});

/**************** Test Adaptivity Settings Ends **********************/

router.post('/test_anlt', function(req, res, next){

	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"rd","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id    = req.body.u_id.trim();
	var gid        = req.body.g_id.trim();
	var plugin_id  = req.body.pl_id.trim();
	var catg_id    = req.body.catg_id.trim();
	var test_id    = req.body.test_id.trim();
	var tim        = new Date().getTime();

	Category.findOne({
		_id   : catg_id,
		gid   : gid,
		act   : true
	},function(err,category){
		if(err){
			logger.error({"r":"rd","er":err,"p":req.body,"msg":"Category_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!category){

		}
		Test.findOne({
			_id      : test_id,
			act      : true
		},function(err,tests){
			if(err){

			}
			if(tests.length == 0){

			}
			var test_attempts_submits = {};
			var total = tests.length;
			var tempd;
			var test_ids = [];
			for(var i=0; i<total; i++){
				tempd = {
					a : tests[i].total_attempts,
					s : tests[i].total_submits
				};
				test_attempts_submits[tests[i]._id+''] = tempd;
			}
			return sendSuccess(res,{graph_data:getDummyData()});
		});
	});
});

/******************** Test CRUD Starts **************************/

router.post('/rd', function(req, res, next){
	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"rd","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var gid             = req.body.g_id.trim();
	var plugin_id       = req.body.pl_id.trim();
	var category_id     = req.body.catg_id.trim();
	var tim             = new Date().getTime();

	Category.findOne({
		_id       : category_id,
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,category){
		if(err){
			logger.error({"r":"rd","er":err,"p":req.body,"msg":"Category_Find_Error"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!category){
			logger.error({"r":"rd","p":req.body,"msg":"category_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		Test.find({
			catg_id : category_id,
			act     : true
		},{
			name       : 1,
			level      : 1,
			type       : 1,
			is_default : 1,
			desc       : 1,
			num_ques   : 1,
			mpq        : 1,
			total_marks: 1,
			timed      : 1,
			max_tim    : 1,
			ineg       : 1,
			negm       : 1,
			publ       : 1,
			stim       : 1,
			ssol       : 1,
			q_order    : 1,
			createdAt  : 1,
			model      : 1,
			sub_model  : 1,
			i_fl_st    : 1,
			adpv       : 1,
			adpstg     : 1,
			tags       : 1,
			mxqlv      : 1
		},function(err,tests){
			if(err){
				logger.error({"r":"rd","er":err,"p":req.body,"msg":"Test_Find_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			return sendSuccess(res,{tests:tests, tim:tim});
		});
	});
});

router.post('/ad', function(req, res, next){
	
	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('nm',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('typ',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('lvl',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('mxqlv',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('mpq',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ineg',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('negm',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('desc',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('timed',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('max_tim',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('ssol',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('q_ord',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('stimdiff',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('model',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('sub_model',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('i_fl_st',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()) { 
		logger.error({"r":"ad","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var gid             = req.body.g_id.trim();
	var plugin_id       = req.body.pl_id.trim();
	var category_id     = req.body.catg_id.trim();
	var name            = req.body.nm.trim();
	var type            = (!isNaN(req.body.typ)) ? parseInt(req.body.typ) : "";
	var level           = (!isNaN(req.body.lvl)) ? parseInt(req.body.lvl) : "";
	var marks_per_ques  = (!isNaN(req.body.mpq)) ? parseInt(req.body.mpq) : "";
	var ineg            = req.body.ineg;
	var negm            = req.body.negm;
	var desc            = (req.body.desc) ? req.body.desc.trim() : "";
	var is_timed        = req.body.timed == 'true' || req.body.timed == true
	var max_time        = (req.body.max_tim) ? parseInt(req.body.max_tim) : 0;
	var ssol            = req.body.ssol;
	var que_order       = (req.body.q_ord) ? parseInt(req.body.q_ord) : null;
	var tim             = new Date().getTime();
	var stimdiff        = (!req.body.stimdiff) ? 0 : req.body.stimdiff;
	var ques_level_max  = configs.DEFAULT_MAXIMUM_QUESTION_LEVEL;

	if(isNaN(stimdiff) || stimdiff < 0){
		logger.error({"r":"ad","p":req.body,"msg":"invalid_stimdiff_for_test"});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	var stim = tim + stimdiff;
	if(is_timed && (!max_time || (max_time < configs.MIN_TIME_FOR_TIMED_TEST))){
		logger.error({"r":"ad","p":req.body,"msg":"invalid_max_time_for_timed_test"});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	if(validTestTypes.indexOf(type) < 0){
		logger.error({"r":"ad","p":req.body,"msg":"question_type_not_valid","typ":type});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	if(validLevels.indexOf(level) < 0){
		logger.error({"r":"ad","p":req.body,"msg":"question_level_not_valid","level":level});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	if(!marks_per_ques || (marks_per_ques < configs.MIN_MARKS_PER_QUESTION_IN_A_TEST) || (marks_per_ques > configs.MAX_MARKS_PER_QUESTION_IN_A_TEST)){
		logger.error({"r":"ad","msg":"marks_per_ques_invalid","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	if(ineg && ineg == true && negm && (isNaN(negm) || negm <= 0)){
		logger.error({"r":"ad","msg":"negm_value_invalid","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	if(stim < tim){
		logger.error({"r":"ad","msg":"test_Start_Time_not_valid","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	if(req.body.mxqlv && req.body.mxqlv > 0 && req.body.mxqlv <= configs.FIXED_MAXIMUM_QUESTION_LEVEL){
		ques_level_max = parseInt(req.body.mxqlv);
	}
	if("ssol" in req.body){
		if(ssol == true || ssol == 'true'){
			ssol = true;
		} else if(ssol == false || ssol == 'false'){
			ssol = false;
		} else {
			logger.error({"r":"ad","msg":"ssol_value_invalid","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
	}
	if(que_order && validQuestionOrderTypes.indexOf(que_order) < 0){
		logger.error({"r":"ad","msg":"question_order_value_invalid","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	Category.findOne({
		_id       : category_id,
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,category){
		if(err){
			logger.error({"r":"ad","er":err,"p":req.body,"msg":"Category_Find_Error"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!category){
			logger.error({"r":"ad","p":req.body,"msg":"category_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		var new_test_obj = {
			name        : name,
			catg_id     : category_id,
			type        : type,
			level       : level,
			mxqlv       : ques_level_max,
			act         : true,
			mpq         : marks_per_ques,
			desc        : desc,
			timed       : is_timed,
			max_tim     : max_time,
			tim         : tim,
			stim        : stim
		};
		if(ineg && ineg == true){
			new_test_obj.ineg = true;
		}
		if(negm){
			new_test_obj.negm = parseFloat(negm);
		} 
		if("ssol" in req.body){
			new_test_obj.ssol = ssol;
		}
		if(que_order){
			new_test_obj.q_order = que_order;
		}
		if("model" in req.body){
			new_test_obj.model = parseInt(req.body.model);
		}
		if("sub_model" in req.body){
			new_test_obj.sub_model = parseInt(req.body.sub_model);
		}
		if("i_fl_st" in req.body){
			if(req.body.i_fl_st == true || req.body.i_fl_st == 'true')
				new_test_obj.i_fl_st = true;
		}
		
		var new_test = new Test(new_test_obj);
		new_test.save(function(err,test_created){
			if(err){
				logger.error({"r":"ad","er":err,"p":req.body,"msg":"Test_Insertion_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			logger.info({"r":"ad","p":req.body,"msg":"Test_Added_Success","tq":new_test});
			return sendSuccess(res,{test:new_test});
		});

	});
});

router.post('/ed', function(req, res, next){

	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('nm',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('typ',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('lvl',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('mxqlv',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('ineg',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('negm',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('desc',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('timed',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('max_tim',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('ssol',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('q_ord',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('publ',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('stimdiff',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('model',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('sub_model',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('i_fl_st',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()) { 
		logger.error({"r":"ed","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var gid             = req.body.g_id.trim();
	var plugin_id       = req.body.pl_id.trim();
	var category_id     = req.body.catg_id.trim();
	var test_id         = req.body.test_id.trim();
	
	var negm            = (req.body.negm) ? req.body.negm : null;
	var max_time        = (req.body.max_tim) ? parseInt(req.body.max_tim) : 0;
	
	var tim             = new Date().getTime();
	var toEdit          = {};
	var test_num;

	if("timed" in req.body){
		var is_timed        = req.body.timed;
		if(is_timed == true || is_timed == 'true') {
			if(!max_time || (max_time < configs.MIN_TIME_FOR_TIMED_TEST)){
				logger.error({"r":"ed","p":req.body,"msg":"invalid_max_time_for_timed_test"});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			toEdit.timed    = true;
			toEdit.max_tim  = max_time;
		} else {
			toEdit.timed    = false;
		}
	}	
	if('type' in req.body){
		var type            = (!isNaN(req.body.typ)) ? parseInt(req.body.typ) : "";
		if(validTestTypes.indexOf(type) < 0){
			logger.error({"r":"ed","p":req.body,"msg":"question_type_not_valid"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		toEdit.type = type;
	}	
	if('level' in req.body){
		var level           = (!isNaN(req.body.lvl)) ? parseInt(req.body.lvl) : "";
		if(validLevels.indexOf(level) < 0){
			logger.error({"r":"ed","p":req.body,"msg":"question_level_not_valid"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		toEdit.level = level;
	}	
	if("ineg" in req.body){
		var ineg            = req.body.ineg;
		if(ineg == true || ineg == 'true'){
			if(negm){
				if(isNaN(negm) || negm <= 0){
					logger.error({"r":"ed","msg":"negm_value_invalid","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				}
				toEdit.negm = negm;
			}
			toEdit.ineg = true;
		} else if(ineg == false || ineg == 'false'){
			toEdit.ineg = false;
		} else {
			logger.error({"r":"ed","p":req.body,"msg":"ineg_value_not_valid"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
	}	
	if("ssol" in req.body){
		var ssol            = req.body.ssol;
		if(ssol == true || ssol == 'true'){
			toEdit.ssol = true;
		} else if(ssol == false || ssol == 'false'){
			toEdit.ssol = false;
		} else {
			logger.error({"r":"ed","msg":"ssol_value_invalid","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
	}	
	if('q_ord' in req.body){
		var que_order       = (req.body.q_ord) ? parseInt(req.body.q_ord) : null;
		if(validQuestionOrderTypes.indexOf(que_order) < 0){
			logger.error({"r":"ed","msg":"question_order_value_invalid","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		toEdit.q_order = que_order;
	}
	if('nm' in req.body){
		var name            = req.body.nm.trim();
		toEdit.name = name;
	}
	if('desc' in  req.body){
		var desc            = (req.body.desc) ? req.body.desc.trim() : "";
		toEdit.desc = desc;
	}
	if(!toEdit || Object.keys(toEdit).length == 0){
		logger.error({"r":"ed","p":req.body,"msg":"no_data_to_edit"});
		return sendError(res,"No Data In Input To Edit","no_data_to_edit",HttpStatuses.NO_INPUT_DATA);
	}
	if("publ" in req.body){
		var publ = req.body.publ;
		if(publ == true || publ == 'true'){
			toEdit.publ = true;
		} else if(publ == false || publ == 'false'){
			toEdit.publ = false;
		}
	}
	if("stimdiff" in req.body){
		var stimdiff = req.body.stimdiff;
		if(isNaN(stimdiff) || stimdiff < 0){
			logger.error({"r":"ed","p":req.body,"msg":"invalid_stimdiff_for_test"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		stim = tim + stimdiff;
		if(stim < tim){
			logger.error({"r":"ed","msg":"test_Start_Time_not_valid","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		toEdit.stim = stim;
	}
	if("model" in req.body){
		toEdit.model = parseInt(req.body.model);
	}
	if("sub_model" in req.body){
		toEdit.sub_model = parseInt(req.body.sub_model);
	}
	if("i_fl_st" in req.body){
		if(req.body.i_fl_st == true || req.body.i_fl_st == 'true'){
			toEdit.i_fl_st = true;
		} else if(req.body.i_fl_st == false || req.body.i_fl_st == 'false') {
			toEdit.i_fl_st = false;
		}
	}
	if("mxqlv" in req.body){
		var mxqlv = parseInt(req.body.mxqlv);
		if(mxqlv > 0 && mxqlv <= configs.FIXED_MAXIMUM_QUESTION_LEVEL){
			toEdit.mxqlv = mxqlv;
		}
	}
	
	Category.findOne({
		_id       : category_id,
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},{
		total_num_tests : 1
	},function(err,category){
		if(err){
			logger.error({"r":"ed","er":err,"p":req.body,"msg":"Category_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!category){
			logger.error({"r":"ed","p":req.body,"msg":"category_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		Test.findOne({
			_id     : test_id,
			catg_id : category_id,
			act     : true
		},{
			publ : 1,
			mxqlv: 1,
			adpstg: 1
		},function(err,old_test){
			if(err){
				logger.error({"r":"ed","er":err,"p":req.body,"msg":"Test_Find_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!old_test){
				logger.error({"r":"ed","p":req.body,"msg":"test_not_found"});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			if(old_test.adpstg && old_test.adpstg == 2){
				if(toEdit.mxqlv && toEdit.mxqlv != old_test.mxqlv){
					toEdit.adpstg = 1;
				}
			}
			console.log(toEdit);
			Test.update({
				_id     : test_id,
				catg_id : category_id,
				act     : true
			},toEdit,function(err,test_updated){
				if(err){
					logger.error({"r":"ed","er":err,"p":req.body,"msg":"Testquestion_Update_Error"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				Test.findOne({
					_id     : test_id
				},{
					name                : 1,
					catg_id             : 1,
					type                : 1,
					level               : 1,
					desc                : 1,
					timed               : 1,
					max_tim             : 1,
					num_ques            : 1,
					ssol                : 1,
					ineg                : 1,
					negm                : 1,
					publ                : 1,
					stim                : 1,
					q_order             : 1,
					model               : 1,
					sub_model           : 1,
					tg                  : 1,
					adpv                : 1,
					adpstg              : 1,
					mxqlv               : 1
				},function(err,modified_test){
					if(err){
						logger.error({"r":"ed","er":err,"p":req.body,"msg":"ModifiedTest_Find_Error"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!modified_test){
						logger.error({"r":"ed","p":req.body,"msg":"modified_test_not_found"});
						return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
					}
					manageTestQuestionOnTestEdit("ed", modified_test, function(err,resp_tq_ed){
						if(err){
							logger.error({"r":"ed","er":err,"p":req.body,"msg":"manageTestQuestionOnTestEdit_Error"});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						manageCategoryOnTestEdit("ed", category_id, old_test.publ, modified_test.publ, function(err,resp_ctg_ed){
							if(err){
								logger.error({"r":"ed","er":err,"p":req.body,"msg":"manageCategoryOnTestEdit_Error"});
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}
							logger.info({"r":"ed","p":req.body,"msg":"success","d":{test:modified_test,tim:tim,toEdit:toEdit},"ctg_ed":resp_ctg_ed,"tq_ed":resp_tq_ed});
							return sendSuccess(res,{test:modified_test});
						});
					});
				});
			});

		});
	});
});

router.post('/dl', function(req, res, next){
	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"dl","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id         = req.body.u_id.trim();
	var gid             = req.body.g_id.trim();
	var plugin_id       = req.body.pl_id.trim();
	var category_id     = req.body.catg_id.trim();
	var test_id         = req.body.test_id.trim();

	Category.findOne({
		_id       : category_id,
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,category){
		if(err){
			logger.error({"r":"dl","er":err,"p":req.body,"msg":"Category_Find_Error"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!category){
			logger.error({"r":"dl","p":req.body,"msg":"category_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		Test.findOne({
			_id     : test_id,
			catg_id : category_id,
			act     : true
		},function(err,test){
			if(err){
				logger.error({"r":"dl","er":err,"p":req.body,"msg":"Test_Find_Error"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!test){
				logger.error({"r":"dl","p":req.body,"msg":"test_not_found"});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			Test.update({
				_id     : test_id,
				catg_id : category_id,
				act     : true
			},{
				act : false
			},function(err,test_removed){
				if(err){
					logger.error({"r":"dl","er":err,"p":req.body,"msg":"Testquestion_Update_Error"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!test.publ){
					logger.info({"r":"dl","p":req.body,"msg":"Test_Deleted_From_Category_wthout_publ_Success","tq":test_removed});
					return sendSuccess(res,{});
				}
				Category.update({
					_id       : category_id
				},{
					$inc : {total_num_tests : -1}
				},function(err,category_updated){
					if(err){
						logger.error({"r":"dl","er":err,"p":req.body,"msg":"CategoryUpdate_Find_Error"});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					logger.info({"r":"dl","p":req.body,"msg":"Test_Deleted_From_Category_with_publ_Success","tq":test_removed});
					return sendSuccess(res,{});
				});
			});
		});
	});
});

/******************** Test CRUD Ends **************************/

function manageTestQuestionOnTestEdit(route_name, new_test, cb){
	if(!("ineg" in new_test)){
		return cb(null,false);
	}
	var toUpdate = {
		ineg   : new_test.ineg
	};
	if(new_test.ineg == true){
		if(new_test.negm){
			toUpdate.negm = new_test.negm;
		} 
	}
	TestQuestion.update({
		test_id : new_test._id,
		act     : true
	},toUpdate,{
		multi : true
	},function(err,resp){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
}

function manageCategoryOnTestEdit(route_name, category_id, old_test_publ, new_test_publ, cb){
	var test_num;
	if(new_test_publ == old_test_publ){
		return cb(null,false);
	} else if(new_test_publ == true){
		test_num = 1;
	} else {
		test_num = -1;
	}
	Category.update({
		_id       : category_id
	},{
		$inc : { total_num_tests : test_num }
	},function(err,category_updated){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
}

module.exports = router;
