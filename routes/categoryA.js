var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    							  = require('jsonwebtoken');

var configs					      = require('../configs/configs.js');
var HttpStatuses					= require('../global/http_api_return_codes.js');
var ApiReturnHandlers			= require('../global/api_return_handlers.js');
var errorCodes 						= require('../global/error_codes.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var RootServerApi      		= require('../server/apicalls.js');

require('../models/categories.js');
require('../models/tests.js');
require('../models/users.js');
require('../models/pluginpaidinfos.js');
var PluginPaidInfo 				= mongoose.model('PluginPaidInfo');
var User                  = mongoose.model('User');
var Category 							= mongoose.model('Category');
const Test 								= mongoose.model('Test');

var sendError 						= ApiReturnHandlers.sendError;
var sendSuccess 					= ApiReturnHandlers.sendSuccess;
var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_CATEGORY_ADMIN);

//route middleware to verify the jwt token for all user whether they are group admin
router.use(function(req, res, next){
	var user_id   = req.body.u_id;
	var gid       = req.body.g_id;
	var plugin_id = req.body.pl_id;
	var private_key;
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body,"r":"2nd_middleware","tkn":req.body.tokn});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
	  jwt.verify(req.body.tokn, private_key, function(err, decoded) {      
	    if(err){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err,"tkn":req.body.tokn});
	      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
	    } 
	    if(decoded.id1 != user_id){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_userid","p":req.body,"tkn":req.body.tokn});
		  	return sendError(res,"Access without valid user id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
	    }
	    if(decoded.id4 != gid){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body,"tkn":req.body.tokn});
		  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
	    }
	    User.findOne({
	    	_id : user_id,
				act : true,
			},function(err,user){
				if(err){
					logger.error({"url":req.originalUrl,"msg":"user_find_failed","er":err,"p":req.body,"tkn":req.body.tokn});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!user){
					logger.info({"url":req.originalUrl,"msg":"user_already_exists","p":req.body,"tkn":req.body.tokn});
					return sendSuccess(res,{ id : user._id, if_new : 0, srv_id : user.server_id, gid : user.gid });
				}
				RootServerApi.checkIfAdmin(user.identifier, function(err,resp){
					if(err){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_err","er":err,"p":req.body,"tkn":req.body.tokn});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!resp.success){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_failed","p":req.body,"tkn":req.body.tokn});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!resp.is_admin){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_UserIsNotGroupAdmin","p":req.body,"tkn":req.body.tokn});
						return sendError(res,"unauthorised","unauthorised",HttpStatuses.UNAUTHORIZED);
					}
			    next();
				});
			});
	  });
	});
});

/**************** Category Adaptivity Settings Starts *********************/

router.post('/a_adpv', function(req, res, next){

	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"a_adpv","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var catg_id         = req.body.catg_id.trim();
	var tim             = new Date().getTime();

	Category.findOne({
		_id     : catg_id,
		act     : true
	},{
		adpv : 1
	},function(err,category){
		if(err){
			logger.error({"r":"a_adpv","er":err,"p":req.body,"msg":"Category_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!category){
			logger.error({"r":"a_adpv","p":req.body,"msg":"category_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(category.adpv){
			logger.error({"r":"a_adpv","p":req.body,"msg":"category_already_adaptive"});
			return sendError(res,"category_already_adaptive","category_already_adaptive",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		Test.find({
			catg_id  : catg_id,
			act      : true,
			num_ques : {$gt : 0},
			publ     : true,
			stim     : {$lte:tim}
		},{
			adpv : 1
		},function(err,tests){
			if(err){
				logger.error({"r":"a_adpv","er":err,"p":req.body,"msg":"Category_Find_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var total = tests.length;
			if(total == 0){
				logger.error({"r":"a_adpv","p":req.body,"msg":"category_no_tests_found_for_adaptivity"});
				return sendError(res,"category_no_tests_found_for_adaptivity","category_no_tests_found_for_adaptivity",HttpStatuses.SERVER_SPECIFIC_ERROR);
			}
			for(var i=0; i<total; i++){
				if(!tests[i].adpv){
					logger.error({"r":"a_adpv","p":req.body,"msg":"category_tests_not_all_adaptive"});
					return sendError(res,"category_tests_not_all_adaptive","category_tests_not_all_adaptive",HttpStatuses.SERVER_SPECIFIC_ERROR);
				}
			}
			Category.update({
				_id : catg_id
			},{
				adpv : true
			},function(err,updated_test){
				if(err){
					logger.error({"r":"a_adpv","er":err,"p":req.body,"msg":"CategoryUpdate_DbError"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				return sendSuccess(res,{});
			});
		});
	});
});

router.post('/adpv_stg', function(req, res, next){

	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('stg_no',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()) { 
		logger.error({"r":"adpv_stg","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var catg_id         = req.body.catg_id.trim();
	var stage_num       = 2;
	var tim             = new Date().getTime();

	Category.findOne({
		_id     : catg_id,
		act     : true
	},{
		adpv  : 1,
		adpstg: 1
	},function(err,category){
		if(err){
			logger.error({"r":"adpv_stg","er":err,"p":req.body,"msg":"Category_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!category){
			logger.error({"r":"adpv_stg","p":req.body,"msg":"category_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(!category.adpv){
			logger.error({"r":"adpv_stg","p":req.body,"msg":"category_not_adaptive"});
			return sendError(res,"category_not_adaptive","category_not_adaptive",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		if(category.adpstg == 2){
			logger.error({"r":"adpv_stg","p":req.body,"msg":"category_already_adaptive_stage"});
			return sendError(res,"category_already_adaptive_stage","category_already_adaptive_stage",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		Test.find({
			catg_id  : catg_id,
			act      : true,
			num_ques : {$gt : 0},
			publ     : true,
			stim     : {$lte:tim}
		},{
			adpv   : 1,
			adpstg : 1
		},function(err,tests){
			if(err){
				logger.error({"r":"adpv_stg","er":err,"p":req.body,"msg":"Category_Find_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var total = tests.length;
			if(total == 0){
				logger.error({"r":"adpv_stg","p":req.body,"msg":"category_no_tests_found_for_adaptivity"});
				return sendError(res,"category_no_tests_found_for_adaptivity","category_no_tests_found_for_adaptivity",HttpStatuses.SERVER_SPECIFIC_ERROR);
			}
			for(var i=0; i<total; i++){
				if(!tests[i].adpv){
					logger.error({"r":"adpv_stg","p":req.body,"msg":"category_tests_not_all_adaptive"});
					return sendError(res,"category_tests_not_all_adaptive","category_tests_not_all_adaptive",HttpStatuses.SERVER_SPECIFIC_ERROR);
				}
			}
			for(var j=0; j<total; j++){
				if(tests[j].adpstg != 2){
					logger.error({"r":"adpv_stg","p":req.body,"msg":"category_tests_not_all_adaptive_stage"});
					return sendError(res,"category_tests_not_all_adaptive_stage","category_tests_not_all_adaptive_stage",HttpStatuses.SERVER_SPECIFIC_ERROR);
				}
			}
			Category.update({
				_id : catg_id
			},{
				adpstg : 2
			},function(err,updated_test){
				if(err){
					logger.error({"r":"adpv_stg","er":err,"p":req.body,"msg":"CategoryUpdate_DbError"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				return sendSuccess(res,{});
			});
		});
	});
});

router.post('/r_adpv_stg', function(req, res, next){

	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('stg_no',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()) { 
		logger.error({"r":"r_adpv_stg","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var catg_id         = req.body.catg_id.trim();
	var stage_num       = 2;
	var tim             = new Date().getTime();

	Category.findOne({
		_id     : catg_id,
		act     : true
	},{
		adpv  : 1,
		adpstg: 1
	},function(err,category){
		if(err){
			logger.error({"r":"r_adpv_stg","er":err,"p":req.body,"msg":"Category_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!category){
			logger.error({"r":"r_adpv_stg","p":req.body,"msg":"category_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(!category.adpstg){
			logger.error({"r":"r_adpv_stg","p":req.body,"msg":"category_not_having_any_stage"});
			return sendError(res,"category_not_having_any_stage","category_not_having_any_stage",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		if(category.adpstg != 2){
			logger.error({"r":"r_adpv_stg","p":req.body,"msg":"category_not_having_any_stage_2"});
			return sendError(res,"category_not_having_any_stage_2","category_not_having_any_stage_2",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		Test.update({
			catg_id : catg_id,
			act     : true
		},{
			adpstg : 1
		},{
			multi : true
		},function(err,test_updated){
			if(err){
				logger.error({"r":"r_adpv_stg","er":err,"p":req.body,"msg":"Category_Find_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			Category.update({
				_id : catg_id
			},{
				adpstg : 1
			},function(err,updated_test){
				if(err){
					logger.error({"r":"r_adpv_stg","er":err,"p":req.body,"msg":"CategoryUpdate_DbError"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				return sendSuccess(res,{});
			});
		});
	});
});

router.post('/r_adpv', function(req, res, next){

	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"r_adpv","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var catg_id         = req.body.catg_id.trim();

	Category.findOne({
		_id     : catg_id,
		act     : true
	},{
		adpv : 1
	},function(err,category){
		if(err){
			logger.error({"r":"r_adpv","er":err,"p":req.body,"msg":"Category_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!category){
			logger.error({"r":"r_adpv","p":req.body,"msg":"category_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(!category.adpv || category.adpv != true){
			logger.error({"r":"r_adpv","p":req.body,"msg":"category_already_not_adaptive"});
			return sendError(res,"category_already_not_adaptive","category_already_not_adaptive",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		Category.update({
			_id : catg_id
		},{
			adpv : false
		},function(err,updated_category){
			if(err){
				logger.error({"r":"r_adpv","er":err,"p":req.body,"msg":"CategoryUpdate_DbError"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			Test.update({
				catg_id : catg_id,
				act     : true
			},{
				adpv : false
			},{
				multi : true
			},function(err,updated_test){
				if(err){
					logger.error({"r":"r_adpv","er":err,"p":req.body,"msg":"TestUpdate_DbError"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				return sendSuccess(res,{});
			});
		});
	});
});

/**************** Category Adaptivity Settings Ends **********************/



/**************** Admin Chapter level analysis ******************/

function findTestsBasedAccuracy(test_ids, cb){

}

function findEachTestAccuracy(argument) {
	// body...
}

/***************************************************************/


/********************** Category CRUD Starts *************************/

router.post('/rd', function(req, res, next){
	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"rd","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id    = req.body.u_id.trim();
	var gid        = req.body.g_id.trim();
	var plugin_id  = req.body.pl_id.trim();

	Category.find({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},{
		name : 1,
		desc : 1,
		type : 1,
		adpv : 1,
		adpstg : 1,
		total_num_tests : 1,
		createdAt : 1,
		updatedAt : 1
	},function(err,categories){
		if(err){
			logger.error({"r":"rd","er":err,"p":req.body,"msg":"Category_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		return sendSuccess(res,{categories:categories});
	});
});

router.post('/ad', function(req, res, next){
	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('nm',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('desc',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('adpv',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()) { 
		logger.error({"r":"ad","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	var gid        = req.body.g_id;
	var plugin_id  = req.body.pl_id;
	var name       = (!req.body.nm) ? "" : req.body.nm.trim();
	var type       = 1;
	var desc       = (!req.body.desc) ? "" : req.body.desc.trim();
	var tim        = new Date().getTime();

	if(!name){
		logger.error({"r":"ad","msg":"Name_is_required_to_create_Category","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	var adpv = false;
	if(req.body.adpv && (req.body.adpv == "true" || req.body.adpv == true)){
		adpv = true;
	}
	Category.findOne({},{},{
		sort : {
			catg_no : -1
		}
	},function(err,max_category){
		if(err){
			logger.error({"r":"ad","er":err,"p":req.body,"msg":"Category_Count_Error"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(max_category){
			max_category_num = max_category.catg_no+1;
		} else {
			max_category_num = 1;
		}
		Category.update({
			plugin_id  : plugin_id,
			gid        : gid,
			name       : name,
			catg_no    : max_category_num,
			act        : true
		},{
			$setOnInsert : {
				plugin_id  : plugin_id,
				gid        : gid,
				name       : name,
				catg_no    : max_category_num,
				type       : type,
				act        : true,
				tim        : tim
			}
		},{
			upsert : true,
			setDefaultsOnInsert: true
		},function(err,new_document){
			if(err){
				logger.error({"r":"ad","er":err,"p":req.body,"msg":"Category_Update_Error"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(new_document.nUpserted == 0){
				logger.error({"r":"ad","msg":"Data Not Upserted","p":req.body,"resp":new_document});
				return sendError(res,"Category Already Exists","category_already_exists",HttpStatuses.ALREADY_EXISTS);
			}
			Category.findOne({
				plugin_id : plugin_id,
				gid       : gid,
				catg_no   : max_category_num,
				act       : true
			},{
				catg_no : 1,
				name    : 1,
				type    : 1
			},function(err,category){
				if(err){
					logger.error({"r":"ad","er":err,"p":req.body,"msg":"Category_Find_Error"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!category){
					logger.error({"r":"ad","p":req.body,"msg":"category_not_found","p":{pl_id:plugin_id,gid:gid,catg_no:max_category_num}});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				logger.info({"r":"ad","p":req.body,"msg":"Category_Added_Success","catg":category});
				return sendSuccess(res,{category:category});
			});
		});
	});
});

router.post('/ed', function(req, res, next){
	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('nm',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('desc',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('adpv',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()) { 
		logger.error({"r":"ed","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id      = req.body.u_id.trim();
	var gid          = req.body.g_id.trim();
	var plugin_id    = req.body.pl_id.trim();
	var category_id  = req.body.catg_id.trim();
	var tim          = req.tim;
	var name, desc;
	var toEdit       = {};

	if(req.body.nm){
		name        = req.body.nm.trim();
		toEdit.name = name;
	}
	if(req.body.desc){
		desc        = req.body.desc.trim();
		toEdit.desc = desc;
	}

	if(!name && !desc){
		logger.error({"r":"ed","msg":"NO_Params_for_Data_Modification","p":req.body});
		return sendError(res,"No Data In Input To Edit","no_data_to_edit",HttpStatuses.NO_INPUT_DATA);
	}
	if("adpv" in req.body){
		if(req.body.adpv == "true" || req.body.adpv == true){
			toEdit.adpv = true;
		} else {
			toEdit.adpv = false;
		}
	}

	Category.findOne({
		_id       : category_id,
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,category){
		if(err){
			logger.error({"r":"ed","er":err,"p":req.body,"msg":"Category_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!category){
			logger.error({"r":"ed","p":req.body,"msg":"category_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		checkIfCatgQuizzesAreAdaptive(category_id, req.body.adpv, function(err, is_adaptive){
			if(err){
				logger.error({"r":"ed","er":err,"p":req.body,"msg":"checkIfCatgQuizzesAreAdaptive_Error"});
				return sendError(res,"category_tests_not_adaptive","category_tests_not_adaptive",HttpStatuses.SERVER_SPECIFIC_ERROR);
			}
			if(is_adaptive == "true" || is_adaptive == true){
				toEdit.adpv = true;
			}
			Category.update({
				_id : category_id
			},toEdit,function(err,updated_category){
				if(err){
					logger.error({"r":"ed","er":err,"p":req.body,"msg":"Category_Find_Error"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if("adpv" in req.body){
					if(toEdit.adpv == false || toEdit.adpv == "false"){
						removeCategoryTestsAdaptivity(category_id, function(err, resp){
							if(err){
								logger.error({"r":"ed","er":err,"p":req.body,"msg":"removeCategoryTestsAdaptivity_Error"});
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}
							logger.info({"r":"ed","p":req.body,"msg":"success"});
							return sendSuccess(res,{});
						});
					}
				} else {
					return sendSuccess(res,{});
				}
			});
		});
	});
});

/********************** Category CRUD Ends *************************/

function removeCategoryTestsAdaptivity(category_id, cb){
	Test.update({
		catg_id : category_id,
		act     : true
	},{
		adpv : false
	},{
		multi : true
	},function(err,test_edited){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
}

function checkIfCatgQuizzesAreAdaptive(category_id, is_adaptive, cb){
	if(!is_adaptive){
		return cb(null,false);
	}
	Test.find({
		catg_id : category_id,
		act     : true
	},{
		adpv : 1,
		level: 1
	},function(err,tests){
		if(err){
			return cb(err,null);
		}
		if(tests.length == 0){
			return cb(null,false);
		}
		var total = tests.length;
		var succs = 0;
		for(var i=0; i<total; i++){
			if(tests[i].adpv && tests[i].level){
				succs++;
			}
		}
		if(succs == total){
			return cb(null, true);
		}
		return cb("Oops_all_quizzes_are_not_adaptive",null);
	});
}

module.exports = router;