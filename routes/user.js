var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    								= require('jsonwebtoken');

var configs					      = require('../configs/configs.js');
var HttpStatuses					= require('../global/http_api_return_codes.js');
var ApiReturnHandlers			= require('../global/api_return_handlers.js');
var errorCodes 						= require('../global/error_codes.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var AuthSecurity      		= require('../security/auth_tokens.js');
var RootServerApi      		= require('../server/apicalls.js');

require('../models/users.js');
require('../models/usertests.js');
require('../models/usertokens.js');
require('../models/groupstats.js');
require('../models/pluginpaidinfos.js');
var User 									= mongoose.model('User');
var UserTest 							= mongoose.model('UserTest');
var UserToken 						= mongoose.model('UserToken');
var GroupStats		        = mongoose.model('GroupStats');
var PluginPaidInfo 				= mongoose.model('PluginPaidInfo');

var sendError 						= ApiReturnHandlers.sendError;
var sendSuccess 					= ApiReturnHandlers.sendSuccess;
var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_USER);

var DEFAULT_NUM_LEADERS_IN_LEADERBOARD = configs.DEFAULT_NUM_LEADERS_IN_LEADERBOARD;
var CODE_KERNEL_GROUPS                 = configs.CODE_KERNEL_GROUPS;

router.post('/register', function(req, res, next){
	req.checkBody('user_idnt',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"register","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_identifier   = req.body.user_idnt.trim();
	var tim         			= new Date().getTime();

	User.findOne({
		identifier : user_identifier,
		act        : true
	},function(err,user){
		if(err){
			logger.error({"r":"register","msg":"user_find_failed","er":err,"p":req.body});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(user){
			logger.info({"r":"register","msg":"user_already_exists","p":req.body});
			return sendSuccess(res,{ id : user._id, if_new : 0, srv_id : user.server_id, gid : user.gid, plugin_id : user.plugin_id});
		}
		RootServerApi.verifyIdentifier(user_identifier, function(err,resp){
			if(err){
				logger.error({"r":"register","msg":"rootServer_verifyIdentifier_err","er":err,"p":req.body});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!resp.success){
				logger.error({"r":"register","msg":"rootServer_verifyIdentifier_failed","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			var data = resp.data;
			User.update({
				identifier : user_identifier,
				act        : true
			},{
				$setOnInsert : {
					act  : true,
					identifier : user_identifier,
					server_id  : data.srv_id,
					name       : data.name,
					img_url    : data.ppic,
					role       : data.role,
					gid        : data.gid,
					plugin_id  : data.plugin_id
				}
			},{
				upsert : true,
				setDefaultsOnInsert: true
			},function(err,created_user){
				if(err){
					logger.error({"r":"register","msg":"user_token_update_failed","er":err,"p":req.body});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				User.findOne({
					identifier : user_identifier,
					act        : true
				},function(err,newly_created_user){
					if(err){
						logger.error({"r":"register","msg":"user_find_failed_after_create","er":err,"p":req.body});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!newly_created_user){
						logger.error({"r":"register","msg":"user_not_found","p":req.body});
						return sendError(res,"Invalid Parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
					}
					return sendSuccess(res,{id : newly_created_user._id, if_new : 1, srv_id : data.srv_id, gid : data.gid, plugin_id : data.plugin_id});
				});
			});
		})
	});
});

router.post('/init', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_idnt',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('srv_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"init","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id     			= req.body.user_id.trim();
	var server_id     		= req.body.srv_id.trim();
	var group_id     		  = req.body.g_id.trim();
	var user_identifier   = req.body.user_idnt.trim();
	var plugin_id         = req.body.pl_id.trim();
	var tim         			= new Date().getTime();

	User.findOne({
		_id        : user_id,
		identifier : user_identifier,
		plugin_id  : plugin_id,
		gid        : group_id,
		act        : true,
	},function(err,user){
		if(err){
			logger.error({"r":"init","msg":"user_find_failed","er":err,"p":req.body});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!user){
			logger.error({"r":"init","msg":"user_not_found","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		PluginPaidInfo.findOne({
			plugin_id : plugin_id,
			gid       : group_id,
			act       : true
		},function(err,pluginIsPaid){
			if(err){
				logger.error({"r":"init","msg":"PluginInfo_find_failed","er":err,"p":req.body});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var object_for_at = {
				id1 : user_id,
				id2 : server_id,
				id3 : plugin_id,
				id4 : user.gid
			};
			var at;
			if(pluginIsPaid){
				console.log('inside pluginIsPaid');
				AuthSecurity.getSubscriptionAT(plugin_id, object_for_at, user_identifier, group_id, tim, function(err, at){
					if(err){
						logger.error({"r":"init","msg":"user_token_update_failed","er":err,"p":req.body});
						console.trace(err);
						if(err == "USER_SUBSCRIPTION_EXPIRED"){
							return sendError(res,"USER_SUBSCRIPTION_EXPIRED","user_subscription_ended",HttpStatuses.USER_SUBSCRIPTION_EXPIRED);
						}
						if(err == "USER_SUBSCRIPTION_REQUIRED"){
							return sendError(res,"USER_SUBSCRIPTION_EXPIRED","user_subscription_required",HttpStatuses.USER_SUBSCRIPTION_REQUIRED);
						}
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					logger.info({"r":"init","msg":"AuthSecurity_getSubscriptionAT_Success","p":req.body,at:at});
					return sendSuccess(res,{at : at});
				});
			} else {
				console.log('outside pluginIsPaid');
				at = AuthSecurity.getAT(object_for_at);
				updateUserToken(user_id, plugin_id, at, function(err, resp){
					if(err){
						logger.error({"r":"init","msg":"user_token_update_failed","er":err,"p":req.body});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					logger.info({"r":"init","msg":"AuthSecurity_getAT_Success","p":req.body,at:at});
					return sendSuccess(res,{at : at});
				});
			}
		});
	});
});

//route middleware to verify if plugin in group is paid
router.use(function(req, res, next){
	var tim           = new Date().getTime();
	var plugin_id     = req.body.pl_id;
	var token         = req.body.tokn;
	var gid           = req.body.g_id;
	var user_id       = req.body.user_id;

	if(!token || token == 'null' || token === "undefined"){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body,"tkn":token});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!gid){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"gid_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","gid_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!plugin_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"plugin_id_missing","p":req.body});
	  return sendError(res,"Plugin Id Not Found","plugin_id_missing",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			req.private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			req.private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
		req.tim = tim;
	  next();
	});
});

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
  jwt.verify(req.body.tokn, req.private_key, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != req.body.user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != req.body.pl_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_app_id","p":req.body});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id4 != req.body.g_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body});
	  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    next();
  });
});

router.post('/u_stats', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"u_stats","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id     = req.body.user_id.trim();
	var plugin_id   = req.body.pl_id;
	var tim         = new Date().getTime();

	User.findOne({
		_id       : user_id,
		act       : true
	},{
		name      : 1,
		img_url   : 1,
		attemtps  : 1,
		submits   : 1,
		score     : 1,
		marks     : 1,
		attempts  : 1,
		rank      : 1,
		all_correct_count : 1,
		role      : 1
	},function(err, user_stats){
		if(err){
			logger.error({"r":"u_stats","er":err,"p":req.body});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!user_stats){
			logger.error({"r":"u_stats","msg":"User Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		return sendSuccess(res,{user_stats:user_stats});
	});
});

router.post('/u_stats_g', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"u_stats_g","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id     = req.body.user_id.trim();
	var gid         = req.body.g_id.trim();
	var plugin_id   = req.body.pl_id.trim();
	var tim         = req.tim;
	var final_resp  = {
		user_id  : user_id
	};

	GroupStats.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		user_id   : user_id
	},{
		attemtps  : 1,
		score     : 1,
		submits   : 1
	},function(err, user_stats){
		if(err){
			logger.error({"r":"u_stats_g","er":err,"p":req.body,"msg":"GroupStats_findOne_err"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!user_stats){
			final_resp.attemtps = 0;
			final_resp.submits  = 0;
			final_resp.score    = 0;
			final_resp.rank     = 'NA';
			final_resp.lscore   = 'NA';
			final_resp.tmarks   = 'NA';
			logger.warn({"r":"u_stats_g","msg":"User Group Stats Not Found","p":req.body});
			return sendSuccess(res,{stats : final_resp});
		} else {
			final_resp.attemtps = user_stats.attemtps;
			final_resp.submits  = user_stats.submits;
			final_resp.score    = user_stats.score;
		}
		var toDo = 2;
		var done = 0;
		GroupStats.count({
			plugin_id : plugin_id,
			gid       : gid,
			score     : { $gt : user_stats.score }
		},function(err, rank){
			if(err){
				logger.error({"r":"u_stats_g","er":err,"p":req.body,"msg":"GroupStats_Count_err"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			done++;
			final_resp.rank = rank+1;
			if(done == toDo){
				return sendSuccess(res,{stats : final_resp});
			}
		});
		UserTest.find({
			plugin_id : plugin_id,
			user_id   : user_id
		},{
			marks_obtained : 1,
			total_marks    : 1,
			end_time       : 1
		},{
			sort : {
	      end_time : -1 
	    },
	    limit  : 1
		},function(err, usertest){
			if(err){
				logger.error({"r":"u_stats_g","er":err,"p":req.body,"msg":"Usertest_find_err"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			done++;
			if(usertest.length == 1){
				final_resp.lscore = usertest[0].marks_obtained ? usertest[0].marks_obtained : 'NA';
				final_resp.tmarks = usertest[0].total_marks;
			} else {
				final_resp.lscore = 'NA';
				final_resp.tmarks = 'NA';
			}
			if(done == toDo){
				return sendSuccess(res,{stats : final_resp});
			}
		});
	});
});

router.post('/ldb', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"ldb","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id     = req.body.user_id.trim();
	var plugin_id   = req.body.pl_id.trim();
	var ipp         = DEFAULT_NUM_LEADERS_IN_LEADERBOARD;
	var tim         = new Date().getTime();

	User.find({
		plugin_id : plugin_id,
		score     : { $gt : 0 }
	},{
		name      : 1,
		img_url   : 1,
		attemtps  : 1,
		submits   : 1,
		score     : 1,
		marks     : 1,
		attempts  : 1,
		rank      : 1,
		all_correct_count : 1,
		role      : 1
	},{
    sort : {
      score : -1 
    },
    limit  : ipp
	},function(err, leaders){
		if(err){
			logger.error({"r":"ldb","er":err,"p":req.body});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		return sendSuccess(res,{leaders:leaders});
	});
});


router.post('/ldb_g', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"ldb_g","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id     = req.body.user_id.trim();
	var gid         = req.body.g_id.trim();
	var plugin_id   = req.body.pl_id.trim();
	var ipp         = DEFAULT_NUM_LEADERS_IN_LEADERBOARD;
	var tim         = new Date().getTime();

	GroupStats.find({
		plugin_id : plugin_id,
		gid       : gid,
		score     : { $gt : 0 }
	},{
		user_id   : 1,
		gid       : 1,
		attemtps  : 1,
		submits   : 1,
		score     : 1,
		marks     : 1,
		all_correct_count : 1,
	},{
    sort : {
      score : -1 
    },
    limit  : ipp
	},function(err, leaders){
		if(err){
			logger.error({"r":"ldb_g","er":err,"p":req.body,"er":err});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(leaders.length == 0){
			return sendSuccess(res,{leaders:[],info:[]});
		}
		var total = leaders.length;
		var userids = [];
		var id;
		for(var i=0; i<total; i++){
			id = leaders[i].user_id+'';
			if(userids.indexOf(id) < 0)
				userids.push(id);
		}
		User.find({
			_id : { $in : userids}
		},{
			name      : 1,
		  img_url   : 1,
		  role      : 1
		},function(err,users){
			if(err){
				logger.error({"r":"ldb_g","er":err,"p":req.body,"er":err});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var infoByUserId = {};
			var user_total = users.length;
			for(var i=0; i<user_total; i++){
				infoByUserId[users[i]._id+''] = users[i];
			}
			var if_code_kernel = (CODE_KERNEL_GROUPS && CODE_KERNEL_GROUPS.indexOf(gid) >= 0) ? true : false;
			return sendSuccess(res,{leaders:leaders,info:infoByUserId,ick:if_code_kernel});
		});
	});
});

function updateUserToken(user_id, plugin_id, at, cb){
	UserToken.update({
		plugin_id : plugin_id,
		user_id   : user_id,
		act       : true
	},{
		act : false
	},{
		multi : true
	},function(err,prev_updated){
		if(err){
			return cb(err,null);
		}
		var new_user_token = new UserToken({
			user_id     : user_id,
			plugin_id   : plugin_id,
			act 		    : true,
			xcess_token : at
		});
		new_user_token.save(function(err, new_token){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}

module.exports = router;