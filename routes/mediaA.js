var express 					    = require('express');
var mongoose 					    = require('mongoose');
var jwt    						    = require('jsonwebtoken');

var configs					      = require('../configs/configs.js');
var HttpStatuses					= require('../global/http_api_return_codes.js');
var ApiReturnHandlers			= require('../global/api_return_handlers.js');
var errorCodes 						= require('../global/error_codes.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var RootServerApi      		= require('../server/apicalls.js');
var AwsModule             = require('../aws/aws.js');
var AwsBuckets            = require('../aws/buckets.js');

require('../models/users.js');
require('../models/pluginpaidinfos.js');
var PluginPaidInfo 				= mongoose.model('PluginPaidInfo');
var User                  = mongoose.model('User');

var sendError 						= ApiReturnHandlers.sendError;
var sendSuccess 					= ApiReturnHandlers.sendSuccess;
var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_MEDIA_ADMIN);

//route middleware to verify the jwt token for all user whether they are group admin
router.use(function(req, res, next){
	var user_id   = req.body.u_id;
	var gid       = req.body.g_id;
	var plugin_id = req.body.pl_id;
	var private_key;
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body,"r":"2nd_middleware","tkn":req.body.tokn});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
	  jwt.verify(req.body.tokn, private_key, function(err, decoded) {      
	    if(err){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err,"tkn":req.body.tokn});
	      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
	    } 
	    if(decoded.id1 != user_id){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_userid","p":req.body,"tkn":req.body.tokn});
		  	return sendError(res,"Access without valid user id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
	    }
	    if(decoded.id4 != gid){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body,"tkn":req.body.tokn});
		  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
	    }
	    User.findOne({
	    	_id : user_id,
				act : true,
			},function(err,user){
				if(err){
					logger.error({"url":req.originalUrl,"msg":"user_find_failed","er":err,"p":req.body,"tkn":req.body.tokn});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!user){
					logger.info({"url":req.originalUrl,"msg":"user_already_exists","p":req.body,"tkn":req.body.tokn});
					return sendSuccess(res,{ id : user._id, if_new : 0, srv_id : user.server_id, gid : user.gid });
				}
				RootServerApi.checkIfAdmin(user.identifier, function(err,resp){
					if(err){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_err","er":err,"p":req.body,"tkn":req.body.tokn});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!resp.success){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_failed","p":req.body,"tkn":req.body.tokn});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!resp.is_admin){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_UserIsNotGroupAdmin","p":req.body,"tkn":req.body.tokn});
						return sendError(res,"unauthorised","unauthorised",HttpStatuses.UNAUTHORIZED);
					}
			    next();
				});
			});
	  });
	});
});

router.post('/tmediaupload', function(req, res, next) {
	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('m_od',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('f_tp',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('f_nm',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('prps',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()){ 
		logger.error({"r":"tmediaupload","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var mode            = req.body.m_od;
  var file_type       = req.body.f_tp;
  var file_name       = req.body.f_nm;
  var purpose         = req.body.prps;

  if(!AwsModule.isValidPurpose(purpose)){
    logger.error({"r":"tmediaupload","msg":"media_purpose_invalid","p":req.body});
    return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  }
  var bucket = AwsBuckets.QUES_PIC;
  if(!AwsModule.isValidMode(file_type, mode)){
    logger.error({"r":"tmediaupload","msg":"isValidMode_error","p":req.body});
    return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  }
  if(!AwsModule.isValidFile(file_type, file_name)){
    logger.error({"r":"tmediaupload","msg":"isValidCpicFile_error","p":req.body});
    return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  }
  var credentials = AwsModule.getCredentials(bucket, file_name, file_type, mode);
  return sendSuccess(res, {data:credentials});
});

module.exports = router;