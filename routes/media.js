var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    							  = require('jsonwebtoken');

var configs					      = require('../configs/configs.js');
var HttpStatuses					= require('../global/http_api_return_codes.js');
var ApiReturnHandlers			= require('../global/api_return_handlers.js');
var errorCodes 						= require('../global/error_codes.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var AwsModule             = require('../aws/aws.js');
var AwsBuckets            = require('../aws/buckets.js');
var AwsModes              = require('../aws/file_modes.js');

require('../models/users.js');
require('../models/pluginpaidinfos.js');
var User                  = mongoose.model('User');
var PluginPaidInfo 				= mongoose.model('PluginPaidInfo');

var sendError 						= ApiReturnHandlers.sendError;
var sendSuccess 					= ApiReturnHandlers.sendSuccess;
var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_MEDIA);

//route middleware to verify if plugin in group is paid
router.use(function(req, res, next){
	var tim         = new Date().getTime();
	var plugin_id   = req.body.pl_id;
	var token       = req.body.tokn;
	var gid         = req.body.g_id;
	var user_id     = req.body.user_id;

	if(!token || token == 'null' || token === "undefined"){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body,"tkn":token});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!gid){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"gid_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","gid_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!plugin_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"plugin_id_missing","p":req.body});
	  return sendError(res,"Plugin Id Not Found","plugin_id_missing",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			req.private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			req.private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
		req.tim = tim;
	  next();
	});
});

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
  jwt.verify(req.body.tokn, req.private_key, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != req.body.user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != req.body.pl_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_app_id","p":req.body});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id4 != req.body.g_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body});
	  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    next();
  });
});

router.post('/gettmedia', function(req, res, next) {
	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('f_tp',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('f_nm',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('prps',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()){ 
		logger.error({"r":"gettmedia","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var mode            = req.body.m_od;
  var file_type       = req.body.f_tp;
  var file_name       = req.body.f_nm;
  var purpose         = req.body.prps;

  if(!AwsModule.isValidPurpose(purpose)){
    logger.error({"r":"gettmedia","msg":"media_purpose_invalid","p":req.body});
    return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  }
  var bucket = AwsBuckets.QUES_PIC;
  if(!AwsModule.isValidFile(file_type, file_name)){
    logger.error({"r":"gettmedia","msg":"isValidCpicFile_error","p":req.body});
    return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  }
  AwsModule.getSignedUrlS3Download(bucket, file_name, file_type, function(err, data){ 
    if(err){ 
    	logger.error({"r":"gettmedia","msg":"getSignedUrlS3_error","p":req.body,"err":err});
    	return sendError(res,"server_error","server_error"); 
    }
    return sendSuccess(res, data);
  });
});

router.post('/getqmedia', function(req, res, next) {
	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ques_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()){ 
		logger.error({"r":"gettmedia","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var user_id       = req.body.user_id.trim();
	var test_id       = req.body.test_id.trim();
	var ques_id       = req.body.ques_id.trim();

	UserTest.findOne({
		user_id  : user_id,
		test_id  : test_id,
		act   	 : true,
	},function(err,usertest){
		if(err){
			logger.error({"r":"gettmedia","er":err,"p":req.body,"er":err,"msg":"UserTestFind_DbError"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!usertest){
			logger.error({"r":"gettmedia","msg":"usertest_not_found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		TestQuestion.findOne({
			test_id : test_id,
			ques_id : ques_id,
			act     : true
		},function(err,testquestion){
			if(err){
				logger.error({"r":"gettmedia","er":err,"p":req.body,"er":err,"msg":"TestQuestion_find_DbError"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!testquestion){
				logger.error({"r":"gettmedia","msg":"User testquestion_not_found","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			Question.findOne({
				_id : ques_id
			},function(err,question){
				if(err){
					logger.error({"r":"gettmedia","er":err,"p":req.body,"er":err,"msg":"Question_find_DbError"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!question){
					logger.error({"r":"gettmedia","msg":"question_not_found","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				}
				if(!question.img_arr || question.img_arr.length == 0){
					logger.warn({"r":"gettmedia","msg":"no_images_found","p":req.body,"que":question});
					return sendError(res,"No Media Found","no_media_found",HttpStatuses.NO_DATA);
				}
				getSignedUrlForAllMedia(question.img_arr, function(err,urlsArrayObject){
					if(err){
						logger.error({"r":"gettmedia","er":err,"p":req.body,"er":err,"msg":"UserTest_find_DbError"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					return sendSuccess(res,{ques_id:ques_id,img_arr:urlsArrayObject});
				});
			});
		});
	});
});

function getSignedUrlForAllMedia(image_array, cb){
	var toDo   = image_array.length;
	var done   = 0;
	var bucket = AwsBuckets.QUES_PIC;
	var mode   = AwsModes.IMAGE;
	var resultArray = [];
	for(var i=0; i<toDo; i++){
		getAwsUrl(bucket, image_array[i].fnm, image_array[i].ftp, mode, function(err,resp){
			if(err){
				return cb(err,null);
			}
			resultArray[resp.file_name] = resp.signedUrl;
			done++;
			if(done == toDo){
				return cb(null,resultArray);
			}
		});
	}
}

function getAwsUrl(bucket,file_name, file_type, mode, cb){
	AwsModule.getSignedUrlS3Upload(bucket, file_name, file_type, mode, function(err,signedUrl){
		if(err){
			return cb(err,null);
		}
		return cb(null,{file_name:signedUrl});
	});
}

module.exports = router;