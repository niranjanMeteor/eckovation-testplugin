const express 							= require('express');
const mongoose 							= require('mongoose');
const jwt    							  = require('jsonwebtoken');

const configs					      = require('../configs/configs.js');
const catConfigs					  = require('../configs/catconfigs.js');
const HttpStatuses					= require('../global/http_api_return_codes.js');
const ApiReturnHandlers			= require('../global/api_return_handlers.js');
const errorCodes 						= require('../global/error_codes.js');
const log4jsLogger  				= require('../loggers/log4js_module.js');
const log4jsConfigs  			  = require('../loggers/log4js_configs.js');
const RootServerApi      		= require('../server/apicalls.js');

require('../models/categories.js');
require('../models/tests.js');
require('../models/testquestions.js');
require('../models/questions.js');
require('../models/users.js');
require('../models/pluginpaidinfos.js');
require('../models/tags.js');
require('../models/testtags.js');
const PluginPaidInfo 				= mongoose.model('PluginPaidInfo');
const User                  = mongoose.model('User');
const Category 							= mongoose.model('Category');
const Test 									= mongoose.model('Test');
const TestQuestion					= mongoose.model('TestQuestion');
const Question					    = mongoose.model('Question');
const Tag 								  = mongoose.model('Tag');
const TestTag 					    = mongoose.model('TestTag');

var sendError 						  = ApiReturnHandlers.sendError;
var sendSuccess 					  = ApiReturnHandlers.sendSuccess;
var router 								  = express.Router();
var logger        				  = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_TESTQUESTION_ADMIN);

//route middleware to verify the jwt token for all user whether they are group admin
router.use(function(req, res, next){
	var user_id   = req.body.u_id;
	var gid       = req.body.g_id;
	var plugin_id = req.body.pl_id;
	var private_key;
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body,"r":"2nd_middleware","tkn":req.body.tokn});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
	  jwt.verify(req.body.tokn, private_key, function(err, decoded) {      
	    if(err){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err,"tkn":req.body.tokn});
	      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
	    } 
	    if(decoded.id1 != user_id){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_userid","p":req.body,"tkn":req.body.tokn});
		  	return sendError(res,"Access without valid user id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
	    }
	    if(decoded.id4 != gid){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body,"tkn":req.body.tokn});
		  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
	    }
	    User.findOne({
	    	_id : user_id,
				act : true,
			},function(err,user){
				if(err){
					logger.error({"url":req.originalUrl,"msg":"user_find_failed","er":err,"p":req.body,"tkn":req.body.tokn});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!user){
					logger.info({"url":req.originalUrl,"msg":"user_already_exists","p":req.body,"tkn":req.body.tokn});
					return sendSuccess(res,{ id : user._id, if_new : 0, srv_id : user.server_id, gid : user.gid });
				}
				RootServerApi.checkIfAdmin(user.identifier, function(err,resp){
					if(err){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_err","er":err,"p":req.body,"tkn":req.body.tokn});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!resp.success){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_failed","p":req.body,"tkn":req.body.tokn});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!resp.is_admin){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_UserIsNotGroupAdmin","p":req.body,"tkn":req.body.tokn});
						return sendError(res,"unauthorised","unauthorised",HttpStatuses.UNAUTHORIZED);
					}
			    next();
				});
			});
	  });
	});
});

router.post('/rd', function(req, res, next){

	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"ad","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var gid             = req.body.g_id;
	var plugin_id       = req.body.pl_id;
	var category_id     = req.body.catg_id;
	var test_id         = req.body.test_id;

	Category.findOne({
		_id       : category_id,
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,category){
		if(err){
			logger.error({"r":"ad","er":err,"p":req.body,"msg":"Category_Find_Error"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!category){
			logger.error({"r":"ad","p":req.body,"msg":"category_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		Test.findOne({
			_id     : test_id,
			catg_id : category_id,
			act     : true
		},{
			name                : 1,
			catg_id             : 1,
			type                : 1,
			timed               : 1,
			num_ques            : 1,
			mpq                 : 1,
			ssol                : 1,
			ineg                : 1,
			negm                : 1,
			tags                : 1
		},function(err,test){
			if(err){
				logger.error({"r":"ad","er":err,"p":req.body,"msg":"Test_Find_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!test){
				logger.error({"r":"ad","p":req.body,"msg":"test_not_found"});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			TestQuestion.find({
				test_id : test_id,
				act     : true
			},{
				ques_id   : 1,
				marks     : 1,
				ineg      : 1,
				negm      : 1,
				tgid      : 1,
				createdAt : 1,
				updatedAt : 1,
				tags      : 1,
				lv        : 1,
				_id       : 0
			},{
				sort : {
					key : 1
				}
			},function(err,testquestions){
				if(err){
					logger.error({"r":"ad","er":err,"p":req.body,"msg":"TestQuestion_Find_Error"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				getTestTags(test, function(err,tagById){
					if(err){
						logger.error({"r":"ad","er":err,"p":req.body,"msg":"getTestTags_Error"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					var total = testquestions.length;
					if(total == 0){
						logger.info({"r":"ad","p":req.body,"msg":"TestQuestions_Empty"});
						return sendSuccess(res,{questions:[],test:test,tags:tagById});
					}
					var qids = [];
					for(var i=0; i<total; i++){
						qids.push(testquestions[i].ques_id+'');
					}
					Question.find({
						_id : { $in : qids }
					},{
						type        : 1,
						level       : 1,
						text        : 1,
						ans_options : 1,
						correct_ans : 1,
						image_url   : 1,
						audio_url   : 1,
						hint_text   : 1,
						tag         : 1,
						expl        : 1,
						createdAt   : 1,
					},function(err,questions){
						if(err){
							logger.error({"r":"ad","er":err,"p":req.body,"msg":"Question_Find_Error"});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						return sendSuccess(res,{
							questions     : questions,
							testquestions : testquestions,
							test          : test,
							tags          : tagById
						});
					});
				});
			});
		});
	});
});

router.post('/ad', function(req, res, next){

	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ques_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('lv',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('ineg',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('negm',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('marks',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('code',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('tags',errorCodes.invalid_parameters[1]).optional();
	
	if(req.validationErrors()) { 
		logger.error({"r":"ad","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var gid             = req.body.g_id;
	var plugin_id       = req.body.pl_id;
	var category_id     = req.body.catg_id;
	var test_id         = req.body.test_id;
	var ques_id         = req.body.ques_id;
	var ineg            = (req.body.ineg) ? req.body.ineg : false;
	var negm            = (req.body.negm) ? req.body.negm : null;
	var level           =  1;
	var marks;
	
	if(ineg && ineg == true && (isNaN(negm) || negm <= 0)){
		logger.error({"r":"ad","msg":"negm_value_invalid","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	Test.findOne({
		_id     : test_id,
		catg_id : category_id,
		act     : true
	},function(err,test){
		if(err){
			logger.error({"r":"ad","er":err,"p":req.body,"msg":"Test_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"ad","p":req.body,"msg":"test_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(!test.ineg && ineg && ineg == true){
			logger.error({"r":"ad","p":req.body,"msg":"test_not_enable_with_negative_marking"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(test.ineg && !test.negm && !negm){
			logger.error({"r":"ad","p":req.body,"msg":"ineg_sent_but_no_negm_for_test_with_no_default_negm"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		// if(!req.body.tags || typeof(req.body.tags) != 'object' || req.body.tags.length == 0){
		// 	logger.error({"r":"ad","p":req.body,"msg":"tags_are_compulsary_for_adding_question"});
		// 	return sendError(res,"tags_are_compulsary_for_adding_question","tags_are_compulsary_for_adding_question",HttpStatuses.SERVER_SPECIFIC_ERROR);
		// }
		// if(!test.tags){
		// 	logger.error({"r":"ad","p":req.body,"msg":"test_must_have_tags_now_onwards"});
		// 	return sendError(res,"test_must_have_tags_now_onwards","test_must_have_tags_now_onwards",HttpStatuses.SERVER_SPECIFIC_ERROR);
		// }
		// if(test.tags.length < configs.MINIMUM_TAGS_PER_TEST_FOR_ADAPTIVITY){
		// 	logger.error({"r":"ad","p":req.body,"msg":"test_should_have_minimum_tags"});
		// 	return sendError(res,"test_should_have_minimum_tags","test_should_have_minimum_tags",HttpStatuses.SERVER_SPECIFIC_ERROR);
		// }
		if("marks" in req.body){
			marks = req.body.marks;
			if(isNaN(marks) || marks <= 0){
				marks = test.mpq;
			}
		} else {
			marks = test.mpq;
		}
		if(req.body.lv && isNaN(req.body.lv)){
			logger.error({"r":"ad","p":req.body,"msg":"level_not_a_number"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		Question.findOne({
			_id : ques_id
		},function(err,question){
			if(err){
				logger.error({"r":"ad","er":err,"p":req.body,"msg":"Question_Find_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!question){
				logger.error({"r":"ad","p":req.body,"msg":"question_not_found"});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			// var que_level = question.level;
			// if(que_level < 1 || que_level > test.mxqlv){
			// 	logger.error({"r":"ad","p":req.body,"msg":"level_is_not_valid"});
			// 	return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			// }
			TestQuestion.count({
				test_id : test_id
			},function(err,count){
				if(err){
					logger.error({"r":"ad","er":err,"p":req.body,"msg":"Testquestion_Count_Error"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				var new_test_question_obj = {
					test_id : test_id,
					ques_id : ques_id,
					act     : true,
					key     : count+1,
					marks   : marks,
					lv      : question.level
				};
				if(ineg && ineg == true){
					new_test_question_obj.ineg = true;
					if(negm && !isNaN(negm) && negm >= 0){
						new_test_question_obj.negm = parseFloat(negm);
					} else {
						new_test_question_obj.negm = parseFloat(test.negm);
					}
				}
				if("code" in req.body){
					new_test_question_obj.code = getTestTypeCode(test_id, req.body.code);
				}
				// if(req.body.tags && typeof(req.body.tags) != "object" && req.body.tags.length == 0){
				// 	logger.error({"r":"ad","p":req.body,"msg":"tags_found_in_req_body_invalid"});
				// 	return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				// }
				checkTestTags(test_id, req.body, test, function(err, resp){
					if(err){
						logger.error({"r":"ad","er":err,"p":req.body,"msg":"checkTestTags_Error"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(resp.exists){
					  if(!resp.match){
					  	logger.error({"r":"ad","p":req.body,"msg":"testquestion_tags_are_not_matching_with_test"});
							return sendError(res,"testquestion_tags_are_not_matching_with_test","testquestion_tags_are_not_matching_with_test",HttpStatuses.SERVER_ERROR);
					  }
					  new_test_question_obj.tags = req.body.tags;
					}
					var new_test_question = new TestQuestion(new_test_question_obj);
					new_test_question.save(function(err,new_test_question_created){
						if(err){
							logger.error({"r":"ad","er":err,"p":req.body,"msg":"Testquestion_Save_Error"});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						Test.update({
							_id : test_id
						},{
							$inc : {
								num_ques    : 1,
								total_marks : marks
							}
						},function(err,test_updated){
							if(err){
								logger.error({"r":"ad","er":err,"p":req.body,"msg":"Test_UpdateInc_Error"});
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}
							if(test.adpstg != 2){
								logger.info({"r":"ad",k:new_test_question_obj.key,q:ques_id,t:test_id,"msg":"success_without_adpstg2_disable"});
								return sendSuccess(res,{});
							}
							Test.update({
								_id : test_id
							},{
								adpstg : 1
							},function(err,test_updated){
								if(err){
									logger.error({"r":"ad","er":err,"p":req.body,"msg":"Test_Update_Error"});
									return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
								}
								// logger.info({"r":"ad","p":req.body,"msg":"TestQuestion_Added_Success","tq":new_test_question});
								logger.info({"r":"ad",k:new_test_question_obj.key,q:ques_id,t:test_id,"msg":"success_with_adpstg2_disable"});
								return sendSuccess(res,{});
							});
						});
					});
				});
			});
		});
	});
});

router.post('/ed', function(req, res, next){
	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ques_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('lv',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('ineg',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('negm',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('marks',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('code',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('tags',errorCodes.invalid_parameters[1]).optional();
	
	if(req.validationErrors()) { 
		logger.error({"r":"ed","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var category_id     = req.body.catg_id;
	var test_id         = req.body.test_id;
	var ques_id         = req.body.ques_id;
	var negm            = (req.body.negm);
	var toEdit          = {};
	var marks;

	Test.findOne({
		_id     : test_id,
		catg_id : category_id,
		act     : true
	},function(err,test){
		if(err){
			logger.error({"r":"ed","er":err,"p":req.body,"msg":"Test_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"ed","p":req.body,"msg":"test_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		
		TestQuestion.findOne({
			test_id : test_id,
			ques_id : ques_id,
			act     : true
		},function(err,testquestion){
			if(err){
				logger.error({"r":"ed","er":err,"p":req.body,"msg":"TestQuestion_Find_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!testquestion){
				logger.error({"r":"ed","p":req.body,"msg":"testquestion_not_found"});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			// if(test.adpstg && test.adpstg == 2){
			// 	if(toEdit.mxqlv && test.mxqlv != toEdit.mxqlv){
			// 		toEdit.adpstg = 1;
			// 	}
			// }
			Question.findOne({
				_id : ques_id
			},function(err,question){
				if(err){
					logger.error({"r":"ed","er":err,"p":req.body,"msg":"Question_Find_Error"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!question){
					logger.error({"r":"ed","p":req.body,"msg":"question_not_found"});
					return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				}
				toEdit.lv = question.level;
				if("ineg" in req.body){
					if(req.body.ineg == true){
						if(negm && !isNaN(negm) && negm > 0){
							toEdit.negm = parseFloat(negm);
						} else {
							toEdit.negm = parseFloat(test.negm);
						}
						toEdit.ineg = true;
					} else if(req.body.ineg == false){
						toEdit.ineg = false;
					}
				}
				if(toEdit.ineg && !test.negm && (!toEdit.negm || isNaN(toEdit.negm))){
					logger.error({"r":"ed","p":req.body,"msg":"ineg_sent_but_no_negm_for_test_with_no_default_negm"});
					return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				}
				if("marks" in req.body){
					marks = req.body.marks;
					if(isNaN(marks) || marks <= 0){
						marks = test.mpq;
					}
					toEdit.marks = marks;
				}
				if("code" in req.body){
					toEdit.code = getTestTypeCode(test_id, req.body.code);//req.body.code;
				}
				// if("lv" in req.body){
				// 	if(isNaN(req.body.lv)){
				// 		logger.error({"r":"ad","p":req.body,"msg":"level_not_a_number"});
				// 		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				// 	}
				// 	var que_level = parseInt(req.body.lv);
				// 	if(que_level < 1 || que_level > test.mxqlv){
				// 		logger.error({"r":"ad","p":req.body,"msg":"level_is_not_valid"});
				// 		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				// 	}
				// 	toEdit.lv = que_level;
				// }
				// if(req.body.tags && typeof(req.body.tags) != "object" && req.body.tags.length == 0){
				// 	logger.error({"r":"ed","p":req.body,"msg":"tags_found_in_req_body_invalid"});
				// 	return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				// }
				checkTestTags(test_id, req.body, test, function(err, resp){
					if(err){
						logger.error({"r":"ed","er":err,"p":req.body,"msg":"checkTestTags_Error"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(resp.exists){
					  if(!resp.match){
					  	logger.error({"r":"ed","p":req.body,"msg":"testquestion_tags_are_not_matching_with_test"});
							return sendError(res,"testquestion_tags_are_not_matching_with_test","testquestion_tags_are_not_matching_with_test",HttpStatuses.SERVER_ERROR);
					  }
					  toEdit.tags = req.body.tags;
					}
					TestQuestion.update({
						_id : testquestion._id
					},toEdit,function(err,testquestion_updated){
						if(err){
							logger.error({"r":"ed","er":err,"p":req.body,"msg":"TestQuestion_Find_Error"});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						var tEdit = {};
						if(toEdit.marks && toEdit.marks != testquestion.marks){
							tEdit.total_marks = test.total_marks + (toEdit.marks - testquestion.marks);
						}
						if(test.adpstg && test.adpstg == 2){
							if(toEdit.lv && toEdit.lv != testquestion.lv){
								tEdit.adpstg = 1;
							}
						}
						if(Object.keys(tEdit).length == 0){
							logger.info({"r":"ed","p":req.body,"msg":"Success_without_marks_and_without_adpstg","tedt":toEdit});
							return sendSuccess(res,{});
						}
						Test.update({
							_id : test_id
						},tEdit,function(err,resp){
							if(err){
								logger.error({"r":"ed","er":err,"p":req.body,"msg":"Test_Update_Error_adpstg_1"});
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}
							logger.info({"r":"ed","p":req.body,"msg":"Success_with_marks_with_or_adpstg_disable","tedt":toEdit,"tsEdit":tEdit});
							return sendSuccess(res,{});
						});
					});
				});
			});
		});
	});
});

router.post('/dl', function(req, res, next){
	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ques_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"dl","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var gid             = req.body.g_id;
	var plugin_id       = req.body.pl_id;
	var category_id     = req.body.catg_id;
	var test_id         = req.body.test_id;
	var ques_id         = req.body.ques_id;

	Test.findOne({
		_id     : test_id,
		catg_id : category_id,
		act     : true
	},function(err,test){
		if(err){
			logger.error({"r":"dl","er":err,"p":req.body,"msg":"Test_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"dl","p":req.body,"msg":"test_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		TestQuestion.findOne({
			test_id : test_id,
			ques_id : ques_id,
			act     : true
		},function(err,testquestion){
			if(err){
				logger.error({"r":"dl","er":err,"p":req.body,"msg":"Testquestion_Count_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!testquestion){
				logger.error({"r":"dl","p":req.body,"msg":"TestQuestion_not_found"});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			TestQuestion.update({
				test_id : test_id,
				ques_id : ques_id,
				act     : true
			},{
				act  : false
			},function(err,testquestion_updated){
				if(err){
					logger.error({"r":"dl","er":err,"p":req.body,"msg":"Testquestion_Update_Error"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				Test.update({
					_id     : test_id,
				},{
					$inc : {
						num_ques : -1,
						total_marks : (-1 * testquestion.marks)
					}
				},function(err,test_updated){
					if(err){
						logger.error({"r":"dl","er":err,"p":req.body,"msg":"Test_Update_Error"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!test.adpstg || test.adpstg != 2){
						logger.info({"r":"dl","p":req.body,"msg":"TestQuestion_Deleted_Success","tq":testquestion_updated});
						return sendSuccess(res,{});
					}
					Test.update({
						_id : test_id
					},{
						adpstg : 1
					},function(err,resp){
						if(err){
							logger.error({"r":"dl","er":err,"p":req.body,"msg":"Testquestion_Update_Error"});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						logger.info({"r":"dl","p":req.body,"msg":"TestQuestion_Deleted_Success_with_Stage2_disabling","tq":testquestion_updated});
						return sendSuccess(res,{});
					});
				});
			});
		});
	});
});

function getTestTags(test, cb){
	var tagById = {};
	if(!test.tags || test.tags.length == 0){
		return cb(null,tagById);
	}
	Tag.find({
		_id : {$in : test.tags},
		act : true
	},{
		nm : 1
	},function(err,tags){
		if(err){
			return cb(err,null);
		}
		var ttotal = tags.length;
		if(ttotal == 0){
			return cb("test_question_tags_not_found_in_tag",null);
		}
		for(var i=0; i<ttotal; i++){
			tagById[tags[i]._id+''] = tags[i].nm;
		}
		return cb(null,tagById);
	});
}

function checkTestTags(test_id, req_body, test, cb){
	if(!req_body.tags || typeof(req_body.tags) != "object" || req_body.tags.length == 0){
		console.log("no_tags_supplied_in_request_body");
		return cb(null,{exists:false,match:false});
	}
	if(!test.tags || test.tags.length == 0){
		console.trace("tags_supplied_in_request_body_but_test_does_not_have_tag_ids");
		return cb(null,{exists:true,match:false});
	}
	var testtagsids = test.tags;
	var tag_ids = req_body.tags;
	var tlen    = tag_ids.length;
	for(var i=0; i<tlen; i++){
		if(testtagsids.indexOf(tag_ids[i]) < 0){
			console.trace("tags_supplied_in_request_body_does_not_match_with_test_tag_ids");
			return cb(null,{exists:true,match:false});
		}
	}
	return cb(null,{exists:true,match:true});
}

function getTestTypeCode(test_id, code){
	if(catConfigs.TARGET_TEST_IDS.indexOf(test_id) >= 0){
		if(code == "A"){
			return "A";
		}
		if(code == "T"){
			return "B";
		}
		if(code == "P"){
			return "B";
		}
		if(code == "R"){
			return "C";
		}
	}
	return code;
}

module.exports = router;