var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    							  = require('jsonwebtoken');

var configs					      = require('../configs/configs.js');
var HttpStatuses					= require('../global/http_api_return_codes.js');
var ApiReturnHandlers			= require('../global/api_return_handlers.js');
var errorCodes 						= require('../global/error_codes.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var AuthSecurity      		= require('../security/auth_tokens.js');
var RootServerApi      		= require('../server/apicalls.js');

require('../models/tests.js');
require('../models/testquestions.js');
require('../models/categories.js');
require('../models/usertests.js');
require('../models/users.js');
require('../models/pluginpaidinfos.js');
var User                  = mongoose.model('User');
var Test 									= mongoose.model('Test');
var TestQuestion 				  = mongoose.model('TestQuestion');
var Category 							= mongoose.model('Category');
var UserTest 							= mongoose.model('UserTest');
var PluginPaidInfo 				= mongoose.model('PluginPaidInfo');

var sendError 						= ApiReturnHandlers.sendError;
var sendSuccess 					= ApiReturnHandlers.sendSuccess;
var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_TEST);

//route middleware to verify if plugin in group is paid
router.use(function(req, res, next){
	var tim         = new Date().getTime();
	var plugin_id   = req.body.pl_id;
	var token       = req.body.tokn;
	var gid         = req.body.g_id;
	var user_id     = req.body.user_id;
	
	if(!token || token == 'null' || token === "undefined"){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body,"tkn":token});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!gid){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"gid_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","gid_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!plugin_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"plugin_id_missing","p":req.body});
	  return sendError(res,"Plugin Id Not Found","plugin_id_missing",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			req.private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			req.private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
		req.tim = tim;
	  next();
	});
});

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
  jwt.verify(req.body.tokn, req.private_key, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != req.body.user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != req.body.pl_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_app_id","p":req.body});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id4 != req.body.g_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body});
	  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    next();
  });
});

router.post('/g_test', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"g_test","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id     = req.body.user_id.trim();
	var test_id     = req.body.catg_id.trim();
	var tim         = new Date().getTime();

	Test.findOne({
		_id : test_id,
		act : true,
		publ: true
	},{
		name                : 1,
		catg_id             : 1,
		type                : 1,
		num_ques            : 1,
		timed               : 1,
		max_tim             : 1,
		total_marks         : 1,
		total_attempts      : 1,
		total_submits       : 1,
		submits_all_correct : 1,
		max_score_attained  : 1,
	},function(err, test){
		if(err){
			logger.error({"r":"g_test","er":err,"p":req.body});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!tests){
			logger.error({"r":"g_test","msg":"no Test Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		return sendSuccess(res,{test:test});
	});
});

router.post('/g_catg_tests', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('start',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ipp',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"g_catg_tests","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id     = req.body.user_id.trim();
	var category_id = req.body.catg_id.trim();
	var start       = req.body.start.trim();
	var ipp         = req.body.ipp.trim();
	var tim         = new Date().getTime();

	start           = parseInt(start);
	ipp             = parseInt(ipp);

	if(ipp <= 0 || ipp > configs.DEFAULT_IPP_FOR_TEST){
		logger.error({"r":"g_catg_tests","msg":"Invalid IPP for categories","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	User.findOne({
  	_id : user_id,
		act : true,
	},{
		identifier : 1
	},function(err,user){
		if(err){
			logger.error({"r":"g_catg_tests","er":err,"p":req.body,"msg":"User_Find_Failed"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!user){
			logger.error({"r":"g_catg_tests","msg":"user_not_found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		Category.findOne({
			_id : category_id
		},function(err,category){
			if(err){
				logger.error({"r":"g_catg_tests","er":err,"p":req.body,"msg":"Category_Find_Failed"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!category){
				logger.error({"r":"g_catg_tests","msg":"category_not_found","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			if(category.act == false){
				logger.error({"r":"g_catg_tests","msg":"category_not_found","p":req.body});
				return sendError(res,"category_not_active_anymore","category_not_active_anymore",HttpStatuses.SERVER_SPECIFIC_ERROR);
			}
			var sorting_condition = {};
			if(category.adpv && category.adpv == true){
				sorting_condition = {
					level    : 1,
					createdAt: 1
				};
			} else {
				sorting_condition.createdAt = 1;
			}
			Test.find({
				catg_id  : category_id,
				act      : true,
				num_ques : {$gt : 0},
				publ     : true,
				stim     : {$lte:tim} 
			},{
				name                : 1,
				catg_id             : 1,
				type                : 1,
				level               : 1,
				desc                : 1,
				timed               : 1,
				max_tim             : 1,
				num_ques            : 1,
				total_attempts      : 1,
				total_submits       : 1,
				submits_all_correct : 1,
				max_score_attained  : 1,
				ssol                : 1,
				ineg                : 1,
				negm                : 1,
				publ                : 1,
				stim                : 1,
				tim                 : 1,
				model               : 1,
				ml_at               : 1,
				adpv                : 1,
				level               : 1,
				adpstg              : 1
			},{
		    sort : sorting_condition
			},function(err, tests){
				if(err){
					logger.error({"r":"g_catg_tests","er":err,"p":req.body,"msg":"Test_Find_Failed"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(tests.length == 0 || start > tests.length){
					logger.error({"r":"g_catg_tests","msg":"no Test Found","p":req.body});
					return sendError(res,"No Tests available in category","no_test_in_category_found",HttpStatuses.NO_DATA);
				}

				var targetTestIds = [];
				var allTestIds    = [];
				var targetRange   = start+ipp;
				var total_tests   = tests.length;
				var test_adpv_lv  = {};
				
				targetRange = (targetRange <= total_tests) ? targetRange : total_tests;

				for(var i=start; i<targetRange; i++){
					targetTestIds.push(tests[i]._id+'');
				}
				if(targetTestIds.length == 0){
					logger.error({"r":"g_catg_tests","msg":"no Test Found targetRange","p":req.body});
					return sendError(res,"No Tests available in category","no_test_in_category_found",HttpStatuses.NO_DATA);
				}
				var temptest;
				for(var j=0; j<total_tests; j++){
					temptest = tests[j];
					allTestIds.push(temptest._id+'');
					if(temptest.adpv && temptest.adpv == true){
						if(temptest.adpstg && temptest.adpstg > Test.ADPATIVE_STAGE.FIRST){
							test_adpv_lv[temptest._id+''] = temptest.adpstg;
						}
					}
				}
				UserTest.find({
					user_id      : user_id,
					test_id      : {$in : targetTestIds},
					is_submitted : true
				},{
					test_id        : 1,
					marks_obtained : 1
				},{
					sort : {
						start_time : 1
					}
				},function(err,usertests){
					if(err){
						logger.error({"r":"g_catg_tests","er":err,"p":req.body,"msg":"usertest_distinct_failed"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					var total = usertests.length;
					var testScores = {};
					var test_idT;
					for(var i=0; i<total; i++){
						test_idT = usertests[i].test_id+'';
						if(!(test_idT in testScores)){
							testScores[test_idT] = usertests[i].marks_obtained;
						}
					}
					var allowed = {};
					var tempId;
					for(var j=0; j<total_tests; j++){
						tempId = tests[j]._id+'';
						if("ml_at" in tests[j]){
							if(tests[j].ml_at == true){
								allowed[tempId] = true;
							} else if(!testScores[tempId]){
								allowed[tempId] = true;
							}
						} else {
							allowed[tempId] = true;
						} 
					}
					UserTest.distinct('test_id',{
						user_id      : user_id,
						test_id      : {$in : allTestIds},
						is_submitted : true
					},function(err,tests_completed){
						if(err){
							logger.error({"r":"g_catg_tests","er":err,"p":req.body,"msg":"usertest_distinct_failed"});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						RootServerApi.checkIfAdmin(user.identifier, function(err,resp){
							if(err){
								logger.error({"url":"g_catg_tests","msg":"rootServer_checkIfAdmin_err","er":err,"p":req.body});
								console.trace(err);
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}
							if(!resp.success){
								logger.error({"url":"g_catg_tests","msg":"rootServer_checkIfAdmin_failed","p":req.body});
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}
							var is_admin = false;
							if(("is_admin" in resp)  && resp.is_admin == true){
								is_admin = true;
							}
							var done = tests_completed.length;
							// console.log('test lists');
							// console.log(sorting_condition);
							// console.log(category);
							// console.log(tests);
							logger.info({"url":"g_catg_tests","msg":"g_catg_tests_Success","d":{tt:total_tests}});
							return sendSuccess(res,{
								tests        : tests,
								tim          : tim,
								atests       : testScores,
								altests      : allowed,
								done         : done,
								total        : total_tests,
								is_admin     : is_admin,
								test_adpv_lv : test_adpv_lv
							});
						});
					});
				});
			});
		});
	});
});

router.post('/test_anlt', function(req, res, next){

	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"test_anlt","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var gid        = req.body.g_id.trim();
	var plugin_id  = req.body.pl_id.trim();
	var catg_id    = req.body.catg_id.trim();
	var test_id    = req.body.test_id.trim();
	var tim        = new Date().getTime();

	Category.findOne({
		_id   : catg_id,
		gid   : gid,
		act   : true
	},function(err,category){
		if(err){
			logger.error({"r":"test_anlt","er":err,"p":req.body,"msg":"Category_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!category){
			logger.error({"r":"test_anlt","msg":"category_not_found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		Test.findOne({
			_id         : test_id,
			act         : true,
			num_ques    : {$gt : 0},
			publ        : true,
			stim        : {$lte:tim}
		},{
			num_ques       : 1,
			total_marks    : 1,
			total_attempts : 1,
			total_submits  : 1,
			name           : 1,
			adpv           : 1,
			adpstg         : 1,
			mxqlv          : 1,
			timed          : 1,
			max_tim        : 1
		},function(err,test){
			if(err){
				logger.error({"r":"rd","er":err,"p":req.body,"msg":"TestFind_DbError"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!test){
				logger.error({"r":"test_anlt","msg":"test_not_found","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			if(!test.total_marks || test.total_marks <= 0){
				logger.error({"r":"test_anlt","msg":"test_total_marks_not_perfect","p":req.body});
				return sendError(res,"test_ineligible_for_analytics","test_ineligible_for_analytics",HttpStatuses.SERVER_SPECIFIC_ERROR);
			}
			if(!test.total_submits || test.total_submits <= 0){
				logger.error({"r":"test_anlt","msg":"test_total_submit_not_perfect","p":req.body});
				return sendError(res,"test_ineligible_for_analytics","test_ineligible_for_analytics",HttpStatuses.SERVER_SPECIFIC_ERROR);
			}
			averageDataForTest(test, function(err,data){
				if(err){
					logger.error({"r":"test_anlt","er":err,"p":req.body,"msg":"TestFind_DbError"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				console.log('averageDataForTest');
				// console.log(data.marks);
				// console.log(data.accuracy);
				console.log(data.atmpAndCrct);
				var graph1 = prepareMarksGraphData(data.marks);
				var graph2 = prepareAccuracyGraphData(data.accuracy);
				var graph3 = prepareTimeGraphData(data.time);
				var graph4 = preapreAtmpCrctGraphData(data.atmpAndCrct);
				var graph_data = [graph1, graph2, graph3, graph4];
				if(data.levelWiseData){
					graph_data.push(prepareAdaptiveStageExtraGraph(data.levelWiseData));
				}
				// console.log(graph4);
				return sendSuccess(res,{graph_data:graph_data});
			});
		});
	});
});

function averageDataForTest(test, cb){

	var test_id       = test._id;
	var test_name     = test.name;
	var total_ques    = test.num_ques;
	var total_marks   = test.total_marks;
	var submits       = test.total_submits;
	var if_stage2     = (test.adpv && test.adpstg && test.adpstg == 2) ? true : false;
	var timed         = (test.timed && test.timed == true) ? true : false;
	var test_max_time = (!test.max_time) ? 0 : parseInt(test.max_time / 60000);

	UserTest.find({
    test_id      : test_id,
    is_submitted : true,
    is_first     : true
  },{
    marks_obtained : 1,
    total_correct  : 1,
    time_taken     : 1,
    resm_c         : 1
  },{
    sort : {
      marks_obtained : 1
    }
  },function(err,usertests){
    if(err){
      return cb(err,null);
    }
    if(usertests.length == 0){
      return cb(null,{average_score : average_score, average_correct : average_correct});
    }

    var sum_marks            = 0;
    var sum_correct          = 0;
    var sum_time_taken       = 0;
    var time_data_points     = [];
    var tlen                 = usertests.length;
    var ttlen                = usertests.length;

    var temp1, temp2, temp3;
    var marks_bucket   = [];
    var correct_bucket = [];
    var time_bucket    = [];

    for(var m=0; m<10;m++){
    	marks_bucket[m]   = 0;
    	correct_bucket[m] = 0;
    }
    for(var n=0;n<7;n++){
    	time_bucket[n] = 0;
    }
    var time_bucket_levels = getTimeBucketLevels(timed, test_max_time);

    for(var i=0; i<tlen; i++){

      temp1 = parseInt((usertests[i].marks_obtained / total_marks)*100);
      temp2 = parseInt((usertests[i].total_correct / total_ques)*100);

      sum_marks   = sum_marks + temp1;
      sum_correct = sum_correct + temp2;

      marks_bucket[getBucketNumber(temp1)]   += 1;
      correct_bucket[getBucketNumber(temp2)] += 1;

      if(!usertests[i].resm_c){
      	temp3 = parseInt(usertests[i].time_taken / 60000);
      	time_bucket[getTimeBucketNum(timed, test_max_time, temp3)] += 1;
      	sum_time_taken = sum_time_taken + temp3;
      } else if(usertests[i].resm_c > 0){
      	ttlen--;
      }
    }

    var average_score     = Math.round(sum_marks / tlen); 
    var average_correct   = Math.round(sum_correct / tlen); 
    var average_time      = (ttlen == 0) ? 0 : parseInt((sum_time_taken / ttlen) * 100);

    TestQuestion.find({
    	test_id : test_id,
    	act     : true
    },{
    	atmp : 1,
    	crct : 1,
    	lv   : 1
    },function(err,testquestions){
    	if(err){
	      return cb(err,null);
	    }
	    final_data = {
	    	marks : {
	    		average_score : average_score,
	    		// data_point    : marks_data_points,
	    		bucket        : marks_bucket,
	    		min           : usertests[0].marks_obtained,
	    		max           : usertests[tlen-1].marks_obtained 
	    	},
	    	accuracy : {
	    		average_correct : average_correct,
	    		// data_points     : accuracy_data_points,
	    		bucket          : correct_bucket
	    	},
	    	time : {
	    		average_time : average_time,
	    		bucket       : time_bucket,
	    		bucket_levels: time_bucket_levels
	    	},
	    	name            : test_name,
	    	test_id         : test_id,
	    	atmpAndCrct     : categoriseQuestionsLevel(testquestions, submits, test.total_attempts)
	    };
	    if(if_stage2 && test.mxqlv > 1){
	    	final_data.levelWiseData = categorizeQuestionByAdaptiveLevel(testquestions, submits, test.mxqlv);
	    	// console.log('ADPATIVE_STAGE data');
	    	// console.log(final_data.levelWiseData);
	    }
	    return cb(null,final_data);
    }).read('s');
  }).read('s');
}

function categorizeQuestionByAdaptiveLevel(testquestions, submits, max_ques_level){

	var tlen = testquestions.length;

	var leveldata = [];
	for(var j=0; j<max_ques_level; j++){
		leveldata[j] = {
			a : 0,
			c : 0,
			s : 0
		};
	}
	// console.log(testquestions);
	for(var i=0; i<tlen; i++){
		if(testquestions[i].lv && testquestions[i].lv <= max_ques_level){
			leveldata[testquestions[i].lv-1].a += testquestions[i].atmp;
			leveldata[testquestions[i].lv-1].c += testquestions[i].crct;
			leveldata[testquestions[i].lv-1].s += submits;
		}
	}

	for(var m=0; m<max_ques_level; m++){
		var temp = {
			ap : parseInt((leveldata[m].a / leveldata[m].s) * 100),
			cp : parseInt((leveldata[m].c / leveldata[m].s) * 100)
		}
		leveldata[m] = temp;
	}
	return leveldata;
}

function categoriseQuestionsLevel(testquestions, submits, attempts){

	var atmp_barrier    = configs.TEST_ANALYTICS_QUES_ATMP_BARRIER;
	var correct_barrier = configs.TEST_ANALYTICS_QUES_CRCT_BARRIER;
	var tlen = testquestions.length;

	var atemp,ctemp;

	var easy       = [];
	var hard       = [];
	var tricky     = [];
	var conceptual = [];

	// console.log('categoriseQuestionsLevel');
	// console.log(submits);
	console.log(testquestions);
	var temp;
	for(var i=0; i<tlen; i++){
		temp = testquestions[i];
		if(temp.atmp >= submits){
			atemp = parseInt((temp.atmp / attempts) * 100);
		} else {
			atemp = parseInt((temp.atmp / submits) * 100);
		}
		if(temp.crct >= submits){
			ctemp = parseInt((temp.crct / attempts) * 100);
		} else {
			ctemp = parseInt((temp.crct / submits) * 100);
		}

		var temp = [atemp, ctemp];
		// console.log(temp);

		if(atemp >= atmp_barrier){
			if(ctemp >= correct_barrier){
				easy.push(temp);
			} else {
				tricky.push(temp);
			}
		} else {
			if(ctemp >= correct_barrier){
				conceptual.push(temp);
			} else {
				hard.push(temp);
			}
		}
	}
	return {easy:easy,hard:hard,conceptual:conceptual,tricky:tricky};
}

function prepareAdaptiveStageExtraGraph(levelWiseData){
	var graph5 = {
		chart: {
        type: 'column'
    },
    title: {
        text: 'Question Level Analysis'
    },
    xAxis: {
        categories: [],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Attempt, Accuracy'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: '% Attempt',
        data: []

    }, {
        name: '% Accuracy',
        data: []

    }]
	};
	var total = levelWiseData.length;
	for(var i=0; i<total; i++){
		graph5.xAxis.categories.push('Level'+(i+1));
		graph5.series[0].data.push(levelWiseData[i].ap);
		graph5.series[1].data.push(levelWiseData[i].cp);
	}
	return graph5;
}

function preapreAtmpCrctGraphData(atmpAndCrct){
	var graph4 = {
		chart: {
        type: 'scatter',
        zoomType: 'xy'
    },
    title: {
        text: 'Attempt '
    },
    xAxis: {
        title: {
            enabled: true,
            text: '% Attempt'
        },
        startOnTick: true,
        endOnTick: true,
        showLastLabel: true
    },
    yAxis: {
        title: {
            text: '% Correct'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 100,
        y: 70,
        floating: true,
        backgroundColor: '#FFFFFF',
        borderWidth: 1
    },
    plotOptions: {
        scatter: {
            marker: {
                radius: 5,
                states: {
                    hover: {
                        enabled: true,
                        lineColor: 'rgb(100,100,100)'
                    }
                }
            },
            states: {
                hover: {
                    marker: {
                        enabled: false
                    }
                }
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x}% Attempt, {point.y}% Correct'
            }
        }
    },
    series: [{
        name: 'Easy',
        color: 'rgba(29, 165, 58, 1)',
        data: atmpAndCrct.easy

    }, {
        name: 'Hard',
        color: 'rgba(0, 0, 0, 1)',
        data: atmpAndCrct.hard
    }, {
        name: 'Tricky',
        color: 'rgba(223, 83, 83, .5)',
        data: atmpAndCrct.tricky
    }, {
        name: 'Conceptual',
        color: 'rgba(55, 138, 150, 1)',
        data: atmpAndCrct.conceptual
    }]
	};
	return graph4;
}

function prepareMarksGraphData(data){
	var bucket = data.bucket;
	var average_score = data.average_score;
	var graph1 = {
		chart: {
        type: 'column'
    },
    title: {
        text: "Marks Distrubution"
    },
    xAxis: {
        categories: [],
        crosshair: true,
        title: {
            text: 'Marks Division (in %)'
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Number of Students'
        }
    },
    tooltip: {
        // headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        // pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        //     '<td style="padding:0"><b>{point.y:.1f}%</b></td></tr>',
        // footerFormat: '</table>',
        // shared: true,
        // useHTML: true
        headerFormat: '<b>{series.name}</b> : {point.x}<br>',
        pointFormat: '<b>No. Of Students </b> : {point.y}'
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: '%Marks Range',
        data: []
    },{
    	name: '%Average Marks',
      data: []
    }]
	};

	var series_data = [];
	var avg_data    = [];
	var total       = bucket.length;
	var avg_total   = 0;
	var categories  = [];
	var avg_found   = false;
	var temp, prev;

	for(var i=0; i<10; i++){
		temp = i*10;
		if(!avg_found){
			if(average_score > temp){
				avg_total += bucket[i];
				avg_data.push('');
			} else {
				avg_found = true;
				if(average_score <= 0){
					avg_data.push(bucket[0]);
				} else {
					avg_data.push(avg_total);
				}
				series_data.push('');
				categories.push(average_score);
			}
		}
		if(i == 0){
			categories.push('<=0');
		} else {
			categories.push(((i-1)*10)+'-'+(i*10));
		}
		series_data.push(bucket[i]);
	}
	graph1.xAxis.categories = categories;
	graph1.series[0].data   = series_data;
	graph1.series[1].data   = avg_data;
	// console.log('marking');
	// console.log(categories);
	// console.log(series_data);
	return graph1;
}

// function prepareMarksGraphData(data){
// 	var bucket = data.bucket;
// 	var graph1 = {
// 		chart: {
//         type: 'spline'
//     },
//     title: {
//         text: 'Marks Distribution'
//     },
//     xAxis: {
//         title: {
//             text: '% Marks Obtained'
//         }
//     },
//     yAxis: {
//         title: {
//             text: 'Number of Users'
//         },
//         min: 0
//     },
//     tooltip: {
//         headerFormat: '<b>{series.name}</b><br>',
//         pointFormat: '{point.x}% Marks, {point.y} Users'
//     },

//     plotOptions: {
//         spline: {
//             marker: {
//                 enabled: true
//             }
//         }
//     },

//     series: [{
//         name: "Marks Distribution of Users",
//         data: []
//     }]
// 	};
// 	var series_data = [];
// 	for(var key in bucket){
// 		var temp = [key,bucket[key]];
// 		series_data.push(temp)
// 	}
// 	graph1.series[0].data = series_data;
// 	return graph1;
// }

function prepareAccuracyGraphData(data){
	var bucket  = data.bucket;
	var average_correct = data.average_correct;
	var graph2  = {
		chart: {
        type: 'column'
    },
    title: {
        text: "Accuracy Distrubution"
    },
    xAxis: {
        categories: [],
        crosshair: true,
        title: {
            text: 'Accuracy Division (in %)'
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Number of Students'
        }
    },
    tooltip: {
        headerFormat: '<b>{series.name}</b> : {point.x}<br>',
        pointFormat: '<b>No. Of Students </b> : {point.y}'
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: '%Accuracy Range',
        data: []
    },{
    	name : '%Average Accuracy',
    	data : []
    }]
	};

	var series_data = [];
	var avg_data    = [];
	var total       = bucket.length;
	var avg_total   = 0;
	var categories  = [];
	var avg_found   = false;
	var temp, prev;

	for(var i=0; i<10;i++){
		temp = i*10;
		if(!avg_found){
			if(average_correct > temp){
				avg_total += bucket[i];
				avg_data.push('');
			} else {
				avg_found = true;
				if(average_correct <= 0){
					avg_data.push(bucket[0]);
				} else {
					avg_data.push(avg_total);
				}
				series_data.push('');
				categories.push(average_correct);
			}
		}
		if(i == 0){
			categories.push('<=0');
		} else {
			categories.push(((i-1)*10)+'-'+temp);
		}
		series_data.push(bucket[i]);
	}
	graph2.xAxis.categories = categories;
	graph2.series[0].data   = series_data;
	graph2.series[1].data   = avg_data;
	// console.log('accuracy');
	// console.log(categories);
	// console.log(series_data);
	return graph2;
}

// function prepareAccuracyGraphData(data){
// 	var bucket = data.bucket;
// 	var graph2 = {
// 		chart: {
//         type: 'spline'
//     },
//     title: {
//         text: 'Accuracy Distribution'
//     },
//     xAxis: {
//         title: {
//             text: '% Accuracy'
//         }
//     },
//     yAxis: {
//         title: {
//             text: 'Number of Users'
//         },
//         min: 0
//     },
//     tooltip: {
//         headerFormat: '<b>{series.name}</b><br>',
//         pointFormat: '{point.x}% Accuracy, {point.y} Users'
//     },

//     plotOptions: {
//         spline: {
//             marker: {
//                 enabled: true
//             }
//         }
//     },

//     series: [{
//         name: "Accuracy Distribution of Users",
//         data: []
//     }]
// 	};
// 	var series_data = [];
// 	for(var key in bucket){
// 		var temp = [key,bucket[key]];
// 		series_data.push(temp)
// 	}
// 	graph2.series[0].data = series_data;
// 	return graph2;
// }

// function prepareTimeGraphData(data){
// 	var bucket = data.bucket;
// 	var graph3 = {
// 		chart: {
//         type: 'spline'
//     },
//     title: {
//         text: 'Time Spent by Users'
//     },
//     xAxis: {
//         title: {
//             text: 'Time (in Mins)'
//         }
//     },
//     yAxis: {
//         title: {
//             text: 'Number of Students'
//         },
//         min: 0
//     },
//     tooltip: {
//         headerFormat: '<b>{series.name}</b><br>',
//         pointFormat: '{point.x} Mins Avg, {point.y} Users'
//     },

//     plotOptions: {
//         spline: {
//             marker: {
//                 enabled: true
//             }
//         }
//     },

//     series: [{
//         name: "Time Spent By Users",
//         data: []
//     }]
// 	};
// 	var series_data = [];
// 	for(var key in bucket){
// 		var temp = [key,bucket[key]];
// 		series_data.push(temp)
// 	}
// 	graph3.series[0].data = series_data;
// 	return graph3;
// }

function prepareTimeGraphData(data){

	var bucket_levels = data.bucket_levels;
	var bucket        = data.bucket;

	var average_correct = data.average_correct;
	var graph2  = {
		chart: {
        type: 'column'
    },
    title: {
        text: "Time Distrubution"
    },
    xAxis: {
        categories: [],
        crosshair: true,
        title: {
            text: 'Time Spent Division (in mins)'
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Number of Students'
        }
    },
    tooltip: {
        headerFormat: '<b>{series.name}</b> : {point.x}<br>',
        pointFormat: '<b>No. Of Students </b> : {point.y}'
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: '%Time Spent Range',
        data: []
    }]
	};

	graph2.xAxis.categories = bucket_levels;
	graph2.series[0].data   = bucket;
	
	return graph2;
}

function getTimeBucketLevels(is_timed, max_time){
	var points;
	if(!is_timed || max_time < 30){
		points = ["<1","1-3","3-5","5-10","10-15","15-20","20-30"];
		return points;
	}
	if(max_time <= 60){
		points = ["<1","1-3","3-5","5-10","10-15","15-30",">30"];
		return points;
	}
	if(max_time <= 120){
		points = ["<1","1-5","5-10","10-15","15-30","30-60",">60"];
		return points;
	}
	if(max_time <= 180){
		points = ["<1","1-5","5-15","15-30","30-60","60-120",">120"];
		return points;
	}
	if(max_time > 180){
		points = ["<1","1-10","10-30","30-60","60-120","120-180",">180"];
		return points;
	}
}

function getTimeBucketNum(is_timed, max_time, time_taken){
	if(!is_timed || max_time < 30){
		if(time_taken < 1){
			return 0;
		}
		if(time_taken < 3){
			return 1;
		}
		if(time_taken < 5){
			return 2;
		}
		if(time_taken < 10){
			return 3;
		}
		if(time_taken < 15){
			return 4;
		} 
		if(time_taken < 30){
			return 5;
		}
		return 6;
	}
	if(max_time <= 60){
		if(time_taken < 1){
			return 0;
		}
		if(time_taken < 3){
			return 1;
		}
		if(time_taken < 5){
			return 2;
		}
		if(time_taken < 10){
			return 3;
		}
		if(time_taken < 15){
			return 4;
		} 
		if(time_taken < 30){
			return 5;
		}
		return 6;
	}
	if(max_time <= 120){
		if(time_taken < 1){
			return 0;
		}
		if(time_taken < 5){
			return 1;
		}
		if(time_taken < 10){
			return 2;
		}
		if(time_taken < 15){
			return 3;
		} 
		if(time_taken < 30){
			return 4;
		}
		if(time_taken < 60){
			return 5;
		}
		return 6;
	}
	if(max_time <= 180){
		if(time_taken < 1){
			return 0;
		}
		if(time_taken < 5){
			return 1;
		}
		if(time_taken < 15){
			return 2;
		}
		if(time_taken < 30){
			return 3;
		} 
		if(time_taken < 60){
			return 4;
		}
		if(time_taken < 120){
			return 6;
		}
		return 6;
	}
	if(time_taken > 180){
		if(time_taken < 1){
			return 0;
		}
		if(time_taken < 10){
			return 1;
		}
		if(time_taken < 30){
			return 2;
		}
		if(time_taken < 60){
			return 3;
		} 
		if(time_taken < 120){
			return 4;
		}
		if(time_taken < 180){
			return 5;
		}
		return 6;
	}
}

function getBucketNumber(data){
	if(data <= 0){
		return 0;
	}
	if(data == 100){
		return 9;
	}
	return (parseInt(data/10));
}

module.exports = router;