const express 							= require('express');
const mongoose 							= require('mongoose');
const jwt    							  = require('jsonwebtoken');

const configs					      = require('../configs/configs.js');
const catConfigs					  = require('../configs/catconfigs.js');
const HttpStatuses					= require('../global/http_api_return_codes.js');
const ApiReturnHandlers			= require('../global/api_return_handlers.js');
const errorCodes 						= require('../global/error_codes.js');
const log4jsLogger  				= require('../loggers/log4js_module.js');
const log4jsConfigs  			  = require('../loggers/log4js_configs.js');
const AuthSecurity      		= require('../security/auth_tokens.js');
const RootServerApi      		= require('../server/apicalls.js');
const MailClient            = require('../mail/mail_client.js');
const CertificateModule     = require('../certificates/module.js');
const TestReviewModule      = require('../review/test.js');
const TestStatistics        = require('../statistics/test.js');
const TestAnswersModule     = require('../cat/test_answer_module.js');
const TestResumable         = require('../cat/testresume.js');

require('../models/users.js');
require('../models/categories.js');
require('../models/tests.js');
require('../models/questions.js');
require('../models/usertests.js');
require('../models/testquestions.js');
require('../models/usertestquestions.js');
require('../models/groupstats.js');
require('../models/pluginpaidinfos.js');
const User                  = mongoose.model('User');
const Category 							= mongoose.model('Category');
const Test 									= mongoose.model('Test');
const Question					    = mongoose.model('Question');
const UserTest 							= mongoose.model('UserTest');
const TestQuestion					= mongoose.model('TestQuestion');
const UserTestQuestion		  = mongoose.model('UserTestQuestion');
const GroupStats		        = mongoose.model('GroupStats');
const PluginPaidInfo 				= mongoose.model('PluginPaidInfo');

const sendError 						= ApiReturnHandlers.sendError;
const sendSuccess 					= ApiReturnHandlers.sendSuccess;
const router 								= express.Router();
const logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_CAT_USERTEST);

//route middleware to verify if plugin in group is paid
router.use(function(req, res, next){

	var tim         = new Date().getTime();
	var plugin_id   = req.body.pl_id;
	var token       = req.body.tokn;
	var gid         = req.body.g_id;
	var user_id     = req.body.user_id;

	if(!token || token == 'null' || token === "undefined"){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body,"tkn":token});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!gid){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"gid_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","gid_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!plugin_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"plugin_id_missing","p":req.body});
	  return sendError(res,"Plugin Id Not Found","plugin_id_missing",HttpStatuses.FORBIDDEN);
	}

	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			req.private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			req.private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
		req.tim = tim;
	  next();
	});
});

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
  jwt.verify(req.body.tokn, req.private_key, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != req.body.user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != req.body.pl_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_app_id","p":req.body});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id4 != req.body.g_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body});
	  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    req.decoded = decoded;
    next();
  });
});

router.post('/ut_report', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"ut_report","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	var user_id     = req.body.user_id.trim();
	var test_id     = req.body.test_id.trim();
	var user_test_id= req.body.user_test_id.trim();
	var tim         = new Date().getTime();

	Test.findOne({
		_id : test_id,
		act : true
	},function(err,test){
		if(err){
			logger.error({"r":"ut_report","er":err,"p":req.body,"er":err,"msg":"Test_find_DbError"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"ut_report","msg":"Test_Not_Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		var ssol = true;
		if(test.ssol && test.ssol == false){
			ssol = false;
		}
		UserTest.findOne({
			_id          : user_test_id,
			user_id      : user_id,
			test_id      : test_id,
			is_started   : true,
			is_submitted : true
		},function(err,usertest){
			if(err){
				logger.error({"r":"ut_report","er":err,"p":req.body,"msg":"UserTestFind_DbError"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!usertest){
				logger.error({"r":"ut_report","p":req.body,"msg":"usertest_not_found"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			TestAnswersModule.fetch(
				test_id, 
				user_test_id, 
			function(err,answers_data){
				if(err){
					logger.error({"r":"ut_report","er":err,"p":req.body,"msg":"TestAnswersModule_Error"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}

				console.log('answers_data_from_: TestAnswersModule_fetch');
				console.log(answers_data);

				var finalData   = {};
				finalData.user_test_id      = user_test_id;
				finalData.test_id           = test_id;
				finalData.start_time        = usertest.start_time;
				finalData.time_taken        = usertest.time_taken;
				finalData.total_ques        = usertest.total_ques;
				finalData.total_marks       = usertest.total_marks;
				finalData.test_total_marks  = test.total_marks;
				finalData.ssol              = ssol;
				finalData.ansdata           = answers_data;
				finalData.sec_info          = catConfigs.CAT_TEST_SECTIONS,

				logger.info({"r":"subm","p":req.body,"msg":"Success"});
				return sendSuccess(res,finalData);
			});
		});
	});
});

router.post('/start', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()){ 
		logger.error({"r":"start","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id       = req.body.user_id.trim();
	var test_id       = req.body.test_id.trim();
	var plugin_id     = req.body.pl_id.trim();
	var gid           = req.body.g_id;
	var server_id     = req.decoded.id2;
	var tim           = new Date().getTime();

	Test.findOne({
		_id : test_id,
		act : true
	},function(err,test){
		if(err){
			logger.error({"r":"start","er":err,"p":req.body,"msg":"Test_find_DbError"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"start","msg":"Test Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		// if(!test.icat){
		// 	logger.error({"r":"start","msg":"test is not for cat","p":req.body});
		// 	return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		// }

		TestResumable.isPossible(user_id, test_id, function(err,resp){
			if(err){
				logger.error({"r":"start","er":err,"p":req.body,"msg":"getTestResumableValue_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(resp.is_resm == true){
				logger.info({"r":"start","msg":"Test is Resumable and hence Usertest start returns","p":req.body});
				return sendSuccess(res,{
					is_resumable : true,
					user_id      : user_id,
					test_id      : test_id,
					user_test_id : resp.user_test_id,
					auto_submit  : true
				});
			}

			UserTest.update({
				user_id 			: user_id,
				test_id 			: test_id,
				act           : true
			},{
				act : false
			},{
				multi : true
			},function(err,old_deactivated){
				if(err){
					logger.error({"r":"start","er":err,"p":req.body,"msg":"UserTest_update_DbError"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				var is_timed   = true;
				var max_tim    = catConfigs.CAT_TEST_MAXIMUM_TIME;
				var expirytime = catConfigs.JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME_CAT;
				var texpirytime= expirytime+catConfigs.JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME_BUFFER;
				var time_quanta= catConfigs.CAT_SECTION_MAX_TIME*1000;

				var object_for_at = {
					id1 : user_id,
					id2 : test_id,
					id3 : plugin_id,
					id4 : gid,
					itm : is_timed,
					mt  : max_tim,
					e1  : (tim+time_quanta),
					e2  : (tim+(2*time_quanta)),
					e3  : (tim+(3*time_quanta)),
					et  : (tim+(texpirytime)*1000)
				};

				var at = AuthSecurity.getCatTestAT(object_for_at, texpirytime);

				var new_user_test = new UserTest({
					user_id 			: user_id,
					pid 			    : server_id,
					test_id 			: test_id,
					catg_id       : test.catg_id,
					plugin_id     : plugin_id,
					gid           : gid,
					xcess_token   : at,
					test_type     : test.type,
					q_order       : test.q_order,
					is_started 		: true,
					is_submitted 	: false,
					start_time 		: tim,
					l_u_tim       : tim,
					total_ques 		: test.num_ques,
					total_marks		: test.total_marks,
					act           : true,
					lcatsec       : "A",
					lcatsecst     : tim,
					lcatsecut     : tim,
					lresmst       : tim,
					tconsm        : 0
				});

				getQuestionsData(test_id, "A", function(err, questions){
					if(err){
						logger.error({"r":"start","er":err,"p":req.body,"msg":"getQuestionsData_Error"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					new_user_test.save(function(err,new_user_test_created){
						if(err){
							logger.error({"r":"start","er":err,"p":req.body,"msg":"new_user_test_creation_DbError"});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						TestStatistics.updateAttempt(
							plugin_id, 
							user_id, 
							server_id, 
							test_id, 
							test.catg_id, 
						function(err,resp){
							if(err){
								logger.error({"r":"start","er":err,"p":req.body,"msg":"TestStatistics_updateAttempt_Error"});
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}
							var rtim         = catConfigs.CAT_INITIAL_REMAINING_TIME;
							var result_data = {
								is_resumable : false,
								user_test_id : new_user_test._id,
								test_id      : test_id,
								test_at      : at,
								num_ques     : questions.length,
								test_name    : test.name,
								test_type    : test.type,
								itm          : is_timed,
								rtim         : rtim,
								next_sec     : "B",
								ques         : questions,
								auto_submit  : true,
								sec_info     : catConfigs.CAT_TEST_SECTIONS,
								asec         : "A"
							}
							logger.info({"r":"start","p":req.body,"msg":"success","dt":result_data});
							return sendSuccess(res,result_data);
						});
					});
				});
			});

		});
	});
});

router.post('/start_f', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()){ 
		logger.error({"r":"start_f","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id       = req.body.user_id.trim();
	var test_id       = req.body.test_id.trim();
	var plugin_id     = req.body.pl_id.trim();
	var gid           = req.body.g_id;
	var server_id     = req.decoded.id2;
	var tim           = new Date().getTime();

	Test.findOne({
		_id : test_id,
		act : true
	},function(err,test){
		if(err){
			logger.error({"r":"start_f","er":err,"p":req.body,"msg":"Test_find_DbError"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"start_f","msg":"Test Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		// if(!test.icat){
		// 	logger.error({"r":"start_f","msg":"test is not for cat","p":req.body});
		// 	return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		// }

		UserTest.update({
			user_id 			: user_id,
			test_id 			: test_id,
			act           : true
		},{
			act : false
		},{
			multi : true
		},function(err,old_deactivated){
			if(err){
				logger.error({"r":"start_f","er":err,"p":req.body,"msg":"UserTest_update_DbError"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var is_timed   = true;
			var max_tim    = catConfigs.CAT_TEST_MAXIMUM_TIME;
			var expirytime = catConfigs.JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME_CAT;
			var texpirytime= expirytime+catConfigs.JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME_BUFFER;
			var time_quanta= catConfigs.CAT_SECTION_MAX_TIME;

			var object_for_at = {
				id1 : user_id,
				id2 : test_id,
				id3 : plugin_id,
				id4 : gid,
				itm : is_timed,
				mt  : max_tim,
				e1  : (tim+time_quanta),
				e2  : (tim+(2*time_quanta)),
				e3  : (tim+(3*time_quanta)),
				et  : (tim+(texpirytime)*1000)
			};

			var at = AuthSecurity.getCatTestAT(object_for_at, texpirytime);

			var new_user_test = new UserTest({
				user_id 			: user_id,
				pid 			    : server_id,
				test_id 			: test_id,
				catg_id       : test.catg_id,
				plugin_id     : plugin_id,
				gid           : gid,
				xcess_token   : at,
				test_type     : test.type,
				q_order       : test.q_order,
				is_started 		: true,
				is_submitted 	: false,
				start_time 		: tim,
				l_u_tim       : tim,
				total_ques 		: test.num_ques,
				total_marks		: test.total_marks,
				act           : true,
				lcatsec       : "A",
				lcatsecst     : tim,
				lcatsecut     : tim,
				lresmst       : tim,
				tconsm        : 0
			});

			getQuestionsData(test_id, "A", function(err, questions){
				if(err){
					logger.error({"r":"start_f","er":err,"p":req.body,"msg":"getQuestionsData_Error"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				new_user_test.save(function(err,new_user_test_created){
					if(err){
						logger.error({"r":"start_f","er":err,"p":req.body,"msg":"new_user_test_creation_DbError"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					TestStatistics.updateAttempt(
						plugin_id, 
						user_id, 
						server_id, 
						test_id, 
						test.catg_id, 
					function(err,resp){
						if(err){
							logger.error({"r":"start_f","er":err,"p":req.body,"msg":"TestStatistics_updateAttempt_Error"});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						var rtim         = catConfigs.CAT_INITIAL_REMAINING_TIME;
						var result_data = {
							is_resumable : false,
							user_test_id : new_user_test._id,
							test_id      : test_id,
							test_at      : at,
							num_ques     : questions.length,
							test_name    : test.name,
							test_type    : test.type,
							itm          : is_timed,
							rtim         : rtim,
							next_sec     : "B",
							ques         : questions,
							auto_submit  : true,
							sec_info     : catConfigs.CAT_TEST_SECTIONS,
							asec         : "A"
						}
						logger.info({"r":"start_f","p":req.body,"msg":"success","dt":result_data});
						return sendSuccess(res,result_data);
					});
				});
			});
		});
	});
});

router.post('/resume', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()){ 
		logger.error({"r":"resume","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id       = req.body.user_id.trim();
	var test_id       = req.body.test_id.trim();
	var user_test_id  = req.body.user_test_id.trim();
	var plugin_id     = req.body.pl_id.trim();
	var gid           = req.body.g_id;
	var server_id     = req.decoded.id2;
	var tim           = new Date().getTime();

	Test.findOne({
		_id : test_id,
		act : true
	},function(err,test){
		if(err){
			logger.error({"r":"resume","er":err,"p":req.body,"msg":"Test_find_DbError"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"resume","msg":"Test Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		// if(!test.icat){
		// 	logger.error({"r":"resume","msg":"test is not for cat","p":req.body});
		// 	return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		// }
		TestResumable.isPossibleInfo(user_id, test_id, tim, function(err,resume_response){
			if(err){
				logger.error({"r":"resume","er":err,"p":req.body,"msg":"getTestResumableValue_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(resume_response.is_resm == false){
				logger.info({"r":"resume","msg":"Test is Resumable and hence Usertest start returns","p":req.body,"resmd":resume_response});
				return sendError(res,"Test cannot not be resumed","test_could_not_be_resumed",HttpStatuses.SERVER_SPECIFIC_ERROR);
			}
			if(resume_response.user_test_id != user_test_id){
				logger.info({"r":"resume","msg":"test is resumable but user test id is mismatching","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			
			var is_timed   = true;
			var max_tim    = catConfigs.CAT_TEST_MAXIMUM_TIME;

			var object_for_at = {
				id1 : user_id,
				id2 : test_id,
				id3 : plugin_id,
				id4 : gid,
				itm : is_timed,
				mt  : max_tim,
				e1  : resume_response.allexpiry.e1,
				e2  : resume_response.allexpiry.e2,
				e3  : resume_response.allexpiry.e3,
				et  : (tim+(resume_response.allexpiry.et * 1000))
			};

			var at = AuthSecurity.getCatTestAT(object_for_at, resume_response.allexpiry.et);

			getResumeQuestionsData(test_id, resume_response.section, user_test_id, function(err, questions){
				if(err){
					logger.error({"r":"resume","er":err,"p":req.body,"msg":"getQuestionsData_Error"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				
				UserTest.update({
					_id : user_test_id
				},{
					lcatsec       : resume_response.asec,
					lcatsecut     : tim,
					lresmst       : tim,
					tconsm        : resume_response.time_consumed
				},function(err,updated_section_info){
					if(err){
						logger.error({"r":"resume","er":err,"p":req.body,"msg":"getQuestionsData_Error"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}

					var result_data = {
						is_resumable : true,
						user_test_id : user_test_id,
						test_id      : test_id,
						test_at      : at,
						num_ques     : questions.length,
						test_name    : test.name,
						test_type    : test.type,
						itm          : is_timed,
						rtim         : resume_response.rtim,
						next_sec     : resume_response.next_sec,
						ques         : questions,
						auto_submit  : true,
						sec_info     : catConfigs.CAT_TEST_SECTIONS,
						asec         : resume_response.asec
					}
					logger.info({"r":"resume","p":req.body,"msg":"success","dt":result_data});
					return sendSuccess(res,result_data);
				});
			});
		});
	});
});

function getQuestionsData(test_id, section, cb){
	TestQuestion.find({
		test_id : test_id,
		code    : section,
		act     : true
	},{
		ques_id : 1,
		ineg    : 1,
		negm    : 1,
		marks   : 1,
		key     : 1
	},function(err,testquestions){
		if(err){
			return cb(err,null);
		}
		if(testquestions.length == 0){
			return cb("no_testquestion_found_for_this_test",null);
		}
		
		var ques_ids = [];
		var tques    = testquestions.length;
		var testquestionsBYId = {};
		var qtemp;

		for(var i=0; i<tques; i++){
			qtemp = testquestions[i].ques_id+'';
			if(ques_ids.indexOf(qtemp) < 0){
				ques_ids.push(qtemp);
				testquestionsBYId[qtemp] = testquestions[i];
			}
		}

		Question.find({
			_id : {$in : ques_ids}
		},{
			type : 1,
			level: 1,
			text : 1,
			ans_options : 1,
			img_arr : 1,
			tim     : 1
		},function(err,questions){
			if(err){
				return cb(err,null);
			}
			if(questions.length == 0){
				return cb("No_Question_found_for_this_test",null);
			}
			var ques_arr = [];
			var totalQ   = questions.length;
			var temp, tempQ;
			for(var m=0; m<totalQ; m++){
				temp = testquestionsBYId[questions[m]._id];
				tempQ= {
					_id  : questions[m]._id,
					text : questions[m].text,
					ans_options : questions[m].ans_options
				}
				ques_arr.push(
					{
						question : tempQ,
						img_arr  : [],
						ineg     : temp.ineg,
						negm     : (!temp.negm) ? 0 : temp.negm,
						marks    : temp.marks,
						key      : temp.key
					}
				);
			}
			return cb(null,ques_arr);
		});
	});
}

function getResumeQuestionsData(test_id, section, user_test_id, cb){
	TestQuestion.find({
		test_id : test_id,
		code    : section,
		act     : true
	},{
		ques_id : 1,
		ineg    : 1,
		negm    : 1,
		marks   : 1,
		key     : 1
	},function(err,testquestions){
		if(err){
			return cb(err,null);
		}
		if(testquestions.length == 0){
			return cb("no_testquestion_found_for_this_test",null);
		}
		
		var ques_ids = [];
		var tques    = testquestions.length;
		var testquestionsBYId = {};
		var qtemp;

		for(var i=0; i<tques; i++){
			qtemp = testquestions[i].ques_id+'';
			if(ques_ids.indexOf(qtemp) < 0){
				ques_ids.push(qtemp);
				testquestionsBYId[qtemp] = testquestions[i];
			}
		}
		UserTestQuestion.find({
			user_test_id : user_test_id,
			is_attempted : true
		},{
			_id          : 0,
			ques_id 		 : 1,
			ans_no       : 1,
			is_attempted : 1
		},function(err,usertestquestions){
			if(err){
				return cb(err,null);
			}
			var total_subm_ques          = usertestquestions.length;
			var usertestquestionsByQueId = {};
			var temp_que;
			for(var i=0; i<total_subm_ques; i++){
				temp_que = usertestquestions[i].ques_id+''
				usertestquestionsByQueId[temp_que] = usertestquestions[i];
			}
			Question.find({
				_id : {$in : ques_ids}
			},{
				type : 1,
				level: 1,
				text : 1,
				ans_options : 1,
				img_arr : 1,
				tim     : 1
			},function(err,questions){
				if(err){
					return cb(err,null);
				}
				if(questions.length == 0){
					return cb("No_Question_found_for_this_test",null);
				}
				var ques_arr = [];
				var totalQ   = questions.length;
				var temp, tempQ, tempU;
				for(var m=0; m<totalQ; m++){
					temp = testquestionsBYId[questions[m]._id];
					tempQ= {
						_id  : questions[m]._id,
						text : questions[m].text,
						ans_options : questions[m].ans_options
					}
					tempU = usertestquestionsByQueId[questions[m]._id+''];
					if(tempU && tempU.is_attempted == true){
						tempQ["is_attempted"] = true;
						tempQ["ans_no"]       = tempU.ans_no;
					} else {
						tempQ["is_attempted"] = false;
					}
					ques_arr.push(
						{
							question : tempQ,
							img_arr  : [],
							ineg     : temp.ineg,
							negm     : (!temp.negm) ? 0 : temp.negm,
							marks    : temp.marks,
							key      : temp.key
						}
					);
				}
				return cb(null,ques_arr);
			});
		});
	});
}

module.exports = router;