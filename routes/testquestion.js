var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    							  = require('jsonwebtoken');

var configs					      = require('../configs/configs.js');
var HttpStatuses					= require('../global/http_api_return_codes.js');
var ApiReturnHandlers			= require('../global/api_return_handlers.js');
var errorCodes 						= require('../global/error_codes.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');

require('../models/tests.js');
require('../models/testquestions.js');
require('../models/usertestquestions.js');
require('../models/usertests.js');
require('../models/questions.js');
require('../models/users.js');
require('../models/pluginpaidinfos.js');
var User                  = mongoose.model('User');
var Test 									= mongoose.model('Test');
var TestQuestion					= mongoose.model('TestQuestion');
var UserTestQuestion		  = mongoose.model('UserTestQuestion');
var UserTest		          = mongoose.model('UserTest');
var Question					    = mongoose.model('Question');
var PluginPaidInfo 				= mongoose.model('PluginPaidInfo');

var sendError 						= ApiReturnHandlers.sendError;
var sendSuccess 					= ApiReturnHandlers.sendSuccess;
var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_TESTQUESTION);
var DEFAULT_NUM_QUESTIONS_FOR_TEST = configs.DEFAULT_NUM_QUESTIONS_FOR_TEST;

//route middleware to verify if plugin in group is paid
router.use(function(req, res, next){
	var tim         = new Date().getTime();
	var plugin_id   = req.body.pl_id;
	var token       = req.body.tokn;
	var gid         = req.body.g_id;
	var user_id     = req.body.user_id;
	
	if(!token || token == 'null' || token === "undefined"){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body,"tkn":token});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!gid){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"gid_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","gid_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!plugin_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"plugin_id_missing","p":req.body});
	  return sendError(res,"Plugin Id Not Found","plugin_id_missing",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			req.private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			req.private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
		req.tim = tim;
	  next();
	});
});

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
  jwt.verify(req.body.tokn, req.private_key, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != req.body.user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != req.body.pl_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_app_id","p":req.body});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id4 != req.body.g_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body});
	  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    next();
  });
});

//route middleware to verify the jwt user test token
router.use(function(req, res, next){
	var token     = req.body.t_tokn;
	var user_id   = req.body.user_id;
	var test_id   = req.body.test_id;
	if(!token){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","invalid_parameters",HttpStatuses.FORBIDDEN);
	}
	if(!test_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","invalid_parameters",HttpStatuses.FORBIDDEN);
	}
	req.decoded = null;
  jwt.verify(token, configs.JWT_USERTEST_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"test_token_jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate Test token.","usertest_token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid id is not authorised","invalid_usertest_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id2 != test_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid test id is not authorised","invalid_usertest_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != req.body.pl_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_app_id","p":req.body,});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_usertest_token",HttpStatuses.FORBIDDEN);
    }
    req.decoded = decoded;
    next();
  });
});

router.post('/get_s', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"get_s","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id     = req.body.user_id.trim();
	var test_id     = req.body.test_id.trim();
	var tim         = new Date().getTime();

	Test.findOne({
		_id : test_id,
		act : true
	},function(err, test){
		if(err){
			logger.error({"r":"get_s","er":err,"p":req.body,"er":err});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"get_s","msg":"no Test Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		test = null
		var low  = 0;
		var high = test.num_ques;
		var key  = generateRandomeKeys(1, low, high);
		TestQuestion.find({
			key : key,
			act : true
		},function(err,testquestion){
			if(err){
				logger.error({"r":"get_s","er":err,"p":req.body,"er":err});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!testquestion){
				logger.error({"r":"get_s","msg":"no Test Question Found","p":req.body});
				return sendError(res,"No Cateogries Found","no_testquestion_found",HttpStatuses.NO_DATA);
			}
			var question_id = testquestion.ques_id;
			Question.findOne({
				_id : question_id
			},{
				hint_text    : 0,
				hint_img_url : 0,
				hint_aud_url : 0,
				correct_ans  : 0
			},function(err,question){
				if(err){
					logger.error({"r":"get_s","er":err,"p":req.body,"er":err});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				return sendSuccess(res,{question:question});
			});
		})
	});
});

router.post('/get_m', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('start',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkBody('ipp',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"get_m","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	var user_id     = req.body.user_id.trim();
	var test_id     = req.body.test_id.trim();
	var user_test_id= req.body.user_test_id.trim();
	var start       = parseInt(req.body.start);
  var ipp         = parseInt(req.body.ipp);
	var tim         = req.tim;
	var expTime     = (req.decoded.et) ? req.decoded.et : (tim+(configs.JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME*1000));

	Test.findOne({
		_id : test_id,
		act : true
	},function(err, test){
		if(err){
			logger.error({"r":"get_m","er":err,"p":req.body,"er":err});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"get_m","msg":"no Test Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(!test.num_ques || test.num_ques <= 0){
			logger.error({"r":"get_m","msg":"Test does not have Number of questions field","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		getTestQuestionBasedOnShuffling(test.q_order, user_test_id, test_id, start, ipp, function(err,testquestions){
			if(err){
				logger.error({"r":"get_m","er":err,"p":req.body,"msg":"getTestQuestionBasedOnShuffling"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(testquestions.length == 0){
				logger.error({"r":"get_m","msg":"no Test Questions Found","p":req.body});
				return sendError(res,"No Cateogries Found","no_testquestions_found",HttpStatuses.NO_DATA);
			}
			var question_ids = [];
			var total        = testquestions.length;
			var testquestionsBYId = {};
			for(var i=0; i<total; i++){
				question_ids.push(testquestions[i].ques_id+'');
				testquestionsBYId[testquestions[i].ques_id+''] = testquestions[i];
			}
			Question.find({
				_id : { $in : question_ids }
			},{
				text        : 1,
				img_arr     : 1,
				ans_options : 1,
				correct_ans : 1
			},function(err,questions){
				if(err){
					logger.error({"r":"get_m","er":err,"p":req.body,"er":err});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				var ques_arr = [];
				var totalQ   = questions.length;
				var temp, tempQ;
				for(var m=0; m<totalQ; m++){
					temp = testquestionsBYId[questions[m]._id];
					tempQ= {
						_id  : questions[m]._id,
						text : questions[m].text,
						ans_options : questions[m].ans_options
					}
					ques_arr.push(
						{
							question : tempQ,
							img_arr  : [],
							ineg     : temp.ineg,
							negm     : (!temp.negm) ? 0 : temp.negm,
							marks    : temp.marks,
							key      : temp.key
						}
					);
				}
				var isTimed = false;
				var rtim    = (expTime-(new Date().getTime()));
				if(test.timed && (test.timed == true || test.timed == 'true')){
					isTimed = true;
				} 
				UserTest.update({
					_id : user_test_id
				},{
					l_u_tim : tim
				},function(err,resp){
					if(err){
						logger.error({"r":"get_m","er":err,"p":req.body,"msg":"UserTest_Update_DbErrro"});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					logger.info({"r":"get_m","msg":"success","p":req.body,"data":{ques_arr:ques_arr,itm:isTimed,rtim:rtim}});
					return sendSuccess(res,{ques_arr:ques_arr,itm:isTimed,rtim:rtim,total:test.num_ques});
				});
			});
		})
	});
});

function getTestQuestionBasedOnShuffling(question_order, user_test_id, test_id, start, ipp, cb){
	TestQuestion.find({
		test_id : test_id,
		act : true
	},{},{
		sort : {
			key : 1
		},
		skip  : start,
    limit : ipp
	},function(err,testquestions){
		if(err){
			return cb(err,null);
		}
		if(question_order == Test.QUESTION_ORDER.ORDERED){
			return cb(null,testquestions);
		}
		return cb(null,shuffleArray(testquestions));
	});
}

function generateRandomKeys(num_of_keys, low, high){
	var keys = [];
	for(var i=0; i<num_of_keys; i++){
		var key = Math.floor(Math.random() * high) + 1;
		if(keys.indexOf(key) < 0){
			keys.push(key);
		} else {
			num_of_keys++;
		}
	}
	return keys;
}

function shuffleArray(a) {
  var j, x, i;
  for (i = a.length; i; i--) {
    j = Math.floor(Math.random() * i);
    x = a[i - 1];
    a[i - 1] = a[j];
    a[j] = x;
  }
  return a;
}


module.exports = router;