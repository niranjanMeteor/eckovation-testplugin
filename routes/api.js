const express 							= require('express');
const mongoose 							= require('mongoose');
const jwt    							  = require('jsonwebtoken');

const configs					      = require('../configs/configs.js');
const HttpStatuses					= require('../global/http_api_return_codes.js');
const ApiReturnHandlers			= require('../global/api_return_handlers.js');
const errorCodes 						= require('../global/error_codes.js');
const log4jsLogger  				= require('../loggers/log4js_module.js');
const log4jsConfigs  			  = require('../loggers/log4js_configs.js');
const CompletionProficiency = require('../analysisdata/completion_proficiency.js');
const StudentRanking        = require('../analysisdata/student_ranking.js');

require('../models/users.js');
require('../models/questions.js');
require('../models/categories.js');
require('../models/tests.js');
require('../models/usertests.js');
require('../models/testquestions.js');
require('../models/usertestquestions.js');
require('../models/groupstats.js');
require('../models/pluginpaidinfos.js');
require('../models/apitokens.js');

const User                  = mongoose.model('User');
const Category 							= mongoose.model('Category');
const Test 									= mongoose.model('Test');
const UserTest 							= mongoose.model('UserTest');
const TestQuestion					= mongoose.model('TestQuestion');
const UserTestQuestion		  = mongoose.model('UserTestQuestion');
const GroupStats		        = mongoose.model('GroupStats');
const PluginPaidInfo 				= mongoose.model('PluginPaidInfo');
const Question 							= mongoose.model('Question');
const Apitoken 							= mongoose.model('Apitoken');

const sendError 						= ApiReturnHandlers.sendError;
const sendSuccess 					= ApiReturnHandlers.sendSuccess;
const router 								= express.Router();
const logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_ANALYTICS);

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
	Apitoken.find({
		key : req.body.key,
		secret : req.body.secret,
		act : true,
	},function(err,apitokens){
		console.log(apitokens);

		if(err) {
			logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.FORBIDDEN);  
		}

		if(apitokens.length == 0) {
			logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.FORBIDDEN);  
		}

		next();
	});
});

router.post('/group_data', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();

	var g_id    = req.body.g_id;

	//gid > category > tests > usertests > usertestquestions
	//gid > category > tests > testquestions
	Category.find({
		gid : g_id,
		act : true,
	},function(err,catg){
		if(err) {
			console.trace(err);
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}

		console.log("Catg:");
		console.log(catg);

		var catg_ids = [];

		for(var i = 0 ; i < catg.length; i++) {
			catg_ids.push(catg[i]["_id"]);
		}

		Test.find({
			catg_id : {$in : catg_ids},
			act : true,
		},function(err,tests){
			if(err) {
				console.trace(err);
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}

			console.log("Tests:");
			console.log(tests);

			var test_ids = [];

			for(var i = 0 ; i < tests.length; i++) {
				test_ids.push(tests[i]["_id"]);
			}

			var all_user_ids = [];

			UserTest.find({
				test_id : {$in : test_ids},
				is_first:true,is_submitted:true
			},function(err,usertests){
				if(err) {
					console.trace(err);
					return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				}

				console.log("Usertests:");
				console.log(usertests);

				var final_usertests = [];

				var usertest_ids = [];

				for(var i = 0; i < usertests.length; i++) {
					usertest_ids.push(usertests[i]["_id"]);
					all_user_ids.push(usertests[i]["user_id"]);
				}

				User.find({
					_id : {
						$in : all_user_ids
					}
				},function(err,users){
					if(err) {
						console.trace(err);
						return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
					}

					var user_names_by_id = {};

					users.forEach(function(user){
						user_names_by_id[user["_id"]] = user["name"];
					});

					usertests.forEach(function(usertest){
						usertest = JSON.parse(JSON.stringify(usertest));
						usertest["name"] = user_names_by_id[usertest["user_id"]];
						final_usertests.push(usertest);
					});

					UserTestQuestion.find({
						gid : g_id,
						act : true,
					},{
						user_test_id : 1,
						is_attempted : 1,
						ques_key : 1,
						ques_id : 1,
						feedback : 1,
						ans_text : 1,
						ans_no : 1
					},function(err,usertestquestions) {
						if(err) {
							console.trace(err);
							return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
						}

						console.log("UserTestQuestion:");
						console.log(usertestquestions);

						TestQuestion.find({
							test_id : {$in : test_ids},
							act : true,
						},function(err,test_questions) {
							if(err) {
								console.trace(err);
								return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
							}

							console.log("TestQuestion:");
							console.log(test_questions);

							var question_ids = [];

							for(var i = 0; i < test_questions.length; i++){
								question_ids.push(test_questions[i]["ques_id"]);
							}

							Question.find({
									_id : {$in : question_ids}
							},function(err,ques){
								if(err) {
									console.trace(err);
									return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
								}

								console.log("Question:");
								console.log(ques);

								return sendSuccess(res,{questions:ques,test_questions:test_questions,tests:tests,usertests:final_usertests,usertestquestions:usertestquestions});
							});
						});
					});
				});
			});
		});
	});
});

module.exports = router;
