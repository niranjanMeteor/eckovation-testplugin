const express 							= require('express');
const mongoose 							= require('mongoose');
const jwt    							  = require('jsonwebtoken');

const configs					      = require('../configs/configs.js');
const HttpStatuses					= require('../global/http_api_return_codes.js');
const ApiReturnHandlers			= require('../global/api_return_handlers.js');
const errorCodes 						= require('../global/error_codes.js');
const log4jsLogger  				= require('../loggers/log4js_module.js');
const log4jsConfigs  			  = require('../loggers/log4js_configs.js');
const AuthSecurity      		= require('../security/auth_tokens.js');
const RootServerApi      		= require('../server/apicalls.js');
const MailClient            = require('../mail/mail_client.js');
const CertificateModule     = require('../certificates/module.js');
const TestReviewModule      = require('../review/test.js');
const UserTestResultsByTag  = require('../lib/tags/usertagresults.js');

require('../models/users.js');
require('../models/categories.js');
require('../models/tests.js');
require('../models/questions.js');
require('../models/usertests.js');
require('../models/testquestions.js');
require('../models/usertestquestions.js');
require('../models/groupstats.js');
require('../models/pluginpaidinfos.js');
const User                  = mongoose.model('User');
const Category 							= mongoose.model('Category');
const Test 									= mongoose.model('Test');
const Question					    = mongoose.model('Question');
const UserTest 							= mongoose.model('UserTest');
const TestQuestion					= mongoose.model('TestQuestion');
const UserTestQuestion		  = mongoose.model('UserTestQuestion');
const GroupStats		        = mongoose.model('GroupStats');
const PluginPaidInfo 				= mongoose.model('PluginPaidInfo');

const sendError 						= ApiReturnHandlers.sendError;
const sendSuccess 					= ApiReturnHandlers.sendSuccess;
const router 								= express.Router();
const logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_USERTEST);
const answerLogger          = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_USERANSWERS_SUBMIT);


//route middleware to verify if plugin in group is paid
router.use(function(req, res, next){
	var tim         = new Date().getTime();
	var plugin_id   = req.body.pl_id;
	var token       = req.body.tokn;
	var gid         = req.body.g_id;
	var user_id     = req.body.user_id;
	if(!token || token == 'null' || token === "undefined"){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body,"tkn":token});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!gid){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"gid_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","gid_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!plugin_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"plugin_id_missing","p":req.body});
	  return sendError(res,"Plugin Id Not Found","plugin_id_missing",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			req.private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			req.private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
		req.tim = tim;
	  next();
	});
});

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
  jwt.verify(req.body.tokn, req.private_key, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != req.body.user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != req.body.pl_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_app_id","p":req.body});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id4 != req.body.g_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body});
	  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    next();
  });
});

router.post('/score_breakup', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"score_breakup","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id       = req.body.user_id.trim();
	var plugin_id     = req.body.pl_id.trim();
	var gid           = req.body.g_id;

	UserTest.find({
		user_id      : user_id,
		gid          : gid,
		plugin_id    : plugin_id,
		is_submitted : true
	},{
		test_id        : 1,
		catg_id        : 1,
		marks_obtained : 1,
		start_time     : 1
	},{
		sort : {
			start_time : 1
		}
	},function(err,usertests){
		if(err){
			logger.error({"r":"score_breakup","er":err,"p":req.body,"msg":"Test_find_DbError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		var finalData = [];
		var testIds   = [];
		var total     = usertests.length;
		var total_marks = 0;
		var temp;
		var category_ids = [];
		for(var i=0; i<total; i++){
			temp = usertests[i];
			if(testIds.indexOf(temp.test_id+'') < 0){
				finalData.push(temp);
				testIds.push(temp.test_id+'');
				total_marks = total_marks + temp.marks_obtained;
			}
			if(category_ids.indexOf(temp.catg_id+'') < 0){
				category_ids.push(temp.catg_id+'');
			}
		}
		Category.find({
			_id : {$in:category_ids}
		},{
			name : 1
		},function(err,categories){
			if(err){
				logger.error({"r":"score_breakup","er":err,"p":req.body,"msg":"Category_find_DbError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			Test.find({
				_id : {$in : testIds}
			},{
				name : 1
			},function(err,tests){
				if(err){
					logger.error({"r":"score_breakup","er":err,"p":req.body,"msg":"Test_find_DbError"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				var catg_t       = categories.length;
				var categoryById = {};
				var test_t       = tests.length;
				var testById     = {};

				for(var m=0; m<catg_t; m++){
					categoryById[categories[m]._id+''] = categories[m].name;
				}
				for(var n=0; n<catg_t; n++){
					testById[tests[n]._id+''] = tests[n].name;
				}
				logger.info({"r":"score_breakup","p":req.body,"msg":"ScoreBreakUp_Success","data":finalData});
				return sendSuccess(res,{usertests:finalData, total_marks:total_marks,tests:testById,categories:categoryById});
			});
		});
	});
});

router.post('/g_ut_report', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"g_ut_report","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id       = req.body.user_id.trim();
	var test_id       = req.body.test_id.trim();
	var user_test_id  = req.body.user_test_id.trim();
	var if_full_set   = false;
	var tim           = new Date().getTime();
	var test_model;

	Test.findOne({
		_id : test_id,
		act : true
	},function(err,test){
		if(err){
			logger.error({"r":"g_ut_report","er":err,"p":req.body,"er":err,"msg":"Test_find_DbError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"g_ut_report","msg":"Test Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(("ssol" in test) && test.ssol == false){
			logger.error({"r":"g_ut_report","msg":"Test Not Authorised for Showing Solutions","p":req.body,"test":test});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(!test.model){
			test_model = Test.MODEL.REGULAR;
		} else {
			test_model = test.model;
		}
		UserTest.findOne({
			_id 					: user_test_id,
			user_id 			: user_id,
			test_id 			: test_id,
			is_submitted 	: true,
		},function(err,usertest){
			if(err){
				logger.error({"r":"g_ut_report","er":err,"p":req.body,"er":err,"msg":"UserTest_find_DbError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!usertest){
				logger.error({"r":"g_ut_report","msg":"User Test Not Found","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			if(("i_fl_st" in test) && test.i_fl_st == true){
				if_full_set = true;
			}
			getQuestionIdsToShowSolutions(
				test, 
				if_full_set, 
				user_test_id,
				usertest.total_marks,
				usertest.total_correct,
			function(err,resp_data){
				if(err){
					logger.error({"r":"g_ut_report","err":err,"msg":"getQuestionIdsToShowSolutions_Error","p":req.body,"d":resp_data});
					return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				}
				// console.log('getQuestionIdsToShowSolutions');
				// console.log(resp_data);
				var question_ids      = resp_data.ques_ids;
				var usertestquestions = resp_data.usertestquestions;

				if(question_ids.length == 0){
					logger.error({"r":"g_ut_report","msg":"No_Question_iD_found_in_UserTestQuestion","p":req.body});
					return sendError(res,"no_usertest_report","no_usertest_report",HttpStatuses.BAD_REQUEST);
				}
				Question.find({
					_id : { $in : question_ids}
				},{
					text : 1,
					ans_options : 1,
					correct_ans : 1,
					expl        : 1
				},function(err,questions){
					if(err){
						logger.error({"r":"g_ut_report","er":err,"p":req.body,"er":err,"msg":"UserTestQuestion_find_DbError"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(questions.length == 0){
						logger.error({"r":"g_ut_report","msg":"Question_iD_UserTestQuestion_not_found_in_Question","p":req.body});
						return sendError(res,"no_usertest_report","no_usertest_report",HttpStatuses.BAD_REQUEST);
					}
					var questions_data = {};
					var total_q = questions.length;
					for(var j=0; j<total_q; j++){
						questions_data[questions[j]._id+''] = questions[j];
					}
					var ssol = false;
					if(test.ssol && test.ssol == true){
						ssol = true;
					}
					var tlen = resp_data.allAtmpAndCrct;
					var returnData = {
						s_time 		  : usertest.start_time,
						e_time   		: usertest.end_time,
						t_ques      : usertest.total_ques,
						t_marks     : usertest.total_marks,
						t_correct 	: usertest.total_correct,
						i_a_correct : usertest.if_all_correct,
						marks_o     : usertest.marks_obtained,
						u_ques      : usertestquestions,
						all_ques    : questions_data,
						test_model  : test_model,
						ssol        : ssol,
						test_id     : test_id, 
						user_test_id: user_test_id,
						allAtmpAndCrct : resp_data.allAtmpAndCrct
					};
					prepareExtraSubmissionData(
						user_id, 
						user_test_id, 
						test, 
						resp_data.answers,
						resp_data.bulkUpdateData, 
						usertest.time_taken,
						usertest.marks_obtained,
						usertest.total_correct,
						"g_ut_report",
					function(err,extraData){
						if(err){
							logger.error({"r":"g_ut_report","er":err,"p":req.body,"er":err,"msg":"prepareExtraSubmissionData_Error"});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						for(var key in extraData){
							returnData[key] = extraData[key];
						}
						return sendSuccess(res,{report:returnData});
					});
				});
			});
		});
	});
});

router.post('/g_oth_ut_report', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('oth_user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('oth_user_test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"g_oth_ut_report","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var oth_user_id       = req.body.oth_user_id.trim();
	var test_id           = req.body.test_id.trim();
	var oth_user_test_id  = req.body.oth_user_test_id.trim();
	var if_full_set       = false;
	var tim               = new Date().getTime();
	var usertestquestions;
	var test_model;

	Test.findOne({
		_id : test_id,
		act : true
	},function(err,test){
		if(err){
			logger.error({"r":"g_oth_ut_report","er":err,"p":req.body,"er":err,"msg":"Test_find_DbError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"g_oth_ut_report","msg":"Test Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(("ssol" in test) && test.ssol == false){
			logger.error({"r":"g_oth_ut_report","msg":"Test Not Authorised for Showing Solutions","p":req.body,"test":test});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(!test.model){
			test_model = Test.MODEL.REGULAR;
		} else {
			test_model = test.model;
		}
		User.findOne({
			_id : oth_user_id,
			act : true
		},{
			name : 1
		},function(err,user){
			if(err){
				logger.error({"r":"g_oth_ut_report","er":err,"p":req.body,"er":err,"msg":"User_find_DbError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!user){
				logger.error({"r":"g_oth_ut_report","msg":"User Not Found","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			UserTest.findOne({
				_id 					: oth_user_test_id,
				user_id 			: oth_user_id,
				test_id 			: test_id,
				is_submitted 	: true,
			},function(err,usertest){
				if(err){
					logger.error({"r":"g_oth_ut_report","er":err,"p":req.body,"er":err,"msg":"UserTest_find_DbError"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!usertest){
					logger.error({"r":"g_oth_ut_report","msg":"User Test Not Found","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				}
				if(("i_fl_st" in test) && test.i_fl_st == true){
					if_full_set = true;
				}
				getQuestionIdsToShowSolutions(
					test, 
					if_full_set, 
					oth_user_test_id, 
					usertest.total_marks,
					usertest.total_correct,
				function(err,resp_data){
					if(err){
						logger.error({"r":"g_oth_ut_report","err":err,"msg":"getQuestionIdsToShowSolutions_Error","p":req.body,"d":resp_data});
						return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
					}
					question_ids      = resp_data.ques_ids;
					usertestquestions = resp_data.usertestquestions;
					// console.log('question_ids length : '+question_ids.length);
					// console.log('usertestquestions length : '+usertestquestions.length);
					if(question_ids.length == 0){
						logger.error({"r":"g_oth_ut_report","msg":"No_Question_iD_found_in_UserTestQuestion","p":req.body});
						return sendError(res,"no_usertest_report","no_usertest_report",HttpStatuses.BAD_REQUEST);
					}
					Question.find({
						_id : { $in : question_ids}
					},{
						text : 1,
						ans_options : 1,
						correct_ans : 1,
						expl        : 1
					},function(err,questions){
						if(err){
							logger.error({"r":"g_oth_ut_report","er":err,"p":req.body,"er":err,"msg":"UserTestQuestion_find_DbError"});
							console.trace(err);
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						if(questions.length == 0){
							logger.error({"r":"g_oth_ut_report","msg":"Question_iD_UserTestQuestion_not_found_in_Question","p":req.body});
							return sendError(res,"no_usertest_report","no_usertest_report",HttpStatuses.BAD_REQUEST);
						}
						var questions_data = {};
						var total_q = questions.length;
						for(var j=0; j<total_q; j++){
							questions_data[questions[j]._id+''] = questions[j];
						}
						var ssol = false;
						if(test.ssol && test.ssol == true){
							ssol = true;
						}
						var tlen = resp_data.allAtmpAndCrct;
						var returnData = {
							s_time 		  : usertest.start_time,
							e_time   		: usertest.end_time,
							t_ques      : usertest.total_ques,
							t_marks     : usertest.total_marks,
							t_correct 	: usertest.total_correct,
							i_a_correct : usertest.if_all_correct,
							marks_o     : usertest.marks_obtained,
							u_ques      : usertestquestions,
							all_ques    : questions_data,
							test_model  : test_model,
							ssol        : ssol,
							test_id     : test_id, 
							user_test_id: oth_user_test_id,
							allAtmpAndCrct : resp_data.allAtmpAndCrct
						};
						prepareExtraSubmissionData(
							oth_user_id, 
							oth_user_test_id, 
							test, 
							resp_data.answers,
							resp_data.bulkUpdateData, 
							usertest.time_taken,
							usertest.marks_obtained,
							usertest.total_correct,
							"g_ut_report",
						function(err,extraData){
							if(err){
								logger.error({"r":"g_ut_report","er":err,"p":req.body,"er":err,"msg":"prepareExtraSubmissionData_Error"});
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}
							for(var key in extraData){
								returnData[key] = extraData[key];
							}
							return sendSuccess(res,{report:returnData});
						});
						// var returnData = {
						// 	s_time 		  : usertest.start_time,
						// 	e_time   		: usertest.end_time,
						// 	t_ques      : usertest.total_ques,
						// 	t_marks     : usertest.total_marks,
						// 	t_correct 	: usertest.total_correct,
						// 	i_a_correct : usertest.if_all_correct,
						// 	marks_o     : usertest.marks_obtained,
						// 	u_ques      : usertestquestions,
						// 	all_ques    : questions_data,
						// 	test_model  : test_model,
						// 	name        : user.name,
						// 	allAtmpAndCrct : resp_data.allAtmpAndCrct
						// };
						// prepPsychometeryResult(oth_user_id, test, oth_user_test_id, function(err,psychometric_data){
						// 	if(err){
						// 		logger.error({"r":end_v2,"err":err,"p":req.body});
						// 	}
						// 	if(psychometric_data){
						// 		returnData.data = psychometric_data;
						// 	}
						// 	if(test.model && test.model == Test.MODEL.PSYCHOMETRY){
						// 		returnData.is_psyo = true;
						// 	}
						// 	if(!test.sub_model){
						// 		returnData.sub_model = 1;
						// 	} else {
						// 		returnData.sub_model = test.sub_model;
						// 	}
						// 	if(test.model && test.model == Test.MODEL.SURVEY){
						// 		returnData.is_feedback = true;
						// 	}
						// 	var x_title,y_title;
						// 	if(test.sub_model == 1){
						// 		x_title = "FEEDBACK";
						// 		y_title = "EXPOSURE";
						// 	} else if(test.sub_model == 2){
						// 		x_title = "X";
						// 		y_title = "Y";
						// 	} else if(test.sub_model == 3) {
						// 		x_title = "X";
						// 		y_title = "Y";
						// 	} else {
						// 		x_title = "X";
						// 		y_title = "Y";
						// 	}
						// 	returnData.x_title = x_title;
						// 	returnData.y_title = y_title;
						// 	// console.log('ut report data is');
						// 	// console.log(returnData);
						// 	return sendSuccess(res,{report:returnData});
						// });
					});
				});
			});
		});
	});
});

router.post('/usr_spcf_ldb', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('oth_user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"usr_spcf_ldb","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id       = req.body.user_id.trim();
	var oth_user_id   = req.body.oth_user_id.trim();
	var test_id       = req.body.test_id.trim();

	User.findOne({
		_id : user_id,
		act : true
	},{
		identifier : 1
	},function(err,user){
		if(err){
			logger.error({"r":"usr_spcf_ldb","er":err,"p":req.body,"er":err,"msg":"User_find_DbError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!user){
			logger.error({"r":"usr_spcf_ldb","msg":"User Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		RootServerApi.checkIfAdmin(user.identifier, function(err,resp){
			if(err){
				logger.error({"url":"usr_spcf_ldb","msg":"rootServer_checkIfAdmin_err","er":err,"p":req.body});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!resp.success){
				logger.error({"url":"usr_spcf_ldb","msg":"rootServer_checkIfAdmin_failed","p":req.body});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var is_admin = false;
			if(("is_admin" in resp)  && resp.is_admin == true){
				is_admin = true;
			}
			Test.findOne({
				_id : test_id,
				act : true
			},function(err,test){
				if(err){
					logger.error({"r":"usr_spcf_ldb","er":err,"p":req.body,"msg":"Test_find_DbError"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!test){
					logger.error({"r":"usr_spcf_ldb","msg":"Test Not Found","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				}
				UserTest.find({
					user_id      : oth_user_id,
					test_id      : test_id,
					is_submitted : true
				},{
					_id            : 1,
					user_id        : 1,
					marks_obtained : 1,
					start_time     : 1,
					time_taken     : 1,
					total_correct  : 1
				},{
					sort : {
						start_time : 1
					}
				},function(err,usertests){
					if(err){
						logger.error({"r":"usr_spcf_ldb","er":err,"p":req.body,"msg":"UserTest_find_DbError"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					logger.info({"r":"usr_spcf_ldb","p":req.body,"msg":"User_Test_Specific_Ldb_Success"});
					return sendSuccess(res,{usertests:usertests,is_admin:is_admin});
				});
			});
		});
	});
});

router.post('/s_ut_fdb', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('feedback',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"s_ut_fdb","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id       = req.body.user_id.trim();
	var test_id       = req.body.test_id.trim();
	var user_test_id  = req.body.user_test_id.trim();
	var feedback      = req.body.feedback.trim();
	var tim           = new Date().getTime();

	Test.findOne({
		_id : test_id,
		act : true
	},function(err,test){
		if(err){
			logger.error({"r":"s_ut_fdb","er":err,"p":req.body,"er":err,"msg":"Test_find_DbError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"s_ut_fdb","msg":"Test Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		UserTest.findOne({
			_id 					: user_test_id,
			user_id 			: user_id,
			test_id 			: test_id,
			is_submitted 	: true,
		},function(err,usertest){
			if(err){
				logger.error({"r":"s_ut_fdb","er":err,"p":req.body,"er":err,"msg":"UserTest_find_DbError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!usertest){
				logger.error({"r":"s_ut_fdb","msg":"User Test Not Found","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			UserTest.update({
				_id : user_test_id
			},{
				feedback : feedback
			},function(err,usertest_updated){
				if(err){
					logger.error({"r":"s_ut_fdb","er":err,"p":req.body,"er":err,"msg":"UserTest_find_DbError"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				return sendSuccess(res,{});
			});
		});
	});
});

router.post('/start', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()){ 
		logger.error({"r":"start","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id       = req.body.user_id.trim();
	var test_id       = req.body.test_id.trim();
	var plugin_id     = req.body.pl_id.trim();
	var gid           = req.body.g_id;
	var tim           = new Date().getTime();

	User.findOne({
		_id : user_id,
		act : true
	},{
		server_id : 1
	},function(err,user){
		if(err){
			logger.error({"r":"start","er":err,"p":req.body,"msg":"User_find_DbError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!user){
			logger.error({"r":"start","msg":"User Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		Test.findOne({
			_id : test_id,
			act : true
		},function(err,test){
			if(err){
				logger.error({"r":"start","er":err,"p":req.body,"msg":"Test_find_DbError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!test){
				logger.error({"r":"start","msg":"Test Not Found","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			getTestResumableValue(user_id, test_id, test, function(err,resp){
				if(err){
					logger.error({"r":"start","er":err,"p":req.body,"msg":"getTestResumableValue_Error"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(resp.autosubmit && resp.autosubmit == true){
					logger.info({"r":"start","msg":"Test is true for AutoSubmit and hence Usertest start returns","p":req.body});
					return processTimedUsertesForAutoSubmit("start", res, req.body, test, resp);
				}
				if(resp.is_resm == true){
					logger.info({"r":"start","msg":"Test is Resumable and hence Usertest start returns","p":req.body});
					return sendSuccess(res,{
						is_resumable : true,
						user_id      : user_id,
						test_id      : test_id,
						user_test_id : resp.user_test_id,
						adpv         : test.adpv,
						adpstg       : test.adpstg
					});
				}
				UserTest.update({
					user_id 			: user_id,
					test_id 			: test_id,
					act           : true
				},{
					act : false
				},{
					multi : true
				},function(err,old_deactivated){
					if(err){
						logger.error({"r":"start","er":err,"p":req.body,"msg":"UserTest_update_DbError"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					var is_timed = false;
					var max_tim  = 0;
					var expirytime = configs.JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME;
					if(("timed" in test) 
						&& (test.timed == true || test.timed == 'true') 
						&& (test.max_tim > configs.MIN_TIME_FOR_TIMED_TEST))
					{
						is_timed = true;
						max_tim  = test.max_tim;
						expirytime = (max_tim / 1000) + configs.EXTRA_BUFFER_FOR_TEST_SUBMIT
					}
					var object_for_at = {
						id1 : user_id,
						id2 : test_id,
						id3 : plugin_id,
						itm : is_timed,
						mt  : max_tim,
						et  : (tim+max_tim)
					};
					var at = AuthSecurity.getTestAT(object_for_at, expirytime);
					var new_user_test_obj = {
						user_id 			: user_id,
						pid 			    : user.server_id,
						test_id 			: test_id,
						catg_id       : test.catg_id,
						plugin_id     : plugin_id,
						tlvl          : test.level,
						gid           : gid,
						xcess_token   : at,
						test_type     : test.type,
						q_order       : test.q_order,
						is_started 		: true,
						is_submitted 	: false,
						start_time 		: tim,
						l_u_tim       : tim,
						total_ques 		: test.num_ques,
						total_marks		: test.total_marks,
						act : true
					};
					if("adpv" in test){
						new_user_test_obj.adpv = test.adpv;
					}
					if("adpstg" in test){
						new_user_test_obj.adpstg = test.adpstg;
					}
					if("mxqlv" in test){
						new_user_test_obj.mxqlv = test.mxqlv;
					}
					var new_user_test = new UserTest(new_user_test_obj);
					new_user_test.save(function(err,new_user_test_created){
						if(err){
							logger.error({"r":"start","er":err,"p":req.body,"msg":"new_user_test_creation_DbError"});
							console.trace(err);
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						updateAttemptStatistics(plugin_id, user_id, user.server_id, test_id, test.catg_id, function(err,resp){
							if(err){
								logger.error({"r":"start","er":err,"p":req.body,"msg":"updateAttemptStatistics_Error"});
								console.trace(err);
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}
							return sendSuccess(res,{
								id           : new_user_test._id,
								is_resumable : false,
								test_id      : test_id,
								test_at      : at,
								num_ques     : test.num_ques,
								test_name    : test.name,
								test_type    : test.type,
								itm          : is_timed,
								adpv         : test.adpv,
								adpstg       : test.adpstg
							});
						});
					});
				});
			});
		});
	});
});

router.post('/start_f', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"start_f","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id       = req.body.user_id.trim();
	var test_id       = req.body.test_id.trim();
	var plugin_id     = req.body.pl_id.trim();
	var gid           = req.body.g_id;
	var tim           = new Date().getTime();

	User.findOne({
		_id : user_id,
		act : true
	},{
		server_id : 1
	},function(err,user){
		if(err){
			logger.error({"r":"start_f","er":err,"p":req.body,"msg":"User_find_DbError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!user){
			logger.error({"r":"start_f","msg":"User Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		Test.findOne({
			_id : test_id,
			act : true
		},function(err,test){
			if(err){
				logger.error({"r":"start_f","er":err,"p":req.body,"msg":"Test_find_DbError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!test){
				logger.error({"r":"start_f","msg":"Test Not Found","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			var is_resumable = false;
			UserTest.update({
				user_id 			: user_id,
				test_id 			: test_id,
				act           : true
			},{
				act : false
			},{
				multi : true
			},function(err,old_deactivated){
				if(err){
					logger.error({"r":"start_f","er":err,"p":req.body,"msg":"UserTest_update_DbError"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				var is_timed = false;
				var max_tim  = 0;
				var expirytime = configs.JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME;
				if(("timed" in test) 
					&& (test.timed == true || test.timed == 'true') 
					&& (test.max_tim > configs.MIN_TIME_FOR_TIMED_TEST))
				{
					is_timed = true;
					max_tim  = test.max_tim;
					expirytime = (max_tim / 1000) + configs.EXTRA_BUFFER_FOR_TEST_SUBMIT
				}
				var object_for_at = {
					id1 : user_id,
					id2 : test_id,
					id3 : plugin_id,
					itm : is_timed,
					mt  : max_tim,
					et  : (tim+max_tim)
				};
				var at = AuthSecurity.getTestAT(object_for_at, expirytime);
				var new_user_test_obj = {
					user_id 			: user_id,
					pid 			    : user.server_id,
					test_id 			: test_id,
					catg_id       : test.catg_id,
					plugin_id     : plugin_id,
					gid           : gid,
					tlvl          : test.level,
					xcess_token   : at,
					test_type     : test.type,
					q_order       : test.q_order,
					is_started 		: true,
					is_submitted 	: false,
					start_time 		: tim,
					l_u_tim       : tim,
					total_ques 		: test.num_ques,
					total_marks		: test.total_marks,
					act : true
				};
				if("adpv" in test){
					new_user_test_obj.adpv = test.adpv;
				}
				if("adpstg" in test){
					new_user_test_obj.adpstg = test.adpstg;
				}
				if("mxqlv" in test){
					new_user_test_obj.mxqlv = test.mxqlv;
				}
				var new_user_test = new UserTest(new_user_test_obj);
				new_user_test.save(function(err,new_user_test_created){
					if(err){
						logger.error({"r":"start_f","er":err,"p":req.body,"msg":"new_user_test_creation_DbError"});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					updateAttemptStatistics(plugin_id, user_id, user.server_id, test_id, test.catg_id, function(err,resp){
						if(err){
							logger.error({"r":"start_f","er":err,"p":req.body,"msg":"updateAttemptStatistics_Error"});
							console.trace(err);
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						return sendSuccess(res,{
							id        : new_user_test._id,
							test_id   : test_id,
							test_at   : at,
							num_ques  : test.num_ques,
							test_name : test.name,
							test_type : test.type,
							itm       : is_timed
						});
					});
				});
			});
		});
	});
});

router.post('/resume', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"resume","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id       = req.body.user_id.trim();
	var test_id       = req.body.test_id.trim();
	var user_test_id  = req.body.user_test_id.trim();
	var plugin_id     = req.body.pl_id.trim();
	var gid           = req.body.g_id;
	var tim           = new Date().getTime();

	User.findOne({
		_id : user_id,
		act : true
	},{
		server_id : 1
	},function(err,user){
		if(err){
			logger.error({"r":"resume","er":err,"p":req.body,"msg":"User_find_DbError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!user){
			logger.error({"r":"resume","msg":"User Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		Test.findOne({
			_id : test_id,
			act : true
		},function(err,test){
			if(err){
				logger.error({"r":"resume","er":err,"p":req.body,"msg":"Test_find_DbError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!test){
				logger.error({"r":"resume","msg":"Test Not Found","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
			}
			getTestResumableValue(user_id, test_id, test, function(err,resp){
				if(err){
					logger.error({"r":"resume","er":err,"p":req.body,"msg":"getTestResumableValue_Error"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(resp.is_resm == false){
					logger.info({"r":"resume","msg":"Test is not Resumable","p":req.body});
					return sendError(res,"Test cannot not be resumed","test_could_not_be_resumed",HttpStatuses.SERVER_ERROR);
				}
				if(resp.user_test_id != user_test_id){
					logger.info({"r":"resume","msg":"test is resumable but user test id is mismatching","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				}
				var max_tim  = 0;
				var expirytime = configs.JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME;
				if(resp.is_tm == true)
				{
					max_tim    = test.max_tim;
					expirytime = (resp.tm_rm / 1000) + configs.EXTRA_BUFFER_FOR_TEST_SUBMIT
				}
				var object_for_at = {
					id1 : user_id,
					id2 : test_id,
					id3 : plugin_id,
					itm : resp.is_tm,
					mt  : max_tim,
					et  : (tim+resp.tm_rm)
				};
				var at = AuthSecurity.getTestAT(object_for_at, expirytime);
				
				fetchResumeQuestions(user_test_id, test_id, function(err,questions){
					if(err){
						logger.error({"r":"resume","er":err,"p":req.body,"msg":"fetchResumeQuestions"});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					UserTest.update({
						_id : user_test_id
					},{
						$inc : { resm_c : 1 }
					},function(err,usertest_updated){
						if(err){
							logger.error({"r":"resume","er":err,"p":req.body,"msg":"new_user_test_creation_DbError"});
							console.trace(err);
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						UserTest.update({
							_id : user_test_id
						},{
							l_u_tim : tim
						},function(err,usertest_updated){
							if(err){
								logger.error({"r":"resume","er":err,"p":req.body,"msg":"new_user_test_creation_DbError"});
								console.trace(err);
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}
							logger.info({"r":"resume","p":req.body,"msg":"success",d:{rtim:resp.tm_rm,itm:resp.is_tm,test_id:test_id,resp:resp}});
							return sendSuccess(res,{
								user_id     : user_id,
								test_id     : test_id,
								user_test_id: user_test_id,
								test_at     : at,
								num_ques    : test.num_ques,
								test_name   : test.name,
								test_type   : test.type,
								itm         : resp.is_tm,
								rtim        : resp.tm_rm,
								ques        : questions,
								l_u_q_id    : (resp.l_u_q_id) ? resp.l_u_q_id : null
							});
						});
					});
				});
			})
		});
	});
});

router.post('/scores', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"scores","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id       = req.body.user_id.trim();
	var test_id       = req.body.test_id.trim();
	var plugin_id     = req.body.pl_id.trim();
	var gid           = req.body.g_id;
	var tim           = new Date().getTime();

	Test.findOne({
		_id : test_id,
		act : true
	},function(err,test){
		if(err){
			logger.error({"r":"scores","er":err,"p":req.body,"msg":"Test_find_DbError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"scores","msg":"Test Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		UserTest.find({
			user_id      : user_id,
			test_id      : test_id,
			is_submitted : true
		},{
			marks_obtained : 1,
			time_taken     : 1,
			total_correct  : 1,
			start_time     : 1
		},{
			sort : {
				start_time : 1
			}
		},function(err,usertests){
			if(err){
				logger.error({"r":"scores","er":err,"p":req.body,"msg":"Test_find_DbError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			logger.info({"r":"scores","p":req.body,"msg":"UserTest_Scores_Success","data":usertests});
			return sendSuccess(res,{usertests:usertests});
		});
	});
});

router.post('/ldb', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"ldb","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id       = req.body.user_id.trim();
	var test_id       = req.body.test_id.trim();
	var plugin_id     = req.body.pl_id.trim();
	var gid           = req.body.g_id;
	var tim           = new Date().getTime();

	Test.findOne({
		_id : test_id,
		act : true
	},function(err,test){
		if(err){
			logger.error({"r":"ldb","er":err,"p":req.body,"msg":"Test_find_DbError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"ldb","msg":"Test Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		UserTest.find({
			test_id      : test_id,
			is_submitted : true
		},{
			_id            : 1,
			user_id        : 1,
			marks_obtained : 1
		},{
			sort : {
				start_time : 1
			}
		},function(err,usertests){
			if(err){
				logger.error({"r":"ldb","er":err,"p":req.body,"msg":"Test_find_DbError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var user_idT;
			var modified_usertests = [];
			var user_ids           = [];
			var attemptsById       = {};
			var attemptsByIdF      = {};
			var total              = usertests.length;
			for(var i=0; i<total; i++){
				user_idT = usertests[i].user_id+'';
				if(user_ids.indexOf(user_idT) < 0){
					user_ids.push(user_idT);
					modified_usertests.push(usertests[i]);
					attemptsById[user_idT] = 0;
				}
				attemptsById[user_idT] = attemptsById[user_idT] + 1;
			}
			user_ids = [];
			var user_details       = {};
			modified_usertests.sort(function(a, b){
			 return b.marks_obtained-a.marks_obtained;
			});
			var total_k = modified_usertests.length;
			for(var k=0; k<total_k; k++){
				if(modified_usertests[k].user_id+'' == user_id){
					user_details = {
						user_id        : user_id,
						rank           : k,
						marks_obtained : modified_usertests[k].marks_obtained,
						atmpt          : attemptsById[modified_usertests[k].user_id+'']
					};
					break;
				}
			}
			modified_usertests = modified_usertests.splice(0,configs.SIZE_OF_TEST_LEADERBOARD);
			var total_n = modified_usertests.length;
			for(var m=0; m<total_n; m++){
				user_idT = modified_usertests[m].user_id+'';
				if(user_ids.indexOf(user_idT) < 0){
					user_ids.push(user_idT);
					attemptsByIdF[user_idT] = attemptsById[user_idT];
				}
			}
			attemptsById == null;
			User.find({
				_id : {$in : user_ids}
			},{
				name : 1
			},function(err,users){
				if(err){
					logger.error({"r":"ldb","er":err,"p":req.body,"msg":"User_find_DbError"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				var nameById = {};
				var new_total = users.length;
				for(var j=0; j<new_total; j++){
					nameById[users[j]._id+''] = users[j].name;
				}
				User.findOne({
					_id : user_id,
					act : true
				},{
					identifier : 1
				},function(err,user){
					if(err){
						logger.error({"r":"ldb","er":err,"p":req.body,"msg":"User_find_DbError"});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!user){
						logger.error({"r":"ldb","msg":"User Not Found","p":req.body});
						return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
					}
					RootServerApi.checkIfAdmin(user.identifier, function(err,resp){
						if(err){
							logger.error({"url":"ldb","msg":"rootServer_checkIfAdmin_err","er":err,"p":req.body});
							console.trace(err);
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						if(!resp.success){
							logger.error({"url":"ldb","msg":"rootServer_checkIfAdmin_failed","p":req.body});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						var is_admin = false;
						if(("is_admin" in resp)  && resp.is_admin == true){
							is_admin = true;
						}
						logger.info({"r":"ldb","p":req.body,"msg":"UserTest_Ldb_Success","data":{usertests:modified_usertests,user_details:user_details}});
						return sendSuccess(res,{usertests:modified_usertests,usernames:nameById,attemptsById:attemptsById,user_details:user_details,is_admin:is_admin});
					});
				});
			});
		});
	});
});


//route middleware to verify the jwt user test token
router.use(function(req, res, next){
	var token        = req.body.t_tokn;
	var user_id      = req.body.user_id;
	var test_id      = req.body.test_id;
	var plugin_id    = req.body.pl_id.trim();
	if(!token){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","invalid_parameters",HttpStatuses.FORBIDDEN);
	}
	if(!test_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","invalid_parameters",HttpStatuses.FORBIDDEN);
	}
	req.decoded = null;
  jwt.verify(token, configs.JWT_USERTEST_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"test_token_jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate Test token.","usertest_token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid id is not authorised","invalid_usertest_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id2 != test_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid test id is not authorised","invalid_usertest_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != plugin_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_app_id","p":req.body});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_usertest_token",HttpStatuses.FORBIDDEN);
    }
    next();
  });
});

// evaluate user test marks
// evaluate user time taken for submitting test
// evaluate user test total correct answers
// update user submits
// update category submits
// update test submit
// update test total submits
// update test submit all correct
router.post('/end', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"end","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id       					= req.body.user_id.trim();
	var test_id       					= req.body.test_id.trim();
	var user_test_id  					= req.body.user_test_id.trim();
	var plugin_id     					= req.body.pl_id.trim();
	var tim           					= new Date().getTime();
	var user_already_gave_test 	= false;
	var ssol          					= true;

	Test.findOne({
		_id : test_id,
		act : true
	},function(err,test){
		if(err){
			logger.error({"r":"end","er":err,"p":req.body,"er":err,"msg":"Test_find_DbError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"end","msg":"Test Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(test.ssol && test.ssol == false){
			ssol = false;
		}
		UserTest.findOne({
			user_id 			: user_id,
			test_id 			: test_id,
			is_submitted 	: true,
		},function(err,test_already_given){
			if(err){
				logger.error({"r":"end","er":err,"p":req.body,"er":err,"msg":"UserTest_find_DbError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var initial_update = {
				is_submitted : true,
				act 				 : false,
				end_time     : tim
			};
			if(test_already_given){
				user_already_gave_test  = true;
			} else {
				initial_update.is_first = true;
			}
			UserTest.findOne({
				_id 					: user_test_id,
				user_id 			: user_id,
				test_id 			: test_id,
				act           : true,
				is_started 		: true,
				is_submitted 	: false
			},function(err,usertest){
				if(err){
					logger.error({"r":"end","er":err,"p":req.body,"er":err,"msg":"UserTest_find_DbError"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!usertest){
					logger.error({"r":"end","msg":"User Test Not Found","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				}
				if(test.timed && test.timed == true){
					if((tim - usertest.start_time) > (test.max_tim+(configs.EXTRA_BUFFER_FOR_TEST_SUBMIT*1000))){
						logger.error({"r":"end","msg":"For TimedTest TestTimeToSubmitIncludingExtraBuffer Has Expired","p":req.body});
						return sendError(res,"Test Time Expired","test_time_expired",HttpStatuses.BAD_REQUEST);
					}
				} 
				// else {
				// 	if((tim - usertest.start_time) > configs.ATTEMPTED_TEST_VALIDITY_TIME){
				// 		logger.error({"r":"end","msg":"For UntimedTest TestTimeToSubmit Has Expired","p":req.body});
				// 		return sendError(res,"Test Time Expired","test_time_expired",HttpStatuses.BAD_REQUEST);
				// 	}
				// }
				UserTest.update({
					_id : user_test_id
				},initial_update,function(err,updated_usertest){
					if(err){
						logger.error({"r":"end","er":err,"p":req.body,"er":err,"msg":"UserTest_update_DbError"});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					var total_ques  = usertest.total_ques;
					var total_marks = usertest.total_marks;
					var start_time  = usertest.start_time;
					var time_taken  = tim - usertest.start_time;
					var finalData   = {};

					if(!test.model){
						test_model = Test.MODEL.REGULAR;
					}
					
					updateAllStatisticsOnSubmit(plugin_id, user_test_id, test_id, total_ques, time_taken, user_id, user_already_gave_test, test_model, function(err,statsData){
						if(err){
							logger.error({"r":"end","er":err,"p":req.body,"er":err,"msg":"updateAllStatisticsOnSubmit_Error"});
							console.trace(err);
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						for(var key in statsData){
							finalData[key] = statsData[key];
						}
						finalData.user_test_id = user_test_id;
						finalData.test_id      = test_id;
						finalData.start_time   = start_time;
						finalData.time_taken   = time_taken;
						finalData.total_ques   = total_ques;
						finalData.total_marks  = total_marks;
						finalData.ssol         = ssol;
						if(test.model && test.model == Test.MODEL.PSYCHOMETRY){
								finalData.is_psyo = true;
							}
						if(test.model && test.model == Test.MODEL.SURVEY){
							finalData.is_feedback = true;
						}
						logger.info({"r":"end","msg":"success","p":req.body});
						prepPsychometeryResult(user_id, test, user_test_id, function(err,resp){
							if(err){
								logger.error({"r":"end","err":err,"p":req.body});
							}
							if(resp){
								var data1 = [];
								var data2 = [];
								data2.push(resp.x);
								data2.push(resp.y);
								data1.push(data2);
								finalData.data = data1;
								finalData.name = resp.name;
								finalData.group_code = resp.group_code;
							}
							return sendSuccess(res,{final_report:finalData});
						});
					});
				});
			});
		});
	});
});

// Version 2 of the test submit, with bulk submit
// evaluate user test marks
// evaluate user time taken for submitting test
// evaluate user test total correct answers
// update user submits
// update category submits
// update test submit
// update test total submits
// update test submit all correct
router.post('/end_v2', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('answers',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"end_v2","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id       					= req.body.user_id.trim();
	var test_id       					= req.body.test_id.trim();
	var user_test_id  					= req.body.user_test_id.trim();
	var plugin_id     					= req.body.pl_id.trim();
	var gid     					      = req.body.g_id.trim();
	var answers     					  = req.body.answers;
	var tim           					= new Date().getTime();
	var user_already_gave_test 	= false;
	var ssol          					= true;

	if(typeof(answers) != "object" || Object.keys(answers).length == 0){
		logger.error({"r":"end_v2","msg":"invalid_parameters_answers","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	Test.findOne({
		_id : test_id,
		act : true
	},function(err,test){
		if(err){
			logger.error({"r":"end_v2","er":err,"p":req.body,"er":err,"msg":"Test_find_DbError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"end_v2","msg":"Test Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(!test.ssol || test.ssol == false){
			ssol = false;
		}
		UserTest.findOne({
			user_id 			: user_id,
			test_id 			: test_id,
			is_submitted 	: true,
		},function(err,test_already_given){
			if(err){
				logger.error({"r":"end_v2","er":err,"p":req.body,"er":err,"msg":"UserTest_find_DbError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var final_update = {
				is_submitted : true,
				act 				 : false,
				end_time     : tim
			};
			if(test_already_given){
				user_already_gave_test  = true;
			} else {
				final_update.is_first = true;
			}
			UserTest.findOne({
				_id 					: user_test_id,
				user_id 			: user_id,
				test_id 			: test_id,
				act           : true,
				is_started 		: true,
				is_submitted 	: false
			},function(err,usertest){
				if(err){
					logger.error({"r":"end_v2","er":err,"p":req.body,"er":err,"msg":"UserTest_find_DbError"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!usertest){
					logger.error({"r":"end_v2","msg":"User Test Not Found","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				}
				if(test.timed && test.timed == true){
					if((tim - usertest.start_time) > (test.max_tim+(configs.EXTRA_BUFFER_FOR_TEST_SUBMIT*1000))){
						logger.error({"r":"end_v2","msg":"For TimedTest TestTimeToSubmitIncludingExtraBuffer Has Expired","p":req.body});
						return sendError(res,"Test Time Expired","test_time_expired",HttpStatuses.BAD_REQUEST);
					}
				} 
				// else {
				// 	if((tim - usertest.start_time) > configs.ATTEMPTED_TEST_VALIDITY_TIME){
				// 		logger.error({"r":"end_v2","msg":"For UntimedTest TestTimeToSubmit Has Expired","p":req.body});
				// 		return sendError(res,"Test Time Expired","test_time_expired",HttpStatuses.BAD_REQUEST);
				// 	}
				// }
				
				var total_ques  = usertest.total_ques;
				var start_time  = usertest.start_time;
				var total_marks = usertest.total_marks;
				var time_taken  = tim - usertest.start_time;
				var finalData   = {};
				
				updateBulkSubmitAnswers(
					plugin_id, 
					gid, 
					user_test_id, 
					test_id, 
					answers, 
					test.total_attempts,
					test.total_submits,
				function(err,bulkUpdateData){
					if(err){
						logger.error({"r":"end_v2","er":err,"p":req.body,"er":err,"msg":"updateBulkSubmitAnswers_Error"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					
					var test_model;
					if(!test.model){
						test_model = Test.MODEL.REGULAR;
					} else {
						test_model = test.model;
					}

					updateAllStatisticsOnSubmit(
						plugin_id, 
						user_test_id, 
						test_id, 
						total_ques, 
						time_taken, 
						user_id, 
						user_already_gave_test, 
						test_model, 
					function(err,statsData){
						if(err){
							logger.error({"r":"end_v2","er":err,"p":req.body,"er":err,"msg":"updateAllStatisticsOnSubmit_Error"});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						for(var key in statsData){
							finalData[key] = statsData[key];
						}

						final_update.marks_obtained = bulkUpdateData.final_marks;
						final_update.total_correct  = bulkUpdateData.total_correct;

						if(final_update.total_correct == total_ques){
							final_update.if_all_correct = true;
						} else {
							final_update.if_all_correct = false;
						}

						UserTest.update({
							_id : user_test_id
						},final_update,function(err,updated_usertest){
							if(err){
								logger.error({"r":"end_v2","er":err,"p":req.body,"er":err,"msg":"UserTest_update_DbError"});
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}

							finalData.user_test_id = user_test_id;
							finalData.test_id      = test_id;
							finalData.start_time   = start_time;
							finalData.time_taken   = time_taken;
							finalData.total_ques   = total_ques;
							finalData.total_marks  = total_marks;
							finalData.ssol         = ssol;
							finalData.allAtmpAndCrct = bulkUpdateData.allAtmpAndCrct;

							prepareExtraSubmissionData(
								user_id, 
								user_test_id, 
								test, 
								answers,
								bulkUpdateData, 
								time_taken,
								bulkUpdateData.final_marks,
								final_update.total_correct,
								"end_v2",
							function(err,extraData){
								if(err){
									logger.error({"r":"end_v2","er":err,"p":req.body,"er":err,"msg":"prepareExtraSubmissionData_Error"});
									return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
								}
								for(var key in extraData){
									// console.log(extraData);
									finalData[key] = extraData[key];
								}
								sendSuccess(res,{final_report:finalData});
								answerLogger.info({
									"r"           : "end_v2",
									"msg"         : "success",
									"ans"         : JSON.stringify(req.body.answers),
									"user_id"     : user_id,
									"test_id"     : test_id,
									"user_test_id": user_test_id
								});
								handlePostTestSubmitActions(
									user_already_gave_test, 
									test,
									user_id, 
									user_test_id, 
									final_update.marks_obtained, 
									finalData.time_taken,
									finalData.attempted,
									final_update.total_correct, 
									plugin_id, 
									gid,
									"end_v2"
								);
							});
						});
					});
				});
			});
		});
	});
});

function prepareExtraSubmissionData(
	user_id, 
	user_test_id, 
	test, 
	answers,
	bulkAnsUpdateData,
	time_taken,
	marks_obtained,
	total_correct,
	route_name,
	cb
){
	if(test.adpv && test.adpv == true){
		return handleAdaptiveTestResults(
			user_id, 
			user_test_id,
			test._id, 
			test.num_ques,
			test.total_marks, 
			bulkAnsUpdateData,
			time_taken,
			marks_obtained,
			total_correct,
			route_name,
			cb
		);
	} else {
		return handleNonAdaptiveTestResults(
			user_id, 
			user_test_id, 
			test, 
			answers,
			route_name,
			cb
		);
	}
}

function handleAdaptiveTestResults(
	user_id, 
	user_test_id, 
	test_id,
	total_num_ques,
	maximum_marks, 
	bulkAnsUpdateData,
	time_taken,
	marks_obtained,
	total_correct,
	route_name,
	cb
){
	return UserTestResultsByTag.get_data(
		user_id, 
		user_test_id, 
		test_id,
		total_num_ques,
		maximum_marks, 
		bulkAnsUpdateData,
		time_taken,
		total_correct,
		marks_obtained,
		cb
	);
	// return cb(null,{});
}

function handleNonAdaptiveTestResults(
	user_id, 
	user_test_id, 
	test, 
	answers,
	route_name,
	cb
){
	var finalData = {};
	if(test.model && test.model == Test.MODEL.COURSE_MATCHING){
		finalData.is_course_matching   = true;
		course_matching_data           = resultForCourseMatchingQuiz(answers);;
		finalData.course_matching_data = course_matching_data;
		return cb(null,finalData);
	}

	if(test.model && test.model == Test.MODEL.PSYCHOMETRY){
		finalData.is_psyo = true;
	}
	if(!test.sub_model){
		finalData.sub_model = 1;
	} else {
		finalData.sub_model = test.sub_model;
	}
	var x_title,y_title;
	if(test.sub_model == 1){
		x_title = "FEEDBACK";
		y_title = "EXPOSURE";
	} else if(test.sub_model == 2){
		x_title = "X";
		y_title = "Y";
	} else if(test.sub_model == 3) {
		x_title = "X";
		y_title = "Y";
	} else {
		x_title = "X";
		y_title = "Y";
	}
	finalData.x_title = x_title;
	finalData.y_title = y_title;
	if(test.model && test.model == Test.MODEL.SURVEY){
		finalData.is_feedback = true;
	}
	prepPsychometeryResult(
		user_id, 
		test, 
		user_test_id, 
	function(err,psychometric_data){
		if(err){
			return cb(err,null);
		}
		if(psychometric_data){
			finalData.data = psychometric_data;
		}
		return cb(null,finalData);
	});
}

function handlePostTestSubmitActions(
	user_id,
	gid,
	plugin_id,
	user_already_gave_test,
	user_test_id,
	test,
	marks_obtained,
	time_taken,
	attempted,
	total_correct,
	route_name
){
	CertificateModule.checkAndSend(
		user_id,
		user_already_gave_test,
		test,
		user_test_id,
		marks_obtained,
	function(err1,certificate_resp){
		if(err1){
			logger.warn({"r":route_name,"msg":"certificate_sending_warning","t":test._id,"u":user_id,"er":err1});
		} else {
			logger.info({"r":route_name,"msg":"success_certificate_sending","t":test._id,"u":user_id});
		}
	});
	TestReviewModule.createTestReport(
		user_already_gave_test, 
		test,
		user_id, 
		user_test_id, 
		marks_obtained, 
		time_taken,
		attempted,
		total_correct, 
		plugin_id, 
		gid,
	function(err2,test_review_resp){
		if(err2){
			logger.warn({"r":route_name,"msg":"TestReviewModule_createTestReport_warning","t":test._id,"u":user_id,"er":err2});
		} else {
			logger.info({"r":route_name,"msg":"TestReviewModule_createTestReport_success","t":test._id,"u":user_id});
		}
	});
}

function processTimedUsertesForAutoSubmit(
	route_name, 
	res, 
	req_body, 
	test, 
	data
){
	var plugin_id              = req_body.pl_id;
	var user_test_id           = data.user_test_id;
	var test_id                = req_body.test_id;
	var total_ques             = test.total_ques;
	var time_taken             = test.max_tim;
	var user_id                = req_body.user_id;
	var user_already_gave_test = data.user_already_gave_test;
	var test_model             = test.model;

	updateAllStatisticsOnSubmit(
		plugin_id, 
		user_test_id, 
		test_id, 
		total_ques, 
		time_taken, 
		user_id, 
		user_already_gave_test, 
		test_model,
	function(err,statsData){
		if(err){
			logger.error({"r":route_name,"er":err,"p":req_body,"msg":"processTimedUsertesForAutoSubmit_updateAllStatisticsOnSubmit_Error"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		UserTest.update({
			_id : user_test_id
		},{
			is_submitted : true,
			act          : false
		},function(err,updated_usertest){
			if(err){
				logger.error({"r":route_name,"er":err,"p":req_body,"msg":"processTimedUsertesForAutoSubmit_updateUsertest_Error"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var finalData = {};

			for(var key in statsData){
				finalData[key] = statsData[key];
			}

			finalData.user_test_id = user_test_id;
			finalData.test_id      = test_id;
			finalData.total_ques   = test.total_ques;
			finalData.total_marks  = test.total_marks;
			finalData.ssol         = test.ssol;
			
			if(test.model && test.model == Test.MODEL.PSYCHOMETRY){
				finalData.is_psyo = true;
			}
			if(!test.sub_model){
				finalData.sub_model = 1;
			} else {
				finalData.sub_model = test.sub_model;
			}
			if(test.model && test.model == Test.MODEL.SURVEY){
				finalData.is_feedback = true;
			}
			logger.info({"r":route_name,"p":req_body,"msg":"processTimedUsertesForAutoSubmit_Success"});
			return res.status(HttpStatuses.TIMED_RESUMABLE_AUTO_AUBMIT).json({
				autosubmit   : true, 
				user_test_id : user_test_id, 
				message      : configs.TIMED_TEST_EXPIRY_AUTO_SUBMIT_MESSAGE,
				final_report : finalData
			});
			// return sendError(res,"TIMED_RESUMABLE_AUTO_AUBMIT","timed_resumable_auto_submit",HttpStatuses.TIMED_RESUMABLE_AUTO_AUBMIT);
		});
	});

}

function fetchResumeQuestions(user_test_id, test_id, cb){
	UserTestQuestion.find({
		user_test_id : user_test_id
		// is_attempted : true
	},{
		_id          : 0,
		ques_id 		 : 1,
		ans_no       : 1,
		is_attempted : 1,
		qtim         : 1
	},function(err,usertestquestions){
		if(err){
			return cb(err,null);
		}
		var total_subm_ques          = usertestquestions.length;
		var ques_ids                 = [];
		var usertestquestionsByQueId = {};
		var temp_que;
		for(var i=0; i<total_subm_ques; i++){
			temp_que = usertestquestions[i].ques_id+''
			if(ques_ids.indexOf(temp_que) < 0)
				ques_ids.push(temp_que);
			usertestquestionsByQueId[temp_que] = usertestquestions[i];
		}
		TestQuestion.find({
			test_id : test_id,
			ques_id : {$in : ques_ids},
			act : true
		},{
			_id     : 0,
			test_id : 0,
			act     : 0
		},function(err,testquestions){
			if(err){
				return cb(err,null);
			}
			if(testquestions.length == 0){
				return cb(null,[]);
			}
			var testquestionsBYId = {};
			var total             = testquestions.length;
			var question_ids      = [];
			for(var i=0; i<total; i++){
				question_ids.push(testquestions[i].ques_id+'');
				testquestionsBYId[testquestions[i].ques_id+''] = testquestions[i];
			}
			Question.find({
				_id : { $in : question_ids }
			},{
				text        : 1,
				img_arr     : 1,
				ans_options : 1,
				correct_ans : 1
			},function(err,questions){
				if(err){
					logger.error({"r":"get_m","er":err,"p":req.body,"er":err});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				var ques_arr = [];
				var totalQ   = questions.length;
				var temp, tempQ, tempU;
				for(var m=0; m<totalQ; m++){
					temp = testquestionsBYId[questions[m]._id];
					tempQ= {
						_id         : questions[m]._id,
						text        : questions[m].text,
						ans_options : questions[m].ans_options
					}				
					tempU = usertestquestionsByQueId[questions[m]._id+''];
					if(tempU && tempU.is_attempted == true){
						tempQ["is_attempted"] = true;
						tempQ["ans_no"]       = tempU.ans_no;
						tempQ["qtim"]         = tempU.qtim;
					} else {
						tempQ["is_attempted"] = false;
					}
					ques_arr.push(
						{
							question : tempQ,
							img_arr  : [],
							ineg     : temp.ineg,
							negm     : (!temp.negm) ? 0 : temp.negm,
							marks    : temp.marks,
							key      : temp.key
						}
					);
				}
				return cb(null,ques_arr);
			});
		});
	})
}

function getTestResumableValue(user_id, test_id, test, cb){

	var user_already_gave_test = false;

	UserTest.findOne({
		user_id 	   : user_id,
		test_id 	   : test_id,
		act          : true,
		is_submitted : false,
		is_started   : true
	},{
		start_time : 1,
		l_u_tim    : 1,
		l_u_q_id   : 1
	},{
		sort : {
			start_time : -1
		}
	},function(err,usertest){
		if(err){
			return cb(err,null);
		}
		var response = {is_resm:false,is_tm:false,tm_rm:0};
		if(!usertest){
			console.log("getTestResumableValue checking");
			console.log("no previous pending user test found");
			return cb(null,response);
		}
		response.user_test_id = usertest._id;
		if("l_u_q_id" in usertest){
			response.l_u_q_id  = usertest.l_u_q_id+'';
		}
		if(!test.timed){
			console.log("getTestResumableValue checking");
			console.log("test is not timesd and previous usertest found pending");
			response.is_resm = true;
			response.is_tm = false;
			return cb(null,response);
		} 
		if(test.timed == true || test.timed == 'true'){
			var max_tim = usertest.start_time + test.max_tim;
			var time_remaining = max_tim - usertest.l_u_tim;
			console.log("getTestResumableValue checking");
			console.log(time_remaining);
			response.is_tm = true;
			response.tm_rm = time_remaining;
			if(time_remaining > 0){
				response.is_resm = true;
				return cb(null,response);
			}
			console.log('no time remaining');
			response.user_test_id = usertest._id;
			response.autosubmit = true;

			UserTest.findOne({
				user_id 	   : user_id,
				test_id 	   : test_id,
				is_submitted : true
			},function(err,given_usertest){
				if(err){
					return cb(err,null);
				}
				if(given_usertest){
					user_already_gave_test = true
				}
				response.user_already_gave_test = user_already_gave_test;
				console.log(response);
				return cb(null,response);
			});
		} else {
			return cb(null,response);
		}
	});
}

function prepPsychometeryResult(user_id, test, user_test_id, cb){
	if(!test.model ||test.model != Test.MODEL.PSYCHOMETRY){
		console.log("Test is not a pychometery test, hence no psychometric results will be computed for this");
		return cb(null,null);
	}
	UserTestQuestion.find({
		user_test_id : user_test_id,
		is_attempted : true
	},{
		ques_id :  1,
		ques_key : 1,
		ans_no   : 1,
		code     : 1
	},{
		sort : {
			ques_key : 1
		}
	},function(err,usertestquestions){
		if(err){
			return cb("UserTestQuestionFind_error",null);
		}
		if(usertestquestions.length == 0){
			return cb("no answers submitted for psychometery type tests",null);
		}
		var sub_model;
		if(!test.sub_model){
			sub_model = 1;
		} else {
			sub_model = test.sub_model;
		}
		finalPyschometricResult(usertestquestions, sub_model, test._id, function(err,psychometric_data){
			if(err){
				return cb(err,null);
			}
			console.log('test model type is Psychometry');
			console.log('sub_model value is : '+sub_model);
			console.log(psychometric_data);
			return cb(null,psychometric_data);
		});
	});
}

function finalPyschometricResult(usertestquestions, sub_model, test_id, cb){
	switch (sub_model){

		case 1:
			return cb(null,resultPyschometricType1(usertestquestions));
			break;

		case 2:
			return cb(null,resultPyschometricType2(usertestquestions));
			break;

		case 3:
			return resultPyschometricType3(usertestquestions, test_id, cb);
			break;

		case 4:
			return cb(null,resultPyschometricType4(usertestquestions));
			break;

	}
}

function resultPyschometricType1(usertestquestions){
	var x = 0;
	var y = 0;
	var total = usertestquestions.length;
	for(var i=0; i<total; i++){
		if(usertestquestions[i].ques_key % 2 == 0){
			if(usertestquestions[i].ques_key >= 10)
				x = x + (usertestquestions[i].ans_no * 2);
			x = x + usertestquestions[i].ans_no;
		} else {
			if(usertestquestions[i].ques_key >= 10)
				y = y + (usertestquestions[i].ans_no * 2);
			y = y + usertestquestions[i].ans_no;
		}
	}
	x = parseInt((x / 75) * 100);
	y = parseInt((y / 75) * 100);
	console.log('after');
	console.log('x from test is '+x);
	console.log('y from test is '+y);
	var data1 = [];
	var data2 = [];
	data2.push(x);
	data2.push(y);
	data1.push(data2);
	return data1;
}

function resultPyschometricType2(usertestquestions){
	var data = [];
	var total = usertestquestions.length;
	for(var i=0; i<total; i++){
		data.push({
			y         : (usertestquestions[i].ans_no+1),
			name      : (usertestquestions[i].ques_key)+'',
			drilldown : (usertestquestions[i].ques_key)+''
		});
	}
	return data;
}

function resultPyschometricType3(usertestquestions, test_id, cb){
	var data = [];
	var total = usertestquestions.length;
	var ques_ids = [];
	for(var i=0; i<total; i++){
		if(ques_ids.indexOf(usertestquestions[i].ques_id+'') < 0){
			ques_ids.push(usertestquestions[i].ques_id+'');
		}
	}
	// console.log(ques_ids);
	TestQuestion.find({
		test_id  : test_id,
		ques_id  : {$in : ques_ids},
		act      : true
	},function(err,testquestions){
		if(err){
			return cb(err,null);
		}
		var QuestionCode = {};
		for(var i = 0; i<testquestions.length; i++){
			QuestionCode[testquestions[i].ques_id+''] = testquestions[i].code;
		}
		// console.log(testquestions);
		// console.log(QuestionCode);
		var codeWiseResults = {};
		codeWiseResults['A'] = 0;
		codeWiseResults['R'] = 0;
		codeWiseResults['T'] = 0;
		codeWiseResults['P'] = 0;
		var temp;
		for(var i=0; i<total; i++){
			temp = QuestionCode[usertestquestions[i].ques_id+''];
			if(usertestquestions[i].ans_no == 0)
				codeWiseResults[temp] = codeWiseResults[temp] + 1;
		}
		console.log("resultPyschometricType3");
		console.log(codeWiseResults);
		var x1 = parseInt((codeWiseResults['A'] / 20) * 100);
		var x2 = parseInt((codeWiseResults['R'] / 20) * 100);
		var y1 = parseInt((codeWiseResults['T'] / 20) * 100);
		var y2 = parseInt((codeWiseResults['P'] / 20) * 100);
		var data = [];
		var data1 = [];
		var data2 = [];
		var data3 = [];
		var data4 = [];
		data1.push(x1);
		data1.push(0);
		data2.push((-x2));
		data2.push(0);
		data3.push(0);
		data3.push(y1);
		data4.push(0);
		data4.push((-y2));
		data.push(data1);
		data.push(data4);
		data.push(data2);
		data.push(data3);
		data.push(data1);
		return cb(null,data);
	});
}

function resultPyschometricType4(usertestquestions){
	var data = [];
	var total = usertestquestions.length;
	var x = 0;
	var y = 0;
	AForXRange = [2,3,7,8,9,10,12];
	BForXRange = [1,4,5,6,11,13,14];
	var tempkey;
	for(var i=0; i<total; i++){
		tempkey = usertestquestions[i].ques_key;
		if(usertestquestions[i].ans_no == 0){
			if(AForXRange.indexOf(tempkey) >= 0){
				x = x + (tempkey);
			} else {
				y = y + (tempkey);
			}
		} else {
			if(BForXRange.indexOf(tempkey) >= 0){
				x = x + (tempkey);
			} else {
				y = y + (tempkey);
			}
		}
	}
	var data1 = [];
	var data2 = [];
	data2.push(x);
	data2.push(y);
	data1.push(data2);
	return data1;
}

function getQuestionIdsToShowSolutions(
	test, 
	if_full_set, 
	user_test_id,
	final_marks,
	total_correct,
	cb
){

	var total, temp;
	var ques_ids = [];
	var test_id  = test._id;
	var test_total_attempts = test.total_attempts;
	var test_total_submits  = test.total_submits;
	var if_stage2 = (test.adpstg && test.adpstg == 2) ? true : false;

	UserTestQuestion.find({
		user_test_id : user_test_id,
		act          : true
	},{
		_id            : 0,
		ques_id 			 : 1,
		is_attempted 	 : 1,
		ans_no 				 : 1,
		ans_text 			 : 1,
		is_correct     : 1,
		marks_obtained : 1,
		max_marks_ques : 1,
		qtim           : 1
	},{
		sort : {
			tim : 1
		}
	},function(err,usertestquestions){
		if(err){
			return cb(err,null);
		}
		var result = {
			ques_ids          : ques_ids,
			usertestquestions : usertestquestions,
			allAtmpAndCrct    : {}
		};
		if(!if_full_set && !test.adpv && !is_course_matching){
			total        = usertestquestions.length;
			for(var j=0; j<total; j++){
				temp = usertestquestions[j].ques_id+'';
				if(ques_ids.indexOf(temp) < 0){
					ques_ids.push(temp);
				}
			}
			return cb(null,result);
		}
		if(if_stage2){
			total = usertestquestions.length;
			for(var j=0; j<total; j++){
				temp = usertestquestions[j].ques_id+'';
				if(ques_ids.indexOf(temp) < 0){
					ques_ids.push(temp);
				}
			}
		}
		var condition1 = {
			test_id : test_id,
			act     : true
		};
		var condition2 = {
			test_id : test_id,
			ques_id : {$in : ques_ids},
			act     : true
		};
		var condition;
		if(test.adpstg == 2){
			condition = condition2
		} else {
			condition = condition1;
		}
		console.log(condition);
		TestQuestion.find(condition,function(err,testquestions){
			if(err){
				return cb(err,null);
			}

			total = testquestions.length;
			var allAtmpAndCrct = {};
			var ttemp, atmp, crct;

			for(var i=0; i<total; i++){

				ttemp= testquestions[i];
				temp = testquestions[i].ques_id+'';

				if(ques_ids.indexOf(temp) < 0){
					ques_ids.push(temp);
				}
				if(ttemp.atmp >= test_total_submits){
					atmp = parseInt((ttemp.atmp / test_total_attempts) * 100);
				} else {
					atmp = parseInt((ttemp.atmp / test_total_submits) * 100);
				}
				if(ttemp.crct >= test_total_submits){
					crct = parseInt((ttemp.crct / test_total_attempts) * 100);
				} else {
					crct = parseInt((ttemp.crct / test_total_submits) * 100);
				}
				allAtmpAndCrct[temp] = {
					atmp : atmp,
					crct : crct
				};
			}
			result.allAtmpAndCrct = allAtmpAndCrct;
			
			if(!test.adpv){
				return cb(null,result);
			}
			var testquestionsById = {};
			for(var j=0; j<total; j++){
				testquestionsById[testquestions[j].ques_id+''] = testquestions[j];
			}
			var uqtotal = usertestquestions.length;
			var ansdata = {};
			var answers = [];
			var ques_id, qtim;
			for(var k=0; k<uqtotal; k++){

				ques_id = usertestquestions[k].ques_id+'';
				qtim    = (!usertestquestions[k].qtim) ? 0 : parseInt(usertestquestions[k].qtim);

				ansdata[ques_id] = {
					qt : qtim,
					ict: usertestquestions[k].is_correct,
					mrk: usertestquestions[k].marks_obtained
				};
				answers.push({
					q : ques_id,
					a : usertestquestions[k].ans_no,
					t : qtim
				});
			}
			result.answers = answers;
			result.bulkUpdateData = {
				final_marks   : final_marks,
				total_correct : total_correct,
				ansdata       : ansdata,
				tqs           : testquestionsById,
			};
			return cb(null,result);
		});
	});
}

function updateBulkSubmitAnswers(
	plugin_id, 
	gid, 
	user_test_id, 
	test_id, 
	answers, 
	test_total_attempts,
	test_total_submits,
	cb
){

	var submitted_que_ids     = [];
	var to_rem_que_ids        = [];
	var a_length              = answers.length;

	for(var m=0; m<a_length; m++){
		submitted_que_ids.push(answers[m].q+'');
	}

	UserTestQuestion.find({
		user_test_id : user_test_id,
		is_attempted : true
	},{
		ques_id : 1,
		ans_no  : 1
	},function(err,usertestquestions){
		if(err){
			return cb(err,null);
		}

		var usertestquestionsById = {};
		var s_length              = usertestquestions.length;
		var temp_q_id;

		for(var j=0; j<s_length; j++){
			temp_q_id = usertestquestions[j].ques_id+'';
			usertestquestionsById[temp_q_id] = usertestquestions[j].ans_no;
			if(submitted_que_ids.indexOf(temp_q_id) < 0)
				to_rem_que_ids.push(temp_q_id);
		}

		TestQuestion.find({
			test_id : test_id,
			// ques_id : {$in : submitted_que_ids},
			act     : true
		},{
			createdAt : 0,
			updatedAt : 0
		},function(err,testquestions){
			if(err){
				return cb(err,null);
			}

			var testquestionsById = {};
			var t_length          = testquestions.length;
			var tempq;
			var allAtmpAndCrct    = {};

			for(var k=0; k<t_length; k++){
				tempq = testquestions[k].ques_id+'';
				testquestionsById[tempq] =  testquestions[k];
				allAtmpAndCrct[tempq] = {
					atmp : (test_total_submits > 0) ? ((testquestions[k].atmp / test_total_submits)*100).toFixed(2) : 0,
					crct : (test_total_submits > 0) ? ((testquestions[k].crct / test_total_submits)*100).toFixed(2) : 0
				};
			}

			Question.find({
				_id : {$in : submitted_que_ids}
			},{
				correct_ans : 1
			},function(err,questions){
				if(err){
					return cb(err,null);
				}
				var questionsById = {};
				for(var m=0; m<questions.length; m++){
					questionsById[questions[m]._id+''] =  questions[m].correct_ans;
				}

				var final_marks = 0;
				var done        = 0;
				var toDo        = a_length;
				var ques_id, ans_no, p_ans, r_ans, c_ans_no, time_spent;

				// console.log('all submitted ques id');
				// console.log(JSON.stringify(answers));
				// console.log(submitted_que_ids);
				// console.log('to remove ques id');
				// console.log(to_rem_que_ids);

				UserTestQuestion.update({
					user_test_id : user_test_id,
					ques_id      : {$in : to_rem_que_ids}
				},{
					act : false
				},{
					multi : true
				},function(err,removed_unsubmitted){
					if(err){
						return cb(err,null);
					}

					var total_correct = 0;
					var ansdata       = {};

					for(var i=0; i<toDo; i++){
						if(typeof(answers[i].q) == "undefined" || typeof(answers[i].a) == "undefined"){
							done++;
							if(done == toDo){
								return cb(null,{
									final_marks   : final_marks,
									total_correct : total_correct,
									ansdata       : ansdata,
									tqs           : testquestionsById,
									allAtmpAndCrct: allAtmpAndCrct
								});
							}
							continue;
						}
						ques_id   = answers[i].q;

						if(to_rem_que_ids.indexOf(ques_id) >= 0){
							done++;
							if(done == toDo){
								return cb(null,{
									final_marks   : final_marks,
									total_correct : total_correct,
									ansdata       : ansdata,
									tqs           : testquestionsById,
									allAtmpAndCrct: allAtmpAndCrct
								});
							}
							continue;
						}

						ans_no    = answers[i].a;
						p_ans_no  = (!usertestquestionsById[ques_id]) ? -1 : usertestquestionsById[ques_id];
						r_ans     = testquestionsById[ques_id];
						c_ans_no  = questionsById[ques_id];
						time_spent= (!answers[i].t) ? 0 : parseInt(answers[i].t);

						updateSingleAnsSubmit(
							test_id, 
							plugin_id, 
							gid, 
							user_test_id, 
							ques_id, 
							ans_no, 
							p_ans_no, 
							r_ans, 
							c_ans_no, 
							time_spent,
						function(err,answerUpdate){
							if(err){
								return cb(err,null);
							}
							done++;
							final_marks = final_marks + answerUpdate.marks;
							if(answerUpdate.is_correct == true){
								total_correct++;
							}
							ansdata[answerUpdate.qid] = {
								qt : answerUpdate.qt,
								ict: answerUpdate.is_correct,
								mrk: answerUpdate.marks
							};
							if(done == toDo){
								final_marks = parseFloat(final_marks.toFixed(2));
								return cb(null,{
									final_marks   : final_marks,
									total_correct : total_correct,
									ansdata       : ansdata,
									tqs           : testquestionsById,
									allAtmpAndCrct: allAtmpAndCrct
								});
							}
						});
					}
				});
			});
		});
	});
}

function updateSingleAnsSubmit(
	test_id, 
	plugin_id, 
	gid, 
	user_test_id, 
	ques_id, 
	ans_no, 
	p_ans_no, 
	dt_ans, 
	c_ans_no,
	time_spent, 
  cb
 ){
	
	var marks      = 0;
	var is_correct = false;
	var ineg,negm;

	if(ans_no == c_ans_no){
		marks = dt_ans.marks;
		is_correct = true;
	} else if(dt_ans.ineg && dt_ans.negm && dt_ans.ineg == true && dt_ans.negm > 0){
		marks = -1 * dt_ans.negm;
		ineg  = dt_ans.ineg;
		negm  = dt_ans.negm;
	}
	// if(p_ans_no >= 0 && ans_no == p_ans_no){
	// 	return cb(null,{marks:marks,is_correct:is_correct});
	// }

	var setOnInsertObj = {
		plugin_id       : plugin_id,
	  gid             : gid,
		user_test_id 		: user_test_id,
		test_id         : test_id,
		ques_id 				: ques_id,
		ques_key 				: dt_ans.key,
		max_marks_ques 	: dt_ans.marks,
		correct_answer  : c_ans_no,
		tim             : new Date().getTime(),
		act             : true
	};
	if(ineg && negm > 0){
		setOnInsertObj.ineg = dt_ans.ineg;
		setOnInsertObj.negm = dt_ans.negm;
	}

	UserTestQuestion.update({
		plugin_id       : plugin_id,
	  gid             : gid,
		user_test_id 		: user_test_id,
		ques_id 				: ques_id
	},{
		$setOnInsert : setOnInsertObj,
		$set : {
			ans_no         : ans_no,
			is_correct     : is_correct,
			marks_obtained : parseFloat(marks.toFixed(2)),
			qtim           : time_spent,
			is_attempted   : true
		}
	},{
		upsert : true
	},function(err,updated_usertest_question){
		if(err){
			return cb(err,null);
		}
		// console.log('final_total_marks');
		// console.log(marks);
		return cb(null,{
			marks      : marks,
			is_correct : is_correct,
			qid        : ques_id,
			qt         : time_spent
		});
	});
}

function resultForCourseMatchingQuiz(answers){

	var temp, tans;
	var a_length     = answers.length;
	var final_marks  = 0;
	var marksByIndex = [1,10,50,100];
	var result       = {};

	for(var m=0; m<a_length; m++){
		temp = answers[m];
		if("a" in temp){
			tans = parseInt(temp.a);
			final_marks += marksByIndex[tans];
		}
	}
	if(final_marks >= 7 && final_marks <= 35){
		result.course_name = "Android";
		result.course_url  = "https://eckovation.com/course/android-development";
		result.course_t_url= "https://eckppic.s3.amazonaws.com/5a706ab6323e7c08175c88151517317003426.jpg";
	} else if(final_marks >= 36 && final_marks <= 149){
		result.course_name = "Full Stack Development";
		result.course_url  = "https://eckovation.com/course/full-stack-certification-training---complete-beginners-(node.js-+-reactjs)";
		result.course_t_url= "https://eckppic.s3.amazonaws.com/5a706c57323e7c08175c88171517317237128.jpg";
	} else if(final_marks >= 150 && final_marks <= 299){
		result.course_name = "Internet of Things";
		result.course_url  = "https://eckovation.com/course/iot-certification-course";
		result.course_t_url= "https://eckppic.s3.amazonaws.com/5a706bfc323e7c08175c88161517317171292.jpg";
	} else if(final_marks >= 300 && final_marks <= 700){
		result.course_name = "Machine Learning";
		result.course_url  = "https://eckovation.com/course/machine-learning-value-package";
		result.course_t_url= "https://eckppic.s3.amazonaws.com/5a706c9d323e7c08175c88181517317303884.jpg";
	}
	result.desc = "Thank you for your wonderful responses. Based on your answers, the best skill for you is following";
	return result;
}

function updateAttemptStatistics(plugin_id, user_id, server_id, test_id, catg_id, cb){
	var toDo = 3;
	var done = 0;
	Category.update({
		_id : catg_id
	},{
		$inc : { total_attempts : 1 }
	},function(err,resp){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
	Test.update({
		_id : test_id
	},{
		$inc : { total_attempts : 1 }
	},function(err,resp){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
	updateGroupStatsForStartTest(plugin_id, user_id, server_id, catg_id, function(err,resp){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
}

function updateGroupStatsForStartTest(plugin_id, user_id, server_id, catg_id, cb){
	Category.findOne({
		_id : catg_id
	},function(err,category){
		if(err){
			return cb(err,null);
		}
		if(!category){
			return cb("category_id : "+catg_id+' , not found');
		}
		var gid = category.gid
		GroupStats.findOne({
			plugin_id : plugin_id,
			gid       : gid,
			user_id   : user_id
		},function(err,if_groupstats_exists){
			if(err){
				return cb(err,null);
			}
			if(if_groupstats_exists){
				return cb(null,true);
			}
			var new_group_stats = new GroupStats({
				plugin_id : plugin_id,
				user_id   : user_id,
				pid       : server_id,
				gid       : gid,
				submits 	: 0, 
				attempts  : 0,
				score 		: 0,
				marks 		: 0,
				all_correct_count : 0
			});
			new_group_stats.save(function(err,resp){
				if(err){
					return cb(err,null);
				}
				return cb(null,true);
			});
		});
	});
}

function updateAllStatisticsOnSubmit(
	plugin_id, 
	user_test_id, 
	test_id, 
	total_ques, 
	time_taken, 
	user_id, 
	user_already_gave_test, 
	test_model, 
	cb)
{
	var marks 				= 0;
	var correct_count = 0;
	var attempted     = 0;
	var if_all_correct= false;
	
	UserTestQuestion.find({
		user_test_id : user_test_id,
		is_attempted : true,
		act          : true
	},{
		_id 					 : 0,
		is_correct     : 1,
		marks_obtained : 1,
		is_attempted   : 1
	},function(err,usertestquestions){
		if(err){
			return cb(err,null);
		}
		var total = usertestquestions.length;
		for(var i=0; i<total; i++){
			if(!isNaN(usertestquestions[i].marks_obtained))
				marks += usertestquestions[i].marks_obtained;
			if(usertestquestions[i].is_correct)
				correct_count++;
			if(usertestquestions[i].is_attempted)
				attempted++;
		}
		if(correct_count == total_ques){
			if_all_correct = true;
		}
		var toDo = 5;
		var done = 0;
		marks = parseFloat(marks.toFixed(2));
		var result = {
			marks          : marks,
			correct_count  : correct_count,
			if_all_correct : if_all_correct,
			attempted      : attempted
		};
		updateUserTestOnSubmit(user_test_id, marks, correct_count, if_all_correct, time_taken, function(err,resp){
			if(err){
				return cb(err,null);
			}
			done++;
			if(done == toDo){
				return cb(null,result);
			}
		});
		updateTestOnSubmit(test_id, if_all_correct, user_already_gave_test,  marks, function(err,resp){
			if(err){
				return cb(err,null);
			}
			done++;
			if(done == toDo){
				return cb(null,result);
			}
		});
		updateCategoryOnSubmit(test_id, function(err,resp){
			if(err){
				return cb(err,null);
			}
			done++;
			if(done == toDo){
				return cb(null,result);
			}
		});
		updateUserOnSubmit(user_id, if_all_correct, marks, user_already_gave_test, function(err,resp){
			if(err){
				return cb(err,null);
			}
			done++;
			if(done == toDo){
				return cb(null,result);
			}
		});
		updateGroupStatsOnSubmit(plugin_id, user_id, test_id, if_all_correct, marks, user_already_gave_test, test_model, function(err,resp){
			if(err){
				return cb(err,null);
			}
			done++;
			if(done == toDo){
				return cb(null,result);
			}
		});
	});
}

function updateUserTestOnSubmit(user_test_id, marks, correct_count, if_all_correct, time_taken, cb){
	UserTest.update({
		_id : user_test_id
	},{
		marks_obtained : marks,
		total_correct  : correct_count,
		if_all_correct : if_all_correct,
		time_taken     : time_taken
	},function(err,resp){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
}

function updateTestOnSubmit(test_id, if_all_correct, if_already_given, marks, cb){
	var toIncrement = {
		total_submits 			: 1, 
		submits_all_correct : 0
	};
	if(if_all_correct){
		toIncrement.submits_all_correct = 1;
	}
	if(!if_already_given){
		toIncrement.marks_scored = marks;
	}
	Test.update({
		_id : test_id
	},{
		$inc : toIncrement
	},function(err,resp){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
}

function updateCategoryOnSubmit(test_id, cb){
	Test.findOne({
		_id : test_id
	},{
		catg_id : 1
	},function(err,test){
		if(err){
			return cb(err,null);
		}
		if(!test){
			return cb('test_id : '+test_id+', not found',null);
		}
		Category.update({
			_id : test.catg_id
		},{
			$inc : { total_submits : 1 }
		},function(err,resp){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}

function updateUserOnSubmit(user_id, if_all_correct, marks, user_already_gave_test, cb){
	if(user_already_gave_test){
		return cb(null,true);
	}
	var total_correct = 0;
	if(if_all_correct){
		total_correct = 1;
	}
	if(!marks){
		marks = 0;
	}
	User.update({
		_id : user_id
	},{
		$inc : { 
			submits 	: 1, 
			attempts  : 1,
			score 		: marks,
			marks 		: marks,
			all_correct_count : total_correct
		}
	},function(err,resp){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
}

function updateGroupStatsOnSubmit(plugin_id, user_id, test_id, if_all_correct, marks, user_already_gave_test, test_model, cb){
	if(test_model && test_model != Test.MODEL.REGULAR){
		console.log("updateGroupStatsOnSubmit : test model is not of type regular");
		return cb(null,true);
	}
	if(user_already_gave_test){
		return cb(null,true);
	}
	var total_correct = 0;
	if(if_all_correct){
		total_correct = 1;
	}
	if(!marks){
		marks = 0;
	}
	Test.findOne({
		_id : test_id
	},function(err,test){
		if(err){
			return cb(err,null);
		}
		if(!test){
			return cb("test_id : "+test_id+' not found',null);
		}
		Category.findOne({
			_id : test.catg_id
		},function(err,category){
			if(err){
				return cb(err,null);
			}
			if(!category){
				return cb("category_id : "+test.catg_id+' not found',null);
			}
			GroupStats.update({
				plugin_id : plugin_id,
				gid       : category.gid,
				user_id   : user_id
			},{
				$inc : { 
					submits 	: 1, 
					attempts  : 1,
					score 		: marks,
					marks 		: marks,
					all_correct_count : total_correct
				}
			},function(err,resp){
				if(err){
					return cb(err,null);
				}
				return cb(null,true);
			});
		});
	});
}

function dummydata(){
	var data = {
		analytics_header : "Quiz Analytics",
    strong_and_weak_header : "Identify your Strong and Weak Areas",
    recommendation_header  : "Based on above analytics, following are some recommendations for you",
    tagsById : {
    	"5a73fc76ac5ff55cfb2c06fe" : "Permutation",
    	"5a73fc76ac5ff55cfb2c06fa" : "Combination",
    	"5a73fc76ac5ff55cfb2c06fb" : "Factoriral",
    	"5a73fc76ac5ff55cfb2c06fc" : "Arragement",
    	"5a73fc76ac5ff55cfb2c06fd" : "Dearragement"
    },
		strong_areas : [
			{_id : "5a73fc76ac5ff55cfb2c06fe", nm : "Permutation"},
			{_id : "5a73fc76ac5ff55cfb2c06fa", nm : "Combination"},
			{_id : "5a73fc76ac5ff55cfb2c06fb", nm : "Factoriral"}
		],
		weak_areas : [
			{_id : "5a73fc76ac5ff55cfb2c06fc", nm : "Arragement"},
			{_id : "5a73fc76ac5ff55cfb2c06fd", nm : "Dearragement"}
		],
		resources : {
			"5a73fc76ac5ff55cfb2c06fe" : [{rst:1,rsd:[{"n":"Lec1"},{"n":"Lec2"}]},{rst:2,rsd:[{"n":"Quiz3"},{"n":"Quiz4"}]}],
			"5a73fc76ac5ff55cfb2c06fa" : [{rst:1,rsd:[{"n":"Lec1"},{"n":"Lec2"}]},{rst:2,rsd:[{"n":"Quiz3"},{"n":"Quiz4"}]}],
			"5a73fc76ac5ff55cfb2c06fb" : [{rst:1,rsd:[{"n":"Lec1"},{"n":"Lec2"}]},{rst:2,rsd:[{"n":"Quiz3"},{"n":"Quiz4"}]}],
			"5a73fc76ac5ff55cfb2c06fc" : [{rst:1,rsd:[{"n":"Lec1"},{"n":"Lec2"}]},{rst:2,rsd:[{"n":"Quiz3"},{"n":"Quiz4"}]}],
			"5a73fc76ac5ff55cfb2c06fd" : [{rst:3,rsd:[{"n":"pdf1"},{"n":"pdf2"}]},{rst:2,rsd:[{"n":"Quiz31"},{"n":"Quiz41"}]}]
		},
		time_graph : {
			title : "Ratio of Time Spent in Strong and Weak areas",
			data : [
          { name: 'Strong Areas', y: 56.33 },
          { name: 'Weak Areas', y: 24.03 }
        ]
		},
		tag_time_graph : {
			title : "Topic Wise Time Spent Ratio",
			header: "Topic Wise Time Spent Ratio",
			data : [{
          name: 'Permutation',
          y: 56.33
      }, {
          name: 'Combination',
          y: 24.03,
          sliced: true,
          selected: true
      }, {
          name: 'Factorial',
          y: 10.38
      }, {
          name: 'Arrangement',
          y: 4.77
      }, {
          name: 'Multiplication',
          y: 0.91
      }]
		},
		tag_marks_graph : {
			title : "Topic Wise Marks Obtained Ratio",
			header: "Topic Wise Marks Obtained Ratio",
			data : [{
          name: 'Permutation',
          y: 56.33
      }, {
          name: 'Combination',
          y: 24.03,
          sliced: true,
          selected: true
      }, {
          name: 'Factorial',
          y: 10.38
      }, {
          name: 'Arrangement',
          y: 4.77
      }, {
          name: 'Multiplication',
          y: 0.91
      }]
		},
		marks_bar_graph : {
			title : "Scoring Comparison",
			header: "Scoring Comparison",
			x_axis: "Candidate",
			y_axis: "Score",
			categories : ["Topper","Average","You"],
			data : [{
          name: 'Topper',
          y: 56.33,
          drilldown: 'Topper'
      }, {
          name: 'Average',
          y: 24.03,
          drilldown: 'Average'
      }, {
          name: 'You',
          y: 10.38,
          drilldown: 'You'
      }]
		},
		correct_bar_graph : {
			title : "Accuracy Comparison",
			header: "Accuracy Comparison",
			x_axis: "Candidate",
			y_axis: "#of Correct Ans",
			categories : ["Topper","Average","You"],
			data : [{
          name: 'Topper',
          y: 56.33,
          drilldown: 'Topper'
      }, {
          name: 'Average',
          y: 24.03,
          drilldown: 'Average'
      }, {
          name: 'You',
          y: 10.38,
          drilldown: 'You'
      }]
		}
	};
	return data;
}


module.exports = router;
module.exports.updateAllStatisticsOnSubmit = updateAllStatisticsOnSubmit;