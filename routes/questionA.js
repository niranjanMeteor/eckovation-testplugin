var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    							  = require('jsonwebtoken');

var configs					      = require('../configs/configs.js');
var HttpStatuses					= require('../global/http_api_return_codes.js');
var ApiReturnHandlers			= require('../global/api_return_handlers.js');
var errorCodes 						= require('../global/error_codes.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var RootServerApi      		= require('../server/apicalls.js');

require('../models/questions.js');
require('../models/users.js');
require('../models/pluginpaidinfos.js');
var PluginPaidInfo 				= mongoose.model('PluginPaidInfo');
var User                  = mongoose.model('User');
var Question					    = mongoose.model('Question');

var sendError 						= ApiReturnHandlers.sendError;
var sendSuccess 					= ApiReturnHandlers.sendSuccess;
var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_QUESTION_ADMIN);
var MAX_ALLOWED_OPTIONS   = 15;

//route middleware to verify the jwt token for all user whether they are group admin
router.use(function(req, res, next){
	var user_id   = req.body.u_id;
	var gid       = req.body.g_id;
	var plugin_id = req.body.pl_id;
	var private_key;
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body,"r":"2nd_middleware","tkn":req.body.tokn});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
	  jwt.verify(req.body.tokn, private_key, function(err, decoded) {      
	    if(err){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err,"tkn":req.body.tokn});
	      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
	    } 
	    if(decoded.id1 != user_id){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_userid","p":req.body,"tkn":req.body.tokn});
		  	return sendError(res,"Access without valid user id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
	    }
	    if(decoded.id4 != gid){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body,"tkn":req.body.tokn});
		  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
	    }
	    User.findOne({
	    	_id : user_id,
				act : true,
			},function(err,user){
				if(err){
					logger.error({"url":req.originalUrl,"msg":"user_find_failed","er":err,"p":req.body,"tkn":req.body.tokn});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!user){
					logger.info({"url":req.originalUrl,"msg":"user_already_exists","p":req.body,"tkn":req.body.tokn});
					return sendSuccess(res,{ id : user._id, if_new : 0, srv_id : user.server_id, gid : user.gid });
				}
				RootServerApi.checkIfAdmin(user.identifier, function(err,resp){
					if(err){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_err","er":err,"p":req.body,"tkn":req.body.tokn});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!resp.success){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_failed","p":req.body,"tkn":req.body.tokn});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!resp.is_admin){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_UserIsNotGroupAdmin","p":req.body,"tkn":req.body.tokn});
						return sendError(res,"unauthorised","unauthorised",HttpStatuses.UNAUTHORIZED);
					}
			    next();
				});
			});
	  });
	});
});

router.post('/rd', function(req, res, next){
	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('st',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ipp',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"ad","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}	

	var plugin_id  = req.body.pl_id;
	var start      = (!isNaN(req.body.st)) ? parseInt(req.body.st) : "";
	var ipp        = (!isNaN(req.body.ipp)) ? parseInt(req.body.ipp) : "";

	if(!start || (start < configs.MIN_START_FOR_QUESTION_BANK)){
		logger.error({"r":"rd","msg":"invalid_start_value","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	if(!ipp || (ipp < configs.MIN_IPP_FOR_QUESTION_BANK) || (ipp > configs.MAX_IPP_FOR_QUESTION_BANK)){
		logger.error({"r":"ad","msg":"invalid_ipp_value","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	Question.count({
		plugin_id : plugin_id
	},function(err,count){
		if(err){
			logger.error({"r":"rd","er":err,"p":req.body,"msg":"Question_Count_Error"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(count == 0){
			logger.info({"r":"rd","p":req.body,"msg":"Question_Bank_Empty"});
			return sendSuccess(res,{total : count, questions:[]});
		}
		Question.find({
			plugin_id : plugin_id
		},{
			type        : 1,
			level       : 1,
			text        : 1,
			ans_options : 1,
			correct_ans : 1,
			img_arr     : 1,
			audio_url   : 1,
			hint_text   : 1,
			tag         : 1,
			expl        : 1,
			createdAt   : 1,
			updatedAt   : 1
		},{
			sort : {
				createdAt : -1
			},
			skip : start,
			limit: ipp
		},function(err,questions){
			if(err){
				logger.error({"r":"rd","er":err,"p":req.body,"msg":"Question_Find_Error"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			logger.info({"r":"rd","p":req.body,"msg":"Question_Bank_Read_Success","tot":count});
			return sendSuccess(res,{total : count, questions:questions});
		});
	});
});

router.post('/ad', function(req, res, next){
	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('typ',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('lvl',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('text',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('nmop',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('opns',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ans',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('iurl',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('aurl',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('hint',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('tag',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('expl',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()) { 
		logger.error({"r":"ad","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var plugin_id  = req.body.pl_id;
	var type       = (!isNaN(req.body.typ)) ? parseInt(req.body.typ) : "";
	var level      = (!isNaN(req.body.lvl)) ? parseInt(req.body.lvl) : "";
	var text       = req.body.text.trim();
	var nmop       = (!isNaN(req.body.nmop)) ? parseInt(req.body.nmop) : "";
	var opns       = req.body.opns;
	var ans        = (!isNaN(req.body.ans)) ? parseInt(req.body.ans) : "";
	var iurl       = (req.body.iurl) ? req.body.iurl : "";
	var aurl       = (req.body.aurl) ? req.body.aurl : "";
	var hint       = (req.body.hint) ? req.body.hint : "";
	var tag        = (req.body.tag) ? req.body.tag : "";
	var expl       = (req.body.expl) ? req.body.expl : "";
	var tim        =  new Date().getTime();

	var validQuestionTypes = [Question.QUESTION_TYPE.SINGLE_CORRECT,Question.QUESTION_TYPE.MULTIPLE_CORRECT];
	if(validQuestionTypes.indexOf(type) < 0){
		logger.error({"r":"ad","p":req.body,"msg":"question_type_not_valid","typ":type});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	var validLevels = [Question.QUESTION_LEVEL.EASY,Question.QUESTION_LEVEL.MEDIUM,Question.QUESTION_LEVEL.HARD,Question.QUESTION_LEVEL.NPHARD];
	if(validLevels.indexOf(level) < 0){
		logger.error({"r":"ad","p":req.body,"msg":"question_level_not_valid","level":level});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var optioncheck = checkOptionsValidity(nmop, opns, ans);
	if(optioncheck.resp == true){
		nmop = optioncheck.data.nmop;
		opns = optioncheck.data.opns;
		ans  = optioncheck.data.ans;
	} else {
		logger.error({"r":"ad","p":req.body,"msg":"checkOptionsValidity_error","err":optioncheck.er});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	var new_question = new Question({
		type        : type,
		level       : level,
		plugin_id   : plugin_id,
		text        : text,
		ans_options : opns,
		correct_ans : ans,
		img_arr     : iurl,
		audio_url   : aurl,
		hint_text   : hint,
		tag         : tag,
		expl        : expl
	});
	new_question.save(function(err,question_created){
		if(err){
			logger.error({"r":"ad","er":err,"p":req.body,"msg":"Question_Insertion_Error"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		logger.info({"r":"ad","p":req.body,"msg":"Question_Added_Success","ques":new_question});
		return sendSuccess(res,{question:new_question});
	});
});

router.post('/ed', function(req, res, next){
	req.checkBody('u_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ques_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('typ',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('lvl',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('text',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('nmop',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('opns',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ans',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('iurl',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('aurl',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('hint',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('tag',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('expl',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()) { 
		logger.error({"r":"ed","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var ques_id    = req.body.ques_id;
	var type       = (!isNaN(req.body.typ)) ? parseInt(req.body.typ) : "";
	var level      = (!isNaN(req.body.lvl)) ? parseInt(req.body.lvl) : "";
	var text       = req.body.text.trim();
	var nmop       = (!isNaN(req.body.nmop)) ? parseInt(req.body.nmop) : "";
	var opns       = req.body.opns;
	var iurl       = (req.body.iurl) ? req.body.iurl : "";
	var aurl       = (req.body.aurl) ? req.body.aurl : "";
	var hint       = (req.body.hint) ? req.body.hint : "";
	var tag        = (req.body.tag) ? req.body.tag : "";
	var expl       = (req.body.expl) ? req.body.expl : "";
	var tim        =  new Date().getTime();
	var toEdit     = {};
	var ans;

	if("ans" in req.body){
		if(isNaN(req.body.ans) || req.body.ans < 0){
			logger.error({"r":"ed","p":req.body,"msg":"correct_answer_invalid"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		toEdit.correct_ans = req.body.ans;
		ans = req.body.ans;
	}
	if(type){
		toEdit.type = type;
	}
	if(level){
		toEdit.level = level;
	}
	if(text){
		toEdit.text = text;
	}
	if(opns){
		toEdit.ans_options = opns;
	}
	if(iurl){
		toEdit.img_arr = iurl;
	}
	if(aurl){
		toEdit.audio_url = aurl;
	}
	if(hint){
		toEdit.hint_text = hint;
	}
	if(tag){
		toEdit.tag = tag;
	}
	if(expl){
		toEdit.expl = expl;
	}

	if(Object.keys(toEdit).length == 0){
		logger.error({"r":"ed","msg":"NO_Params_for_Data_Modification","p":req.body,"toEd":toEdit});
		return sendError(res,"No Data In Input To Edit","no_data_to_edit",HttpStatuses.NO_INPUT_DATA);
	}

	Question.findOne({
		_id : ques_id
	},function(err,question){
		if(err){
			logger.error({"r":"ed","er":err,"p":req.body,"msg":"Question_Find_Error"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!question){
			logger.error({"r":"ed","p":req.body,"msg":"question_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(typeof(ans) == 'undefined'){
			ans = question.correct_ans;
		}
		var validQuestionTypes = [Question.QUESTION_TYPE.SINGLE_CORRECT,Question.QUESTION_TYPE.MULTIPLE_CORRECT];
		if(validQuestionTypes.indexOf(type) < 0){
			logger.error({"r":"ed","p":req.body,"msg":"question_type_not_valid","typ":type});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		var validLevels = [Question.QUESTION_LEVEL.EASY,Question.QUESTION_LEVEL.MEDIUM,Question.QUESTION_LEVEL.HARD,Question.QUESTION_LEVEL.NPHARD];
		if(validLevels.indexOf(level) < 0){
			logger.error({"r":"ed","p":req.body,"msg":"question_level_not_valid","level":level});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		var optioncheck = checkOptionsValidity(nmop, opns, ans);
		if(optioncheck.resp == true){
			nmop = optioncheck.data.nmop;
			opns = optioncheck.data.opns;
			ans  = optioncheck.data.ans;
		} else {
			logger.error({"r":"ed","p":req.body,"msg":"checkOptionsValidity_error","err":optioncheck.er});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		Question.update({
			_id : ques_id
		},toEdit,function(err,question_edited){
			if(err){
				logger.error({"r":"ed","er":err,"p":req.body,"msg":"Question_Updation_Error"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			logger.info({"r":"ed","p":req.body,"msg":"Question_Edited_Success","edit_obj":toEdit});
			return sendSuccess(res,{});
		});
	});
});

function checkOptionsValidity(num_options, options, correct_ans){
	if(num_options < 0 || num_options > MAX_ALLOWED_OPTIONS){
		return {resp : false, er : "number_of_options_invalid"};
	}
	if(correct_ans < 0 || correct_ans > num_options){
		return {resp : false, er : "correct_ans_invalid"};
	}
	if(typeof(options) != 'object'){
		return {resp : false, er : "options_not_invalid"};
	}
	if(options.length != num_options){
		return {resp : false, er : "options_length_invalid"};
	}
	var tmp, temp;
	var ans_options = [];
	var ans_array = [];
	for(var i=0;i<options.length; i++){
		temp = {};
		tmp = options[i];
		if( (!("index" in tmp)) || (!("text" in tmp)) ){
			return {resp : false, er : "options_value_invalid"};
		}
		if(isNaN(tmp["index"])){
			return {resp : false, er : "options_index_value_invalid"};
		}
		temp.index = tmp.index;
		temp.text  = tmp.text;
		ans_options.push(temp);
		ans_array[parseInt(tmp["index"])] = 1;
	}
	for(var j=0; j<options.length; j++){
		if(ans_array[j] != 1){
			return {resp : false, er : "options_index_value_missing_or_redundant"};
		}
	}
	return {resp : true, data : {nmop : num_options, ans : correct_ans, opns : ans_options}};
}

module.exports = router;