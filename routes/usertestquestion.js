var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    							  = require('jsonwebtoken');

var configs					      = require('../configs/configs.js');
var HttpStatuses					= require('../global/http_api_return_codes.js');
var ApiReturnHandlers			= require('../global/api_return_handlers.js');
var errorCodes 						= require('../global/error_codes.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var AuthSecurity      		= require('../security/auth_tokens.js');
var AwsModule             = require('../aws/aws.js');
var AwsBuckets            = require('../aws/buckets.js');
var AwsModes              = require('../aws/file_modes.js');

require('../models/tests.js');
require('../models/testquestions.js');
require('../models/questions.js');
require('../models/users.js');
require('../models/usertests.js');
require('../models/usertestquestions.js');
require('../models/pluginpaidinfos.js');
var User                  = mongoose.model('User');
var Test 									= mongoose.model('Test');
var UserTest 						  = mongoose.model('UserTest');
var TestQuestion					= mongoose.model('TestQuestion');
var Question					    = mongoose.model('Question');
var UserTestQuestion		  = mongoose.model('UserTestQuestion');
var PluginPaidInfo 				= mongoose.model('PluginPaidInfo');

var sendError 						= ApiReturnHandlers.sendError;
var sendSuccess 					= ApiReturnHandlers.sendSuccess;
var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_USERTESTQUESTION);


//route middleware to verify if plugin in group is paid
router.use(function(req, res, next){
	var tim         = new Date().getTime();
	var plugin_id   = req.body.pl_id;
	var token       = req.body.tokn;
	var gid         = req.body.g_id;
	var user_id     = req.body.user_id;
	if(!token || token == 'null' || token === "undefined"){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body,"tkn":token});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!gid){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"gid_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","gid_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!plugin_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"plugin_id_missing","p":req.body});
	  return sendError(res,"Plugin Id Not Found","plugin_id_missing",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			req.private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			req.private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
		req.tim = tim;
	  next();
	});
});

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
  jwt.verify(req.body.tokn, req.private_key, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != req.body.user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != req.body.pl_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_app_id","p":req.body});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id4 != req.body.g_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body});
	  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    next();
  });
});

//route middleware to verify the jwt user test token
router.use(function(req, res, next){
	var token       = req.body.t_tokn;
	var user_id     = req.body.user_id;
	var test_id     = req.body.test_id;
	var tim         = new Date().getTime();
	if(!token){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","invalid_parameters",HttpStatuses.FORBIDDEN);
	}
	if(!test_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","invalid_parameters",HttpStatuses.FORBIDDEN);
	}
  jwt.verify(token, configs.JWT_USERTEST_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate Test token.","usertest_token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid id is not authorised","invalid_usertest_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id2 != test_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid test id is not authorised","invalid_usertest_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != req.body.pl_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_app_id","p":req.body,});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_usertest_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.itm && decoded.et < tim){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"TEST_NEXT_QUES_AND_ANS_TIME_HAS_EXPIRED","p":req.body,});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_usertest_token",HttpStatuses.TEST_EXPIRED);
    }
    req.tim     = tim;
    req.decoded = decoded; 
    next();
  });
});

router.post('/next_ad_q', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"next_ad_q","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id        = req.body.user_id.trim();
	var test_id        = req.body.test_id.trim();
	var user_test_id   = req.body.user_test_id.trim();
	var plugin_id      = req.body.pl_id.trim();
	var gid            = req.body.g_id;
	var tim            = req.tim;
	var is_timed       = (req.decoded.itm) ? req.decoded.itm : false;
	var expTime        = (req.decoded.et) ? req.decoded.et : (tim+(configs.JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME*1000));

	UserTest.findOne({
		_id						: user_test_id,
		test_id       : test_id,
		user_id       : user_id,
		act         	: true,
		is_started  	: true,
		is_submitted	: false
	},function(err, usertest){
		if(err){
			logger.error({"r":"next_ad_q","er":err,"p":req.body,"er":err});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!usertest){
			logger.error({"r":"next_ad_q","msg":"no Test Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		if(usertest.q_cnt >= usertest.total_ques){
			logger.error({"r":"next_ad_q","msg":"question_quota_over","p":req.body});
			return sendError(res,"question_quota_over","question_quota_over",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		var next_ques_level = 1;
		if(usertest.r_q_lv){
			if(usertest.r_q_st == UserTest.RECENT_MOST_QUESTION_STATUS.RIGHT){
				next_ques_level = usertest.r_q_lv + 1;
			} else if(usertest.r_q_st == UserTest.RECENT_MOST_QUESTION_STATUS.WRONG && usertest.r_q_lv > 1){
				next_ques_level = usertest.r_q_lv - 1;
			} else if(usertest.r_q_st == UserTest.RECENT_MOST_QUESTION_STATUS.SKIPPED){
				next_ques_level = usertest.r_q_lv;
			}
		}
		if(next_ques_level > usertest.mxqlv){
			next_ques_level = usertest.mxqlv;
		}
		TestQuestion.find({
			test_id : test_id,
			act     : true,
      lv      : next_ques_level
		},{
			key : 1,
			lv  : 1,
			_id : 0
		},function(err,test_questions){
			if(err){
				logger.error({"r":"next_ad_q","er":err,"p":req.body,"msg":"testQuestion_allkey_find_error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var test_question_keys = [];
			var total              = test_questions.length;
			for(var i=0; i<total; i++){
				test_question_keys.push(test_questions[i].key);
			}
			UserTestQuestion.find({
				user_test_id : user_test_id,
				qlv          : next_ques_level
			},{
				ques_key : 1,
				_id : 0
			},function(err,user_test_questions){
				if(err){
					logger.error({"r":"next_ad_q","er":err,"p":req.body,"msg":"usertestQuestion_find_error"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				var u_total                 = user_test_questions.length;
				var user_test_question_keys = [];
				if(u_total >= usertest.total_ques){
					logger.error({"r":"next_ad_q","msg":"All_Questions_For_Test_Attempted","p":req.body,"test_id":test_id,"u_total":u_total});
					return sendError(res,"All Questions of This Test Has Been Attempted","all_test_questions_attempted",HttpStatuses.NO_DATA);
				}
				for(var i=0; i<u_total; i++){
					user_test_question_keys.push(user_test_questions[i].ques_key);
				}
				var final_keys          = filter_array(test_question_keys, user_test_question_keys);
				var question_key_create = generateRandomNumFromSetStage2(final_keys);

				console.log('stage2 details');
				console.log(user_test_question_keys);
				console.log(final_keys);
				console.log(question_key_create);
				console.log(next_ques_level);

				TestQuestion.findOne({
					test_id : test_id,
					lv      : next_ques_level, 
					key     : question_key_create
				},function(err,question_create){
					if(err){
						logger.error({"r":"next_ad_q","er":err,"p":req.body,"msg":"testQuestion_find_question_with_key_error"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!question_create){
						logger.error({"r":"next_ad_q","msg":"test_question_with_key_not_found","p":req.body,"test_id":test_id,"key":question_key_create});
						return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
					}
					Question.findOne({
						_id : question_create.ques_id,
					},{
						text        : 1,
						img_arr     : 1,
						ans_options : 1
					},function(err,question){
						if(err){
							logger.error({"r":"next_ad_q","er":err,"p":req.body,"msg":"Question_find_after_ID_error"});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						if(!question){
							logger.error({"r":"next_ad_q","msg":"question_withID_not_found","p":req.body,"test_id":test_id,"key":question_key_create});
							return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
						}
						getSignedUrlForAllMedia(question, function(err,urlsArrayObject){
							if(err){
								logger.error({"r":"next_ad_q","er":err,"p":req.body,"msg":"getSignedUrlForAllMediaError"});
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}
							var new_user_test_question_obj = {
								plugin_id       : plugin_id,
					      gid             : gid,
								user_test_id 		: user_test_id,
								test_id         : test_id,
								ques_id 				: question_create.ques_id,
								ques_key 				: question_key_create,
								max_marks_ques 	: question_create.marks,
								correct_answer  : question.correct_ans,
								qlv             : next_ques_level,
								tim             : tim,
								act             : true
							};
							var ineg = false;
							var negm = 0;
							if(question_create.ineg 
								&& question_create.negm 
								&& question_create.ineg == true 
								&& question_create.negm > 0
							){
								new_user_test_question_obj.ineg = question_create.ineg;
								new_user_test_question_obj.negm = question_create.negm;
								ineg = question_create.ineg;
								negm = question_create.negm;
							}
							var new_user_test_question = new UserTestQuestion(new_user_test_question_obj);
							new_user_test_question.save(function(err,created_question){
								if(err){
									logger.error({"r":"next_ad_q","er":err,"p":req.body,"msg":"new_user_test_question_create_error"});
									return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
								}
								UserTest.update({
									_id : user_test_id
								},{
									$inc : {q_cnt : 1}
								},function(err,updated_usertest){
									if(err){
										logger.error({"r":"next_ad_q","er":err,"p":req.body,"msg":"usertest_udpate_error"});
										return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
									}
									return sendSuccess(res,{
										question : question,
										img_arr  : urlsArrayObject,
										itm      : is_timed, 
										rtim     : (expTime-(new Date().getTime())),
										ineg     : ineg,
										negm     : negm,
										marks    : question_create.marks
									});
								});
							});
						});
					});
				});
			});
		});
	});
});

router.post('/next_s', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"next_s","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id        = req.body.user_id.trim();
	var test_id        = req.body.test_id.trim();
	var user_test_id   = req.body.user_test_id.trim();
	var plugin_id      = req.body.pl_id.trim();
	var gid            = req.body.g_id;
	var tim            = req.tim;
	var is_timed       = (req.decoded.itm) ? req.decoded.itm : false;
	var expTime        = (req.decoded.et) ? req.decoded.et : (tim+(configs.JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME*1000));

	UserTest.findOne({
		_id						: user_test_id,
		test_id       : test_id,
		user_id       : user_id,
		act         	: true,
		is_started  	: true,
		is_submitted	: false
	},function(err, usertest){
		if(err){
			logger.error({"r":"next_s","er":err,"p":req.body,"er":err});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!usertest){
			logger.error({"r":"next_s","msg":"no Test Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		TestQuestion.find({
			test_id : test_id,
			act : true 
		},{
			key : 1,
			_id : 0
		},function(err,test_questions){
			if(err){
				logger.error({"r":"next_s","er":err,"p":req.body,"msg":"testQuestion_allkey_find_error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var test_question_keys = [];
			var total              = test_questions.length;
			for(var i=0; i<total; i++){
				test_question_keys.push(test_questions[i].key);
			}
			UserTestQuestion.find({
				user_test_id : user_test_id,
			},{
				ques_key : 1,
				_id : 0
			},function(err,user_test_questions){
				if(err){
					logger.error({"r":"next_s","er":err,"p":req.body,"msg":"usertestQuestion_find_error"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				var user_test_question_keys = [];
				var u_total                 = user_test_questions.length;
				if(u_total >= usertest.total_ques){
					logger.error({"r":"next_s","msg":"All_Questions_For_Test_Attempted","p":req.body,"test_id":test_id,"u_total":u_total});
					return sendError(res,"All Questions of This Test Has Been Attempted","all_test_questions_attempted",HttpStatuses.NO_DATA);
				}
				for(var i=0; i<u_total; i++){
					user_test_question_keys.push(user_test_questions[i].ques_key);
				}
				var final_keys          = filter_array(test_question_keys, user_test_question_keys);
				var question_key_create = generateRandomNumFromSet(final_keys, usertest.q_order);
				TestQuestion.findOne({
					test_id : test_id,
					key     : question_key_create
				},function(err,question_create){
					if(err){
						logger.error({"r":"next_s","er":err,"p":req.body,"msg":"testQuestion_find_question_with_key_error"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!question_create){
						logger.error({"r":"next_s","msg":"test_question_with_key_not_found","p":req.body,"test_id":test_id,"key":question_key_create});
						return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
					}
					Question.findOne({
						_id : question_create.ques_id,
					},{
						text        : 1,
						img_arr     : 1,
						ans_options : 1,
						correct_ans : 1
					},function(err,question){
						if(err){
							logger.error({"r":"next_s","er":err,"p":req.body,"msg":"Question_find_after_ID_error"});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						if(!question){
							logger.error({"r":"next_s","msg":"question_withID_not_found","p":req.body,"test_id":test_id,"key":question_key_create});
							return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
						}
						getSignedUrlForAllMedia(question, function(err,urlsArrayObject){
							if(err){
								logger.error({"r":"next_s","er":err,"p":req.body,"msg":"getSignedUrlForAllMediaError"});
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}
							var new_user_test_question_obj = {
								plugin_id       : plugin_id,
					      gid             : gid,
								user_test_id 		: user_test_id,
								test_id         : test_id,
								ques_id 				: question_create.ques_id,
								ques_key 				: question_key_create,
								max_marks_ques 	: question_create.marks,
								correct_answer  : question.correct_ans,
								tim             : tim,
								act             : true
							};
							var ineg = false;
							var negm = 0;
							if(question_create.ineg && question_create.negm && question_create.ineg == true && question_create.negm > 0){
								new_user_test_question_obj.ineg = question_create.ineg;
								new_user_test_question_obj.negm = question_create.negm;
								ineg = question_create.ineg;
								negm = question_create.negm;
							}
							var new_user_test_question = new UserTestQuestion(new_user_test_question_obj);
							new_user_test_question.save(function(err,created_question){
								if(err){
									logger.error({"r":"next_s","er":err,"p":req.body,"msg":"new_user_test_question_create_error"});
									return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
								}
								// logger.info({"r":"next_s","msg":"success","resp":question,"p":req.body});
								return sendSuccess(res,{
									question : question,
									img_arr  : urlsArrayObject,
									itm      : is_timed, 
									rtim     : (expTime-(new Date().getTime())),
									ineg     : ineg,
									negm     : negm,
									marks    : question_create.marks
								});
							});
						});
					});
				});
			});
		});
	});
});

router.post('/ans', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ques_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ans_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ans_text',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()) { 
		logger.error({"r":"ans","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id       = req.body.user_id.trim();
	var test_id       = req.body.test_id.trim();
	var user_test_id  = req.body.user_test_id.trim();
	var ques_id       = req.body.ques_id.trim();
	var ans_id        = req.body.ans_id.trim();
	var ans_text      = (req.body.ans_text) ? (req.body.ans_text.trim()) : "";
	var tim           = req.tim;
	var is_timed      = (req.decoded.itm) ? req.decoded.itm : false;
	var expTime       = (req.decoded.et) ? req.decoded.et : (tim+(configs.JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME*1000));

	ans_id            = parseInt(ans_id);

	UserTest.findOne({
		_id					: user_test_id,
		user_id     : user_id,
		test_id     : test_id,
		act         : true,
		is_started  : true,
		is_submitted: false
	},function(err, usertest){
		if(err){
			logger.error({"r":"ans","er":err,"p":req.body,"msg":"UserTest_DbError"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!usertest){
			logger.error({"r":"ans","msg":"no Test Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		var user_test_id = usertest._id+'';
		UserTestQuestion.findOne({
			user_test_id : user_test_id,
			ques_id      : ques_id,
		},function(err,usertestquestion){
			if(err){
				logger.error({"r":"ans","er":err,"p":req.body,"msg":"UserTestQuestion_DbError"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!usertestquestion){
				logger.error({"r":"ans","msg":"no Test Question Found","p":req.body});
				return sendError(res,"No Cateogries Found","no_testquestion_found",HttpStatuses.BAD_REQUEST);
			}
			var is_correct 		 = false;
			var marks_obtained = 0;
			if(usertestquestion.correct_answer == ans_id){
				is_correct     = true;
				marks_obtained = usertestquestion.max_marks_ques;
			} else if(usertestquestion.ineg && usertestquestion.negm && usertestquestion.ineg == true && usertestquestion.negm > 0){
				marks_obtained = (-1*usertestquestion.negm);
			}
			marks_obtained = parseFloat(marks_obtained.toFixed(2));
			UserTestQuestion.findOne({
				user_test_id 		: user_test_id,
				ques_id 				: ques_id
			},{
				is_attempted : 1,
				is_correct   : 1
			},function(err,usertestquestion){
				if(err){
					logger.error({"r":"ans","er":err,"p":req.body,"msg":"UserTestQuestion_DbError"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				UserTestQuestion.update({
					_id : usertestquestion._id
				},{
					ans_no         : ans_id,
					ans_text       : ans_text,
					is_correct     : is_correct,
					marks_obtained : marks_obtained,
					is_attempted   : true
				},function(err,answer_updated){
					if(err){
						logger.error({"r":"ans","er":err,"p":req.body,"msg":"UserTestQuestion_DbError_update"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					UserTest.update({
						_id : user_test_id
					},{
						l_u_tim : tim,
						l_u_q_id: ques_id
					},function(err,resp){
						if(err){
							logger.error({"r":"ans","er":err,"p":req.body,"msg":"UserTest_Update_DbErrro"});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						var inc_obj = {};
						var tinc_obj= {};

						if(usertestquestion && usertestquestion.is_attempted){
							if(usertestquestion.is_correct != is_correct){
								if(usertestquestion.is_correct == true){
									inc_obj.correct_subm = -1;
									tinc_obj.crct = -1;
								} else {
									inc_obj.correct_subm = 1;
									tinc_obj.crct = 1;
								}
							}
						} else {
							inc_obj.attempts = 1;
							tinc_obj.atmp = 1;
							if(is_correct){
								inc_obj.correct_subm = 1;
								tinc_obj.crct = 1;
							}
						}
						
						if(Object.keys(inc_obj).length == 0){
							return sendSuccess(res,{
								itm  : is_timed, 
								rtim : (expTime-(new Date().getTime()))
							});
						}
						Question.update({
							_id : ques_id
						},{
							$inc : inc_obj
						},function(err,question_updated){
							if(err){
								logger.error({"r":"ans","er":err,"p":req.body,"msg":"question_increment_err"});
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}
							TestQuestion.update({
								_id : testquestion._id
							},{
								$inc : tinc_obj
							},function(err,updated_testquestion){
								if(err){
									logger.error({"r":"ans","er":err,"p":req.body,"msg":"TestQuestion_DbError"});
									return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
								}
								return sendSuccess(res,{
									itm  : is_timed, 
									rtim : (expTime-(new Date().getTime()))
								});
							});
						});
					});
				});
			});
		})
	});
});

router.post('/ans_v2', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ques_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ans_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ans_text',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('qtim',errorCodes.invalid_parameters[1]).optional();
	
	if(req.validationErrors()) { 
		logger.error({"r":"ans_v2","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	var plugin_id     = req.body.pl_id.trim();
	var gid     			= req.body.g_id.trim();
	var user_id       = req.body.user_id.trim();
	var test_id       = req.body.test_id.trim();
	var user_test_id  = req.body.user_test_id.trim();
	var ques_id       = req.body.ques_id.trim();
	var ans_id        = req.body.ans_id.trim();
	var time_spent    = (!req.body.qtim) ? 0 :  parseInt(req.body.qtim);
	var ans_text      = (req.body.ans_text) ? (req.body.ans_text.trim()) : "";
	var tim           = req.tim;
	var is_timed      = (req.decoded.itm) ? req.decoded.itm : false;
	var expTime       = (req.decoded.et) ? req.decoded.et : (tim+(configs.JWT_USERTEST_ACCESS_TOKEN_EXPIRY_TIME*1000));

	ans_id            = parseInt(ans_id);

	UserTest.findOne({
		_id					: user_test_id,
		user_id     : user_id,
		test_id     : test_id,
		act         : true,
		is_started  : true,
		is_submitted: false
	},function(err, usertest){
		if(err){
			logger.error({"r":"ans_v2","er":err,"p":req.body,"msg":"UserTest_DbError"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!usertest){
			logger.error({"r":"ans_v2","msg":"no Test Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		TestQuestion.findOne({
			test_id : test_id,
			ques_id : ques_id,
			act     : true
		},function(err,testquestion){
			if(err){
				logger.error({"r":"ans_v2","er":err,"p":req.body,"msg":"TestQuestion_DbError"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!testquestion){
				logger.error({"r":"ans_v2","msg":"no Test Question Found","p":req.body});
				return sendError(res,"no_testquestion_found","no_testquestion_found",HttpStatuses.BAD_REQUEST);
			}
			Question.findOne({
				_id : ques_id
			},{
				correct_ans : 1
			},function(err,question){
				if(err){
					logger.error({"r":"ans_v2","er":err,"p":req.body,"msg":"Question_DbError"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!question){
					logger.error({"r":"ans_v2","msg":"question_not_found","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
				}
				var is_correct 		 = false;
				var marks_obtained = 0;
				var ineg           = false;
				var negm           = 0;

				if(question.correct_ans == ans_id){
					is_correct     = true;
					marks_obtained = testquestion.marks;
				} else if(testquestion.ineg && testquestion.negm && testquestion.ineg == true && testquestion.negm > 0){
					marks_obtained = (-1*testquestion.negm);
					ineg = true;
					negm = testquestion.negm;
				}
				marks_obtained = parseFloat(marks_obtained.toFixed(2));
				UserTestQuestion.findOne({
					user_test_id 		: user_test_id,
					ques_id 				: ques_id
				},{
					is_attempted : 1,
					is_correct   : 1
				},function(err,usertestquestion){
					if(err){
						logger.error({"r":"ans_v2","er":err,"p":req.body,"msg":"UserTestQuestion_DbError"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					UserTestQuestion.update({
						user_test_id 		: user_test_id,
						ques_id 				: ques_id
					},{
						$setOnInsert : {
							plugin_id       : plugin_id,
						  gid             : gid,
							user_test_id 		: user_test_id,
							test_id         : test_id,
							ques_id 				: ques_id,
							ques_key        : testquestion.key,
							qlv             : testquestion.lv,
							max_marks_ques  : testquestion.marks,
							ineg            : ineg,
							negm            : negm,
							correct_answer  : question.correct_ans,
							tim             : tim,
							act             : true
						},
						$set : {
							ans_no         : ans_id,
							ans_text       : ans_text,
							is_correct     : is_correct,
							marks_obtained : marks_obtained,
							qtim           : time_spent,
							is_attempted   : true
						}
					},{
						upsert : true
					},function(err,answer_updated){
						if(err){
							logger.error({"r":"ans_v2","er":err,"p":req.body,"msg":"UserTestQuestion_DbError_update"});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						var recent_ques_status;
						if(is_correct == true){
							recent_ques_status = UserTest.RECENT_MOST_QUESTION_STATUS.RIGHT;
						} else {
							recent_ques_status = UserTest.RECENT_MOST_QUESTION_STATUS.WRONG;
						}
						UserTest.update({
							_id : user_test_id
						},{
							l_u_tim  : tim,
							l_u_q_id : ques_id,
							r_q_lv   : testquestion.lv,
							r_q_st   : recent_ques_status
						},function(err,resp){
							if(err){
								logger.error({"r":"ans_v2","er":err,"p":req.body,"msg":"UserTest_Update_DbErrro"});
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}

							var inc_obj = {};
							var tinc_obj= {};
							
							if(usertestquestion && usertestquestion.is_attempted){
								if(usertestquestion.is_correct != is_correct){
									if(usertestquestion.is_correct == true){
										inc_obj.correct_subm = -1;
										tinc_obj.crct = -1;
									} else {
										inc_obj.correct_subm = 1;
										tinc_obj.crct = 1;
									}
								}
							} else {
								inc_obj.attempts = 1;
								tinc_obj.atmp = 1;
								if(is_correct){
									inc_obj.correct_subm = 1;
									tinc_obj.crct = 1;
								}
							}
							if(Object.keys(inc_obj).length == 0){
								return sendSuccess(res,{
									itm  : is_timed, 
									rtim : (expTime-(new Date().getTime()))
								});
							}
							Question.update({
								_id : ques_id
							},{
								$inc : inc_obj
							},function(err,question_updated){
								if(err){
									logger.error({"r":"ans_v2","er":err,"p":req.body,"msg":"question_increment_err"});
									return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
								}
								TestQuestion.update({
									_id : testquestion._id
								},{
									$inc : tinc_obj
								},function(err,updated_testquestion){
									if(err){
										logger.error({"r":"ans_v2","er":err,"p":req.body,"msg":"TestQuestion_DbError"});
										return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
									}
									return sendSuccess(res,{
										itm  : is_timed, 
										rtim : (expTime-(new Date().getTime()))
									});
								});
							});
						});
					});
				});
			});
		})
	});
});

function getSignedUrlForAllMedia(question, cb){
	var resultArray = [];
	if( !("img_arr" in question) || question.img_arr == null || question.img_arr.length == 0){
		return cb(null,resultArray);
	}
	var image_array = question.img_arr;
	var toDo   = image_array.length;
	var done   = 0;
	var bucket = AwsBuckets.QUES_PIC;
	var mode   = AwsModes.IMAGE;
	var tempArr;
	for(var i=0; i<toDo; i++){
		tempArr = image_array[i];
		if( !tempArr || !("fnm" in tempArr) || !("ftp" in tempArr)){
			done++;
			if(done == toDo){
				return cb(null,resultArray);
			}
			continue;
		}
		getAwsUrl(bucket, tempArr.fnm, tempArr.ftp, mode, function(err,resp){
			if(err){
				return cb(err,null);
			}
			resultArray[resp.file_name] = resp.signedUrl;
			done++;
			if(done == toDo){
				return cb(null,resultArray);
			}
		});
	}
}

function getAwsUrl(bucket,file_name, file_type, mode, cb){
	AwsModule.getSignedUrlS3Upload(bucket, file_name, file_type, mode, function(err,signedUrl){
		if(err){
			return cb(err,null);
		}
		return cb(null,{file_name:signedUrl});
	});
}

function filter_array(myArray, toRemove) {
  myArray = myArray.filter( function( el ) {
	  return toRemove.indexOf( el ) < 0;
	});
	return myArray;
}

function generateRandomNumFromSet(values, ques_order){
	var len = values.length;
 	if(ques_order && ques_order == Test.QUESTION_ORDER.ORDERED){
 		values.sort(function(a, b){
		 return a-b;
		});
		return values[0];
 	} else {
		return values[Math.floor(Math.random() * len)];
 	}
}

function generateRandomNumFromSetStage2(values){
	var len = values.length;
	return values[Math.floor(Math.random() * len)];
}


module.exports = router;