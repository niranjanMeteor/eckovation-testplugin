const express 							  = require('express');
const mongoose 							  = require('mongoose');
const jwt    							    = require('jsonwebtoken');

const configs					        = require('../configs/configs.js');
const HttpStatuses					  = require('../global/http_api_return_codes.js');
const ApiReturnHandlers			  = require('../global/api_return_handlers.js');
const errorCodes 						  = require('../global/error_codes.js');
const log4jsLogger  				  = require('../loggers/log4js_module.js');
const log4jsConfigs  			    = require('../loggers/log4js_configs.js');
const AuthSecurity      		  = require('../security/auth_tokens.js');
const RootServerApi      		  = require('../server/apicalls.js');
const TagModule               = require('../lib/tags/topictags.js');

require('../models/tests.js');
require('../models/users.js');
require('../models/testquestions.js');
require('../models/pluginpaidinfos.js');
require('../models/tags.js');
require('../models/testtags.js');
const Tag 									  = mongoose.model('Tag');
const TestTag 							  = mongoose.model('TestTag');
const PluginPaidInfo 				  = mongoose.model('PluginPaidInfo');
const User                    = mongoose.model('User');
const Test 									  = mongoose.model('Test');
const TestQuestion					  = mongoose.model('TestQuestion');

const sendError 						  = ApiReturnHandlers.sendError;
const sendSuccess 					  = ApiReturnHandlers.sendSuccess;
const router 								  = express.Router();
const logger        				  = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_TAG_ADMIN);

const validLevels             = [Test.TEST_LEVELS.BEGINERS,Test.TEST_LEVELS.EXPERIENCED,Test.TEST_LEVELS.ADVANCED,Test.TEST_LEVELS.EXPERT];

//route middleware to verify the jwt token for all user whether they are group admin
router.use(function(req, res, next){
	var user_id   = req.body.u_id;
	var gid       = req.body.g_id;
	var plugin_id = req.body.pl_id;
	var private_key;
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body,"r":"2nd_middleware","tkn":req.body.tokn});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
	  jwt.verify(req.body.tokn, private_key, function(err, decoded) {      
	    if(err){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err,"tkn":req.body.tokn});
	      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
	    } 
	    if(decoded.id1 != user_id){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_userid","p":req.body,"tkn":req.body.tokn});
		  	return sendError(res,"Access without valid user id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
	    }
	    if(decoded.id4 != gid){
	    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body,"tkn":req.body.tokn});
		  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
	    }
	    User.findOne({
	    	_id : user_id,
				act : true,
			},function(err,user){
				if(err){
					logger.error({"url":req.originalUrl,"msg":"user_find_failed","er":err,"p":req.body,"tkn":req.body.tokn});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!user){
					logger.info({"url":req.originalUrl,"msg":"user_already_exists","p":req.body,"tkn":req.body.tokn});
					return sendSuccess(res,{ id : user._id, if_new : 0, srv_id : user.server_id, gid : user.gid });
				}
				RootServerApi.checkIfAdmin(user.identifier, function(err,resp){
					if(err){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_err","er":err,"p":req.body,"tkn":req.body.tokn});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!resp.success){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_failed","p":req.body,"tkn":req.body.tokn});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!resp.is_admin){
						logger.error({"url":req.originalUrl,"msg":"rootServer_checkIfAdmin_UserIsNotGroupAdmin","p":req.body,"tkn":req.body.tokn});
						return sendError(res,"unauthorised","unauthorised",HttpStatuses.UNAUTHORIZED);
					}
			    next();
				});
			});
	  });
	});
});

//get tags in a test
router.post('/t_tg',function(req, res, next){

	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"t_tg","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var test_id = req.body.test_id.trim();

	Test.findOne({
		_id : test_id,
		act : true
	},{
		adpv : 1
	},function(err,test){
		if(err){
			logger.error({"r":"t_tg","er":err,"p":req.body,"msg":"TestFind_DbError"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"t_tg","p":req.body,"msg":"test_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		TestTag.find({
			tid : test_id,
			act : true
		},{
			tgid: 1,
			rst : 1,
			rsd : 1
		},function(err,testtags){
			if(err){
				logger.error({"r":"t_tg","er":err,"p":req.body,"msg":"TestTag_Find_DbError"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var total   = testtags.length;
			if(total == 0){
				logger.warn({"r":"t_tg","msg":"no_existing_tag_ids_found_for_this_test","p":req.body});
				return sendSuccess(res,{});
			}
			var tag_ids = [];
			var resourceByTagId = {};
			var temp;
			for(var i=0; i<total; i++){
				temp = testtags[i].tgid+'';
				if(tag_ids.indexOf(temp) < 0){
					tag_ids.push(temp);
				}
				if(!resourceByTagId[temp]){
					resourceByTagId[temp] = [];
				}
				var ttags = {};
				var resclen  = testtags[i].rsd.length;
				for(var m=0; m<resclen; m++){
					ttags = {
						t:testtags[i].rst,
						d:testtags[i].rsd[m]
					};
					resourceByTagId[temp].push(ttags);
				}
			}
			if(tag_ids.length == 0){
				logger.warn({"r":"t_tg","msg":"test_tag_ids_mismatch","p":req.body});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			Tag.find({
				_id : {$in : tag_ids},
				act : true
			},{
				nm : 1,
				dsc: 1
			},function(err,tags){
				if(err){
					logger.error({"r":"t_tg","er":err,"p":req.body,"msg":"TagFind_DbError"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				var ttotal = tags.length;
				if(ttotal == 0){
					logger.warn({"r":"t_tg","msg":"test_tag_ids_not_found_in_tag","p":req.body});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				var temp = {};
				var result = {};
				for(var j=0; j<ttotal; j++){
					temp = {
						_id : tags[j]._id+'',
						nm  : tags[j].nm,
						dsc : tags[j].dsc,
						rs  : resourceByTagId[tags[j]._id+'']
					}
					result[tags[j]._id+''] = temp;
				}
				return sendSuccess(res,{tags : result});
			})
		});
	});
});

/************************** Test Tag CRUD PANEL Start ****************************/

// add tag in a test
router.post('/ad', function(req, res, next){

	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('nm',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('rs',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('dsc',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"ed","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	console.log(1);
	var test_id         = req.body.test_id.trim();
	var tagname         = req.body.nm;
	var tag_resource    = req.body.rs;
	var tag_desc        = req.body.dsc;
	var tim             = new Date().getTime();
	console.log(req.body);
	if(typeof(tag_resource) != "object" || tag_resource.length == 0){
		logger.error({"r":"ad","p":req.body,"msg":"tags_resource_invalid"});
		return sendError(res,"tags_resource_invalid","tags_resource_invalid",HttpStatuses.SERVER_SPECIFIC_ERROR);
	}

	var rtot, rtemp;
	console.log(3);
	var rtot = tag_resource.length;
	for(var j=0; j<rtot; j++){
		rtemp = tag_resource[j];
		if(!rtemp.t || !rtemp.d){
			logger.error({"r":"ad","p":req.body,"msg":"tag_resource_data_structure_invalid"});
			return sendError(res,"tag_resource_data_structure_invalid","tag_resource_data_structure_invalid",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
	}
	console.log(4);
	tagname = tagname.toLowerCase();

	Test.findOne({
		_id     : test_id,
		act     : true
	},{
		adpv : 1,
		level: 1
	},function(err,test){
		if(err){
			logger.error({"r":"ad","er":err,"p":req.body,"msg":"Test_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"ad","p":req.body,"msg":"test_not_found"});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		console.log(5);
		TagModule.save_tag(
			test_id, 
			tagname, 
			tag_resource, 
			tag_desc,
			tim,
		function(err, tag_save_resp){
	    if(err){
	      logger.error({"r":"ad","msg":"tag_saving_error","p":req.body,"dt":tag_save_resp,"er":err});
	      return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
	    }
	    if(!tag_save_resp.inew){
	    	logger.error({"r":"ad","msg":"tag_saving_error","p":req.body,"dt":tag_save_resp});
	      return sendError(res,"test_tag_already_exists","test_tag_already_exists",HttpStatuses.SERVER_SPECIFIC_ERROR);	
	    }console.log(6);console.log(tag_save_resp.data);
			Test.update({
				_id : test_id,
			},{
				$push : {tags : tag_save_resp.data._id}
			},function(err,test_updated){
				if(err){
					logger.error({"r":"ad","er":err,"p":req.body,"msg":"TestUpdate_DbError"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}console.log(7);
				return sendSuccess(res,{tag : tag_save_resp.data});
			});
	  });
	});
});

//edit tag data 
router.post('/ed', function(req, res, next){

	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('tg_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('nm',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('rs',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('dsc',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()) { 
		logger.error({"r":"ed","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var test_id         = req.body.test_id.trim();
	var tag_id          = req.body.tg_id.trim();
	
	var rtot, rtemp;
	var toEdit = {};

	if(req.body.rs){
		var resc = req.body.rs;
		if(typeof(resc) != "object" || resc.length == 0){
			logger.error({"r":"ad","p":req.body,"msg":"tags_resource_invalid"});
			return sendError(res,"tags_resource_invalid","tags_resource_invalid",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		rtot = resc.length;
		for(var j=0; j<rtot; j++){
			rtemp = resc[j];
			if(!rtemp.t || !rtemp.d){
				logger.error({"r":"ed","p":req.body,"msg":"tag_resource_data_structure_invalid"});
				return sendError(res,"tag_resource_data_structure_invalid","tag_resource_data_structure_invalid",HttpStatuses.SERVER_SPECIFIC_ERROR);
			}
		}	
		toEdit.resc = resc;
	}
	if(req.body.nm && (req.body.nm != null || req.body.nm != "")){
		toEdit.nm = req.body.nm;
	}
	if(req.body.dsc && (req.body.dsc != null || req.body.dsc != "")){
		toEdit.dsc = req.body.dsc;
	}

	Tag.findOne({
		_id : tag_id,
		act : true
	},{
		nm : 1
	},function(err,tag){
		if(err){
			logger.error({"r":"ed","err":err,"msg":"TagFind_Error","p":req.body});
      return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!tag){
			logger.error({"r":"ed","p":req.body,"msg":"tag_not_found"});
			return sendError(res,"tag_not_found","tag_not_found",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		TestTag.findOne({
			tid : test_id,
			tgid: tag_id,
			act : true
		},{
			_id : 1
		},function(err,testtag){
			if(err){
				logger.error({"r":"ed","msg":"TestTagFind_Error","p":req.body});
	      return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!testtag){
				logger.error({"r":"ed","p":req.body,"msg":"test_tag_not_found"});
				return sendError(res,"test_tag_not_found","test_tag_not_found",HttpStatuses.SERVER_SPECIFIC_ERROR);
			}
			if(Object.keys(toEdit).length == 0){
				logger.error({"r":"ed","p":req.body,"msg":"no_data_to_edit"});
				return sendError(res,"no_data_to_edit","no_data_to_edit",HttpStatuses.SERVER_SPECIFIC_ERROR);
			}
			TagModule.update_tag(test_id, tag_id, toEdit, function(err, updated_tag){
				if(err){
					logger.error({"r":"ad","p":req.body,"msg":"TestTag_UpdateTag_Error"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_SPECIFIC_ERROR);
				}
				return sendSuccess(res,{});
			});
		});
	});
});

//delete a tag from a test
router.post('/dl', function(req, res, next){

	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('tg_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"dl","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var test_id         = req.body.test_id.trim();
	var tag_id          = req.body.tg_id.trim();
	
	Tag.findOne({
		_id : tag_id,
		act : true
	},{
		nm : 1
	},function(err,tag){
		if(err){
			logger.error({"r":"dl","err":err,"msg":"TagFind_Error","p":req.body});
      return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!tag){
			logger.error({"r":"dl","p":req.body,"msg":"tag_not_found"});
			return sendError(res,"tag_not_found","tag_not_found",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		TestTag.findOne({
			tid : test_id,
			tgid: tag_id,
			act : true
		},{
			_id : 1
		},function(err,testtag){
			if(err){
				logger.error({"r":"dl","msg":"TestTagFind_Error","p":req.body});
	      return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!testtag){
				logger.error({"r":"dl","p":req.body,"msg":"test_tag_not_found"});
				return sendError(res,"test_tag_not_found","test_tag_not_found",HttpStatuses.SERVER_SPECIFIC_ERROR);
			}
			TestTag.update({
				tid : test_id,
				tgid: tag_id,
				act : true
			},{
				act : false
			},{
				multi : true
			},function(err,deleted_test_tag){
				if(err){
					logger.error({"r":"dl","p":req.body,"msg":"TestTag_UpdateTag_DbError"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_SPECIFIC_ERROR);
				}
				Test.update({
					_id     : test_id,
				},{
					$pull : {tags : tag_id}
				},function(err,test_updated){
					if(err){
						logger.error({"r":"dl","er":err,"p":req.body,"msg":"TestUpdate_DbError"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					return sendSuccess(res,{});
				});
			});
		});
	});
});

/************************** Test Tag CRUD PANEL End ****************************/

module.exports = router;