const express 							= require('express');
const mongoose 							= require('mongoose');
const jwt    							  = require('jsonwebtoken');

const configs					      = require('../configs/configs.js');
const catConfigs					  = require('../configs/catconfigs.js');
const HttpStatuses					= require('../global/http_api_return_codes.js');
const ApiReturnHandlers			= require('../global/api_return_handlers.js');
const errorCodes 						= require('../global/error_codes.js');
const log4jsLogger  				= require('../loggers/log4js_module.js');
const log4jsConfigs  			  = require('../loggers/log4js_configs.js');
const TestAnswersModule     = require('../cat/test_answer_module.js');
const TestStatistics        = require('../statistics/test.js');

require('../models/tests.js');
require('../models/testquestions.js');
require('../models/usertestquestions.js');
require('../models/usertests.js');
require('../models/questions.js');
require('../models/users.js');
const User                  = mongoose.model('User');
const Test 									= mongoose.model('Test');
const TestQuestion					= mongoose.model('TestQuestion');
const UserTestQuestion		  = mongoose.model('UserTestQuestion');
const UserTest		          = mongoose.model('UserTest');
const Question					    = mongoose.model('Question');

const sendError 						= ApiReturnHandlers.sendError;
const sendSuccess 					= ApiReturnHandlers.sendSuccess;
const router 								= express.Router();
const logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_CAT_ANSWER);

//route middleware to verify the jwt user test token
router.use(function(req, res, next){

	var token     = req.body.t_tokn;
	var user_id   = req.body.user_id;
	var test_id   = req.body.test_id;

	if(!token){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","invalid_parameters",HttpStatuses.FORBIDDEN);
	}
	if(!test_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","invalid_parameters",HttpStatuses.FORBIDDEN);
	}
  jwt.verify(token, catConfigs.JWT_USERTEST_ACCESS_TOKEN_PRIVATE_KEY_CAT, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"test_token_jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate Test token.","usertest_token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid id is not authorised","invalid_usertest_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id2 != test_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid test id is not authorised","invalid_usertest_token",HttpStatuses.FORBIDDEN);
    }
    req.decoded = decoded;
    next();
  });
});

router.post('/ans', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('sec',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ques_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ans_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ans_text',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()) { 
		logger.error({"r":"ans","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	var user_id     = req.body.user_id.trim();
	var test_id     = req.body.test_id.trim();
	var user_test_id= req.body.user_test_id.trim();
	var section     = req.body.sec.trim();
	var ques_id     = req.body.ques_id.trim();
	var ans_id      = req.body.ans_id.trim();
	var ans_text    = (req.body.ans_text) ? (req.body.ans_text.trim()) : "";
	var decoded     = req.decoded;
	var plugin_id   = decoded.id3;
	var gid         = decoded.id4;
	var tim         = new Date().getTime();

	if(!isValidSectionCall(decoded, section, tim)){
		logger.error({"r":"ans","msg":"isValidSectionCall_False","p":req.body,"dec":decoded,"tm":tim});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	UserTest.findOne({
		_id          : user_test_id,
		user_id      : user_id,
		test_id      : test_id,
		act          : true,
		is_started   : true,
		is_submitted : false
	},{
		cats : 1
	},function(err,usertest){
		if(err){
			logger.error({"r":"ans","er":err,"p":req.body,"msg":"UserTestFind_DbError"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!usertest){
			logger.error({"r":"ans","p":req.body,"msg":"usertest_not_found"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		updateAnswer(
			user_id, 
			test_id, 
			user_test_id,
			plugin_id,
			gid,
			ques_id, 
			ans_id, 
			section, 
			tim,
		function(err, resp_update){
			if(err){
				logger.error({"r":"ans","er":err,"p":req.body,"msg":"updateAnswer_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			logger.info({"r":"ans","er":err,"p":req.body,"msg":"Success"});
			sendSuccess(res,{ques_id:ques_id,ans_id:ans_id,user_test_id:user_test_id});
			UserTest.update({
				_id : user_test_id
			},{
				lcatsec       : section,
				lcatsecut     : tim
			},function(err,updated_section_info){
				if(err){
					logger.error({"r":"ans","er":err,"p":req.body,"msg":"UserTest_Update_secion_info_error"});
				}
				return;
			});
		});
	});
});

router.post('/subm', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_test_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('sec',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('answers',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"subm","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	var user_id     = req.body.user_id.trim();
	var test_id     = req.body.test_id.trim();
	var user_test_id= req.body.user_test_id.trim();
	var section     = req.body.sec.trim();
	var decoded     = req.decoded;
	var plugin_id   = decoded.id3;
	var gid         = decoded.id4;
	var tim         = new Date().getTime();

	if(typeof(req.body.answers) != "object"){
		logger.error({"r":"subm","msg":"answers_object_not_valid","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	if(Object.keys(req.body.answers).length == 0){
		logger.error({"r":"subm","msg":"answers_object_array_null","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	if(tim > decoded.et){
		logger.error({"r":"subm","msg":"submit_time_limit_exceeded","p":req.body,"dec":decoded,"tm":tim});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	var answers = req.body.answers;

	Test.findOne({
		_id : test_id,
		act : true
	},function(err,test){
		if(err){
			logger.error({"r":"subm","er":err,"p":req.body,"er":err,"msg":"Test_find_DbError"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!test){
			logger.error({"r":"subm","msg":"Test_Not_Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		UserTest.findOne({
			_id          : user_test_id,
			user_id      : user_id,
			test_id      : test_id,
			act          : true,
			is_started   : true,
			is_submitted : false
		},function(err,usertest){
			if(err){
				logger.error({"r":"subm","er":err,"p":req.body,"msg":"UserTestFind_DbError"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!usertest){
				logger.error({"r":"subm","p":req.body,"msg":"usertest_not_found"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			TestAnswersModule.submit(
				gid, 
				plugin_id, 
				user_id, 
				test_id, 
				user_test_id, 
				answers, 
				section,
			function(err,answers_data){
				if(err){
					logger.error({"r":"subm","er":err,"p":req.body,"msg":"TestAnswersModule_Error"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}

				console.log('answers_data_from_: TestAnswersModule_submit');
				console.log(answers_data);

				var user_already_gave_test 	= false;
				var ssol = true;
				if(test.ssol && test.ssol == false){
					ssol = false;
				}

				UserTest.findOne({
					user_id 			: user_id,
					test_id 			: test_id,
					is_submitted 	: true,
				},function(err,test_already_given){
					if(err){
						logger.error({"r":"subm","er":err,"p":req.body,"er":err,"msg":"UserTest_find_DbError"});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}

					var total_ques  = usertest.total_ques;
					var total_marks = answers_data.umarks;
					var start_time  = usertest.start_time;
					var time_taken  = tim - usertest.start_time;

					var initial_update = {
						is_submitted   : true,
						act 				   : false,
						end_time       : tim,
						total_correct  : answers_data.correct_count,
						if_all_correct : answers_data.if_all_correct,
						time_taken     : time_taken
					};
					if(test_already_given){
						user_already_gave_test  = true;
					} else {
						initial_update.is_first = true;
					}
					
					UserTest.update({
						_id : user_test_id
					},initial_update,function(err,updated_usertest){
						if(err){
							logger.error({"r":"subm","er":err,"p":req.body,"er":err,"msg":"UserTest_update_DbError"});
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}

						TestStatistics.updateCatSubmit(
							plugin_id, 
							user_test_id, 
							test_id, 
							total_ques, 
							time_taken, 
							user_id, 
							user_already_gave_test, 
							test.model, 
							total_marks,
							answers_data.correct_count,
							answers_data.if_all_correct,
							answers_data.attempted,
						function(err,statsData){
							if(err){
								logger.error({"r":"subm","er":err,"p":req.body,"er":err,"msg":"updateAllStatisticsOnSubmit_Error"});
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}

							var finalData   = {};
							finalData.user_test_id = user_test_id;
							finalData.test_id      = test_id;
							finalData.start_time   = start_time;
							finalData.time_taken   = time_taken;
							finalData.total_ques   = total_ques;
							finalData.total_marks  = total_marks;
							finalData.test_total_marks  = test.total_marks;
							finalData.ssol         = ssol;
							finalData.ansdata      = answers_data;
							finalData.sec_info     = catConfigs.CAT_TEST_SECTIONS,

							logger.info({"r":"subm","p":req.body,"msg":"Success"});
							return sendSuccess(res,finalData);
						});
					});
				});
			});
		});
	});
});

function updateAnswer(
	user_id, 
	test_id, 
	user_test_id,
	plugin_id,
	gid,
	ques_id, 
	ans_id, 
	section, 
	tim,
	cb)
{
	TestQuestion.findOne({
		test_id : test_id,
		ques_id : ques_id,
		act     : true
	},function(err,testquestion){
		if(err){
			return cb(err,null);
		}
		if(!testquestion){
			return cb("testquestion_not_found",null);
		}
		Question.findOne({
			_id : ques_id
		},function(err,question){
			if(err){
				return cb(err,null);
			}
			if(!question){
				return cb("question_not_found",null);
			}
			var statusAndMarks = getAnsCorrectAndMarks(testquestion, question, ans_id);
			UserTestQuestion.update({
				user_test_id : user_test_id,
				ques_id      : ques_id,
				act          : true
			},{
				$setOnInsert : {
					user_test_id : user_test_id,
					ques_id      : ques_id,
					test_id      : test_id,
					plugin_id    : plugin_id,
					gid          : gid,
					act          : true
				},
				$set : {
					is_attempted   : true,
					ans_no         : ans_id,
					is_correct     : statusAndMarks.is_correct,
					correct_answer : question.correct_ans,
					marks_obtained : statusAndMarks.marks,
					max_marks_ques : testquestion.marks,
					ques_key       : testquestion.key,
					ineg           : (!testquestion.ineg) ? false : true,
					negm           : (!testquestion.negm) ? 0 : testquestion.negm,
					code           : section,
					tim            : tim,
				}
			},{
				upsert : true
			},function(err,answer_updated){
				if(err){
					return cb(err,null);
				}
				return cb(null,true);
			});
		})
	});
}

function getAnsCorrectAndMarks(testquestion, question, ans_id){
	var is_correct = false;
	if(question.correct_ans == ans_id){
		is_correct = true;
	}
	var marks_obtained = 0;
	if(question.correct_answer == ans_id){
		is_correct     = true;
		marks_obtained = testquestion.marks;
	} else if(testquestion.ineg && testquestion.negm && testquestion.ineg == true && testquestion.negm > 0){
		marks_obtained = (-1*testquestion.negm);
	}
	marks_obtained = parseFloat(marks_obtained.toFixed(2));
	return {
		is_correct : is_correct,
		marks      : marks_obtained
	};
}

function isValidSectionCall(decoded, section, tim){

	var valid = false;

	switch(section){

		case "A":
			if(tim < decoded.e1){
				valid = true;
			}
			break;

		case "B":
			if(tim < decoded.e2){
				valid = true;
			}
			break;

		case "C":
			if(tim < decoded.e3){
				valid = true;
			}
			break;
	}
	return valid;
}

module.exports = router;