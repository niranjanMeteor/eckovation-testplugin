var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    							  = require('jsonwebtoken');

var configs					      = require('../configs/configs.js');
var HttpStatuses					= require('../global/http_api_return_codes.js');
var ApiReturnHandlers			= require('../global/api_return_handlers.js');
var errorCodes 						= require('../global/error_codes.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var RootServerApi      		= require('../server/apicalls.js');

require('../models/categories.js');
require('../models/tests.js');
require('../models/users.js');
require('../models/usertests.js');
require('../models/pluginpaidinfos.js');
var User                  = mongoose.model('User');
var Test 									= mongoose.model('Test');
var UserTest              = mongoose.model('UserTest');
var Category 							= mongoose.model('Category');
var PluginPaidInfo 				= mongoose.model('PluginPaidInfo');

var sendError 						= ApiReturnHandlers.sendError;
var sendSuccess 					= ApiReturnHandlers.sendSuccess;
var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_CATEGORY);

//route middleware to verify if plugin in group is paid
router.use(function(req, res, next){
	var tim         = new Date().getTime();
	var plugin_id   = req.body.pl_id;
	var token       = req.body.tokn;
	var gid         = req.body.g_id;
	var user_id     = req.body.user_id;

	if(!token || token == 'null' || token === "undefined"){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body,"tkn":token});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!gid){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"gid_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","gid_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!plugin_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"plugin_id_missing","p":req.body});
	  return sendError(res,"Plugin Id Not Found","plugin_id_missing",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			req.private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			req.private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
		req.tim = tim;
	  next();
	});
});

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
  jwt.verify(req.body.tokn, req.private_key, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != req.body.user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != req.body.pl_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_app_id","p":req.body});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id4 != req.body.g_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body});
	  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    next();
  });
});


router.post('/catg', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('first',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ipp',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"catg","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id    = req.body.user_id.trim();
	var plugin_id  = req.body.pl_id.trim();
	var start      = req.body.first.trim();
	var ipp        = req.body.ipp.trim();
	var tim        = new Date().getTime();

	start          = parseInt(start);
	ipp            = parseInt(ipp);

	if(ipp <=0 || ipp > configs.DEFAULT_IPP_FOR_CATEGORIES){
		logger.error({"r":"catg","msg":"Invalid IPP for categories","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	Category.find({
		plugin_id : plugin_id,
		type      : 1,
		act       : true
	},{
		name              : 1,
		pic_url		        : 1,
		total_num_tests   : 1,
		most_popular_test : 1,
		total_attempts    : 1,
		total_submits     : 1
	},{
    sort : {
      total_attempts : -1 
    },
    skip : start,
    limit: ipp
	},function(err, categories){
		if(err){
			logger.error({"r":"catg","er":err,"p":req.body});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(categories.length == 0){
			logger.error({"r":"catg","msg":"no Categories Found","p":req.body});
			return sendError(res,"No Cateogries Found","no_categories_found",HttpStatuses.NO_DATA);
		}
		return sendSuccess(res,{categories:categories});
	});
});

router.post('/catg_grp', function(req, res, next){
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('first',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ipp',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"catg_grp","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id    = req.body.user_id.trim();
	var gid        = req.body.g_id.trim();
	var plugin_id  = req.body.pl_id.trim();
	var start      = req.body.first.trim();
	var ipp        = req.body.ipp.trim();
	var tim        = new Date().getTime();

	start          = parseInt(start);
	ipp            = parseInt(ipp);

	if(ipp <=0 || ipp > configs.DEFAULT_IPP_FOR_CATEGORIES){
		logger.error({"r":"catg_grp","msg":"Invalid IPP for categories","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	User.findOne({
  	_id : user_id,
		act : true,
	},{
		identifier : 1
	},function(err,user){
		if(err){
			logger.error({"r":"catg_grp","er":err,"p":req.body,"msg":"User_Find_Failed"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!user){
			logger.error({"r":"catg_grp","msg":"user_not_found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}

		Category.find({
			plugin_id : plugin_id,
			gid       : gid,
			type      : 1,
			act       : true,
			total_num_tests : { $gt : 0 }
		},{
			name              : 1,
			pic_url		        : 1,
			total_num_tests   : 1,
			total_attempts    : 1,
			total_submits     : 1,
			tim               : 1,
			adpv              : 1,
			adpstg            : 1
		},{
	    sort : {
	      tim : 1
	    },
	    skip : start,
	    limit: ipp
		},function(err, categories){
			if(err){
				logger.error({"r":"catg_grp","er":err,"p":req.body});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			RootServerApi.checkIfAdmin(user.identifier, function(err,resp){
				if(err){
					logger.error({"url":"catg_grp","msg":"rootServer_checkIfAdmin_err","er":err,"p":req.body});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(!resp.success){
					logger.error({"url":"catg_grp","msg":"rootServer_checkIfAdmin_failed","p":req.body});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				var is_admin = false;
				if(("is_admin" in resp)  && resp.is_admin == true){
					is_admin = true;
				}
				if(categories.length == 0){
					logger.error({"r":"catg_grp","msg":"no Categories Found","p":req.body});
					if(is_admin){
						return sendError(res,"No Cateogries Found for Admin","no_categories_found_admin",HttpStatuses.NO_DATA);
					} else {
						return sendError(res,"No Cateogries Found","no_categories_found",HttpStatuses.NO_DATA);
					}
				}
				return sendSuccess(res,{categories:categories,tim:tim,is_admin:is_admin});
			});
		});
	});
});

router.post('/catg_mix', function(req, res, next){
	req.checkBody('user_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('first',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ipp',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"catg_grp","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var user_id    = req.body.user_id.trim();
	var gid        = req.body.g_id.trim();
	var plugin_id  = req.body.pl_id.trim();
	var start      = req.body.first.trim();
	var ipp        = req.body.ipp.trim();
	var tim        = new Date().getTime();

	start          = parseInt(start);
	ipp            = parseInt(ipp);

	if(ipp <=0 || ipp > configs.DEFAULT_IPP_FOR_CATEGORIES){
		logger.error({"r":"catg_grp","msg":"Invalid IPP for categories","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	Category.find({
		plugin_id : plugin_id,
		gid       : gid,
		type      : 2,                               
		act       : true
	},{
		name              : 1,
		pic_url		        : 1,
		total_num_tests   : 1,
		total_attempts    : 1,
		total_submits     : 1,
		tim               : 1
	},{
    sort : {
      total_attempts : -1 
    },
    skip : start,
    limit: ipp
	},function(err, categories){
		if(err){
			logger.error({"r":"catg_grp","er":err,"p":req.body});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(categories.length == 0){
			logger.error({"r":"catg_grp","msg":"no Categories Found","p":req.body});
			return sendError(res,"No Cateogries Found","no_categories_found",HttpStatuses.NO_DATA);
		}
		var total = categories.length;
		var category_ids = [];
		for(var i=0; i<total; i++){
			category_ids.push(categories[i]._id+'');
		}
		Test.find({
			catg_id    : { $in : category_ids },
			act        : true,
			is_default : true 
		},{
			_id     		: 1,
			name    		: 1,
			level   		: 1,
			num_ques		: 1,
			total_marks : 1,
			timed       : 1,
			max_tim     : 1,
			catg_id 		: 1
		},function(err,default_tests){
			if(err){
				logger.error({"r":"catg_grp","er":err,"p":req.body,"er":err});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var finalCategoryData = mergeDefaultTestInCategories(categories, default_tests);
			return sendSuccess(res,{categories:finalCategoryData,tim:tim});
		});
	});
});

router.post('/catg_anlt', function(req, res, next){

	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"catg_anlt","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var gid        = req.body.g_id.trim();
	var plugin_id  = req.body.pl_id.trim();
	var catg_id    = req.body.catg_id.trim();
	var tim        = new Date().getTime();

	Category.findOne({
		_id   : catg_id,
		gid   : gid,
		act   : true
	},function(err,category){
		if(err){
			logger.error({"r":"catg_anlt","er":err,"p":req.body,"msg":"Category_Find_Error"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!category){
			logger.error({"r":"catg","msg":"category_not_found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		Test.find({
			catg_id     : catg_id,
			act         : true,
			num_ques    : {$gt : 0},
			publ        : true,
			stim        : {$lte:tim},
			total_marks : {$gt:0}
		},{
			num_ques       : 1,
			total_marks    : 1,
			total_attempts : 1,
			total_submits  : 1,
			name           : 1
		},function(err,tests){
			if(err){
				logger.error({"r":"catg_anlt","er":err,"p":req.body,"msg":"Category_Find_Error"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(tests.length == 0){
				logger.error({"r":"catg","msg":"no_tests_found_in_topic_for_analytics","p":req.body});
				return sendError(res,"no_tests_found_in_topic_for_analytics","no_tests_found_in_topic_for_analytics",HttpStatuses.SERVER_SPECIFIC_ERROR);
			}
			var test_attempts_submits = {};
			var total = tests.length;
			var tempd;
			var test_ids = [];
			for(var i=0; i<total; i++){
				tempd = {
					a : tests[i].total_attempts,
					s : tests[i].total_submits,
					n : tests[i].name
				};
				test_attempts_submits[tests[i]._id+''] = tempd;
			}
			var graph_data = [];
			var graph1     = getGraphDataForAttemptsAndSubmits(test_attempts_submits);
			graph_data.push(graph1);
			// console.log("=====================================");
			// console.log(graph1);
			// console.log("\n");
			getGraphDataForTestAverages(tests, function(err,average_data){
				if(err){
					logger.error({"r":"catg_anlt","er":err,"p":req.body,"msg":"Category_Find_Error"});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				var other_graphs = prepareOtherGraphsData(average_data);
				// console.log("**************************************");
				// console.log(other_graphs);
				graph_data.push(other_graphs.graph3);
				graph_data.push(other_graphs.graph2);
				graph_data.push(other_graphs.graph4);
				return sendSuccess(res,{graph_data:graph_data});
			});
		}).read('s');
	});
});

function getGraphDataForTestAverages(tests, cb){
	var todo = tests.length;
	var done = 0;
	var dataByTestId = {};
	var cb1 = function(err,resp){
		if(err){
			return cb(err,null);
		}
		done++;
		dataByTestId[resp.test_id] = resp.data;
		if(done < todo){
			averageDataForTest(tests[done]._id, tests[done].name, tests[done].num_ques, tests[done].total_marks, cb1);
		} else {
			return cb(null,dataByTestId);
		}
	};
	averageDataForTest(tests[done]._id, tests[done].name, tests[done].num_ques, tests[done].total_marks, cb1);
}

function averageDataForTest(test_id, test_name, total_ques, total_marks, cb){

	UserTest.find({
    test_id      : test_id,
    is_submitted : true,
    is_first     : true
  },{
    marks_obtained : 1,
    total_correct  : 1,
    time_taken     : 1,
    resm_c         : 1
  },{
    sort : {
      marks_obtained : -1
    }
  },function(err,usertests){
    if(err){
      return cb(err,null);
    }
    if(usertests.length == 0){
      return cb(null,{
	    	data : {
		    	average_score   : 0,
		    	average_correct : 0,
		    	average_time    : 0,
		    	name            : test_name
	    	},
	    	test_id         : test_id
	    });
    }

    var sum_marks   = 0;
    var sum_correct = 0;
    var sum_time_taken = 0;
    var tlen        = usertests.length;
    var ttlen       = usertests.length;

    // score_data.data[0].y   =  Math.round((usertests[0].marks_obtained / total_marks) * 100);
    // console.log('category analytics');
    // console.log(usertests);
    for(var i=0; i<tlen; i++){
      sum_marks      = sum_marks + usertests[i].marks_obtained;
      sum_correct    = sum_correct + usertests[i].total_correct;
      if(!usertests[i].resm_c){
      	sum_time_taken = sum_time_taken + parseInt(usertests[i].time_taken / 60000);
      } else if(usertests[i].resm_c > 0){
      	ttlen--;
      }
    }

    var tot_ques_for_avg  = tlen * total_ques;
    var tot_marks_for_avg = tlen * total_marks;

    var average_score     = Math.round((sum_marks / tot_marks_for_avg) * 100); 
    var average_correct   = Math.round((sum_correct / tot_ques_for_avg) * 100); 
    var average_time      = (ttlen == 0) ? 0 :parseInt((sum_time_taken / ttlen) * 100);

    return cb(null,{
    	data : {
	    	average_score   : average_score,
	    	average_correct : average_correct,
	    	average_time    : average_time,
	    	name            : test_name
    	},
    	test_id         : test_id
    });
  }).read('s');
}


function getGraphDataForAttemptsAndSubmits(tests_data){
	var graph1 = {
  	chart: {
        type: 'column'
    },
    title: {
        text: 'Overall Activity in Tests'
    },
    xAxis: {
        categories: [],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Attempts & Submits'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} students</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.1,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Attempts',
        data: []

    }, {
        name: 'Submits',
        data: []

    }]
  };
  var temp;
  for(var key in tests_data){
  	temp = tests_data[key];
  	graph1.xAxis.categories.push(temp.n);
  	graph1.series[0].data.push(temp.a);
  	graph1.series[1].data.push(temp.s);
  }
  return graph1;
}

function prepareOtherGraphsData(average_data){
	var graph2 = {
		chart: {
        type: 'column'
    },
    title: {
        text: 'Average Scoring'
    },
    xAxis: {
        categories: [],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: '% Average Scoring'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}%</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.1,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Average Score',
        data: []

    }]
	};
	var graph3 = {
		chart: {
        type: 'column'
    },
    title: {
        text: 'Average Time Spent'
    },
    xAxis: {
        categories: [],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Time Spent (in Mins)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mins</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.1,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Avg Time Spent',
        data: []

    }]
	};
	var graph4 = {
		chart: {
        type: 'column'
    },
    title: {
        text: "User's Accuracy"
    },
    xAxis: {
        categories: [],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: '% Accuracy'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}%</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Avg Accuracy',
        data: []
    }]
	};
	var temp;
  for(var key in average_data){
  	temp = average_data[key];
  	graph2.xAxis.categories.push(temp.name);
  	graph2.series[0].data.push(temp.average_score);

  	graph3.xAxis.categories.push(temp.name);
  	graph3.series[0].data.push(temp.average_time);

  	graph4.xAxis.categories.push(temp.name);
  	graph4.series[0].data.push(temp.average_correct);
  }
  return {
  	graph2 : graph2,
  	graph3 : graph3,
  	graph4 : graph4
  };
}

function mergeDefaultTestInCategories(categories, tests){
	var testByCategory  = [];
	var finalCategories = [];
	var total_tests = tests.length;
	for(var i=0; i<total_tests; i++){
		testByCategory[tests[i].catg_id] = tests[i];
	}
	var total_catg = categories.length;
	for(var j=0; j<total_catg; j++){
		if(testByCategory[categories[j]._id+'']){
			var data  = {
				c  : categories[j],
				dt :  testByCategory[categories[j]._id+'']
			};
			finalCategories[j]    = data;
		}
	}
	return finalCategories;
}


module.exports = router;
