const express 							= require('express');
const mongoose 							= require('mongoose');
const jwt    							  = require('jsonwebtoken');

const configs					      = require('../configs/configs.js');
const HttpStatuses					= require('../global/http_api_return_codes.js');
const ApiReturnHandlers			= require('../global/api_return_handlers.js');
const errorCodes 						= require('../global/error_codes.js');
const log4jsLogger  				= require('../loggers/log4js_module.js');
const log4jsConfigs  			  = require('../loggers/log4js_configs.js');
const CompletionProficiency = require('../analysisdata/completion_proficiency.js');
const StudentRanking        = require('../analysisdata/student_ranking.js');

require('../models/users.js');
require('../models/categories.js');
require('../models/tests.js');
require('../models/usertests.js');
require('../models/testquestions.js');
require('../models/usertestquestions.js');
require('../models/groupstats.js');
require('../models/pluginpaidinfos.js');
const User                  = mongoose.model('User');
const Category 							= mongoose.model('Category');
const Test 									= mongoose.model('Test');
const UserTest 							= mongoose.model('UserTest');
const TestQuestion					= mongoose.model('TestQuestion');
const UserTestQuestion		  = mongoose.model('UserTestQuestion');
const GroupStats		        = mongoose.model('GroupStats');
const PluginPaidInfo 				= mongoose.model('PluginPaidInfo');

const sendError 						= ApiReturnHandlers.sendError;
const sendSuccess 					= ApiReturnHandlers.sendSuccess;
const router 								= express.Router();
const logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_ANALYTICS);

//route middleware to verify if plugin in group is paid
router.use(function(req, res, next){
	var tim         = new Date().getTime();
	var plugin_id   = req.body.pl_id;
	var token       = req.body.tokn;
	var gid         = req.body.g_id;
	var user_id     = req.body.user_id;

	if(!token || token == 'null' || token === "undefined"){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body,"tkn":token});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!gid){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"gid_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","gid_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!plugin_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"plugin_id_missing","p":req.body});
	  return sendError(res,"Plugin Id Not Found","plugin_id_missing",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			req.private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			req.private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
		req.tim = tim;
	  next();
	});
});

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
  jwt.verify(req.body.tokn, req.private_key, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != req.body.user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != req.body.pl_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_app_id","p":req.body});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id4 != req.body.g_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body});
	  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    next();
  });
});

router.post('/cmpl_prf', function(req, res, next) {
	
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	
	if(req.validationErrors()){ 
		logger.error({"r":"cmpl_prf","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	
	var group_id    = req.body.g_id;
	var user_id     = req.body.user_id;
	var tim         = new Date().getTime();
	
	
	User.findOne({
		_id : user_id,
		act : true
	},{
		server_id : 1
	},function(err,user){
		if(err){
			logger.error({"r":"start","er":err,"p":req.body,"msg":"User_find_DbError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!user){
			logger.error({"r":"start","msg":"User Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		// PluginPaidInfo.find({
		// 	gid : group_id,
		// 	act : true
		// },{
		// 	plugin_id : 1,
		// 	_id       : 0
		// },function(err,plugins){
		// 	if(err){
		// 		logger.error({"r":"catg","er":err,"p":req.body});
		// 		console.trace(err);
		// 		return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		// 	}
		// 	if(plugins.length == 0){
		// 		logger.error({"r":"catg","msg":"no Paid Plugins Found For Completion graphs","p":req.body});
		// 		return sendError(res,"No Graph Data Found","no_graph_data",HttpStatuses.NO_DATA);
		// 	}
		// 	var plugin_ids = [];
		// 	for(var i=0; i<plugins.length; i++){
		// 		if(plugin_ids.indexOf(plugins[i].plugin_id+'') < 0)
		// 			plugin_ids.push(plugins[i].plugin_id+'');
		// 	}
			Category.find({
				// plugin_id : {$in:plugin_ids},
				gid : group_id,
				act : true
			},{
				_id : 1
			},function(err,categories){
				if(err){
					logger.error({"r":"catg","er":err,"p":req.body});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(categories.length == 0){
					logger.error({"r":"catg","msg":"no Categories Found For Completion graphs","p":req.body});
					return sendError(res,"No Graph Data Found","no_graph_data",HttpStatuses.NO_DATA);
				}
				var total = categories.length;
				var catg_ids = [];
				for(var i=0; i<total; i++){
					catg_ids.push(categories[i]._id+'');
				}
				Test.find({
					catg_id : {$in:catg_ids},
					act     : true,
					num_ques : {$gt : 0},
					publ     : true,
					stim     : {$lte:tim} 
				},{
					_id         : 1,
					total_marks : 1,
					marks_scored: 1,
					total_submits : 1
				},{
					sort : {
						tim : 1
					}
				},function(err,tests){
					if(err){
						logger.error({"r":"catg","er":err,"p":req.body});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(tests.length == 0){
						logger.error({"r":"catg","msg":"no Tests Found For Completion graphs","p":req.body});
						return sendError(res,"No Graph Data Found","no_graph_data",HttpStatuses.NO_DATA);
					}
					var total       = tests.length;
					var total_marks = 0;
					var test_ids    = [];

					for(var j=0; j<total; j++){
						test_ids.push(tests[j]._id+'');
						total_marks = total_marks + tests[j].total_marks;
					}
					var series = [];
					CompletionProficiency.getUserData(test_ids, user.server_id, total_marks, function(err,userdata){
						if(err){
							logger.error({"r":"catg","er":err,"msg":"CompletionProficiency_getUserData","p":req.body});
							console.trace(err);
							return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
						}
						if(userdata.data.length == 0){
							logger.error({"r":"catg","msg":"No User Data Found for Completion graphs","p":req.body});
							return sendError(res,"No Graph Data Found","no_graph_data",HttpStatuses.NO_DATA);
						}
						series.push(userdata);
						CompletionProficiency.getAverageData2(test_ids, group_id, total_marks, function(err,avgdata){
							if(err){
								logger.error({"r":"catg","er":err,"msg":"CompletionProficiency_getAverageData","p":req.body});
								console.trace(err);
								return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
							}
							if(avgdata.data.length == 0){
								logger.error({"r":"catg","msg":"No Average Data Found For Completion graphs","p":req.body});
								return sendError(res,"No Graph Data Found","no_graph_data",HttpStatuses.NO_DATA);
							}
							series.push(avgdata);
							CompletionProficiency.getTopperData(test_ids, group_id, total_marks, function(err,topperdata){
								if(err){
									logger.error({"r":"catg","er":err,"msg":"CompletionProficiency_getTopperData","p":req.body});
									console.trace(err);
									return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
								}
								if(topperdata.data.length == 0){
									logger.error({"r":"catg","msg":"No Topper Data Found For Completion graphs","p":req.body});
									return sendError(res,"No Graph Data Found","no_graph_data",HttpStatuses.NO_DATA);
								}
								series.push(topperdata);
								var result_data = {
									title     : "Exam Cummulative Analysis",
									subtitle  : "commulative marks vs test completion",
									x_axis_nm : "Completion",
									y_axis_nm : "Commulative %",
									series    : series
								};
								logger.info({"r":"catg","msg":"success","p":req.body,data:JSON.stringify(result_data)});
								return sendSuccess(res,{result_data:result_data});
							});
						});
					});
				});
			});
		// });
	});
});

router.post('/std_rnk', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()){ 
		logger.error({"r":"std_rnk","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var group_id    = req.body.g_id;
	var user_id     = req.body.user_id;
	var tim         = new Date().getTime();
	
	
	User.findOne({
		_id : user_id,
		act : true
	},{
		server_id : 1
	},function(err,user){
		if(err){
			logger.error({"r":"std_rnk","er":err,"p":req.body,"msg":"User_find_DbError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!user){
			logger.error({"r":"std_rnk","msg":"User Not Found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
		}
		Category.find({
			gid : group_id,
			act : true
		},{
			_id : 1
		},function(err,categories){
			if(err){
				logger.error({"r":"std_rnk","er":err,"p":req.body});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(categories.length == 0){
				logger.error({"r":"std_rnk","msg":"no Categories Found For Completion graphs","p":req.body});
				return sendError(res,"No Graph Data Found","no_graph_data",HttpStatuses.NO_DATA);
			}
			var total = categories.length;
			var catg_ids = [];
			for(var i=0; i<total; i++){
				catg_ids.push(categories[i]._id+'');
			}
			Test.find({
				catg_id : {$in:catg_ids},
				act     : true,
				num_ques : {$gt : 0},
				publ     : true,
				stim     : {$lte:tim} 
			},{
				_id         : 0,
				total_marks : 1
			},function(err,tests){
				if(err){
					logger.error({"r":"std_rnk","er":err,"p":req.body});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				if(tests.length == 0){
					logger.error({"r":"std_rnk","msg":"no Tests Found For Completion graphs","p":req.body});
					return sendError(res,"No Graph Data Found","no_graph_data",HttpStatuses.NO_DATA);
				}
				var total       = tests.length;
				var total_marks = 0;

				for(var j=0; j<total; j++){
					total_marks = total_marks + tests[j].total_marks;
				}
				var series = [];
				StudentRanking.getRankData(group_id, user.server_id, total_marks, function(err,rankdata){
					if(err){
						logger.error({"r":"std_rnk","er":err,"msg":"CompletionProficiency_getUserData","p":req.body});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(rankdata.data.length == 0){
						logger.error({"r":"std_rnk","msg":"No User Data Found for Completion graphs","p":req.body});
						return sendError(res,"No Graph Data Found","no_graph_data",HttpStatuses.NO_DATA);
					}
					series.push(rankdata.data);
					var result_data = {
						title     : "Cummulative Pecentile Analysis",
						subtitle  : "commulative marks vs percentile",
						x_axis_nm : "marks",
						y_axis_nm : "percentile",
						series    : series,
						user_data : rankdata.user_data,
						first     : rankdata.first,
						second    : rankdata.second
					};
					logger.info({"r":"std_rnk","msg":"success","p":req.body,data:JSON.stringify(result_data)});
					return sendSuccess(res,{result_data:result_data});
				});
			});
		});
	});
});

router.post('/prfm_tm', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()){ 
		logger.error({"r":"prfm_tm","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var group_id = req.body.g_id;

	var result_data = {
		x_axis_nm : "Time",
		y_axis_nm : "Percentile",
		x_axis    : ["30-01-17","30-02-17","30-03-17","30-04-17","30-05-17"],
		y_axis    : [60,40,90,70,80],
		rank      : [120,160,60,90,70]
	};
	return sendSuccess(res,result_data);
});



router.post('/visitsandsubmits', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('start',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('ipp',errorCodes.invalid_parameters[1]).notEmpty().isValidIPP();

	if(req.validationErrors()){ 
		logger.error({"r":"t_u_v","m":"GET","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id = req.body.g_id;
	var start    = parseInt(req.body.start);
	var ipp      = parseInt(req.body.ipp);

	Category.find({
		gid : group_id,
		act : true,
		total_num_tests : {$gt : 0}
	},{
		_id : 1
	},{
		sort : {
			createdAt : 1
		},
		skip : start,
		limit: ipp
	},function(err,categories){
		if(err){
			logger.error({"r":"t_u_v","er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(categories.length == 0){
			logger.error({"r":"t_u_v","p":req.body,"msg":"no_categories_found_for_the_group"});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		var catg_ids = [];
		var total = categories.length;
		for(var i=0 ; i<total; i++){
			catg_ids.push(categories[i]._id+'');
		}
		Test.find({
			catg_id : {$in : catg_ids},
			act     : true,
			num_ques: {$gt : 0}
		},{
			name        : 1,
			num_ques    : 1,
			total_marks : 1
		},function(err,tests){
			if(err){
				logger.error({"r":"t_u_v","er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(tests.length == 0){
				logger.error({"r":"t_u_v","p":req.body,"msg":"no_tests_found_for_group_categories"});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			var test_ids = [];
			var testsById= {};
			var total    = tests.length;
			for(var i=0 ; i<total; i++){
				test_ids.push(tests[i]._id+'');
				testsById[tests[i]._id+''] = tests[i];
			}
			getUniqueVistsPerTest(test_ids, function(err,resp_visits){
				if(err){
					logger.error({"r":"t_u_v","er":err,"p":req.body,"msg":"getUniqueVistsPerTest"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				getUniqueSubmitsPerTest(test_ids, function(err, resp_submits){
					if(err){
						logger.error({"r":"t_u_v","er":err,"p":req.body,"msg":"getUniqueSubmitsPerTest"});
						console.trace(err);
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					return sendSuccess(res,{tests:testsById,unique_visits:resp_visits,unique_submits:resp_submits});
				});
			});
		})
	});
});

function getUniqueVistsPerTest(test_ids, cb){
	var unique_visits_tests = {};
	var total = test_ids.length;
	var done  = 0;
	var cb1 = function(err,resp){
		if(err){
			return cb(err,null);
		}
		done++;
		unique_visits_tests[resp.tid] = resp.atmpt;
		if(done < total){
			getUniqueVisits(test_ids[done], cb1);
		} else {
			return cb(null,unique_visits_tests);
		}
	};
	getUniqueVisits(test_ids[done], cb1);
}

function getUniqueSubmitsPerTest(test_ids, cb){
	var unique_submits = {};
	var total = test_ids.length;
	var done  = 0;
	var cb1 = function(err,resp){
		if(err){
			return cb(err,null);
		}
		done++;
		unique_submits[resp.tid] = resp.submits;
		if(done < total){
			getUniqueSubmits(test_ids[done], cb1);
		} else {
			return cb(null,unique_submits);
		}
	};
	getUniqueSubmits(test_ids[done], cb1);
}

function getUniqueVisits(test_id, cb){
	UserTest.distinct('user_id',{
		test_id    : test_id,
		is_started : true
	},function(err,unique_attempts){
		if(err){
			return cb(err,null);
		}
		return cb(null,{tid:test_id, atmpt:unique_attempts.length});
	});
}

function getUniqueSubmits(test_id, cb){
	UserTest.distinct('user_id',{
		test_id      : test_id,
		is_submitted : true
	},function(err,unique_submits){
		if(err){
			return cb(err,null);
		}
		return cb(null,{tid:test_id, submits:unique_submits.length});
	});
}

module.exports = router;
