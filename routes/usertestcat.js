const express 							= require('express');
const mongoose 							= require('mongoose');
const jwt    							  = require('jsonwebtoken');

const configs					      = require('../configs/configs.js');
const HttpStatuses					= require('../global/http_api_return_codes.js');
const ApiReturnHandlers			= require('../global/api_return_handlers.js');
const errorCodes 						= require('../global/error_codes.js');
const log4jsLogger  				= require('../loggers/log4js_module.js');
const log4jsConfigs  			  = require('../loggers/log4js_configs.js');
const AuthSecurity      		= require('../security/auth_tokens.js');
const RootServerApi      		= require('../server/apicalls.js');
const MailClient            = require('../mail/mail_client.js');
const CertificateModule     = require('../certificates/module.js');
const TestReviewModule      = require('../review/test.js');

require('../models/users.js');
require('../models/categories.js');
require('../models/tests.js');
require('../models/questions.js');
require('../models/usertests.js');
require('../models/testquestions.js');
require('../models/usertestquestions.js');
require('../models/groupstats.js');
require('../models/pluginpaidinfos.js');
const User                  = mongoose.model('User');
const Category 							= mongoose.model('Category');
const Test 									= mongoose.model('Test');
const Question					    = mongoose.model('Question');
const UserTest 							= mongoose.model('UserTest');
const TestQuestion					= mongoose.model('TestQuestion');
const UserTestQuestion		  = mongoose.model('UserTestQuestion');
const GroupStats		        = mongoose.model('GroupStats');
const PluginPaidInfo 				= mongoose.model('PluginPaidInfo');

const sendError 						= ApiReturnHandlers.sendError;
const sendSuccess 					= ApiReturnHandlers.sendSuccess;
const router 								= express.Router();
const logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_USERTEST);
const answerLogger          = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_USERANSWERS_SUBMIT);


//route middleware to verify if plugin in group is paid
router.use(function(req, res, next){
	var tim         = new Date().getTime();
	var plugin_id   = req.body.pl_id;
	var token       = req.body.tokn;
	var gid         = req.body.g_id;
	var user_id     = req.body.user_id;
	if(!token || token == 'null' || token === "undefined"){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body,"tkn":token});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!user_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"id_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","id_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!gid){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"gid_not_found","p":req.body});
	  return sendError(res,"Access without id is not authorised","gid_not_found",HttpStatuses.FORBIDDEN);
	}
	if(!plugin_id){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"plugin_id_missing","p":req.body});
	  return sendError(res,"Plugin Id Not Found","plugin_id_missing",HttpStatuses.FORBIDDEN);
	}
	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : gid,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
		  logger.error({"url":req.originalUrl,"er":err,"p":req.body,"msg":"PluginPaidInfo_findError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(pluginIsPaid){
			req.private_key = configs.PAID_JWT_ACCESS_TOKEN_PRIVATE_KEY;
		} else {
			req.private_key = configs.JWT_ACCESS_TOKEN_PRIVATE_KEY;
		}
		req.tim = tim;
	  next();
	});
});

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
  jwt.verify(req.body.tokn, req.private_key, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    } 
    if(decoded.id1 != req.body.user_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_id","p":req.body});
	  	return sendError(res,"Access without valid id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id3 != req.body.pl_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_app_id","p":req.body});
	  	return sendError(res,"Access without Valid App id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    if(decoded.id4 != req.body.g_id){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"invalid_token_group_id","p":req.body});
	  	return sendError(res,"Access without Valid Group id is not authorised","invalid_access_token",HttpStatuses.FORBIDDEN);
    }
    next();
  });
});