const express 							= require('express');
const mongoose 							= require('mongoose');
const jwt    								= require('jsonwebtoken');

const configs					      = require('../configs/configs.js');
const HttpStatuses					= require('../global/http_api_return_codes.js');
const ApiReturnHandlers			= require('../global/api_return_handlers.js');
const errorCodes 						= require('../global/error_codes.js');
const log4jsLogger  				= require('../loggers/log4js_module.js');
const log4jsConfigs  			  = require('../loggers/log4js_configs.js');
const AuthSecurity      		= require('../security/auth_tokens.js');

require('../models/users.js');
require('../models/subscriptions.js');
require('../models/pluginpaidinfos.js');
require('../models/usertokens.js');
const User 									= mongoose.model('User');
const Subscription 					= mongoose.model('Subscription');
const PluginPaidInfo 				= mongoose.model('PluginPaidInfo');
const UserToken 						= mongoose.model('UserToken');

const sendError 						= ApiReturnHandlers.sendError;
const sendSuccess 					= ApiReturnHandlers.sendSuccess;
const router 								= express.Router();
const logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_ECKOVATION);


//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
	var tim           = new Date().getTime();
	var parameters    = req.body.parameters;
	if(!parameters){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"paramete_not_found","p":req.body});
	  return sendError(res,"Bad Request","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
	parameters = parameters.trim();
	if(!parameters){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"paramete_not_found_after_trimming","p":req.body});
	  return sendError(res,"Bad Request","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}
  jwt.verify(parameters, configs.ECKOVATION_API_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate Request.","request_auth_failure",HttpStatuses.FORBIDDEN);  
    } 
    req.tim     = tim;
    req.decoded = decoded;
    next();
  });
});

router.post('/get_user_at', function(req, res, next){

	if(!isValidParamsForAt(req.decoded)){
		logger.error({"r":"get_user_at","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var decoded_params    = req.decoded;
	
	var plugin_id    			= decoded_params.PLUGIN_ID;
	var group_id     			= decoded_params.GROUP_ID;
	var identifier   			= decoded_params.IDENTIFIER;
	var server_id         = decoded_params.SERVER_ID;
	var ppic              = decoded_params.PPIC;
	var name              = decoded_params.NAME;
	var role              = decoded_params.ROLE;

	User.findOne({
		identifier : identifier,
		act        : true
	},function(err,user){
		if(err){
			logger.error({"r":"get_user_at","msg":"user_find_failed","er":err,"p":req.body});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		var response;
		if(user){
			logger.info({"r":"get_user_at","msg":"user_already_exists","p":req.body});
			response = { 
				id        : user._id, 
				if_new    : 0, 
				srv_id    : server_id, 
				gid       : group_id, 
				plugin_id : plugin_id
			};
			return  prepareUserAt("get_user_at_existing_user", identifier, response, decoded_params, res);
		} else {
			User.update({
				identifier : identifier,
				act        : true
			},{
				$setOnInsert : {
					act        : true,
					identifier : identifier,
					server_id  : server_id,
					name       : name,
					img_url    : ppic,
					role       : role,
					gid        : group_id,
					plugin_id  : plugin_id
				}
			},{
				upsert : true,
				setDefaultsOnInsert: true
			},function(err,created_user){
				if(err){
					logger.error({"r":"register","msg":"user_token_update_failed","er":err,"p":req.body});
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				User.findOne({
					identifier : identifier,
					act        : true
				},function(err,newly_created_user){
					if(err){
						logger.error({"r":"register","msg":"user_find_failed_after_create","er":err,"p":req.body});
						return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
					}
					if(!newly_created_user){
						logger.error({"r":"register","msg":"user_not_found","p":req.body});
						return sendError(res,"Invalid Parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
					}
					response = {
						id        : newly_created_user._id, 
						if_new    : 1, 
						srv_id    : server_id, 
						gid       : group_id, 
						plugin_id : plugin_id
					};
					return  prepareUserAt("get_user_at_new_user", identifier, response, decoded_params, res);
				});
			});
		}
	})
});

router.post('/make_pd_pl', function(req, res, next){

	if(!isValidParamsForPdPl(req.decoded)){
		logger.error({"r":"make_pd_pl","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var decoded_params    = req.decoded;
	
	var plugin_id    			= decoded_params.PLUGIN_ID;
	var group_code     		= decoded_params.GROUP_CODE;

	PluginPaidInfo.findOne({
  	plugin_id : plugin_id,
  	gid       : group_code,
  },function(err,paid_plugin){
  	if(err){
			logger.error({"r":"make_pd_pl","msg":"PluginPaidInfo_Find_Failed","er":err,"p":req.body});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
  	if(paid_plugin && paid_plugin.act == true){
  		logger.info({"r":"make_pd_pl","msg":"already_existing_as_paid","p":decoded_params});
  		return sendSuccess(res,{});
  	}
  	PluginPaidInfo.update({
  		plugin_id : plugin_id,
    	gid       : group_code
  	},{
  		$setOnInsert : {
  			plugin_id : plugin_id,
    		gid       : group_code,
    		act       : true
  		}
  	},{
  		upsert : true
  	},function(err,insertedPaid_plugin){
  		if(err){
				logger.error({"r":"get_user_at","msg":"PluginPaidInfo_Update_Failed","er":err,"p":req.body});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			logger.info({"r":"make_pd_pl","msg":"brand_new_entry","p":decoded_params});
			return sendSuccess(res,{});
  	});
  });
});

router.post('/eckovationcallback', function(req, res, next){

	if(!isValidParams(req.decoded)){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var decoded_params          = req.decoded;
	
	var plugin_id    						= decoded_params.PLUGIN_ID;
	var group_id     						= decoded_params.GROUP_ID;
	var identifier   						= decoded_params.IDENTIFIER;
	var package_id   						= decoded_params.PACKAGE_ID;
	var package_name 						= decoded_params.PACKAGE_NAME;
	var package_price						= decoded_params.PACKAGE_PRICE;
	var promo_code              = decoded_params.PROMO_CODE;
	var final_amount_paid       = decoded_params.ORDER_AMOUNT;
	var subscription_start_time = decoded_params.START_TIME;
	var subscription_end_time   = decoded_params.END_TIME; 
	var order_id                = decoded_params.ORDER_ID;
	var tim                     = decoded_params.tim;
	var other_plugin_ids        = (!decoded_params.OTHER_PLUGIN_IDS) ? [] : decoded_params.OTHER_PLUGIN_IDS;
	var oth_identifers          = (!decoded_params.OTHER_IDENTIFIERS) ? {} : decoded_params.OTHER_IDENTIFIERS;

	Subscription.update({
		plugin_id : plugin_id,
		group_id  : group_id,
		identifier: identifier,
		act       : true
	},{
		act : false
	},{
		multi : true
	},function(err,existing_subscription){
		if(err){
			logger.error({"r":"eckovationcallback","er":err,"p":req.decoded,"msg":"Subscription_updateError"});
			console.trace(err);
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		var setOnInsertObject = {
			plugin_id 		: plugin_id,
			group_id  		: group_id,
			identifier    : identifier,
			package_id    : package_id,
			name          : package_name,
			price         : package_price,
			order_id      : order_id,
			amount_paid   : final_amount_paid,
			start_time    : subscription_start_time,
			end_time      : subscription_end_time,
			act           : true
		};
		Subscription.update({
			plugin_id : plugin_id,
			group_id  : group_id,
			identifier: identifier,
			act       : true,
			package_id: package_id
		},{
			$setOnInsert : setOnInsertObject
		},{
			upsert : true,
			setDefaultsOnInsert: true
		},function(err,subscription_created){
			if(err){
				logger.error({"r":"eckovationcallback","er":err,"p":req.decoded,"msg":"Subscription_Insertion_Error"});
				console.trace(err);
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(!other_plugin_ids || other_plugin_ids.length == 0){
				logger.info({"r":"eckovationcallback","msg":"New_Subscription_Created_Without_Other_plugins","p":req.decoded});
				return sendSuccess(res,{});
			}
			subscribeOtherPlugins(other_plugin_ids, oth_identifers, setOnInsertObject, function(err,resp){
				if(err){
					logger.error({"r":"eckovationcallback","er":err,"p":req.decoded,"msg":"Subscription_Insertion_Error"});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				logger.info({"r":"eckovationcallback","msg":"subscribeOtherPlugins_Successfull","p":req.decoded});
				return sendSuccess(res,{});
			});
		});
	});
});

function prepareUserAt(route_name, identifier, response, parameters, res){

	var user_id   = response.id;
	var plugin_id = response.plugin_id;
	var server_id = response.srv_id;
	var group_id  = response.gid;
	var tim       = new Date().getTime();

	PluginPaidInfo.findOne({
		plugin_id : plugin_id,
		gid       : group_id,
		act       : true
	},function(err,pluginIsPaid){
		if(err){
			logger.error({"r":route_name,"msg":"PluginInfo_find_failed","er":err,"p":parameters});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		var object_for_at = {
			id1 : user_id,
			id2 : server_id,
			id3 : plugin_id,
			id4 : group_id
		};
		var at;
		if(pluginIsPaid){
			console.log('inside pluginIsPaid for hit from eckovation server');
			AuthSecurity.getSubscriptionAT(plugin_id, object_for_at, identifier, group_id, tim, function(err, at){
				if(err){
					logger.error({"r":route_name,"msg":"user_token_update_failed","er":err,"p":parameters});
					console.trace(err);
					if(err == "USER_SUBSCRIPTION_EXPIRED"){
						return sendError(res,"USER_SUBSCRIPTION_EXPIRED","user_subscription_ended",HttpStatuses.USER_SUBSCRIPTION_EXPIRED);
					}
					if(err == "USER_SUBSCRIPTION_REQUIRED"){
						return sendError(res,"USER_SUBSCRIPTION_EXPIRED","user_subscription_required",HttpStatuses.USER_SUBSCRIPTION_REQUIRED);
					}
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				logger.info({"r":route_name,"msg":"AuthSecurity_getSubscriptionAT_Success","p":parameters});
				response.at = at;
				return sendSuccess(res,response);
			});
		} else {
			console.log('outside pluginIsPaid for hit from eckovation server');
			at = AuthSecurity.getAT(object_for_at);
			updateUserToken(user_id, plugin_id, at, function(err, resp){
				if(err){
					logger.error({"r":route_name,"msg":"user_token_update_failed","er":err,"p":parameters});
					console.trace(err);
					return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
				}
				logger.info({"r":route_name,"msg":"AuthSecurity_getAT_Success","p":parameters});
				response.at = at;
				return sendSuccess(res,response);
			});
		}
	});
}

function updateUserToken(user_id, plugin_id, at, cb){
	UserToken.update({
		plugin_id : plugin_id,
		user_id   : user_id,
		act       : true
	},{
		act : false
	},{
		multi : true
	},function(err,prev_updated){
		if(err){
			return cb(err,null);
		}
		var new_user_token = new UserToken({
			user_id     : user_id,
			plugin_id   : plugin_id,
			act 		    : true,
			xcess_token : at
		});
		new_user_token.save(function(err, new_token){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}

function subscribeOtherPlugins(other_plugin_ids, oth_identifers, setOnInsertObject, cb){
	var total = other_plugin_ids.length;
	var done  = 0;
	var plugin_id, identifier;
	setOnInsertObject.frm_cmplt = true;
	delete setOnInsertObject["plugin_id"];
	delete setOnInsertObject["identifier"];
	for(var i=0; i<total; i++){
		plugin_id  = other_plugin_ids[i];
		identifier = oth_identifers[other_plugin_ids[i]];
		saveOtherPluginSubscription(setOnInsertObject, plugin_id, identifier, function(err,resp){
			if(err){
				return cb(err,null);
			}
			done++;
			if(done == total){
				return cb(null,true);
			}
		});
	}
}
function saveOtherPluginSubscription(setOnInsertObject, plugin_id, identifier, cb){
	var group_id   = setOnInsertObject.group_id;
	Subscription.update({
		plugin_id : plugin_id,
		group_id  : group_id,
		identifier: identifier,
		act       : true
	},{
		act : false
	},{
		multi : true
	},function(err,existing_subscription){
		if(err){
			return cb(err,null);
		}
		Subscription.update({
			plugin_id : plugin_id,
			group_id  : group_id,
			identifier: identifier,
			act       : true,
			package_id: setOnInsertObject.package_id
		},{
			$setOnInsert : setOnInsertObject,
			$set         : {
				plugin_id : plugin_id,
				identifier: identifier
			}
		},{
			upsert : true,
			setDefaultsOnInsert: true
		},function(err,subscription_created){
			if(err){
				return cb(err,null);
			}
			console.log("saveOtherPluginSubscription ,");
			console.log(setOnInsertObject);
			console.log(subscription_created);
			console.log(plugin_id);
			console.log(identifier);
			return cb(null,true);
		});
	});
}

function isValidParams(parameters){
	if(!parameters.PLUGIN_ID){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_PLUGIN_ID","p":parameters});
		return false;
	}
	if(!parameters.GROUP_ID){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_GROUP_ID","p":parameters});
		return false;
	}
	if(!parameters.IDENTIFIER){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_IDENTIFIER","p":parameters});
		return false;
	}
	if(!parameters.PACKAGE_ID){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_PACKAGE_ID","p":parameters});
		return false;
	}
	if(!parameters.PACKAGE_NAME){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_PACKAGE_NAME","p":parameters});
		return false;
	}
	if(!parameters.PACKAGE_PRICE){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_PACKAGE_PRICE","p":parameters});
		return false;
	}
	if(!parameters.ORDER_AMOUNT){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_ORDER_AMOUNT","p":parameters});
		return false;
	}
	if(!parameters.START_TIME){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_START_TIME","p":parameters});
		return false;
	}
	if(!parameters.END_TIME){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_END_TIME","p":parameters});
		return false;
	}
	if(!parameters.ORDER_ID){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_ORDER_ID","p":parameters});
		return false;
	}
	return true;
}

function isValidParamsForAt(parameters){

	if(!parameters.PLUGIN_ID){
		logger.error({"r":"get_user_at","msg":"invalid_parameters_missing_PLUGIN_ID","p":parameters});
		return false;
	}
	if(!parameters.GROUP_ID){
		logger.error({"r":"get_user_at","msg":"invalid_parameters_missing_GROUP_ID","p":parameters});
		return false;
	}
	if(!parameters.IDENTIFIER){
		logger.error({"r":"get_user_at","msg":"invalid_parameters_missing_IDENTIFIER","p":parameters});
		return false;
	}
	if(!parameters.SERVER_ID){
		logger.error({"r":"get_user_at","msg":"invalid_parameters_missing_SERVER_ID","p":parameters});
		return false;
	}
	if(!parameters.NAME){
		logger.error({"r":"get_user_at","msg":"invalid_parameters_missing_NAME","p":parameters});
		return false;
	}
	if(!parameters.ROLE){
		logger.error({"r":"get_user_at","msg":"invalid_parameters_missing_ROLE","p":parameters});
		return false;
	}
	return true;

}

function isValidParamsForPdPl(parameters){

	if(!parameters.PLUGIN_ID){
		logger.error({"r":"get_user_at","msg":"invalid_parameters_missing_PLUGIN_ID","p":parameters});
		return false;
	}
	if(!parameters.GROUP_CODE){
		logger.error({"r":"get_user_at","msg":"invalid_parameters_missing_GROUP_ID","p":parameters});
		return false;
	}
	return true;
}



module.exports = router;