var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    							  = require('jsonwebtoken');

var configs					      = require('../configs/configs.js');
var HttpStatuses					= require('../global/http_api_return_codes.js');
var ApiReturnHandlers			= require('../global/api_return_handlers.js');
var errorCodes 						= require('../global/error_codes.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');

require('../models/categories.js');
require('../models/tests.js');
var Test 									= mongoose.model('Test');
var Category 							= mongoose.model('Category');

var sendError 						= ApiReturnHandlers.sendError;
var sendSuccess 					= ApiReturnHandlers.sendSuccess;
var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_OPS);

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
	var token = req.headers['x-ops-access-token'];console.log('1');
	if(!token){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body});
	  return sendError(res,"Access without token is not authorised","token_not_found",HttpStatuses.FORBIDDEN);
	}
  jwt.verify(token, configs.OPS_PANEL_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",HttpStatuses.TOKEN_EXPIRED);  
    }
  	req.decoded = decoded;
    next();
  });
});

router.post('/grp_catg', function(req, res, next){

	req.checkBody('gcode',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"grp_catg","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var group_code = req.body.gcode.trim();
	var plugin_id  = req.body.pl_id.trim();

	Category.find({
		plugin_id : plugin_id,
		gid       : group_code,
		type      : 1,
		act       : true,
		total_num_tests : { $gt : 0 }
	},{
		name : 1,
		type : 1,
		adpv : 1
	},{
    sort : {
      tim : 1
    }
	},function(err, categories){
		if(err){
			logger.error({"r":"grp_catg","er":err,"p":req.body});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(categories.length == 0){
			logger.error({"r":"catg_test","msg":"ops_no_categories_found_for_this_group","p":req.body});
		  return sendError(res,"ops_no_categories_found_for_this_group","ops_no_categories_found_for_this_group",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		return sendSuccess(res,{categories:categories,plugin_id:plugin_id});
	});
});

router.post('/catg_test', function(req, res, next){

	req.checkBody('catg_id',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"catg_test","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",HttpStatuses.BAD_REQUEST);
	}

	var category_id = req.body.catg_id.trim();
	var tim         = new Date().getTime();

	Category.find({
		_id : category_id,
		act : true
	},{
		name : 1,
		type : 1,
		adpv : 1,
		plugin_id : 1
	},function(err, category){
		if(err){
			logger.error({"r":"catg_test","er":err,"p":req.body});
			return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
		}
		if(!category){
			logger.error({"r":"catg_test","msg":"ops_category_not_found","p":req.body});
			return sendError(res,"ops_category_not_found","ops_category_not_found",HttpStatuses.SERVER_SPECIFIC_ERROR);
		}
		Test.find({
			catg_id  : category_id,
			act      : true,
			num_ques : {$gt : 0},
			publ     : true,
			stim     : {$lte:tim} 
		},{
			name                : 1,
			catg_id             : 1,
			type                : 1,
			level               : 1,
		},function(err,tests){
			if(err){
				logger.error({"r":"catg_test","er":err,"p":req.body});
				return sendError(res,"Failed to process the request","server_error",HttpStatuses.SERVER_ERROR);
			}
			if(tests.length == 0){
				logger.error({"r":"catg_test","msg":"ops_no_tests_found_for_category","p":req.body});
			  return sendError(res,"ops_no_tests_found_for_category","ops_no_tests_found_for_category",HttpStatuses.SERVER_SPECIFIC_ERROR);
			}
			return sendSuccess(res,{tests:tests,category:category,plugin_id:category.plugin_id,prnm:category[0].name});
		});
	});
});

module.exports = router;