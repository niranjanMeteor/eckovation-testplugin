var filemodes = {
	IMAGE               : "image",
	AUDIO               : "audio",
	DOC                 : "doc"
};

module.exports = filemodes;