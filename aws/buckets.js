var buckets = {
	QUES_PIC							: "eckquepic",
	OPNS_PIC              : "eckopnpic",
	HINT_PIC              : "eckhntpic",
	SOLN_PIC              : "ecksolpic"
};

module.exports = buckets;