var filetypes = {
	PDF                 : "pdf",
	JPG                 : "jpg",
	PNG                 : "png",
	JPEG                : "jpeg"
};

module.exports = filetypes;