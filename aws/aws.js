var AWS 				  		   = require('aws-sdk');
var crypto 						   = require('crypto');

var configs              = require('./configs.js');
var FileModes            = require('./file_modes.js');
var FileTypes            = require('./file_types.js');
var Buckets              = require('./buckets.js');
var MediaPurpose 				 = require('../global/media_purpose.js');

var validPurposes        = [MediaPurpose.QUESTION,MediaPurpose.OPTION,MediaPurpose.HINT,MediaPurpose.SOLUTION];

AWS.config.update({
  accessKeyId      : configs.AWS_ACCESS_KEY,
  secretAccessKey  : configs.AWS_SECRET_KEY,
  "region"         : configs.AWS_REGION,
  "output"         : configs.AWS_OUTPUT 
});
var s3 						       = new AWS.S3();

exports.getCredentials = function(bucket, file_name, file_type, mode){
	var format = mode+"/"+file_type;
  var acl    = 'authenticated-read';
	var s3Policy = {
    'expiration'  : getExpirytime(configs.UPLOAD_AMAZON_EXPIRY_TIME),
    'conditions'  : [
    	{'bucket'   : bucket},
      ['starts-with', "$key", ""],
      {'acl'      : acl},
      ['starts-with', '$Content-Type', format],
      ['content-length-range',configs.UPLOAD_FILE_MIN_SIZE,configs.UPLOAD_FILE_MAX_SIZE],
      {'success_action_status' : '201'}
    ]
  };
  var stringPolicy = JSON.stringify(s3Policy);
  var base64Policy = new Buffer(stringPolicy, 'utf-8').toString('base64');
  var signature    = crypto.createHmac('sha1', configs.AWS_SECRET_KEY)
                      .update(new Buffer(base64Policy, 'utf-8')).digest('base64');
  var s3Credentials = {
    s3Policy        : base64Policy,
    s3Signature     : signature,
    AWSAccessKeyId  : configs.AWS_ACCESS_KEY
  };
  return s3Credentials;
};

exports.getSignedUrlS3Upload = function(bucket, file_name, file_type, mode, cb){
	var format = mode+"/"+file_type;
	var params = { 
    Bucket     : bucket, 
    Key        : file_name, 
    Expires    : getExpirytime(configs.AMAZON_EXPIRY_TIME), 
    ContentType: format, 
    ACL        : 'authenticated-read'
  };
  s3.getSignedUrl('putObject', params, function(err, url){ 
    if(err){ 
    	return cb(err,null);
    }
    if(!url) {
    	return cb("Url_could_not_created",null);
    }
    var return_data = {
      signed_url	: url,
      mask_url	  : 'https://'+bucket+'.s3.amazonaws.com/'+file_name 
    };
    return cb(null,return_data);
  });
};

exports.getSignedUrlS3Download = function(bucket, file_name, file_type, cb){
	var params = { 
    Bucket     : bucket, 
    Key        : file_name, 
    Expires    : getExpirytime(configs.DOWNLOAD_AMAZON_EXPIRY_TIME), 
  };
  s3.getSignedUrl('getObject', params, function(err, signed_url){ 
    if(err){ 
    	return cb(err,null);
    }
    if(!signed_url) {
    	return cb("Url_could_not_created",null);
    }
    return cb(null,signed_url);
  });
};


exports.isValidFile = function(filetype, filename){
	if(filetype != FileTypes.JPG && filetype != FileTypes.JPEG && filetype != FileTypes.PNG){
		return false;
	}
	try{
		var filesplit = filename.split(".");
		if(filesplit.length != 2){
			return false;
		}
		var name = filesplit[0].trim();
		var extn = filesplit[1];
		if(!name){
			return false;
		}
		if(extn == FileTypes.JPG || extn == FileTypes.JPEG || extn == FileTypes.PNG){
			return true;
		}
		return false;
	} catch(e){
		console.log(e);
		console.log('invalid filetype or filename arguments supplied');
		console.log('filename : '+filename+' , filetype : '+filetype);
		return false;
	}
};

exports.isValidMode = function(file_type, mode){
	switch(file_type){

		case FileTypes.JPG:
			return mode == FileModes.IMAGE;
		break;

    case FileTypes.JPEG:
      return mode == FileModes.IMAGE;
    break;

		case FileTypes.PNG:
			return mode == FileModes.IMAGE;
		break;

	}
	return false;
};

exports.isValidPurpose = function(purpose){
	if(validPurposes.indexOf(purpose) >= 0)
		return true;
	return false;
};

exports.decideTargetAwsBucket = function (purpose){
	switch(purpose){

		case MediaPurpose.QUESTION:
			return Buckets.QUES_PIC;
			break;

		case MediaPurpose.OPTION:
		return Buckets.OPNS_PIC;
			break;

		case MediaPurpose.HINT:
			return Buckets.HINT_PIC;
			break;

		case MediaPurpose.SOLUTION:
			return Buckets.SOLN_PIC;
			break;

	}
	return false;
}

function getExpirytime(expiry){
  var target_time    = new Date().getTime()+expiry;
  var _date          = new Date(target_time);
  return '' + (_date.getFullYear()) + '-' + (_date.getMonth() + 1) + '-' +
          (_date.getDate()) + 'T' + (_date.getHours()) + ':' + (_date.getMinutes()) + ':' +'00.000Z';
}
