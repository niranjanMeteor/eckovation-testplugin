var Purpose = {

	QUESTION          : "QUES",
	OPTION            : "OPNS",
	HINT              : "HINT",
	SOLUTION          : "SOL"

};

module.exports = Purpose;