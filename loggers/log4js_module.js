var log4js              	     = require('log4js');
var Log4jsConfigs         	     = require('./log4js_configs.js');

var current_date        	     = new Date();
var file_date_ext       	     = current_date.getFullYear()+'_'+(current_date.getMonth()+1)+'_'+current_date.getDate();

var routesCategoryErrorLog              = Log4jsConfigs.LOG_DIR_ROUTES_CATEGORY+'/err_'+file_date_ext+'.log';
var routesTestErrorLog                  = Log4jsConfigs.LOG_DIR_ROUTES_TEST+'/err_'+file_date_ext+'.log';
var routesTestQuestionErrorLog          = Log4jsConfigs.LOG_DIR_ROUTES_TESTQUESTION+'/err_'+file_date_ext+'.log';
var routesQuestionErrorLog              = Log4jsConfigs.LOG_DIR_ROUTES_QUESTION+'/err_'+file_date_ext+'.log';
var routesAnswerErrorLog                = Log4jsConfigs.LOG_DIR_ROUTES_ANSWER+'/err_'+file_date_ext+'.log';
var routesUserErrorLog                  = Log4jsConfigs.LOG_DIR_ROUTES_USER+'/err_'+file_date_ext+'.log';
var routesUserTestErrorLog              = Log4jsConfigs.LOG_DIR_ROUTES_USERTEST+'/err_'+file_date_ext+'.log';
var routesUserTestQuestionErrorLog      = Log4jsConfigs.LOG_DIR_ROUTES_USERTESTQUESTION+'/err_'+file_date_ext+'.log';
var importQuestionLog                   = Log4jsConfigs.LOG_DIR_IMPORT_QUESTION+'/err_'+file_date_ext+'.log';
var routesEckovationLog                 = Log4jsConfigs.LOG_DIR_ROUTES_ECKOVATION+'/info_'+file_date_ext+'.log';
var routesCategoryAdminErrorLog         = Log4jsConfigs.LOG_DIR_ROUTES_CATEGORY_ADMIN+'/err_'+file_date_ext+'.log';
var routesTestAdminErrorLog             = Log4jsConfigs.LOG_DIR_ROUTES_TEST_ADMIN+'/err_'+file_date_ext+'.log';
var routesTestQuestionAdminErrorLog     = Log4jsConfigs.LOG_DIR_ROUTES_TESTQUESTION_ADMIN+'/err_'+file_date_ext+'.log';
var routesQuestionAdminErrorLog         = Log4jsConfigs.LOG_DIR_ROUTES_QUESTION_ADMIN+'/err_'+file_date_ext+'.log';
var routesMediaErrorLog                 = Log4jsConfigs.LOG_DIR_ROUTES_MEDIA+'/err_'+file_date_ext+'.log';
var routesMediaAdminErrorLog            = Log4jsConfigs.LOG_DIR_ROUTES_MEDIA_ADMIN+'/err_'+file_date_ext+'.log';
var routesReportFeedbackErrorLog        = Log4jsConfigs.LOG_DIR_ROUTES_REPORTFEEDBACK+'/err_'+file_date_ext+'.log';
var routesAnalyticsErrorLog             = Log4jsConfigs.LOG_DIR_ROUTES_ANALYTICS+'/err_'+file_date_ext+'.log';
var routesUserAnswersLog                = Log4jsConfigs.LOG_DIR_ROUTES_USERANSWERS_SUBMIT+'/info_'+file_date_ext+'.log';
var routesCatUserTestErrorLog           = Log4jsConfigs.LOG_DIR_ROUTES_CAT_USERTEST+'/info_'+file_date_ext+'.log';
var routesCatUserTestQuestionErrorLog   = Log4jsConfigs.LOG_DIR_ROUTES_CAT_TESTQUESTION+'/info_'+file_date_ext+'.log';
var routesCatAnswerLog                  = Log4jsConfigs.LOG_DIR_ROUTES_CAT_ANSWER+'/info_'+file_date_ext+'.log';
var routesTagAdminLog                   = Log4jsConfigs.LOG_DIR_ROUTES_TAG_ADMIN+'/info_'+file_date_ext+'.log';
var routesOpsLog                        = Log4jsConfigs.LOG_DIR_ROUTES_OPS+'/info_'+file_date_ext+'.log';

log4js.configure({
  appenders: [
    { type: 'file', filename: routesCategoryErrorLog,      	    category: Log4jsConfigs.CATEGORY_ROUTES_CATEGORY},
    { type: 'file', filename: routesTestErrorLog,          	    category: Log4jsConfigs.CATEGORY_ROUTES_TEST},
    { type: 'file', filename: routesTestQuestionErrorLog,  	    category: Log4jsConfigs.CATEGORY_ROUTES_TESTQUESTION},
    { type: 'file', filename: routesUserTestQuestionErrorLog,   category: Log4jsConfigs.CATEGORY_ROUTES_USERTESTQUESTION},
    { type: 'file', filename: routesUserTestErrorLog,           category: Log4jsConfigs.CATEGORY_ROUTES_USERTEST},
    { type: 'file', filename: routesQuestionErrorLog,      	    category: Log4jsConfigs.CATEGORY_ROUTES_QUESTION},
    { type: 'file', filename: routesAnswerErrorLog,   	   	    category: Log4jsConfigs.CATEGORY_ROUTES_ANSWER},
    { type: 'file', filename: routesUserErrorLog,   	        category: Log4jsConfigs.CATEGORY_ROUTES_USER},
    { type: 'file', filename: importQuestionLog,           	    category: Log4jsConfigs.CATEGORY_IMPORT_QUESTION},
    { type: 'file', filename: routesEckovationLog,              category: Log4jsConfigs.CATEGORY_ROUTES_ECKOVATION},
    { type: 'file', filename: routesCategoryAdminErrorLog,      category: Log4jsConfigs.CATEGORY_ROUTES_CATEGORY_ADMIN},
    { type: 'file', filename: routesTestAdminErrorLog,          category: Log4jsConfigs.CATEGORY_ROUTES_TEST_ADMIN},
    { type: 'file', filename: routesQuestionAdminErrorLog,      category: Log4jsConfigs.CATEGORY_ROUTES_QUESTION_ADMIN},
    { type: 'file', filename: routesTestQuestionAdminErrorLog,  category: Log4jsConfigs.CATEGORY_ROUTES_TESTQUESTION_ADMIN},
    { type: 'file', filename: routesMediaErrorLog,              category: Log4jsConfigs.CATEGORY_ROUTES_MEDIA},
    { type: 'file', filename: routesMediaAdminErrorLog,         category: Log4jsConfigs.CATEGORY_ROUTES_MEDIA_ADMIN},
    { type: 'file', filename: routesReportFeedbackErrorLog,     category: Log4jsConfigs.CATEGORY_ROUTES_REPORTFEEDBACK},
    { type: 'file', filename: routesAnalyticsErrorLog,          category: Log4jsConfigs.CATEGORY_ROUTES_ANALYTICS},
    { type: 'file', filename: routesUserAnswersLog,             category: Log4jsConfigs.CATEGORY_ROUTES_USERANSWERS_SUBMIT},
    { type: 'file', filename: routesCatUserTestErrorLog,        category: Log4jsConfigs.CATEGORY_ROUTES_CAT_USERTEST},
    { type: 'file', filename: routesCatUserTestQuestionErrorLog,category: Log4jsConfigs.CATEGORY_ROUTES_CAT_TESTQUESTION},
    { type: 'file', filename: routesCatAnswerLog,               category: Log4jsConfigs.CATEGORY_ROUTES_CAT_ANSWER},
    { type: 'file', filename: routesTagAdminLog,                category: Log4jsConfigs.CATEGORY_ROUTES_TAG_ADMIN},
    { type: 'file', filename: routesOpsLog,                     category: Log4jsConfigs.CATEGORY_ROUTES_OPS}
  ]
});

module.exports = log4js;
