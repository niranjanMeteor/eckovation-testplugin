const mongoose 							= require('mongoose');

const configs					      = require('../configs/configs.js');
const log4jsLogger  				= require('../loggers/log4js_module.js');
const log4jsConfigs  			  = require('../loggers/log4js_configs.js');

require('../models/usertests.js');
require('../models/groupstats.js');
const UserTest 							= mongoose.model('UserTest');
const GroupStats		        = mongoose.model('GroupStats');

exports.getUserData = function(test_ids, pid, total_marks, cb){
	var data    = [];
	var total_l = test_ids.length;
	UserTest.find({
		pid          : pid,
		test_id      : {$in:test_ids},
		is_submitted : true,
		is_first     : true
	},{
		test_id        : 1,
		marks_obtained : 1
	},{
		sort : {
			end_time : 1
		}
	},function(err,usertests){
		if(err){
			return cb(err,null);
		}

		if(usertests.length == 0){
			return cb(null,{name:"Me",data:data});
		}
		var usertestByTestId = {};
		var total = usertests.length;
		for(var j=0; j<total; j++){
			usertestByTestId[usertests[j].test_id+''] = usertests[j].marks_obtained;
		}
		var marks   = 0;
		var marks_f = 0;
		data.push([0,0]);
		for(var i=0; i<total_l; i++){
			if((test_ids[i]+'') in usertestByTestId){
				marks = marks + usertestByTestId[test_ids[i]+''];
				marks_f = Math.round((marks / total_marks)*100);
			}
			data.push([(i+1),marks_f]);
		}
		console.log('total_marks');
		console.log(total_marks);
		console.log('getUserData');
		console.log(data);
		return cb(null,{name:"Me",data:data});
	});
};

// exports.getAverageData = function(tests, total_marks, cb){
// 	var total     = tests.length;
// 	var average_f = [];
// 	var marks     = 0;
// 	var submits   = 0;
// 	var average   = 0.00;

// 	average_f.push([0,0]);
// 	for(var i=0; i<total; i++){
// 		if("total_submits" in tests[i] && tests[i].total_submits > 0){
// 			marks     = marks + ((!tests[i].marks_scored) ? 0 : tests[i].marks_scored);
// 			submits   = submits + tests[i].total_submits;
// 			average   = Math.round((marks / (total_marks*submits))*100);
// 		}
// 		average_f.push([(i+1),average]);
// 	}
// 	console.log('getAverageData');
// 	console.log(submits);
// 	console.log(average_f);
// 	return cb(null,{name:"Average",data:average_f});
// };

exports.getAverageData2 = function(test_ids, gid, total_marks, cb){
	var average_f = [];
	var total_t   = test_ids.length;
	GroupStats.aggregate([
    {
      $match :{
        gid  : gid
      }
    },
    {
      $group :{
        _id : "$pid",
        tscore : {
          $sum : "$score"
        }
      }
    }
  ],function(err,groupstats){
  	if(err){
  		return cb(err,null);
  	}
  	if(groupstats.length == 0){
  		return cb(null,{name:"Average",data:average_f});
  	}
  	console.log('groupstats total entries');
  	console.log(groupstats.length);
  	groupstats.sort(function(a,b){
  		return (b.tscore - a.tscore); 
  	});
  	var total = groupstats.length;
  	if(total < 2){
  		return cb(null,{name:"Average",data:average_f});
  	}
  	var pids  = [];
  	var total_l = total / configs.TEST_AVERGAE_GRAPH_SIZE_RATIO;
  	for(var i=0; i<total_l; i++){
  		if(pids.indexOf(groupstats[i]._id+'') < 0){
  			pids.push(groupstats[i]._id+'');
  		}
  	}
  	// UserTest.aggregate([
	  //   {
	  //     $match :{
	  //       pid          : {$in:pids},
	  //       test_id      : {$in:test_ids},
	  //       is_submitted : true,
	  //       is_first     : true
	  //     }
	  //   },
	  //   {
	  //     $group :{
	  //       _id : "$test_id",
	  //       tmarks : {
	  //         $sum : "$marks_obtained"
	  //       },
	  //       tcount : {
	  //       	$sum : 1
	  //       }
	  //     }
	  //   }
	  // ],function(err,usertests){
	  UserTest.find({
	  	pid          : {$in:pids},
      test_id      : {$in:test_ids},
      is_submitted : true,
      is_first     : true
	  },{
	  	test_id 			 : 1,
	  	marks_obtained : 1
	  },function(err,usertests){
	  	if(err){
	  		return cb(err,null);
	  	}
	  	if(usertests.length == 0){
	  		return cb(null,{name:"Average",data:average_f});
	  	}
			var total     = usertests.length;
			var testWiseMarks = [];
			var testWiseSubmits = []
			var temp;
			for(var j=0; j<total; j++){
				temp = usertests[j].test_id+'';
				if(!(temp in testWiseMarks)){
					testWiseMarks[temp] = 0;
				}
				testWiseMarks[temp] = testWiseMarks[temp] + usertests[j].marks_obtained;
				if(!(temp in testWiseSubmits)){
					testWiseSubmits[temp] = 0;
				}
				testWiseSubmits[temp] = testWiseSubmits[temp] + 1;
			}

			var marks     = 0;
			var submits   = 0;
			var average   = 0.00;
			var temp_test_id;

			average_f.push([0,0]);
			for(var i=0; i<total_t; i++){
				temp_test_id = test_ids[i];
				if((temp_test_id in testWiseMarks) && (temp_test_id in testWiseMarks)){
					marks     = marks + testWiseMarks[temp_test_id];
					submits   = submits + testWiseSubmits[temp_test_id];
					average   = Math.round((marks / (total_marks*submits))*100);
				}
				average_f.push([(i+1),average]);
			}
			console.log('getAverageData');
			console.log(submits);
			console.log(average_f);
			return cb(null,{name:"Average",data:average_f});
	  });
  });	
};

exports.getTopperData = function(test_ids, gid, total_marks, cb){
	var data = [];
	GroupStats.find({
		gid : gid
	},{
		_id   : 0,
		pid   : 1,
		score : 1
	},function(err,groupstats){
		if(err){
			return cb(err,null);
		}
		if(groupstats.length == 0){
			return cb(null,{name:"Topper",data:data});
		}
		var total        = groupstats.length;
		var pidWiseScore = {};
		var max          = 0;
		var max_pid;
		var temp_p, temp_m;

		for(var i=0; i<total; i++){
			temp_p = groupstats[i].pid+'';
			temp_m = ((!groupstats[i].score) ? 0 : groupstats[i].score);
			if(!(temp_p in pidWiseScore)){
				pidWiseScore[temp_p] = temp_m;
			} else {
				pidWiseScore[temp_p] = pidWiseScore[temp_p] + temp_m;
			}
			if(max < pidWiseScore[temp_p]){
				max = pidWiseScore[temp_p];
				max_pid = temp_p;
			}
		}
		// console.log('max_pid : '+max_pid);
		// console.log('max : '+max);
		UserTest.find({
			pid          : max_pid,
			test_id      : {$in:test_ids},
			is_submitted : true,
			is_first     : true
		},{
			test_id        : 1,
			marks_obtained : 1
		},{
			sort : {
				end_time : 1
			}
		},function(err,usertests){
			if(err){
				return cb(err,null);
			}
			if(usertests.length == 0){
				return cb(null,{name:"Topper",data:data});
			}

			var total   = usertests.length;
			var marks   = 0;
			var marks_f = 0;
			var total_l = test_ids.length;
			var usertestByTestId = {};

			for(var j=0; j<total; j++){
				usertestByTestId[usertests[j].test_id+''] = usertests[j].marks_obtained;
			}

			data.push([0,0]);
			for(var i=0; i<total_l; i++){
				if((test_ids[i]+'') in usertestByTestId){
					marks = marks + usertestByTestId[test_ids[i]+''];
					marks_f = Math.round((marks / total_marks)*100);
				}
				data.push([(i+1),marks_f]);
			}
			
			// console.log('getTopperData');
			// console.log(data);
			return cb(null,{name:"Topper",data:data});
		});
	});
};
