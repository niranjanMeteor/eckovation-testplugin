var mongoose 							= require('mongoose');

var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var configs					      = require('../configs/configs.js');

require('../models/groupstats.js');
var GroupStats		        = mongoose.model('GroupStats');

exports.getRankData = function(gid, pid, total_marks, cb){
	var data       = [];
	var user_data  = [];

	GroupStats.find({
		gid : gid
	},{
		_id   : 0,
		pid   : 1,
		score : 1
	},function(err,groupstats){
		if(err){
			return cb(err,null);
		}
		if(groupstats.length == 0){
			return cb(null,{user_data:user_data,data:data});
		}
		var total        = groupstats.length;
		var pidWiseScore = {};
		var temp_p, temp_m;

		for(var i=0; i<total; i++){
			temp_p = groupstats[i].pid+'';
			temp_m = ((!groupstats[i].score) ? 0 : groupstats[i].score);
			if(!(temp_p in pidWiseScore)){
				pidWiseScore[temp_p] = temp_m;
			} else {
				pidWiseScore[temp_p] = pidWiseScore[temp_p] + temp_m;
			}
		}
		var groupBasedStats = [];
		for(var key in pidWiseScore){
			groupBasedStats.push({
				pid   : key,
				score : Math.round((pidWiseScore[key] / total_marks) * 100)
			});
		}

		groupBasedStats.sort(function(a,b){
			return (b.score - a.score);
		});
		var total_students = groupBasedStats.length;

		var data = [];
		var percentile;
		var temp_row;
		var scoreWisePercentile = {};

		for(var m=0; m<total_students; m++){
			percentile = Math.round(((total_students - m) / total_students) * 100);
			temp_row = groupBasedStats[m];
			if(temp_row.pid == pid)
				user_data.push([temp_row.score,percentile]);
			data.push([temp_row.score, percentile]);
			scoreWisePercentile[temp_row.score] = percentile;
		}
		var extraPoints = findExtraPoints(groupBasedStats, scoreWisePercentile, total_marks);
		// console.log('student_ranking');
		// console.log(user_data);
		// console.log(data);
		// console.log(extraPoints);
		return cb(null,{user_data:user_data,data:data,first:extraPoints.first,second:extraPoints.second});
	});
};

function findExtraPoints(sorted_data, scoreWisePercentile, total_marks){
	var first_point_data  = [];
	var second_point_data = [];
	var len               = sorted_data.length;
	var x11, x12, y11, y12, x21, x22, y21, y22;

	var first_point_x  = total_marks * configs.TEST_STUDENT_RANKING_FIRST_POINT;
	var second_point_x = total_marks * configs.TEST_STUDENT_RANKING_SECOND_POINT;

	for(var i=0; i<(len-1); i++){
		if(first_point_x >= sorted_data[i].score && first_point_x <= sorted_data[i].score){
			x11 = sorted_data[i].score;
			y11 = scoreWisePercentile[sorted_data[i].score];

			x12 = sorted_data[i+1].score;
			y12 = scoreWisePercentile[sorted_data[i+1].score];
			break;
		}
	}
	i = 0;
	for(var i=0; i<(len-1); i++){
		if(second_point_y >= sorted_data[i].score && second_point_y <= sorted_data[i].score){
			x21 = sorted_data[i].score;
			y21 = scoreWisePercentile[sorted_data[i].score];

			x22 = sorted_data[i+1].score;
			y22 = scoreWisePercentile[sorted_data[i+1].score];
			break;
		}
	}
	var first_point_y  = findPointFromLineEquation(x11, x12, y11, y12, first_point_x);
	var second_point_y = findPointFromLineEquation(x21, x22, y21, y22, second_point_y);

	first_point_data.push([0,first_point_y]);
	first_point_data.push([first_point_x,first_point_y]);
	first_point_data.push([first_point_x,0]);

	second_point_data.push([0,second_point_y]);
	second_point_data.push([second_point_x,second_point_y]);
	second_point_data.push([second_point_x,0]);

	return {first:first_point_data, second:second_point_data};

}

function findPointFromLineEquation(x1, x2, y1, y2, target_x){
	var slope    = (y2-y1)/(x2-x1);
	var constant = (y2-(slope*x1))
	var target_y = y1 + (slope*x1) + constant;
	return Math.round(target_y);
}