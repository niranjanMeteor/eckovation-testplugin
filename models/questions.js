var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var question = new mongoose.Schema(
	{
		type          : { type : Number, required : true },
		level         : Number,
		plugin_id 	  : { type : ObjectId },
		text 	        : { type : String, required : true },
		audio_url     : String,
		hint_text     : String,
		hint_img_url  : String,
		hint_aud_url  : String,
		ans_options   : { type : Array , required : true },       //  [ {index:1,text:"",img:""},{},{},{}]
		correct_ans   : { type : Number, required : true },
		attempts      : Number,
		correct_subm  : Number,
		img_arr       : { type : Array , required : false },      // [{fnm:"",ftp:""},{}]
		expl          : String,
		tag           : String,
		tim           : Number
	},
	{ 
		timestamps : true
	}
);

question.statics = {
	QUESTION_TYPE : {
		SINGLE_CORRECT     : 1,
		MULTIPLE_CORRECT   : 2
	},
	QUESTION_LEVEL : {
		EASY     : 1,
		MEDIUM   : 2,
		HARD     : 3,
		NPHARD   : 4
	}
};

mongoose.model('Question',question);