var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var answer = new mongoose.Schema(
	{
		ques_id    : { type : ObjectId, required : true },         //  question id
		act        : { type : Boolean,  required : true },         //  if answer is associated with ques then true else false                        
		text 	     : { type : String,   required : true },         //  text of the question
		is_correct : { type : Number,   required : true },         //  1 = correct , 0 = incorrect
		option_no  : String,
		expln      : String,                                       //  Explanation if given
		image_url  : String,                                       //  url of the image attached for this option
		audio_url  : String                                       //  url of the audio for covering explanations in detail 
	},
	{ 
		timestamps : true
	}
);

question.statics = {
	ANSWER_FATE : {
		CORRECT     : 1,
		INCORRECT   : 0
	}
};

mongoose.model('Answer',answer);