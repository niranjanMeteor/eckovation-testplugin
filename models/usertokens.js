var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var usertoken = new mongoose.Schema(
	{
		user_id       : { type : ObjectId, required : true },
		plugin_id     : { type : ObjectId, required : true },
		act 				  : { type : Boolean, required : true },
		xcess_token   : String,
	},
	{ 
		timestamps : true
	}
);

usertoken.index({plugin_id:1,user_id:1,act:1});

mongoose.model('UserToken',usertoken);