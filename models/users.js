var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var user = new mongoose.Schema(
	{
		identifier 		: { type : String, required : true },
		plugin_id     : { type : ObjectId, required : true },
		server_id     : { type : ObjectId, required : true },
		gid           : { type : String, required : true },
		name 					: { type : String, required : false },
		email 				: { type : String, required : false },
		img_url   		: String,
		role          : Number,
		desc      		: String,
		attempts  		: Number,
		submits   		: Number,
		score     		: Number,
		marks     		: Number,
		all_correct_count : Number,
		rank 					: Number,
		act 				  : { type : Boolean, required : true }
	},
	{ 
		timestamps : true
	}
);

user.index({plugin_id:1,score:1});
user.index({identifier:1,act:1});
user.index({identifier:1,plugin_id:1,gid:1,act:1});
user.index({score:1});

mongoose.model('User',user);