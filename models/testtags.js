var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var testtag = new mongoose.Schema(
	{
		tid     : { type : ObjectId, required : true },   // test id
		tgid    : { type : ObjectId, required : true },   // tag id
		rst 	  : { type : Number, required : true},      // resource type
		rsd     : { type : Array, required : true},       // resource data
		tim 	  : { type : Number, required : true},    
		act     : { type : Boolean, required : true}
	},
	{
		timestamps : true
	}
);

testtag.statics = {
	RESOURCE_TYPE : {
		VIDEO   : 1,
		QUIZ    : 2,
		PDF     : 3,
		LINK    : 4
	},
};

testtag.index({tid:1,act:1});
testtag.index({tgid:1,act:1});
testtag.index({tid:1,tgid:1,act:1});

mongoose.model('TestTag',testtag);
