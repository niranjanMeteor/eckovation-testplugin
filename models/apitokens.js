var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var apitoken = new mongoose.Schema(
	{
		key    		 : { type : String,  required : true },         //  if answer is associated with ques then true else false                        
		secret 	   : { type : String,   required : true },         //  text of the question
		act 			 : { type : Boolean,   required : true }
	},
	{ 
		timestamps : true
	}
);

mongoose.model('Apitoken',apitoken);