var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var datacopyprogress = new mongoose.Schema(
	{
		source_id         : { type : ObjectId, required : true },
		target_id					: { type : ObjectId, required : true },
		target_aux				: { type : String, required : true},
		type 							: { type : String, required : true},
	},
	{ 
		timestamps : true
	}
);

datacopyprogress.index({source_id:1,target_id:1,target_aux:1,type:1},{ unique: true });

mongoose.model('DataCopyProgress',datacopyprogress);