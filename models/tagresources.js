var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var tagresource = new mongoose.Schema(
	{
		tgid    : { type : ObjectId, required : true },   // tag ids
		rsty 	  : { type : Number, required : true},      // resource type
		rsdt    : { type : Array, required : true},      // resource data
		tim 	  : { type : Number, required : true},    
		act     : { type : Boolean, required : true}
	},
	{
		timestamps : true
	}
);

tagresource.statics = {
	RESOURCE_TYPE : {
		VIDEO   : 1,
		QUIZ    : 2,
		PDF     : 3,
		LINK    : 4
	},
};

tagresource.index({tgid:1,act:1});

mongoose.model('tagresource',tagresource);
