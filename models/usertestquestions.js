var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var usertestquestions = new mongoose.Schema(
	{
		gid                 : { type : String, required : false },     // group id   
		plugin_id           : { type : ObjectId, required : false },   // plugin id
		user_test_id        : { type : ObjectId, required : true },    // user test id
		test_id        			: { type : ObjectId, required : true },    // test id
		ques_id        			: { type : ObjectId, required : true },    // question id
		ques_key            : Number,                                  // question key
		is_attempted        : { type : Boolean, default : false },     // has user attemtped this question
		ans_no              : Number,                                  // answer submitted by the user for the question
		ans_text            : String,                                  // text based answer for the question
		qlv                 : { type : Number, default : 1 },           // level of the question in the test
		is_correct          : { type : Boolean, default : false },     // is the user answer submitted correct
		qtim                : { type : Number, default : false },      // time spent by user on this question
		correct_answer      : Number,                                  // the correct answer of the question
		marks_obtained      : Number,                                  // marks obtained by the user in this question
		max_marks_ques      : Number,                                  // maximum marks available for this question
		ineg                : Boolean,                                 // is negative marking enabled for this question                             
	  negm                : Number,                                  // is ineg is true, what is negative marking penalty for this question															
		feedback  			    : String,
		tim                 : Number,
		code                : String,
		act                 : { type : Boolean, default : true }
	},
	{ 
		timestamps : true
	}
);

usertestquestions.index({user_test_id:1,ques_id:1});
usertestquestions.index({user_test_id:1,act:1});
usertestquestions.index({user_test_id:1,qlv:1});
usertestquestions.index({user_test_id:1,ques_id:1,act:1});
usertestquestions.index({user_test_id:1,is_attempted:1});
usertestquestions.index({user_test_id:1,is_attempted:1,act:1});
usertestquestions.index({test_id:1,ques_id:1,is_attempted:1,act:1});
usertestquestions.index({test_id:1,ques_id:1,is_correct:1,act:1});
usertestquestions.index({tim:1});

mongoose.model('UserTestQuestion',usertestquestions);