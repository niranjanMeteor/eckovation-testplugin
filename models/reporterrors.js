var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var reporterror = new mongoose.Schema(
	{
		plugin_id           : { type : ObjectId, required : true },
		user_id             : { type : ObjectId, required : true },
		gid                 : { type : String, required : true },
		test_id             : { type : ObjectId, required : false },
		user_test_id        : { type : ObjectId, required : false },
		ques_id        			: { type : ObjectId, required : false },
		feedback  			    : String,
		act                 : { type : Boolean, default :false },
		action_taken        : String,
		tim                 : Number
	},
	{ 
		timestamps : true
	}
);

reporterror.index({plugin_id:1});
reporterror.index({gid:1});
reporterror.index({act:1});
reporterror.index({user_id:1});
reporterror.index({test_id:1});
reporterror.index({user_test_id:1});
reporterror.index({ques_id:1});
reporterror.index({gid:1,plugin_id:1});
reporterror.index({tim:1});

mongoose.model('ReportError',reporterror);