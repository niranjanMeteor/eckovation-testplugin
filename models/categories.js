var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var category = new mongoose.Schema(
	{
		plugin_id         : { type : ObjectId, required : true },
		name 			        : { type : String, required : true },
		catg_no           : { type : Number, required : true, unique : 1 },
		act               : { type : Boolean,  required : true },
		type              : { type : Number,  default : 1 },           // TOPIC : 1 , MIX : 2
		gid               : { type : String, required : true },
		adpv              : { type : Boolean,  required : false, default : false },
		adpstg            : { type : Number, default : 1},        // if adaptive, then at which stage
		total_num_tests   : { type : Number, default : 0},
		total_attempts    : { type : Number, default : 0},
		total_submits     : { type : Number, default : 0},
		desc              : String,
		pic_url		        : String,
		most_popular_test : String,
		visibility        : Number,
		tim               : Number
	},
	{ 
		timestamps : true
	}
);

category.index({plugin_id:1,gid:1,type:1,act:1,total_num_tests:1});
category.index({plugin_id:1,gid:1,act:1});
category.index({plugin_id:1,type:1,act:1});
category.index({gid:1,act:1,total_num_tests:1});
category.index({total_attempts:1});
category.index({createdAt:1});

mongoose.model('Category',category);