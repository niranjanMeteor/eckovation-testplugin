var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var test = new mongoose.Schema(
	{
		name 	             		: { type : String, required : true },
		act                		: { type : Boolean, required : true },
		catg_id            		: { type : ObjectId, required : true },
		adpv                  : { type : Boolean, required : false, default : false }, // is test adaptive
		adpstg                : { type : Number, default : 1},        // if adaptive, then at which stage
		mxqlv                 : { type : Number, default : 1},        // if adaptive and stage > 1, max level of the questions
		level              		: { type : Number, default : 1},        // level of the test
		tags                  : { type : Array, required : false},    // tags of the test
		type                  : { type : Number, default : 1 },       // type of the test
		model                 : { type : Number, default : 1 },       // model of the test
		sub_model             : { type : Number },                    // sub model of the test wrt to some model
		is_default            : { type : Boolean, required : false },
		desc                  : String,                                // description of the test
		num_ques           		: Number,                                // number of questions
		ques_quota            : Number,                                
		mpq                   : Number,                                // marks per question
		total_marks           : Number,                                // maximum marks of the test
		icat                  : { type : Boolean, required : false },  // is this test for cat
		sect                  : { type : Array, required : false },    // sections for cat = ["1","2","3"]
		marks_scored      		: { type : Number, default : 0},         // marks scored by attemptees
		q_order               : { type : Number, default : 2 },        // ordering of the question - ordered or jumbled
		ssol                  : { type : Boolean, default : true },    // show solutions = 1 , don't show solutions = 2
		timed                 : { type : Boolean, default : false },   // is the test timed
		ml_at                 : { type : Boolean, default : true },    // multiple attempts allowed
		min_tim            		: Number,                                 
		max_tim            		: Number,                                // test maximum time duration
		ineg                  : Boolean,                               // is negtive marking allowed
	  negm                  : Number,															   // if negative marking allowed, then default negative marks
		publ                  : { type : Boolean, default : false },   // whether to publish the test 
		stim                  : Number,                                // publish start time
		i_fl_st               : { type : Boolean, default : false },   // if full set showing enabling
		total_attempts     		: { type : Number, default : 0},         // total attempts
		total_submits      		: { type : Number, default : 0},         // total submits
		submits_all_correct		: Number,
		min_time_submit_all_correct : Number,     
		max_score_attained 		: Number,                                // maximum score obtained
		tim                   : Number,
		i_cert                : { type : Boolean, default : false },     // is this test applicable for certificates sending
		t_cert                : { type : Number, default : 0},           // if certicates sending is true, then what type of certificate
		dt_cert               : Object                                   // course details - {title:"", name:"",duration:"", min_req:40,sign_name:"niranjan"}
	},
	{ 
		timestamps : true
	}
);

test.index({act:1});
test.index({catg_id:1,act:1,is_default:1});
test.index({catg_id:1,act:1,num_ques:1});
test.index({catg_id:1,act:1,num_ques:1,publ:1,stim:1});
test.index({act:1,num_ques:1,publ:1,stim:1});
test.index({catg_id:1,act:1,publ:1});
test.index({publ:1});
test.index({stim:1});
test.index({tim:1});
test.index({createdAt:1});
test.index({level:1,createdAt:1});

test.statics = {
	TEST_LEVELS : {
		BEGINERS       : 1,
		EXPERIENCED    : 2,
		ADVANCED       : 3,
		EXPERT         : 4
	},
	TYPE : {
		GENERAL           : 1,
		COMPREHENSION     : 2,
		MULTIPLE_CORRECT  : 3,
		MATCHING          : 4,
		ASSERTION_REASON  : 5,
		SUBJECTIVE        : 6,
		FILL_BLANKS       : 7,
		TRUE_FALSE        : 8
	},
	ADPATIVE_STAGE : {
		FIRST             : 1,
		SECOND            : 2,
		THIRD             : 3,
		FOURTH            : 4
	},
	MODEL : {
		REGULAR           : 1,
		PSYCHOMETRY       : 2,
		SURVEY            : 3,
		COURSE_MATCHING   : 4
	},
	QUESTION_ORDER : {
		ORDERED           : 1,
		JUMBLED           : 2,
	},
	QUESTION_ORDER_VALUES : {
		ORDERED           : "ORDERED",
		JUMBLED           : "JUMBLED",
	},
	CERTIFICATE_TYPES : {
		SIDE_COURSE       : 1,
		MAIN_COURSE       : 2,
		EXCELLENCE        : 3,
		PARTICIPATION     : 4
	}
};

mongoose.model('Test',test);