var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var groupstats = new mongoose.Schema(
	{
		plugin_id     : { type : ObjectId, required : true },
		user_id       : { type : ObjectId, required : true },
		pid           : { type : ObjectId, required : true },
		gid           : { type : String, required : true },
		attempts  		: Number,
		submits   		: Number,
		score     		: Number,
		marks     		: Number,
		all_correct_count : Number,
		rank 					: Number,
		act 				  : Boolean
	},
	{ 
		timestamps : true
	}
);

groupstats.index({plugin_id:1,gid:1,user_id:1});
groupstats.index({plugin_id:1,gid:1,score:1});
groupstats.index({score:1});

mongoose.model('GroupStats',groupstats);