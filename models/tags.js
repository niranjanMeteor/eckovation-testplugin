var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var tag = new mongoose.Schema(
	{
		nm      : { type : String, required : true},    // name of the tag
		dsc     : { type : String, required : false},   // description of the tag
		als     : { type : Array, required : false},    // alias of this tag
		tim 	  : { type : Number, required : true},    
		nflw    : { type : Number, required : false, default : 0},    // number of followers
		act     : { type : Boolean, required : true}
	},
	{
		timestamps : true
	}
);

tag.index({nm:1,act:1});
tag.index({als:1,act:1});

mongoose.model('Tag',tag);
