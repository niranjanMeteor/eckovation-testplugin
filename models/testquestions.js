var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var testquestion = new mongoose.Schema(
	{
		test_id    		: { type : ObjectId, required : true },
		ques_id    		: { type : ObjectId, required : true },
		act        		: { type : Boolean,  required : true },  // if question is associated with test then true else false                        
	  lv            : { type : Number, default : 1 },        // level of the question in this test
	  key           : { type : Number, required : true },    // index kind of field starting from 0
	  marks         : { type : Number, required : true },
	  tags          : { type : Array, required : false},     // tag ids
	  ineg          : { type : Boolean,  required : false }, // is negtive marking allowed for this question in this test
	  negm          : Number,															   // if negative marking allowed , then deduction
		atmp          : { type : Number, default : 0 },        // how many people attempted this ques in this test
		crct          : { type : Number, default : 0 },        // how many people have correctly submitted the answer
		code          : String
	},
  { 
		timestamps : true
	}
);

testquestion.index({key:1,act:1});
testquestion.index({test_id:1,key:1});
testquestion.index({test_id:1,lv:1,key:1});
testquestion.index({test_id:1,act:1});
testquestion.index({test_id:1,ques_id:1,act:1});
testquestion.index({key:1});

mongoose.model('TestQuestion',testquestion);