var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 
var usertest = new mongoose.Schema(
	{
		user_id        			: { type : ObjectId, required : true },
		pid        			    : { type : ObjectId, required : true },  // profile id
		test_id        			: { type : ObjectId, required : true },  // test id
		catg_id        			: { type : ObjectId, required : true },  // category id
		xcess_token         : { type : String, required : false },   
		gid                 : { type : String, required : false },   // group id
		plugin_id           : { type : ObjectId, required : false }, // plugin id
		act                 : { type : Boolean, default : false },   
		is_started          : { type : Boolean, default : false },   // is this test started
		is_submitted        : { type : Boolean, default : false },   // is this user test submitted
		is_first            : { type : Boolean, default : false },   // is this usertest submitted the firt one
		test_type           : { type : Number, default : 1 },        
		adpv                : { type : Boolean, required : false, default : false }, // is test adaptive
		adpstg              : { type : Number, default : 1},         // if adaptive, then at which stage
		mxqlv               : { type : Number, default : 1},         // if adaptive and stage > 1, max level of the questions
		tlvl                : { type : Number, default : 1},         // level of the test
		r_q_lv              : { type : Number, default : 1 },        // level of the recent most question submitted
		r_q_st              : { type : Number, default : 1 },        // status of the recent most question presented to the user
		q_cnt               : { type : Number, default : 0 },        // number of questions presented to user as of now
		q_order             : { type : Number, default : 2 },        // question ordering
		start_time          : Number,                                // start time
		end_time            : Number,                                // end time
		total_ques     			: Number,                                // total question in the test
		total_marks  	      : Number,                                // total marks
		time_alloted        : Number,                               
		time_taken     			: Number,                                // time taken
		total_correct       : Number,                                // total correct
		if_all_correct      : { type : Boolean, default : false },   // whether all answers were found correct
		marks_obtained      : Number,                                // marks obtained
		lcatsec             : String,                                // last cat section user was on
		lcatsecst           : Number,                                // last cat section start time
		lcatsecut           : Number,                                // last cat section last update time
		lresmst             : Number,                                // last resume start time
		tconsm              : Number,                                // time consumed before last resumed
		l_u_tim             : Number,                                // last usertest time recorded
		l_u_q_id            : { type : ObjectId, required : false }, // last question id visited by user in the test
		resm_c              : { type : Number, default : 0 },
		score               : Number,
		test_rank 			    : Number,
		feedback  			    : String
	},
	{ 
		timestamps : true
	}
);

usertest.index({plugin_id:1,user_id:1});
usertest.index({user_id:1,test_id:1,is_submitted:1,is_first:1});
usertest.index({pid:1,test_id:1,is_submitted:1,is_first:1});
usertest.index({test_id:1,is_submitted:1,is_first:1});
usertest.index({test_id:1,is_started:1});
usertest.index({user_id:1,test_id:1,act:1,is_submitted:1,is_started:1});
usertest.index({end_time:-1});
usertest.index({start_time:-1});
usertest.index({catg_id:1});
usertest.index({gid:1});
usertest.index({is_first:1});
usertest.index({marks_obtained:-1});
usertest.index({user_id:1,catg_id:1});
usertest.index({user_id:1,gid:1,plugin_id:1,is_submitted:1});
usertest.index({user_id:1,gid:1,plugin_id:1,is_submitted:1});
usertest.index({is_submitted:1});
usertest.index({is_submitted:1,is_started:1});

usertest.statics = {
	RECENT_MOST_QUESTION_STATUS : {
		SKIPPED  : 1,
		RIGHT    : 2,
		WRONG    : 3
	}
};

mongoose.model('UserTest',usertest);